#pragma once

#include <algorithm>
#include <vector>
#include <string>

#include <maya/MFloatArray.h>
#include <maya/MObject.h>
#include <maya/MQuaternion.h>

#include "Translator.h"

namespace LMA_MayaImporter {

class MayaImporter {
public:
	MayaImporter(std::string filename) :file_name(filename) {}
	~MayaImporter() {}

	void parseActor(ReActionFX::Actor& actor);
	void parseAnim(ReActionFX::Motion& motion);

protected:
	// Bone stuff
	void boneNodeRecurse(ReActionFX::Node& node, const MObject& parent);

	// Mesh stuff
	void doMesh(const std::shared_ptr<ReActionFX::Mesh> mesh);
	void doSubmesh(const ReActionFX::SubMesh& sm, int cumulative_processed_vertices);

	// Skin influences
	void doSkinInfluences(const MObject& meshPath, ReActionFX::SubMesh sm);

	// Materials and textures
	void setShader(MObject& meshPath, const ReActionFX::StandardMaterial& material);
	MObject doMaterial(const ReActionFX::StandardMaterial& material);
	MObject doTexture(const ReActionFX::StandardMaterialLayer& layer);

	// Animations
	void doSkeletalMotion(const ReActionFX::SkeletalMotion& skmotion);

protected:
	std::string file_name;
	std::vector<ReActionFX::StandardMaterial> materials;
	std::vector<std::shared_ptr<ReActionFX::Mesh>> meshes;
	std::vector<std::vector<ReActionFX::SkinInfluence>> skinInfluences;
	MFloatArray Us, Vs;
}; // class


inline bool containsSubstring(const std::string& whole, const std::string& part) {
	return (whole.find(part) != std::string::npos);
}

inline bool containsSubstringIgnoreCase(const std::string& whole, const std::string& part) {
	std::string d = whole;
	std::transform(whole.begin(), whole.end(), d.begin(), ::tolower);
	return (d.find(part) != std::string::npos);
}

// Change from left-handed LMA Quaternion to right-handed Maya Quaternion.
inline MQuaternion LMAQuatToMQuat(const RCore::Quaternion& lmaquat) {
	return MQuaternion(-lmaquat.x, -lmaquat.y, lmaquat.z, lmaquat.w);
}

} // namespace