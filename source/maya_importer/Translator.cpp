#include <RCore.h>
#include <ReActionFX.h>

#include "LMA_MayaImporter.h"
#include "Translator.h"

using namespace std;
using namespace RCore;
using namespace ReActionFX;

namespace LMA_MayaImporter {

MString Translator::mVersionNumber("1.0");
int Translator::mHighVersion(LMA_HIGHVERSION);
int Translator::mLowVersion(LMA_LOWVERSION);

void* Translator::creator() {
	return new Translator();
}

MStatus Translator::reader(const MFileObject& file, const MString& options, MPxFileTranslator::FileAccessMode mode) {
	MStatus status = MS::kFailure;

	LogManager::GetInstance().CreateLogFile("LMAMayaExporter.log");
	string fileName = file.fullName().asChar();

	// try to open the file
	std::ifstream lmaFile(fileName, std::ifstream::binary);
	if (!lmaFile) {
		cout << fileName << " could not be opened for reading." << endl;
		status = MS::kFailure;
		return status;
	}

	MayaImporter imp(fileName);

	if (IMPORTER.IsActor(fileName)) {
		cout << "Actor file detected." << endl;
		Actor *actor = IMPORTER.LoadActor(fileName, fileName, true);
		if (actor != nullptr) {
			imp.parseActor(*actor);
		}
	} else if (IMPORTER.IsMotion(fileName)) {
		cout << "Animation file detected." << endl;
		Motion *motion = IMPORTER.LoadMotion(fileName, fileName);
		if (motion != nullptr) {
			imp.parseAnim(*motion);
		}
	} else {
		cout << "No valid LMA file type detected." << endl;
		return MS::kFailure;
	}

	return MS::kSuccess;
}

MStatus Translator::writer(const MFileObject& file, const MString& options, MPxFileTranslator::FileAccessMode mode) {
	MStatus status = MS::kFailure;
	return status;
}

MPxFileTranslator::MFileKind Translator::identifyFile(const MFileObject & file, const char *buffer, short size) const {
	MFileKind rVal = kNotMyFileType;

	string name(file.name().asChar());

	if ((name.size() > 4) && (name.find(".lma") != string::npos)) {
		rVal = kCouldBeMyFileType;
	}

	std::ifstream lmaFile(name, std::ifstream::binary);
	if (!lmaFile) {
		cout << file.name() << " could not be opened for reading." << endl;
	}
	cout << "Testing file " << file.name() << endl;

	// the temp header.
	LMA_Header header;

	// read in the header information
	lmaFile.read((char*)&header, sizeof(LMA_Header));

	if (header.mHiVersion == LMA_HIGHVERSION &&
		header.mLoVersion == LMA_LOWVERSION &&
		header.mFourcc[0] == 'L' &&
		header.mFourcc[1] == 'M' &&
		header.mFourcc[2] == 'A' &&
		header.mFourcc[3] == ' ') {
		// success, this is our file !
		rVal = kIsMyFileType;
	}

	return rVal;
}

} // namespace