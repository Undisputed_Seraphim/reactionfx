#include <maya/MColor.h>
#include <maya/MDagPath.h>
#include <maya/MDGModifier.h>
#include <maya/MFnPhongShader.h>
#include <maya/MFnSet.h>
#include <maya/MItDependencyNodes.h>
#include <maya/MPlug.h>
#include <maya/MPlugArray.h>
#include <maya/MSelectionList.h>

#include <RCore.h>
#include <ReActionFX.h>
#include "LMA_MayaImporter.h"

using namespace std;
using namespace RCore;
using namespace ReActionFX;

namespace LMA_MayaImporter {

void  MayaImporter::setShader(MObject& meshObj, const StandardMaterial& material) {
	cout << material.GetName() << endl;
	MStatus status;
	MObject shadeObj = doMaterial(material);

	// Create shading group
	MDagPath meshPath;
	MDagPath::getAPathTo(meshObj, meshPath);
	MFnSet set;
	MSelectionList sel;
	sel.add(meshPath);
	set.create(sel, MFnSet::kRenderableOnly, false, &status);
	set.setName("shadingGroup");

	// Set material to the phong function set.
	MFnPhongShader phong(shadeObj);
	phong.setName(material.GetName().c_str(), false, &status);

	// Break default connection that is created.
	MPlugArray plugArray;
	MPlug surfaceShader = set.findPlug("surfaceShader", true, &status);
	surfaceShader.connectedTo(plugArray, true, true, &status);
	MDGModifier dgModifier;
	dgModifier.disconnect(plugArray[0], surfaceShader);

	// Connect outcolor to DG modifier
	dgModifier.connect(phong.findPlug("outColor"), surfaceShader);

	for (int i = 0; i < material.GetNumLayers(); i++) {
		if (material.GetLayer(i) == nullptr) continue;
		const auto layer = *dynamic_pointer_cast<StandardMaterialLayer>(material.GetLayer(i));

		MObject texture = doTexture(layer);
		MFnDependencyNode nodeFn;
		nodeFn.setObject(texture);

		if (layer.GetType() == StandardMaterialLayer::LAYERTYPE_DIFFUSE) {
			status = dgModifier.connect(nodeFn.findPlug("outColor"), phong.findPlug("color"));

			//cout << "\tDiffuse Material";
		}

		if (layer.GetType() == StandardMaterialLayer::LAYERTYPE_OPACITY) {
			status = dgModifier.connect(nodeFn.findPlug("outTransparency"), phong.findPlug("transparency"));
			status = dgModifier.connect(nodeFn.findPlug("outAlpha"), phong.findPlug("alphaGain"));
			//status = phong.findPlug("alphaGain").setValue(material.mAlphaRef);
			//cout << "\tOpacity Material";
		}

		if (layer.GetType() == StandardMaterialLayer::LAYERTYPE_REFLECT) {
			//status = phong.findPlug("transparency").setValue(material.mAlphaRef / 127.0f);

			//cout << "\tReflect Material";
		}

		//cout << "\tShaderMask: " << material.mShaderMask << " AlphaRef: " << material.mAlphaRef << std::endl;

		// Create 2D Texture Placement
		MFnDependencyNode tp2dFn;
		tp2dFn.create("place2dTexture", "2dTexture");
		tp2dFn.findPlug("wrapU").setValue(false);
		tp2dFn.findPlug("wrapV").setValue(false);

		// Connect all the 18 things
		dgModifier.connect(tp2dFn.findPlug("coverage"), nodeFn.findPlug("coverage"));
		dgModifier.connect(tp2dFn.findPlug("mirrorU"), nodeFn.findPlug("mirrorU"));
		dgModifier.connect(tp2dFn.findPlug("mirrorV"), nodeFn.findPlug("mirrorV"));
		dgModifier.connect(tp2dFn.findPlug("noiseUV"), nodeFn.findPlug("noiseUV"));
		dgModifier.connect(tp2dFn.findPlug("offset"), nodeFn.findPlug("offset"));
		dgModifier.connect(tp2dFn.findPlug("outUV"), nodeFn.findPlug("uvCoord"));
		dgModifier.connect(tp2dFn.findPlug("outUvFilterSize"), nodeFn.findPlug("uvFilterSize"));
		dgModifier.connect(tp2dFn.findPlug("repeatUV"), nodeFn.findPlug("repeatUV"));
		dgModifier.connect(tp2dFn.findPlug("rotateFrame"), nodeFn.findPlug("rotateFrame"));
		dgModifier.connect(tp2dFn.findPlug("rotateUV"), nodeFn.findPlug("rotateUV"));
		dgModifier.connect(tp2dFn.findPlug("stagger"), nodeFn.findPlug("stagger"));
		dgModifier.connect(tp2dFn.findPlug("translateFrame"), nodeFn.findPlug("translateFrame"));
		dgModifier.connect(tp2dFn.findPlug("vertexCameraOne"), nodeFn.findPlug("vertexCameraOne"));
		dgModifier.connect(tp2dFn.findPlug("vertexUvOne"), nodeFn.findPlug("vertexUvOne"));
		dgModifier.connect(tp2dFn.findPlug("vertexUvTwo"), nodeFn.findPlug("vertexUvTwo"));
		dgModifier.connect(tp2dFn.findPlug("vertexUvThree"), nodeFn.findPlug("vertexUvThree"));
		dgModifier.connect(tp2dFn.findPlug("wrapU"), nodeFn.findPlug("wrapU"));
		dgModifier.connect(tp2dFn.findPlug("wrapV"), nodeFn.findPlug("wrapV"));
	}

	dgModifier.doIt();
}

MObject MayaImporter::doMaterial(const StandardMaterial& material) {
	MStatus status;
	//Create the material but don't connect it to parent yet
	MFnPhongShader phong;
	MObject obj = phong.create(&status);
	if (status != MS::kSuccess) cout << "Phong shader creation failed: " << status << endl;

	MColor ambient(material.GetAmbient().r, material.GetAmbient().g, material.GetAmbient().b, material.GetAmbient().a);
	MColor diffuse(material.GetDiffuse().r, material.GetDiffuse().g, material.GetDiffuse().b, material.GetDiffuse().a);
	MColor specular(material.GetSpecular().r, material.GetSpecular().g, material.GetSpecular().b, material.GetSpecular().a);
	MColor emissive(material.GetEmissive().r, material.GetEmissive().g, material.GetEmissive().b, material.GetEmissive().a);

	phong.setName(material.GetName().c_str());
	phong.setAmbientColor(ambient);
	phong.setColor(diffuse);
	phong.setSpecularColor(specular);
	phong.setIncandescence(emissive);
	phong.setReflectivity(material.GetShine());
//	phong.setTransparency(material.GetOpacity());
	phong.setRefractiveIndex(material.GetIOR());

	return phong.object();
	//return obj;
}

MObject MayaImporter::doTexture(const StandardMaterialLayer& layer) {
	MStatus status;

	// Attach file extension to file name.
	string file_extension = ".dds";
	ostringstream oss;
	oss << layer.GetFilename() << file_extension << endl;
	MString file_name = oss.str().c_str();

	//create a texture node
	MFnDependencyNode nodeFn;
	MObject obj = nodeFn.create(MString("file"), file_name, &status);

	// Create file object
	MFileObject file;
	file.setName(file_name);
	if (!file.exists()) {
		return MObject::kNullObj;
	}

	// Find the plug for texture files and set its path.
	nodeFn.findPlug(MString("ftn"), true, &status).setValue(file.fullName());

	// Get global textures list
	MItDependencyNodes nodeIt(MFn::kTextureList);
	MObject texture_list = nodeIt.thisNode();
	MFnDependencyNode slFn;
	slFn.setObject(texture_list);

	MPlug textures = slFn.findPlug(MString("textures"));

	// Loop until an available connection is found
	MPlug element;
	int next = 0;
	do {
		element = textures.elementByLogicalIndex(next);
		next++;
	} while (element.isConnected());

	// Connect '.message' plug of render node to "shaders"/"textures" plug of default*List
	MPlug message = nodeFn.findPlug(MString("message"), true, &status);
	MDGModifier dgm;
	dgm.connect(message, element);
	dgm.doIt();

	return obj;
}

} // namespace