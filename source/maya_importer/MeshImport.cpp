#include <map>
#include <maya/MDagPath.h>
#include <maya/MFnIkJoint.h>
#include <maya/MFnMesh.h>
#include <maya/MItDag.h>
#include <maya/MItGeometry.h>
#include <maya/MFloatArray.h>
#include <maya/MFloatPointArray.h>
#include <maya/MIntArray.h>

#include <RCore.h>
#include <ReActionFX.h>
#include "LMA_MayaImporter.h"

using namespace std;
using namespace RCore;
using namespace ReActionFX;

namespace LMA_MayaImporter {

MObject findBone(const std::string& name, MStatus *status);
void setBoneLimits(MFnIkJoint& joint, const NodeLimitAttribute& nla);

void MayaImporter::parseActor(Actor& actor) {
	MStatus status = MStatus::kFailure;;

	// Fetch all materials
	for (int i = 0; i < actor.GetNumMaterials(0); i++) {
		auto mat = actor.GetMaterial(0, i);
		if (mat == nullptr) continue;
		if (mat->GetType() == StandardMaterial::TYPE_ID) {
			auto stdmat = *dynamic_pointer_cast<StandardMaterial>(mat);
			materials.push_back(stdmat);
		}
	}

	// Fetch all bones
	for (int i = 0; i < actor.GetNumNodes(); i++) {
		Node node(*actor.GetNode(i));
		// Skip all bones that aren't root.
		if (node.GetParent()->GetID() == actor.GetID()) {
			boneNodeRecurse(node, MObject::kNullObj);
		}
	}

	// Do all meshes
	for (const auto& mesh : meshes)
		doMesh(mesh);

	materials.clear();
	meshes.clear();
}

void MayaImporter::boneNodeRecurse(Node& node, const MObject& parent) {
	MStatus status;

	string _name = node.GetName();
	replace(_name.begin(), _name.end(), ' ', '_');

	bool has_mesh = false;
	if (node.GetMesh(0) != nullptr) {
		cout << "Mesh found at: " << _name << endl;
		meshes.push_back(node.GetMesh(0));
		has_mesh = true;
	}

	MString name = _name.c_str();
	Vector3 pos = node.GetLocalPos();
	Quaternion rot = node.GetLocalRot();
	double scale[3] = {node.GetLocalScale().x, node.GetLocalScale().y, node.GetLocalScale().z};

	MObject bone = MObject::kNullObj;

	// Skip if node is a mesh node.
	// Also skip if bone already exists(findBone doesn't return a kNullObj)
	if (!has_mesh && findBone(_name, nullptr) == MObject::kNullObj) {
		MFnIkJoint joint;
		bone = joint.create(parent, &status);
		joint.setName(name, &status);
		joint.setTranslation(MVector(pos.x, pos.y, -pos.z), MSpace::kTransform);
		joint.setOrientation(LMAQuatToMQuat(rot));
		joint.setScale(scale);

		// Deal with node attributes, such as bone limits.
		for (int i = 0; i < node.GetNumAttributes(); i++) {
			NodeAttribute *attrib = node.GetAttribute(i);
			if (attrib->GetType() == NodeLimitAttribute::TYPE_ID) {
				NodeLimitAttribute nla = *dynamic_cast<NodeLimitAttribute*>(attrib);
				setBoneLimits(joint, nla);
			}
		}

		// TODO: Add any other properties about bones here
	}

	// Recurse through each child
	for (int i = 0; i < node.GetNumChilds(); i++) {
		boneNodeRecurse(*node.GetChild(i), bone);
	}
}

void setBoneLimits(MFnIkJoint & joint, const NodeLimitAttribute & nla) {
	if (nla.IsLimited(NodeLimitAttribute::ELimitType::TRANSLATIONX)) {
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMaxX, nla.GetTranslationMax().x);
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMinX, nla.GetTranslationMin().x);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMaxX, true);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMinX, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::TRANSLATIONY)) {
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMaxY, nla.GetTranslationMax().y);
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMinY, nla.GetTranslationMin().y);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMaxY, true);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMinY, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::TRANSLATIONZ)) {
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMinZ, nla.GetTranslationMin().z);
		joint.setLimit(MFnIkJoint::LimitType::kTranslateMaxZ, nla.GetTranslationMax().z);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMaxZ, true);
		joint.enableLimit(MFnTransform::LimitType::kTranslateMinZ, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::ROTATIONX)) {
		joint.setLimit(MFnIkJoint::LimitType::kRotateMaxX, nla.GetRotationMax().x);
		joint.setLimit(MFnIkJoint::LimitType::kRotateMinX, nla.GetRotationMin().x);
		joint.enableLimit(MFnTransform::LimitType::kRotateMaxX, true);
		joint.enableLimit(MFnTransform::LimitType::kRotateMinX, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::ROTATIONY)) {
		joint.setLimit(MFnIkJoint::LimitType::kRotateMaxY, nla.GetRotationMax().y);
		joint.setLimit(MFnIkJoint::LimitType::kRotateMinY, nla.GetRotationMin().y);
		joint.enableLimit(MFnTransform::LimitType::kRotateMaxY, true);
		joint.enableLimit(MFnTransform::LimitType::kRotateMinY, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::ROTATIONZ)) {
		joint.setLimit(MFnIkJoint::LimitType::kRotateMaxZ, nla.GetRotationMax().z);
		joint.setLimit(MFnIkJoint::LimitType::kRotateMinZ, nla.GetRotationMin().z);
		joint.enableLimit(MFnTransform::LimitType::kRotateMaxZ, true);
		joint.enableLimit(MFnTransform::LimitType::kRotateMinZ, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::SCALEX)) {
		joint.setLimit(MFnIkJoint::LimitType::kScaleMaxX, nla.GetScaleMax().x);
		joint.setLimit(MFnIkJoint::LimitType::kScaleMinX, nla.GetScaleMin().x);
		joint.enableLimit(MFnTransform::LimitType::kScaleMaxX, true);
		joint.enableLimit(MFnTransform::LimitType::kScaleMinX, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::SCALEY)) {
		joint.setLimit(MFnIkJoint::LimitType::kScaleMaxY, nla.GetScaleMax().y);
		joint.setLimit(MFnIkJoint::LimitType::kScaleMinY, nla.GetScaleMin().y);
		joint.enableLimit(MFnTransform::LimitType::kScaleMaxY, true);
		joint.enableLimit(MFnTransform::LimitType::kScaleMinY, true);
	}

	if (nla.IsLimited(NodeLimitAttribute::ELimitType::SCALEZ)) {
		joint.setLimit(MFnIkJoint::LimitType::kScaleMaxZ, nla.GetScaleMax().z);
		joint.setLimit(MFnIkJoint::LimitType::kScaleMinZ, nla.GetScaleMin().z);
		joint.enableLimit(MFnTransform::LimitType::kScaleMaxZ, true);
		joint.enableLimit(MFnTransform::LimitType::kScaleMinZ, true);
	}
}

void MayaImporter::doMesh(const shared_ptr<Mesh> mesh) {
	MStatus status;

	// Fetch shared vertex attributes.
	for (int i = 0; i < mesh->GetNumSharedVertexAttributeLayers(); i++) {
		auto vtxal = mesh->GetSharedVertexAttributeLayer(i);

		// Fetch skin influences.
		if (vtxal->GetType() == SkinningInfoVertexAttributeLayer::TYPE_ID) {
			const auto sivtxal = *dynamic_cast<SkinningInfoVertexAttributeLayer*>(vtxal);
			for (int j = 0; j < mesh->GetNumOrgVertices(); j++) {
				vector<SkinInfluence> siLayer;
				for (int k = 0; k < sivtxal.GetNumInfluences(j); k++) {
					const SkinInfluence& si = sivtxal.GetInfluence(j, k);
					siLayer.push_back(si);
				}
				skinInfluences.push_back(siLayer);
			}
		}
	}

	// Fetch non-shared vertex attributes.
	for (int i = 0; i < mesh->GetNumVertexAttributeLayers(); i++) {
		auto val = mesh->GetVertexAttributeLayer(i);

		// Fetch UVs.
		if (val->GetType() == UVVertexAttributeLayer::TYPE_ID) {
			auto uvLayer = mesh->GetUVLayer();
			for (int j = 0; j < mesh->GetNumVertices(); j++) {
				Vector2 uv = uvLayer->GetUVs()[j];
				Us.append(uv.x);
				Vs.append(abs(1.0 - uv.y));
			}
		}
	}

	// Do submesh.
	int cumulative_processed_vertices = 0;
	for (int i = 0; i < mesh->GetNumSubMeshes(); i++) {
		cout << "\tDoing polysurface " << (i + 1) << "; ";
		doSubmesh(*mesh->GetSubMesh(i), cumulative_processed_vertices);
		cumulative_processed_vertices += mesh->GetSubMesh(i)->GetNumVertices();
	}

	// At the end of this mesh, clear UV values and skin influences.
	skinInfluences.clear();
	Us.clear();
	Vs.clear();
}

void MayaImporter::doSubmesh(const ReActionFX::SubMesh& sm, int cumulative_processed_vertices) {
	MStatus status;

	MFloatPointArray smVertexArray;
	for (int j = 0; j < sm.GetNumVertices(); j++) {
		Vector3 smv = sm.GetPositions()[j];
		smVertexArray.append(smv.x, smv.y, -smv.z);
	}

	MIntArray smPolygonCounts;
	for (int j = 0; j < sm.GetNumIndices() / 3; j++) {
		smPolygonCounts.append(3);
	}

	MIntArray smPolygonConnects;
	// Reverse order of vertices.
	for (int j = 0; j < sm.GetNumIndices(); j += 3) {
		smPolygonConnects.append(sm.GetIndices()[j + 2] - cumulative_processed_vertices);
		smPolygonConnects.append(sm.GetIndices()[j + 1] - cumulative_processed_vertices);
		smPolygonConnects.append(sm.GetIndices()[j + 0] - cumulative_processed_vertices);
	}

	// Create the mesh.
	MFnMesh smFn;
	MObject meshObj = smFn.create(smVertexArray.length(), smPolygonConnects.length() / 3, smVertexArray, smPolygonCounts, smPolygonConnects);
	MDagPath meshPath;
	status = MDagPath::getAPathTo(meshObj, meshPath);
	meshPath.extendToShape();
	cout << "attaching material #" << sm.GetMaterial() << ": ";
	setShader(meshObj, materials[sm.GetMaterial()]);

	// Collect the UV values for this submesh.
	smFn.clearUVs();
	MFloatArray smUs, smVs;
	for (int i = 0; i < sm.GetNumVertices(); i++) {
		smUs.append(Us[i + cumulative_processed_vertices]);
		smVs.append(Vs[i + cumulative_processed_vertices]);
	}

	// Set the UV values for this submesh.
	status = smFn.setUVs(smUs, smVs, &MString("map1"));
	status = smFn.assignUVs(smPolygonCounts, smPolygonConnects, &MString("map1"));

	// If there are any skin influences, do them.
	if (skinInfluences.size() > 0) {
		doSkinInfluences(meshObj, sm);
	}

	// Ignore the normals, the ones that Maya autosets are better.
//	MVectorArray smNormals;
//	for (int j = 0; j < sm.GetNumVertices(); j += 3) {
//		Vector3 n1 = sm.GetNormals()[j + 2],
//			n2 = sm.GetNormals()[j + 1],
//			n3 = sm.GetNormals()[j + 0];
//		smNormals.append(MVector(n1.x, n1.y, -n1.z));
//		smNormals.append(MVector(n2.x, n2.y, -n2.z));
//		smNormals.append(MVector(n3.x, n3.y, -n3.z));
//	}
//	status = smFn.setVertexNormals(smNormals, smPolygonConnects);
//	if (status != MS::kSuccess) cout << "Error setting vertex normals: " << status << endl;
}

// Helper functions.

MObject findBone(const std::string& name, MStatus *status) {

	MItDag dagIter(MItDag::kDepthFirst, MFn::kJoint, status);
	for (; !dagIter.isDone(); dagIter.next()) {
		MDagPath dagPath;
		status = &dagIter.getPath(dagPath);

		MFnIkJoint possibleJoint(dagPath, status);
		// Existing joint found. Set flag to true.
		if (possibleJoint.name() == MString(name.c_str())) {
			return possibleJoint.dagPath().node();
		}
	}
	return MObject::kNullObj;
}

} // namespace