#include <maya/MFnPlugin.h>
#include "Translator.h"

#if defined(_WIN32)
#define EXPORT __declspec(dllexport)
#else
#define __attribute__ ((visibility ("default")))
#endif

constexpr const char* VENDOR = __DATE__ " @ " __TIME__;
constexpr const char* NAME = "ReActionFX LMA file importer";
constexpr const char* DEFAULTOPTIONS = "exportAnimation=false; exportMeshes=true; exportNodes=true; exportInfluences=true;";

/**
* initializePlugin() contains the code to register any commands,
* tools, devices, and so on, defined by the plug-in with Maya.
* It is called only once immediately after the plug-in is loaded.
* @param obj MObject used internally by Maya
*/
EXPORT MStatus initializePlugin(MObject obj) {
	MFnPlugin plugin(obj, VENDOR, LMA_MayaImporter::Translator::getVersion().asChar());

	MStatus status = plugin.registerFileTranslator(NAME,
												   nullptr, // pixmapName
												   LMA_MayaImporter::Translator::creator,
												   nullptr, // scriptName
												   DEFAULTOPTIONS,
												   true);

	if (!status) {
		status.perror("registerFileTranslator");
		return status;
	}

	return status;
}

/**
 * The unitializePlugin() function contains the code necessary to
 * de-register from Maya whatever was registered through initializePlugin()
 * @param obj MObject used internally by Maya
 */
EXPORT MStatus uninitializePlugin(MObject obj) {
	MFnPlugin plugin(obj);

	MStatus status = plugin.deregisterFileTranslator(NAME);

	if (!status) {
		status.perror("deregisterFileTranslator");
		return status;
	}

	return status;
}