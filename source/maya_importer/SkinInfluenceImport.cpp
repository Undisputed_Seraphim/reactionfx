#include <maya/MDagPath.h>
#include <maya/MFnSkinCluster.h>

#include <RCore.h>
#include <ReActionFX.h>
#include "LMA_MayaImporter.h"

using namespace std;
using namespace RCore;
using namespace ReActionFX;

namespace LMA_MayaImporter {

void MayaImporter::doSkinInfluences(const MObject& meshPath, ReActionFX::SubMesh sm) {
	MStatus status;
/*
	// Get the bones that influence the geometry in this submesh.
	set<string> bonesWithInfluence;
	for (int i = 0; i < sm.GetNumVertices(); i++) {
		for (auto si : skinInfluences[sm.GetOrgVerts()[i]]) {
			string name = si.GetBone()->GetName();
			replace(name.begin(), name.end(), ' ', '_');
			bonesWithInfluence.emplace(name);
		}
	}

	// Bind skin to skeleton.
	string cmd = "skinCluster -sm 1 -tsb ";
	for (auto boneName : bonesWithInfluence) {
		cmd.append(boneName); cmd.append(" ");
	}
	cmd.append(meshPath.partialPathName().asChar());
	cmd.append(";");
	MStringArray result;
	MGlobal::executeCommand(cmd.c_str(), result);

	// Set SkinCluster function set to the skin cluster
	// that was output by the previous command.
	MFnSkinCluster skinCluster;
	MSelectionList sel;
	sel.add(result[0]);
	MObject skin;
	sel.getDependNode(0, skin);
	skinCluster.setObject(skin);
*/

	// Just the list of bones that influence the meshes. Might be handy later.
	/*
	MDagPathArray influenceObjects;
	map<string, int> influenceIndex;
	auto numInfs = skinCluster.influenceObjects(influenceObjects, &status);
	for (unsigned int i = 0; i < numInfs; i++) {
		string name = influenceObjects[i].partialPathName().asChar();
		influenceIndex.emplace(pair<string, int>(name, i));
	}

	//Get a list of all vertices in this mesh
	MItGeometry geom(meshPath);
	for (; !geom.isDone(); geom.next()) {
		MObject component = geom.currentItem();
		for (auto si : skinInfluences[sm.GetOrgVerts()[geom.index()]]) {
			string name = si.GetBone()->GetName().AsChar();
			replace(name.begin(), name.end(), ' ', '_');
			status = skinCluster.setWeights(meshPath, component, influenceIndex[name], si.GetWeight(), false);
			if (status != MS::kSuccess) cout << "Error setting vertex weight: " << status << endl;
		}
	}
	*/
}


} // namespace