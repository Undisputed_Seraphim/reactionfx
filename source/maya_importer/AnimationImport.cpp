#include <maya/MDagPath.h>
#include <maya/MDistance.h>
#include <maya/MEulerRotation.h>
#include <maya/MFnAnimCurve.h>
#include <maya/MFnIkJoint.h>
#include <maya/MItDag.h>
#include <maya/MPlug.h>
#include <maya/MQuaternion.h>
#include <maya/MVector.h>

#include <RCore.h>
#include <ReActionFX.h>
#include "LMA_MayaImporter.h"

using namespace std;
using namespace RCore;
using namespace ReActionFX;

namespace LMA_MayaImporter {

constexpr double FPS = 1.0 / 30.0;

void setPositionAnimKeys(MObject joint, const vector<Vector3>& positionTrack);
void setRotationAnimKeys(MObject joint, const vector<Quaternion>& rotationTrack);
void setAnimCurve(const MObject& joint, const MString attr, MFnAnimCurve& curve, MFnAnimCurve::AnimCurveType type = MFnAnimCurve::kAnimCurveTL);

void MayaImporter::parseAnim(Motion& motion) {
	if (motion.GetType() == SkeletalMotion::TYPE_ID) {
		SkeletalMotion& skmotion = dynamic_cast<SkeletalMotion&>(motion);
		doSkeletalMotion(skmotion);
	} else
		cout << "Error: Unknown motion type (non-skeletal)." << endl;
}

// Search through all existing bones and try to attach an animation plug to them
void MayaImporter::doSkeletalMotion(const SkeletalMotion& skmotion) {

	unordered_map<string, vector<Vector3>> positionTracks;
	unordered_map<string, vector<Quaternion>> rotationTracks;
	unordered_map<string, vector<Vector3>> scalingTracks;

	int frames = 1;
	for (int i = 0; i < skmotion.GetNumParts(); ++i) {
		MotionPart *part = skmotion.GetPart(i);
		string name = part->GetName();
		std::replace(name.begin(), name.end(), ' ', '_');

		auto positionkt = part->GetPosTrack();
		auto rotationkt = part->GetRotTrack();
		auto scalingkt = part->GetScaleTrack();

		vector<Vector3> posTangent;
		if (positionkt.GetNumKeys() > 0) {// Grab the number of frames from anywhere, it doesn't matter.
			frames = (frames > positionkt.GetNumKeys()) ? frames : (int)positionkt.GetNumKeys();
			for (int j = 0; j < positionkt.GetNumKeys(); j++) {
				posTangent.push_back(positionkt.GetKey(j)->GetValue());
			}
		} else {
			posTangent.push_back(part->GetPosePos());
		}
		positionTracks.emplace(name, posTangent);

		vector<Quaternion> rotTangent;
		if (rotationkt.GetNumKeys() > 0) {
			for (int j = 0; j < rotationkt.GetNumKeys(); ++j) {
				rotTangent.push_back(rotationkt.GetKey(j)->GetValue());
			}
		} else {
			rotTangent.push_back(part->GetPoseRot());
		}
		rotationTracks.emplace(name, rotTangent);

		vector<Vector3> scaTangent;
		if (scalingkt.GetNumKeys() > 0) {
			for (int j = 0; j < scalingkt.GetNumKeys(); ++j) {
				scaTangent.push_back(scalingkt.GetKey(j)->GetValue());
			}
		} else {
			scaTangent.push_back(part->GetPoseScale());
		}
		scalingTracks.emplace(name, scaTangent);
	}

	MStatus status;
	MItDag dagIter(MItDag::kDepthFirst, MFn::kJoint, &status);
	for (; !dagIter.isDone(); dagIter.next()) {
		MDagPath dagPath;
		status = dagIter.getPath(dagPath);

		MFnIkJoint joint(dagPath);
		string name_key = joint.name().asChar();

		// You have to set the initial positions of the animation because
		// some joints don't move at all.
		if (positionTracks.find(name_key) != positionTracks.end()) {
			auto pos = positionTracks[name_key][0];
			joint.setTranslation(MVector(pos.x, pos.y, -pos.z), MSpace::kTransform);
			setPositionAnimKeys(dagPath.node(), positionTracks[name_key]);
		}

		if (rotationTracks.find(name_key) != rotationTracks.end()) {
			auto rot = rotationTracks[name_key][0];
			joint.setOrientation(LMAQuatToMQuat(rot));
			setRotationAnimKeys(dagPath.node(), rotationTracks[name_key]);
		}

		if (scalingTracks.find(name_key) != scalingTracks.end()) {
			auto sca = scalingTracks[name_key][0];
		}
	}
}

// Sets all the position keys to a joint, given also its relevant tracks.
void setPositionAnimKeys(MObject joint, const vector<Vector3>& positionTrack) {
	if (positionTrack.size() < 2) return;

	MStatus status;
	MFnAnimCurve posX, posY, posZ;
	setAnimCurve(joint, "translateX", posX);
	setAnimCurve(joint, "translateY", posY);
	setAnimCurve(joint, "translateZ", posZ);

	for (int i = 0; i < positionTrack.size(); i++) {
		MTime time(FPS*i, MTime::kSeconds);

		posX.addKeyframe(time, MDistance::uiToInternal(positionTrack[i].x));
		posY.addKeyframe(time, MDistance::uiToInternal(positionTrack[i].y));
		posZ.addKeyframe(time, MDistance::uiToInternal(-positionTrack[i].z));
	}
}

// Sets all the rotation keys to a joint, given also its relevant tracks.
void setRotationAnimKeys(MObject joint, const vector<Quaternion>& rotationTrack) {
	if (rotationTrack.size() < 2) return;

	MFnAnimCurve rotX, rotY, rotZ;
	setAnimCurve(joint, "rotateX", rotX, MFnAnimCurve::kAnimCurveTA);
	setAnimCurve(joint, "rotateY", rotY, MFnAnimCurve::kAnimCurveTA);
	setAnimCurve(joint, "rotateZ", rotZ, MFnAnimCurve::kAnimCurveTA);

	for (int i = 0; i < rotationTrack.size(); i++) {
		MQuaternion rotation = LMAQuatToMQuat(rotationTrack[i]);
		MQuaternion orientation = LMAQuatToMQuat(rotationTrack[0]);
		rotation = rotation * orientation.inverse();

		const auto euler = rotation.asEulerRotation();
		MTime time(FPS*i, MTime::kSeconds);

		rotX.addKeyframe(time, euler.x);
		rotY.addKeyframe(time, euler.y);
		rotZ.addKeyframe(time, euler.z);
	}
}

// Attaches the anim curve to the joint.
void setAnimCurve(const MObject& joint, const MString attr, MFnAnimCurve& curve, MFnAnimCurve::AnimCurveType type) {
	MStatus status;
	MPlug plug = MFnDependencyNode(joint).findPlug(attr, false, &status);

	if (!plug.isKeyable())
		plug.setKeyable(true);

	if (plug.isLocked())
		plug.setLocked(false);

	if (!plug.isConnected()) {
		curve.create(joint, plug, type, nullptr, &status);
		if (status != MStatus::kSuccess)		cout << "Creating anim curve at joint failed!" << endl;
	} else {
		MFnAnimCurve animCurve(plug, &status);
		if (status == MStatus::kNotImplemented) cout << "Joint " << animCurve.name() << " has more than one anim curve." << endl;
		else if (status != MStatus::kSuccess)	cout << "No anim curves found at joint " << animCurve.name() << endl;
		curve.setObject(animCurve.object(&status));
	}
}

} // namespace