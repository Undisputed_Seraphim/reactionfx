#pragma once

#include <iostream>
#include <string>
#include <maya/MPxFileTranslator.h>

namespace LMA_MayaImporter {

/**
 * Maya .lma Import Plugin
 * This class provides the connection to Maya. It is derived from MPxFileTranslator
 * in order to implement a Maya "File Translator Plug-In." A file translator plug-in
 * allows Maya to read or write 3rd party file formats.
 */
class Translator : public MPxFileTranslator {
public:
	/**
	 * Default constructor.
	 */
	Translator() : mFileExtension("lma") {
		std::cout << "LMA Importer initialized." << std::endl;
	}

	/**
	 * Destructor.
	 */
	virtual ~Translator() {
		std::cout << "LMA Importer is closing." << std::endl;
	}

	/**
	 * The creator methods provide a mechanism for Maya to allocate
	 * an object of unknown characteristics. When you register a new
	 * object, you are actually registering its creator method which
	 * Maya can then call to allocate a new instance of an object.
	 * @return Plugin pointer.
	 */
	static void* creator();

	/**
	 * Read method of the translator.
	 * @param file .
	 * @param optionsString .
	 * @param mode .
	 */
	MStatus reader(const MFileObject& file, const MString& optionsString, MPxFileTranslator::FileAccessMode mode);

	/**
	 * Write method of the translator.
	 * @param file The file object.
	 * @param optionsString The options for the exported file.
	 * @param mode The write mode of the exported file.
	 */
	MStatus writer(const MFileObject& file, const MString& optionsString, MPxFileTranslator::FileAccessMode mode);

	/**
	 * Accessor returns if the plugin has a read method.
	 * @return Read method flag.
	 */
	bool haveReadMethod() const { return true; }

	/**
	 * Accessor returns if the plugin has a write method.
	 * @return Write method flag.
	 */
	bool haveWriteMethod() const { return false; }

	/**
	 * Accessor returns the default Extension.
	 * @return Default extension string.
	 */
	MString defaultExtension() const { return mFileExtension; }

	/**
	 * Tells Maya if the translator can open and
	 * import files or only import files
	 * @return canBeOpened flag.
	 */
	bool canBeOpened() const { return true; }

	/**
	 * Checks if the currently opened file is supported by the plugin.
	 * @return Comparison result.
	 */
	MPxFileTranslator::MFileKind identifyFile(const MFileObject& file, const char *buffer, short size) const;

	/**
	 * Accessor returns the plugin's version number.
	 * @return The plugin version number.
	 */
	static MString getVersion() { return mVersionNumber; }

private:
	MString mFileExtension;			/**< Export file extension. */
	static MString mVersionNumber;	/**< Plug-in version number. */

	static int mHighVersion;
	static int mLowVersion;

	bool mExportMesh;
	bool mExportActor;
};

}