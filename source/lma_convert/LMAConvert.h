#pragma once

#include <algorithm>
#include <iostream>
#include <vector>
#include <string>
#include <unordered_map>
#include "ReActionFX.h"

using namespace ReActionFX;

class LMAExtractor {
public:
	LMAExtractor(std::string filename) :file_name(filename) {}
	~LMAExtractor() {}

	void parse(Actor* actor);
	void parse(Motion* motion);

private:
	void doBones(Node* node);
	void doMaterials();
	void doMeshes(Mesh* mesh);
	void doSubMeshes(SubMesh& submesh);

	std::vector<StandardMaterial> materials;
	std::string file_name;
};
