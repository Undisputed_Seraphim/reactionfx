#include "LMAConvert.h"

using namespace RCore;
using namespace ReActionFX;

using std::cout;
using std::endl;

#define COUT_BONES 0
#define COUT_MESH 0
#define COUT_SUBMESH 1

void doNodeLimitAttributes(const NodeLimitAttribute& nla);


void LMAExtractor::parse(Motion* motion) {
	float motionTime = motion->GetMaxTime();
	float motionType = motion->GetType();

	cout << "Motion Name: " << motion->GetName() << endl
		<< "Motion Type: " << motion->GetTypeString() << endl
		<< "Motion Time: " << motionTime << endl;

	if (motionType != SkeletalMotion::TYPE_ID) {
		return;
	}

	//Cast
	SkeletalMotion *skeleton = (SkeletalMotion*)motion;
	// Get all motion parts
	//	std::list<MotionPart*> motionParts;
	auto numParts = skeleton->GetNumParts();
	cout << numParts << endl;

	for (int i = 0; i < numParts; i++) {
	//		printf("%d:\n", i);
		MotionPart* part = skeleton->GetPart(i);

		cout << part->GetName() << endl;

		RCore::Vector3 position = part->GetPosePos();
		cout << position.x << ' ' << position.y << ' ' << position.z << endl;
		RCore::Quaternion rotation = part->GetPoseRot();
		cout << rotation.x << ' ' << rotation.y << ' ' << rotation.z << ' ' << rotation.w << endl;
		//RCore::Vector3 scaling = part->GetPoseScale();
		//printf("%f %f %f\n", scaling.x, scaling.y, scaling.z);

		KeyTrack<RCore::Vector3, RCore::Vector3> positionkt = part->GetPosTrack();
		KeyTrack<RCore::Quaternion, RCore::Compressed16BitQuaternion> rotationkt = part->GetRotTrack();
		KeyTrack<RCore::Vector3, RCore::Vector3> scalekt = part->GetScaleTrack();

		int numKeys;

		numKeys = positionkt.GetNumKeys();
		cout << "\t" << numKeys << " translation keys." << endl;
		for (int j = 0; j < numKeys; j++) {
			auto positionkf = positionkt.GetKey(j);
			float kf_time = positionkf->GetTime();
			auto kf_vector = positionkf->GetValue();
			printf("\t%f %f %f %f\n", kf_time, kf_vector.x, kf_vector.y, kf_vector.z);
		}
		cout << endl;

		numKeys = rotationkt.GetNumKeys();
		cout << "\t" << numKeys << " rotation keys." << endl;
		for (int j = 0; j < numKeys; j++) {
			auto rotationkf = rotationkt.GetKey(j);
			float kf_time = rotationkf->GetTime();
			auto kf_quat = rotationkf->GetValue();
			printf("\t%f %f %f %f %f\n", kf_time, kf_quat.x, kf_quat.y, kf_quat.z, kf_quat.w);
		}
		cout << endl;

		numKeys = scalekt.GetNumKeys();
		cout << "\t" << numKeys << " scale kets." << endl;
		for (int j = 0; j < numKeys; j++) {
			auto scalekf = scalekt.GetKey(j);
			float kf_time = scalekf->GetTime();
			auto kf_vector = scalekf->GetValue();
			printf("%f %f %f %f\n", kf_time, kf_vector.x, kf_vector.y, kf_vector.z);
		}
		//		motionParts.push_back(part);
	}

	cout << endl;
}


void LMAExtractor::parse(Actor* actor_) {
	if (actor_ == nullptr) {
		cout << "ERROR: Actor was a null pointer." << endl;
		return;
	}

	//	Actor actor = ReActionFX::Actor(*actor_);
	Actor *actor = actor_;
	cout << "Name: " << actor->GetName() << endl;
	cout << "Materials: " << actor->GetNumMaterials(0) << endl;

	for (int i = 0; i < actor->GetNumMaterials(0); i++) {
		Material *mat = actor->GetMaterial(0, i).get();
		if (mat == nullptr) continue;
		std::string mat_type = mat->GetTypeString();
		if (mat_type.compare("StandardMaterial") != 0) {
			materials.push_back(StandardMaterial(*(StandardMaterial*)mat));
		}
	}

	doMaterials();

	doBones(actor);

	//doMeshes(actor.GetMesh(0).get());
}


void LMAExtractor::doBones(Node *node_) {
	if (node_ == nullptr)
		return;

	Node node(*node_);
	std::string name = node.GetName();
	cout << name << endl;

	bool skip = false;
	if (node.GetParent() == nullptr) {
		cout << "(Actor root)" << endl;
		skip = true;
	}

	if (!skip) {
		if (node.GetParent()->GetName().compare(file_name) != 0) {
			cout << "   Parent: " << node.GetParent()->GetName() << endl;
		} else {
			cout << "(skeleton root)" << endl;
		}
	}

	auto vector = node.GetLocalPos();
	//	printf("   Pos: %f %f %f\n", vector.x, vector.y, vector.z);

	auto quat = node.GetLocalRot();
	//	printf("   Rot: %f %f %f %f\n", quat.x, quat.y, quat.z, quat.w);

	cout << endl;

	// Try to do meshes here?
	doMeshes(node.GetMesh(0).get());

	// Get node attributes
	for (int i = 0; i < node.GetNumAttributes(); i++) {
		if (node.GetAttribute(i) == nullptr)
			continue;
		NodeAttribute *attrib = node.GetAttribute(i);
		std::string type = attrib->GetTypeString();
		if (type.compare("NodeLimitAttribute") == 0) {
			NodeLimitAttribute nla = *(NodeLimitAttribute*)attrib;
			//			doNodeLimitAttributes(nla);
		}
	}

	size_t children = node.GetNumChilds();
	if (children == 0)
		return;
	else {
		for (int i = 0; i < children; i++) {
			doBones(node.GetChild(i));
		}
	}
}


void LMAExtractor::doMaterials() {
	for (auto material : materials) {
		cout << material.GetName() << '\n';
		for (int i = 0; i < material.GetNumLayers(); i++) {
			auto layer_ = material.GetLayer(i).get();
			if (layer_ == nullptr) continue;
			auto layer(*layer_);
			cout << '\t' << layer.GetFilename() << endl;
		}
	}
}


void LMAExtractor::doMeshes(Mesh* mesh_) {
	if (mesh_ == nullptr)
		return;
	Mesh mesh(*mesh_);

	cout << "Indices: " << mesh.GetNumIndices() << "\n"
		<< "Faces: " << mesh.GetNumFaces() << "\n"
		<< "Vertices: " << mesh.GetNumVertices() << "\n"
		<< "OrgVertices: " << mesh.GetNumOrgVertices() << "\n"
		<< "Submeshes: " << mesh.GetNumSubMeshes() << endl;

	cout << "Vertex Attribute Layers: " << mesh.GetNumVertexAttributeLayers() << '\n'
		<< "Shared vertex attribute layers: " << mesh.GetNumSharedVertexAttributeLayers() << endl;

	vector<Vector3> Vertices; // == Orgvertices
	for (int i = 0; i < mesh.GetNumVertices(); i++) {
		Vector3 v = mesh.GetPositions()[i];
		Vertices.push_back(v);
	}

	vector<int> Indices;
	for (int i = 0; i < mesh.GetNumIndices(); i++) {
		Indices.push_back(mesh.GetIndices()[i]);
	}

	vector<Vector3> Normals;
	for (int i = 0; i < mesh.GetNumVertices(); i++) {
		Vector3 n = mesh.GetNormals()[i];
		Normals.push_back(n);
	}

	for (int i = 0; i < mesh.GetNumSubMeshes(); i++) {
		doSubMeshes(*mesh.GetSubMesh(i));
	}

	// Fetch UV info

	for (int i = 0; i < mesh.GetNumVertices(); i++) {
		Vector2 uv = mesh.GetUVLayer()->GetUVs()[i];
		//		cout << uv.x << ' ' << uv.y << endl;
	}

	// Fetch UV and skinning information
	vector<Vector2> UVs;
	for (int i = 0; i < mesh.GetNumVertexAttributeLayers(); i++) {
		auto val = mesh.GetVertexAttributeLayer(i);

		if (val->GetType() == UVVertexAttributeLayer::TYPE_ID) {
			for (int j = 0; j < mesh.GetNumVertices(); j++) {
				Vector2 uv = mesh.GetUVLayer()->GetUVs()[j];
				UVs.push_back(uv);
			}
		}
	}

	// Fetch shared vertex attributes.
	for (int i = 0; i < mesh.GetNumSharedVertexAttributeLayers(); i++) {
		auto vtxal = mesh.GetSharedVertexAttributeLayer(i);

		// Fetch skin influences.
		if (vtxal->GetType() == SkinningInfoVertexAttributeLayer::TYPE_ID) {
			auto sivtxal = *(SkinningInfoVertexAttributeLayer*)vtxal;
			for (int j = 0; j < mesh.GetNumOrgVertices(); j++) {
				for (int k = 0; k < sivtxal.GetNumInfluences(j); k++) {
					SkinInfluence si = sivtxal.GetInfluence(j, k);
					//					cout << "\t" << j << ": Skin Influence: " << si.GetBoneNr()<< ' ' << si.GetBone()->GetID() << ": " << si.GetBone()->GetName() << " -> " << si.GetWeight() << endl;
				}
				//				cout << endl;
			}
		}
	}
}


void LMAExtractor::doSubMeshes(SubMesh & submesh) {
#if (COUT_SUBMESH == 1)
	cout << "Submesh Indices:\t" << submesh.GetNumIndices() << endl
		<< "Submesh Vertices:\t" << submesh.GetNumVertices() << endl << endl;
#endif

	//	for (int i = 0; i < submesh.GetNumVertices(); i++) {
	//		Vector3 v = submesh.GetPositions()[i];
	//		cout << '\t' << v.x << ' ' << v.y << ' ' << v.z << endl;
	//	}

	//	for (int i = 0; i < submesh.GetNumIndices(); i+=3) {
	//		cout << '\t' << submesh.GetIndices()[i] << ' ' << submesh.GetIndices()[i + 1] << ' ' << submesh.GetIndices()[i + 2] << endl;
	//	}
}


void doNodeLimitAttributes(const NodeLimitAttribute& nla) {
	auto tmax = nla.GetTranslationMax(),
		tmin = nla.GetTranslationMin(),
		smax = nla.GetScaleMax(),
		smin = nla.GetScaleMin();
	auto rmax = nla.GetRotationMax(), rmin = nla.GetRotationMin();
	cout << "\tTranslation Max: " << tmax.x << ' ' << tmax.y << ' ' << tmax.z << endl;
	cout << "\tTranslation Min: " << tmin.x << ' ' << tmin.y << ' ' << tmin.z << endl;
	cout << "\tRotation Max: " << rmax.x << ' ' << rmax.y << ' ' << rmax.z << endl;
	cout << "\tRotation Min: " << rmin.x << ' ' << rmin.y << ' ' << rmin.z << endl;
	cout << "\tScaling Max: " << smax.x << ' ' << smax.y << ' ' << smax.z << endl;
	cout << "\tScaling Min: " << smin.x << ' ' << smin.y << ' ' << smin.z << endl;
}
