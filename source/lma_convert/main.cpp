#include "LMAConvert.h"

using namespace ReActionFX;
using namespace RCore;

int main(int argc, char *argv[]) {
	if (argc <= 1) {
		printf("No file found.");
		return 0;
	}

	char *filename = argv[1];;

	// If debug build: Make log files.
//#if _DEBUG
	LOG_MANAGER.CreateLogFile("LMALOG.txt");

	IMPORTER.SetLogging(true);
	IMPORTER.SetLogDetails(true);
//#endif

	LMAExtractor extractor(filename);

	if (IMPORTER.IsActor(argv[1])) {
		Actor *actor = IMPORTER.LoadActor(filename, filename, true);
		extractor.parse(actor);

	} else if (IMPORTER.IsMotion(argv[1])) {
		Motion *motion = IMPORTER.LoadMotion(filename, filename);
		extractor.parse(motion);

	} else {
		printf("No LMA file type detected (neither motion nor actor).");
	}

	return 0;
}