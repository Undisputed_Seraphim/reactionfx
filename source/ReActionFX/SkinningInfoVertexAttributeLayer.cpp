#include "SkinningInfoVertexAttributeLayer.h"
#include "Actor.h"

namespace ReActionFX {

// relink the attributes to a given mesh that is part of a given node which again is part of a given actor
void SkinningInfoVertexAttributeLayer::ReLinkToNode(Mesh* mesh, Node* node, Actor* actor) {
	for (auto& influence : mInfluences) {
		for (auto& weight : influence) {
			Node* newBone = actor->GetNodeByID(weight.GetBone()->GetID());
			assert(newBone);

			// update it
			weight.SetBone(newBone);
		}
	}
}


} // namespace