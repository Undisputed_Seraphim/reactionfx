#include "StandardMaterial.h"
#include "StandardMaterial.h"

using namespace RCore;
using std::shared_ptr;

namespace ReActionFX {

//-------------------------------------------------------------------------------------------------
// StandardMaterialLayer
//-------------------------------------------------------------------------------------------------

// clone the layer
StandardMaterialLayer* StandardMaterialLayer::Clone() {
	StandardMaterialLayer* result = new StandardMaterialLayer(mLayerTypeID, mFilename, mAmount);

	result->mUOffset = mUOffset;
	result->mVOffset = mVOffset;
	result->mUTiling = mUTiling;
	result->mVTiling = mVTiling;
	result->mRotationRadians = mRotationRadians;

	return result;
}


//-------------------------------------------------------------------------------------------------
// StandardMaterial
//-------------------------------------------------------------------------------------------------

// constructor
StandardMaterial::StandardMaterial(const std::string & name) : Material(name) {
	mAmbient = RGBAColor(0.2f, 0.2f, 0.2f);
	mDiffuse = RGBAColor(1.0f, 0.0f, 0.0f);
	mSpecular = RGBAColor(1.0f, 1.0f, 1.0f);
	mEmissive = RGBAColor(1.0f, 0.0f, 0.0f);
	mShine = 1;
	mShineStrength = 1;
	mOpacity = 1.0f;
	mIOR = 1.5f;
	mDoubleSided = true;
	mWireFrame = false;

	mShaderMask = 0;
	mAlphaRef = 127;
	mTexFxn = 0;
	mTexFxnUParm = 0.0f;
	mTexFxnVParm = 0.0f;
	mTexFxnSub0Parm = 0.0f;
}

void StandardMaterial::SetTransparencyType(const char transparencyType) {
	switch (transparencyType) {
	case 'a':
	case 'A': { mTransparencyType = TransparencyType::ADDITIVE;		break; }
	case 'f':
	case 'F': { mTransparencyType = TransparencyType::FILTER;		break; }
	case 'n':
	case 'N': { mTransparencyType = TransparencyType::NONE;			break; }
	case 's':
	case 'S': { mTransparencyType = TransparencyType::SUBTRACTIVE;	break; }
	default:	mTransparencyType = TransparencyType::UNKNOWN;
	}
}


// clone the standard material
shared_ptr<Material> StandardMaterial::Clone() {
	// create the new material
	auto clone = std::make_shared<StandardMaterial>(mName);

	// copy the attributes
	clone->mAmbient = mAmbient;
	clone->mDiffuse = mDiffuse;
	clone->mSpecular = mSpecular;
	clone->mEmissive = mEmissive;
	clone->mShine = mShine;
	clone->mShineStrength = mShineStrength;
	clone->mOpacity = mOpacity;
	clone->mIOR = mIOR;
	clone->mDoubleSided = mDoubleSided;
	clone->mWireFrame = mWireFrame;

	clone->mShaderMask = mShaderMask;
	clone->mAlphaRef = mAlphaRef;
	clone->mTexFxn = mTexFxn;
	clone->mTexFxnUParm = mTexFxnUParm;
	clone->mTexFxnVParm = mTexFxnVParm;
	clone->mTexFxnSub0Parm = mTexFxnSub0Parm;


	// copy the layers
	for (int i = 0; i < mLayers.size(); i++) {
		auto stdMatClone = shared_ptr<StandardMaterialLayer>(mLayers[i]->Clone());
		clone->mLayers.push_back(stdMatClone);
	}

	// return the result
	return clone;
}

int StandardMaterial::FindLayer(const int layerType) const {
	// search through all layers
	for (int i = 0; i < mLayers.size(); i++)
		if (mLayers[i].get()->GetType() == layerType)
			return i;

	return -1;
}

} // namespace