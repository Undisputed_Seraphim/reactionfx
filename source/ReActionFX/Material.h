#pragma once

#include <memory>
#include "RCore.h"

namespace ReActionFX {

/**
 * The material base class.
 * All material types are inherited from this base class.
 */
class Material {
public:
	/**
	 * The constructor.
	 * @param name The name of the material.
	 */
	Material(const std::string & name) : mName(name) {}

	/**
	 * The destructor.
	 */
	virtual ~Material() {}

	/**
	 * Get the unique type ID of the material.
	 * Every material class will have a unique ID which can be used to identify the material type.
	 * @result The unique type ID number.
	 */
	virtual int GetType() const = 0;

	/**
	 * Get the material class description or class name.
	 * @result A pointer to the string containing a description or the class name.
	 */
	virtual const char* GetTypeString() const = 0;

	/**
	 * Clone the material.
	 * This returns a duplicated version of this material that is exactly the same.
	 * @result A smartpointer to the new material, which is an exact copy of this material.
	 */
	virtual std::shared_ptr<Material> Clone() = 0;

	/**
	 * Get the material name.
	 * @result The name of the material.
	 */
	const std::string& GetName() const { return mName; }

	/**
	 * Set the material name.
	 * @param name The new name of the material.
	 */
	void SetName(const std::string &name) { mName = name; }


protected:
	std::string mName;	/* The material name. */
};


} // namespace