#include <cstring>
#include "Actor.h"
#include "MotionLayer.h"
#include "Motion.h"
#include "SkeletalMotion.h"
#include "MeshDeformerStack.h"
#include "Material.h"
#include "Mesh.h"
#include "SubMesh.h"
#include "SimpleActorCollisionSystem.h"
#include "SmartMeshMorphDeformer.h"
#include "SkinningInfoVertexAttributeLayer.h"

using namespace RCore;
using std::shared_ptr;
using std::vector;

namespace ReActionFX {

// constructor
Actor::Actor(const std::string &name)
	: Node(name) {
	mMotionSystem = nullptr;
	mOriginalActor = nullptr;
	mActorCollisionSystem = nullptr;
	mRepositioningNode = nullptr;
	mCurrentLOD = 0;
	mNumLODs = 1;

	// set the repositioning mask to only reposition the positions
	SetRepositioningMask(true, false, false);

	// make sure we have at least allocated the first LOD of materials and facial setups
	mMaterials.resize(1);
	mFacialSetups.resize(1);

	// pregenerate the collision system
	mActorCollisionSystem = new SimpleActorCollisionSystem(this);

	// pregenerate the default motion system
	mMotionSystem = new MotionLayerSystem(this);
}


// update the actor
void Actor::Update(const double timePassedInSeconds, const bool isVisible, Matrix *actorTM) {
	// motion system update:
	// 1. update motion queue and instances
	// 2. update the motion tree (automatic layer removement, weight updates etc.)
	// 3. update node transformation data
	if (mMotionSystem)
		mMotionSystem->Update(timePassedInSeconds, isVisible);

	// update all nodes
	if (isVisible) {
		// update the object space controllers
		UpdateControllers(timePassedInSeconds);

		// update the hierarchy (LocalTM and WorldTM matrices) and bounds
		ProcessHierarchy(actorTM);

		// update the post controllers
		UpdatePostControllers(timePassedInSeconds);

		// process the deformer stacks
		UpdateNodeStacks(timePassedInSeconds);
	}

	// update all attachments
	for (auto* attachment : mAttachments)
		attachment->Update(timePassedInSeconds, isVisible, nullptr);
}


// creates a clone of the actor (a copy).
// does NOT copy the motions and motion tree
Actor * Actor::Clone(EActorCloneFlags flags) {
	Actor * result = new Actor(mName);

	// copy the actor attributes
	result->mBoundingBox = mBoundingBox;
	result->mLocalRot = mLocalRot;
	result->mLocalTM = mLocalTM;
	result->mWorldTM = mWorldTM;
	result->mInvBoneTM = mInvBoneTM;
	result->mLocalScale = mLocalScale;
	result->mLocalPos = mLocalPos;
	result->mWorldScale = mWorldScale;
	result->mParent = nullptr;
	result->mOrgPos = mOrgPos;
	result->mOrgRot = mOrgRot;
	result->mOrgScale = mOrgScale;
	result->mOriginalActor = nullptr;
	result->mCurrentLOD = mCurrentLOD;
	result->mNumLODs = mNumLODs;
	result->mFacialSetups = mFacialSetups;
	result->mRepositioningMask = mRepositioningMask;

	result->mMaterials.resize(mMaterials.size());
	for (size_t i = 0; i < mMaterials.size(); i++) {
		for (auto material : mMaterials[i]) {

			// clone the materials
			if (flags & CLONE_MATERIALS) {
				result->AddMaterial(i, material->Clone());

			// share the materials
			} else {
				result->AddMaterial(i, material);
			}
		}
	}


	// create the node cloning flags
	int nodeFlags = 0;
	if (flags & CLONE_NODEATTRIBUTES)
		nodeFlags |= Node::CLONE_ATTRIBUTES;

	// clone all nodes (without parents, childs and motion links)
	for (auto* node : mNodes)
		result->mNodes.push_back(node->Clone(result, (Node::ENodeCloneFlags)nodeFlags));

	// now we have cloned all nodes, we have a problem, since the hierarchy (parent and childs) isn't there yet!
	// to solve this, we can simply do some searches on names of nodes in the original nodes array
	// first find all the parents, once we have the parents we can also find the childs, and once we have the childs we're done! :)
	const size_t numNodes = mNodes.size();
	for (size_t i = 0; i < numNodes; i++) {
		// if this node has no parent we don't need to search for it, so then skip to the next node
		if (mNodes[i]->GetParent() == (Node *)this) {
			result->mNodes[i]->SetParent(result);
			result->AddChild(result->mNodes[i]);
			continue;
		}

		bool foundParent = false;
		const size_t numResNodes = result->mNodes.size();
		for (int a = 0; a < numResNodes && !foundParent; a++) {
			// if the node name (so also its ID) is equal to the original parent name, it means we have found the parent to use :)
			if (result->mNodes[a]->GetID() == mNodes[i]->GetParent()->GetID()) {
				result->mNodes[i]->SetParent(result->mNodes[a]);
				foundParent = true;
			}
		}

		if (foundParent == false)
			LOG("No parent found for node '%s'", result->mNodes[i]->GetNamePtr());
	}

	// ok, we found the parents, now it's time to find the childs
	for (size_t i = 0; i < numNodes; i++) {
		// skip to the next node if this node has no childs
		if (mNodes[i]->GetNumChilds() == 0)
			continue;

		// for all childs
		const size_t numChilds = mNodes[i]->GetNumChilds();
		for (int c = 0; c < numChilds; c++) {
			bool foundChild = false;

			// check all nodes
			const size_t numResNodes = result->mNodes.size();
			for (int a = 0; a < numResNodes && !foundChild; a++) {
				// compare the node name with the original child name, if it's equal we found a match
				if (result->mNodes[a]->GetID() == mNodes[i]->GetChild(c)->GetID()) {
					result->mNodes[i]->AddChild(result->mNodes[a]);
					foundChild = true;
				}
			}
		}

		// make sure we end up with the same number of childs, else something really weird happened
		assert(mNodes[i]->GetNumChilds() == result->mNodes[i]->GetNumChilds());
	}

	// clone all stacks of the nodes
	// this is needed because of some execution order conflicts (if we would have done it inside the Node::Clone)
	const size_t numResNodes = result->mNodes.size();
	for (size_t i = 0; i < numResNodes; i++) {
		result->mNodes[i]->CloneNodeStacksFromNode(mNodes[i], result);
		result->mNodes[i]->CloneNodeCollisionStacksFromNode(mNodes[i], result);
	}

	// clone the actor collision system
	result->SetActorCollisionSystem(mActorCollisionSystem->Clone(result));


	// clone controllers if desired
	if (flags & CLONE_CONTROLLERS) {
		// iterate through the controllers and clone them
		for (auto* controller : mControllers) {
			result->AddController(controller->Clone(result));
		}

		// iterate through the post controllers and clone them
		for (auto* controller : mPostControllers) {
			result->AddPostController(controller->Clone(result));
		}
	}

	// copy the repositioning node
	if (mRepositioningNode)
		result->mRepositioningNode = result->GetNodeByID(mRepositioningNode->GetID());
	else
		result->mRepositioningNode = nullptr;

	// make sure the number of childs is the same
	assert(result->GetNumChilds() == GetNumChilds());

	return result;
}


// release allocated memory
void Actor::Release() {
	// get rid of the motion system
	if (mMotionSystem)
		delete mMotionSystem;

	// delete the actor collision system
	if (mActorCollisionSystem != nullptr)
		delete mActorCollisionSystem;

	// delete all the materials
	RemoveAllMaterials();

	// remove all nodes
	for (auto* node : mNodes) {
		delete node;
	}
	mNodes.clear();

	// remove all controllers
	RemoveAllControllers();
	RemoveAllPostControllers();

	// if we have a facial setup, remove it (smartpointer deletion)
	RemoveAllFacialSetups();

	// remove all attachments, but not from memory
	RemoveAllAttachments(false);
}


// process the hierarchy for all nodes
void Actor::ProcessHierarchy(Matrix * actorTM) {
	// update the local TM
	if (actorTM == nullptr)
		UpdateLocalTM();
	else
		mLocalTM = *actorTM;

	// update this nodes transformations
	if (IsRootNode()) {
		mWorldTM = mLocalTM;
		mWorldScale = mLocalScale;
	} else {
		Vector3 parentScale = mParent->GetWorldScale();
		mWorldScale = Vector3(mLocalScale.x * parentScale.x, mLocalScale.y * parentScale.y, mLocalScale.z * parentScale.z);
		mWorldTM = mLocalTM * mParent->GetWorldTM();
	}

	// process the hierarchy for all childs
	for (auto* child : mChilds)
		child->RecursiveHierarchyUpdate();
}


void Actor::UpdateBounds(const int lodLevel, EBoundsType boundsType, const bool includeAttachments) {
	// re-init the boundingbox
	mBoundingBox = RCore::AABB();

	// update the bounds
	if (boundsType == BOUNDS_NODE_BASED) {	// node based bounds
		// include the node positions
		for (auto* node : mNodes)
			mBoundingBox.Encapsulate(node->GetWorldPos());
	} else if (boundsType == BOUNDS_MESH_BASED) {	// mesh based bounds
		mBoundingBox = CalcMeshBasedAABB(lodLevel);
	} else {
		mBoundingBox = CalcCollisionMeshBasedAABB(lodLevel);
	}

	// include the attachments
	if (includeAttachments) {
		for (auto* attachment : mAttachments) {
			// make sure we have a safe LOD level
			const int numLods = attachment->GetNumLODLevels();
			const int lod = RCore::Clamp(lodLevel, 0, numLods - 1);

			// update the bounds of the attachment
			attachment->UpdateBounds(lod, boundsType, includeAttachments);

			// encapsulate the attachment box into this node box
			mBoundingBox.Encapsulate(attachment->GetBoundingBox());
		}
	}

	// update boundingsphere (containing the bounding box)
	mBoundingSphere = BoundingSphere(mBoundingBox.CalcMiddle(), 0.0f);
	mBoundingSphere.Encapsulate(mBoundingBox.GetMin());
	mBoundingSphere.Encapsulate(mBoundingBox.GetMax());
}



// play a motion
MotionInstance* Actor::PlayMotion(Motion *motion, PlayBackInfo *info) {
	assert(mMotionSystem != nullptr);
	assert(motion != nullptr);

	// if the user didn't specify any playback information, we will just use the default values
	if (info == nullptr) {
		PlayBackInfo defaultInfo;
		return mMotionSystem->PlayMotion(motion, &defaultInfo);
	} else {
		return mMotionSystem->PlayMotion(motion, info);
	}
}



// stop playing the motion in <blendTime> seconds
void Actor::StopMotion(MotionInstance * motion, const float blendTime) {
	assert(mMotionSystem != nullptr);

	mMotionSystem->StopMotion(motion, blendTime);
}


// removes all materials from the actor
void Actor::RemoveAllMaterials() {
	// for all LODs
	// remove all materials for this LOD
	// no delete is needed, because it's a smartpointer
	mMaterials.clear();
	mMaterials.shrink_to_fit();
}


void Actor::AddLODModel(Actor *lodActor, bool delLODActorFromMem) {
/*
	if (lodActor->GetNumNodes() != GetNumNodes())
		throw Exception("The LOD actor has a different amount of bones than the full detail actor, this is not allowed! Only reduce the triangle count, and do not adjust the skeleton structure.", HERE);

	// relink all nodes
	int i;
	const int numNodes = GetNumNodes();
	for (i = 0; i < numNodes; i++) {
		Node* node = GetNode(i);
		Node* lodNode = lodActor->GetNode(i);

		// make sure the nodes are the same
		if (node->GetName() != lodNode->GetName())
			throw Exception("The LOD actor has a different amount of bones than the full detail actor, this is not allowed! Only reduce the triangle count, and do not adjust the skeleton structure.", HERE);

		// copy over the mesh and collision mesh
		node->AddLODMesh(lodNode->GetMesh(0));
		if (node->GetMesh(mNumLODs).get())
			node->GetMesh(mNumLODs)->ReLinkAllVertexAttributeLayers(node->GetMesh(mNumLODs).get(), node, this);

		node->AddLODCollisionMesh(lodNode->GetCollisionMesh(0));
		if (node->GetCollisionMesh(mNumLODs).get())
			node->GetCollisionMesh(mNumLODs)->ReLinkAllVertexAttributeLayers(node->GetCollisionMesh(mNumLODs).get(), node, this);

		// handle the stacks
		if (lodNode->GetMeshDeformerStack(0).get() != nullptr)
			node->AddLODMeshDeformerStack(lodNode->GetMeshDeformerStack(0)->Clone(node->GetMesh(mNumLODs), this));
		else
			node->AddLODMeshDeformerStack(lodNode->GetMeshDeformerStack(0));

		// handle the collision stacks
		if (lodNode->GetCollisionMeshDeformerStack(0).get() != nullptr)
			node->AddLODCollisionMeshDeformerStack(lodNode->GetCollisionMeshDeformerStack(0)->Clone(node->GetCollisionMesh(mNumLODs), this));
		else
			node->AddLODCollisionMeshDeformerStack(lodNode->GetCollisionMeshDeformerStack(0));

		// consistency check
		if (node->GetNumMeshDeformerStacks() != node->GetNumMeshes())
			throw Exception("Something is wrong, the number of meshes in node '" + node->GetName() + "' is not equal to the number of mesh deformer stacks in that node.", HERE);

		// consistency check
		if (node->GetNumCollisionMeshDeformerStacks() != node->GetNumCollisionMeshes())
			throw Exception("Something is wrong, the number of collision meshes in node '" + node->GetName() + "' is not equal to the number of collision mesh deformer stacks in that node.", HERE);
	}
*/

	for (auto node : mNodes) {
		Node * lodNode = lodActor->GetNodeByID(node->GetID());

		// if the node couldn't be found in the lower detail level, then add nullptr meshes
		if (lodNode == nullptr) {
			// copy over the mesh and collision mesh
			node->AddLODMesh(nullptr);
			node->AddLODCollisionMesh(nullptr);

			// handle the stacks
			node->AddLODMeshDeformerStack(nullptr);
			node->AddLODCollisionMeshDeformerStack(nullptr);
		} else {
			// copy over the mesh and collision mesh
			node->AddLODMesh(lodNode->GetMesh(0));
			if (node->GetMesh(mNumLODs))
				node->GetMesh(mNumLODs)->ReLinkAllVertexAttributeLayers(node->GetMesh(mNumLODs).get(), node, this);

			node->AddLODCollisionMesh(lodNode->GetCollisionMesh(0));
			if (node->GetCollisionMesh(mNumLODs))
				node->GetCollisionMesh(mNumLODs)->ReLinkAllVertexAttributeLayers(node->GetCollisionMesh(mNumLODs).get(), node, this);

			// handle the stacks
			if (lodNode->GetMeshDeformerStack(0)) {
				auto clone = std::shared_ptr<MeshDeformerStack>(lodNode->GetMeshDeformerStack(0)->Clone(node->GetMesh(mNumLODs).get(), this));
				node->AddLODMeshDeformerStack(clone);
			} else
				node->AddLODMeshDeformerStack(lodNode->GetMeshDeformerStack(0));

			// handle the collision stacks
			if (lodNode->GetCollisionMeshDeformerStack(0)) {
				auto clone = std::shared_ptr<MeshDeformerStack>(lodNode->GetCollisionMeshDeformerStack(0)->Clone(node->GetCollisionMesh(mNumLODs).get(), this));
				node->AddLODCollisionMeshDeformerStack(clone);
			} else
				node->AddLODCollisionMeshDeformerStack(lodNode->GetCollisionMeshDeformerStack(0));
		}
	}


	// now copy the materials from the highest detail
	mMaterials.resize(mNumLODs + 1);
	const size_t numMaterials = lodActor->GetNumMaterials(0);
	for (int i = 0; i < numMaterials; i++)
		AddMaterial(mNumLODs, lodActor->GetMaterial(0, i));

	// add the facial setup from the LOD actor
	mFacialSetups.push_back(lodActor->GetFacialSetup(0));

	// increase the LOD level count
	mNumLODs++;

	// remove the actor from memory if desired
	if (delLODActorFromMem)
		delete lodActor;
}


// removes all node meshes and stacks
void Actor::RemoveAllNodeMeshes() {
	for (auto node : mNodes)
		node->RemoveMeshes();
}


// generate hardware shader friendly buffers
void Actor::GenerateHardwareShaderBuffers(vector<HwShaderBuffer> &outBuffers, const int lodLevel, const int maxBonesPerShader, const bool sortOnNumWeights, const bool includeSmartMeshMorphedMeshes) {
	// face bones
	std::set<Node*> faceBones;
	size_t lastHit = -1;

	// for all nodes in the actor
	for (auto node : mNodes) {
		Mesh * mesh = node->GetMesh(lodLevel).get();

		// if there is no mesh, we can simply continue to the next node
		if (mesh == nullptr)
			continue;

		// check if this node has facial animation applied on it
		bool hasFacial = false;
		if (node->GetMeshDeformerStack(lodLevel).get())
			hasFacial = node->GetMeshDeformerStack(lodLevel)->HasDeformerOfType(SmartMeshMorphDeformer::TYPE_ID);

		// skip nodes that have facial animation
		if (hasFacial && !includeSmartMeshMorphedMeshes)
			continue;

		// for all submeshes we will now create a buffer for each set of weights
		// submeshes are already sorted on material, so that saves us some work :)
		for (size_t s = 0; s < mesh->GetNumSubMeshes(); s++) {
			SubMesh * subMesh = mesh->GetSubMesh(s);
			int material = subMesh->GetMaterial();

			// start recording the primitives
			for (auto buffer : outBuffers)
				buffer.StartPrimitive(node, material);

			// for all faces in the submesh
			const int numIndices = subMesh->GetNumIndices();
			for (int f = 0; f < numIndices; f += 3) {
				// get the offset in mesh index array of the first vertex of this face
				int startIndex = subMesh->GetStartIndex() + f;

				// calculate the number of weights used by this face
				int maxInfluences = mesh->CalcMaxNumInfluencesForFace(startIndex);

				// gather the bones
				mesh->GatherBonesForFace(startIndex, faceBones);

				// check if there are already existing buffers where this face could be inserted in
				bool foundBufferForFace = false;

				// check the last used buffer (cache)
				if (lastHit != -1 && outBuffers[lastHit].CanHandleFace(maxInfluences, faceBones, material, maxBonesPerShader, !sortOnNumWeights)) {
					outBuffers[lastHit].AddFace(mesh, startIndex);
					foundBufferForFace = true;
				} else {
					// try to find a buffer which can handle this face
					const size_t numBuffers = outBuffers.size();
					for (size_t b = 0; b < numBuffers && !foundBufferForFace; b++) {
						// create a shortcut to the buffer
						HwShaderBuffer & buffer = outBuffers[b];

						// if the buffer can take this face
						if (buffer.CanHandleFace(maxInfluences, faceBones, material, maxBonesPerShader, !sortOnNumWeights)) {
							buffer.AddFace(mesh, startIndex);
							foundBufferForFace = true;
							lastHit = b;
							break;
						}
					}

					// no buffer can handle the current face, so let's create a new buffer
					if (foundBufferForFace == false) {
						HwShaderBuffer newBuffer(material, maxInfluences);
						outBuffers.push_back(newBuffer);
						outBuffers.back().StartPrimitive(node, material);
						outBuffers.back().AddFace(mesh, startIndex);
						lastHit = outBuffers.size() - 1;
					}
				}
			} // for all faces in submesh

			// stop recording the primitives
			for (auto buffer : outBuffers)
				buffer.EndPrimitive();
		} // for all submeshes
	} // for all nodes

/*
	LOG("BEFORE BUFFER OPTIMIZATION:");
	LOG("Generated buffers = %d", outBuffers.size());
	int totalVerts = 0;
	int totalIndices = 0;
	for (i = 0; i < outBuffers.size(); i++) {
		LOG("Buffer #%d/%d: infl=%d | bones=%d | verts=%d | indices=%d | prims=%d", i, outBuffers.size(), outBuffers[i].GetNumInfluences(), outBuffers[i].GetNumBones(), outBuffers[i].GetNumVertices(), outBuffers[i].GetNumIndices(), outBuffers[i].GetNumPrimitives());
		totalVerts += outBuffers[i].GetNumVertices();
		totalIndices += outBuffers[i].GetNumIndices();
	}
	LOG("totalVerts = %d", totalVerts);
	LOG("totalIndices = %d (%d faces)", totalIndices, totalIndices / 3);
*/
//-------------------------------------
// now that we have the buffers generated we have to optimize the buffers
// this can be quite tricky, because what we want to do is:
// - group buffers together, which have the same number of influences, but different materials (reduces number of small buffers)
// - sort buffers on number of influences (reduces vertex shader switches)
// - group primitives inside the buffers together, which share the same node and material (reduces primitive calls and increases batch sizes)

// STEP #1: group buffers together which have the same nr of influences but different materials
//int startBuffers = outBuffers.size();
	for (int i = 0; i < outBuffers.size(); i++) {
		for (int a = 0; a < outBuffers.size(); a++) {
			// don't try to attach this buffer to itself :)
			if (i == a)
				continue;

			// if the number of influences of both buffers is the same
			if (outBuffers[i].GetNumInfluences() == outBuffers[a].GetNumInfluences()) {
				// if attaching will fit our maximum buffers per shader requirement
				if (outBuffers[i].GetNumBones() + outBuffers[a].GetNumBones() <= maxBonesPerShader) {
					//RCore::LOG("Attaching buffer to %d", i);
					outBuffers[i].Attach(outBuffers[a]);
					outBuffers.erase(outBuffers.begin() + a);
					i = 0;
					break;
				}
			}
		}
	}

	// STEP #2: sort the buffers on number of influences
	// in just one line, isn't that cool? :) because of the < and == operator inside the HwShaderBuffer
	std::sort(outBuffers.begin(), outBuffers.end());


	// STEP #3: group primitives inside the buffers
	for (auto buffer : outBuffers)
		buffer.OptimizePrimitives();

	//RCore::LOG("***** BEFORE=%d   AFTER=%d", startBuffers, outBuffers.size());

	//-------------------------------------
/*	LOG("AFTER BUFFER OPTIMIZATION:");
	LOG("Generated buffers = %d", outBuffers.size());
	totalVerts = 0;
	totalIndices = 0;
	for (i = 0; i < outBuffers.size(); i++) {
		LOG("Buffer #%d/%d: infl=%d | bones=%d | verts=%d | indices=%d | prims=%d", i, outBuffers.size(), outBuffers[i].GetNumInfluences(), outBuffers[i].GetNumBones(), outBuffers[i].GetNumVertices(), outBuffers[i].GetNumIndices(), outBuffers[i].GetNumPrimitives());
		totalVerts += outBuffers[i].GetNumVertices();
		totalIndices += outBuffers[i].GetNumIndices();
	}
	LOG("totalVerts = %d", totalVerts);
	LOG("totalIndices = %d (%d faces)", totalIndices, totalIndices / 3);
*/
}


void Actor::CalcMeshTotals(const int lodLevel, int *outNumVertices, int *outNumIndices) {
	// the totals
	int totalVerts = 0;
	int totalIndices = 0;

	// for all nodes
	for (auto node : mNodes) {
		auto mesh = node->GetMesh(lodLevel);

		// if there is no mesh at this LOD level, skip to the next node
		if (mesh == nullptr)
			continue;

		// sum the values to the totals
		totalVerts += mesh->GetNumVertices();
		totalIndices += mesh->GetNumIndices();
	}

	// output the number of vertices
	if (outNumVertices)
		*outNumVertices = totalVerts;

	// output the number of indices
	if (outNumIndices)
		*outNumIndices = totalIndices;
}


void Actor::CalcStaticMeshTotals(const int lodLevel, int *outNumVertices, int *outNumIndices) {
	// the totals
	int totalVerts = 0;
	int totalIndices = 0;

	// for all nodes
	for (auto node : mNodes) {
		auto mesh = node->GetMesh(lodLevel);

		// if there is no mesh at this LOD level, skip to the next node
		if (mesh == nullptr)
			continue;

		// the node is dynamic, and we only want static meshes, so skip to the next node
		if (node->HasDeformableMesh(lodLevel))
			continue;

		// sum the values to the totals
		totalVerts += mesh->GetNumVertices();
		totalIndices += mesh->GetNumIndices();
	}

	// output the number of vertices
	if (outNumVertices)
		*outNumVertices = totalVerts;

	// output the number of indices
	if (outNumIndices)
		*outNumIndices = totalIndices;
}


void Actor::CalcDeformableMeshTotals(const int lodLevel, int * outNumVertices, int * outNumIndices) {
	// the totals
	int totalVerts = 0;
	int totalIndices = 0;

	// for all nodes
	for (auto node : mNodes) {
		auto mesh = node->GetMesh(lodLevel);

		// if there is no mesh at this LOD level, skip to the next node
		if (mesh == nullptr)
			continue;

		// the node is not dynamic (so static), and we only want dynamic meshes, so skip to the next node
		if (!node->HasDeformableMesh(lodLevel))
			continue;

		// sum the values to the totals
		totalVerts += mesh->GetNumVertices();
		totalIndices += mesh->GetNumIndices();
	}

	// output the number of vertices
	if (outNumVertices)
		*outNumVertices = totalVerts;

	// output the number of indices
	if (outNumIndices)
		*outNumIndices = totalIndices;
}


// set new collision system
void Actor::SetActorCollisionSystem(ActorCollisionSystem *collisionSystem) {
	if (collisionSystem != nullptr) {
		// if there is a collision system delete it
		if (mActorCollisionSystem != nullptr)
			delete mActorCollisionSystem;

		mActorCollisionSystem = collisionSystem;
	}
}


// returns the maximum number of weights/influences for this mesh
int Actor::CalcMaxNumInfluences(const int lodLevel) {
	int maxInfluences = 0;
	vector<shared_ptr<Mesh>> validMeshes;

	// add the actor's mesh
	if (GetMesh(lodLevel))
		validMeshes.push_back(GetMesh(lodLevel));

	// add the nodes' meshes to the array
	for (int i = 0; i < GetNumNodes(); i++) {
		// get the node
		Node *node = GetNode(i);

		if (node) {
			// add the mesh
			if (node->GetMesh(lodLevel))
				validMeshes.push_back(node->GetMesh(lodLevel));
		}
	}

	// iterate through the meshes and take the max number of influences
	for (int i = 0; i < validMeshes.size(); i++)
		maxInfluences = std::max(maxInfluences, validMeshes[i]->CalcMaxNumInfluences());

	// return the maximum number of influences
	return maxInfluences;
}


// returns the maximum number of weights/influences for this mesh plus some extra information
int Actor::CalcMaxNumInfluences(const int lodLevel, vector<int> &vertexCounts) {
	int maxInfluences = 0;
	vector<shared_ptr<Mesh>> validMeshes;

	// reset vertex count array
	vertexCounts.clear();

	// resize the vertex count array, min size must be 1, for nonskinned objects
	vertexCounts.resize(CalcMaxNumInfluences(lodLevel) + 1);

	// reset values
	for (int val : vertexCounts)
		val = 0;

	// add the actor's mesh
	if (GetMesh(lodLevel))
		validMeshes.push_back(GetMesh(lodLevel));

	// add the nodes' meshes to the array
	for (auto node : mNodes) {
		if (node) {
			// add the mesh
			if (node->GetMesh(lodLevel))
				validMeshes.push_back(node->GetMesh(lodLevel));
		}
	}

	// iterate through the meshes and take the max number of influences
	for (auto validMesh : validMeshes) {
		// vertex count array for this mesh
		vector<int> meshVertexCounts;

		// get max influences for this mesh
		int meshMaxInfluences = validMesh->CalcMaxNumInfluences(meshVertexCounts);

		// set new max number of influences
		maxInfluences = std::max(maxInfluences, meshMaxInfluences);

		// add vertex counts to the output array
		for (size_t j = 0; j < meshVertexCounts.size(); j++)
			vertexCounts[j] += meshVertexCounts[j];
	}

	// return the maximum number of influences
	return maxInfluences;
}


//-----------------------------------------------------


// add an attachment
int Actor::AddAttachment(Actor *attachment, Node *attachTarget) {
	assert(attachment != nullptr);
	assert(attachTarget != nullptr);
//	assert(mNodes.Contains(attachTarget));

	// add the attachment
	mAttachments.push_back(attachment);

	// link the attachment to the target
	ReLinkAttachment(attachment, attachTarget);

	// return the attachment number
	return mAttachments.size() - 1;
}


// removes a given attachment
void Actor::RemoveAttachment(Actor *attachment, const bool delFromMem) {
	assert(attachment != nullptr);

	// unlink it
	attachment->SetParent(nullptr);

	// delete it from memory, when desired
	if (delFromMem)
		delete attachment;

	// remove the attachment
	std::remove(mAttachments.begin(), mAttachments.end(), attachment);
}


// remove the attachment by index
void Actor::RemoveAttachment(const int nr, const bool delFromMem) {
	assert(nr < mAttachments.size());

	// unlink it
	mAttachments[nr]->SetParent(nullptr);

	// delete it from memory, when desired
	if (delFromMem)
		delete mAttachments[nr];

	// remove the attachment
	mAttachments.erase(mAttachments.begin() + nr);
}


// remove all attachments
void Actor::RemoveAllAttachments(const bool delFromMem) {
	// continue removing the last array element until they are all gone
	for (auto* attachment : mAttachments) {
		attachment->SetParent(nullptr);
		if (delFromMem) delete attachment;
	}
	mAttachments.clear();
	mAttachments.shrink_to_fit();
}


// relink the attachment
void Actor::ReLinkAttachment(Actor *attachment, Node *attachTarget) {
	assert(attachment);
//	assert(mAttachments.Contains(attachment));	// make sure the attachment remains inside this actor
//	assert(mNodes.Contains(attachTarget));		// make sure the atttach target node is part of this actor

	// relink the attachment to the attach target
	// please note that the attachment target does not get the attachment as child
	// this means recursive functions will not directly touch the attachment itself
	attachment->SetParent(attachTarget);
}


// relink the attachment by index
void Actor::ReLinkAttachment(const size_t nr, Node * attachTarget) {
	// make sure the atttach target node is part of this actor
//	assert(mNodes.Contains(attachTarget));

	// relink the attachment to the attach target
	// please note that the attachment target does not get the attachment as child
	// this means recursive functions will not directly touch the attachment itself
	mAttachments[nr]->SetParent(attachTarget);
}


// check if the mesh at the given LOD is deformable
bool Actor::HasDeformableMesh(const int lodLevel) const {
	assert(lodLevel < mNumLODs);

	// check if any of the nodes has a deformable mesh
	for (auto node : mNodes)
		if (node->HasDeformableMesh(lodLevel))
			return true;

	// aaaah, no deformable meshes found
	return false;
}


// check if there is any mesh available
bool Actor::HasMesh() const {
	// check if any of the nodes has a mesh
	for (auto node : mNodes)
		if (node->HasMesh())
			return true;

	// aaaah, no meshes found
	return false;
}


// check if the collision mesh at the given LOD is deformable
bool Actor::HasDeformableCollisionMesh(const int lodLevel) const {
	assert(lodLevel < mNumLODs);

	// check if any of the nodes has a deformable collision mesh
	for (auto node : mNodes)
		if (node->HasDeformableCollisionMesh(lodLevel))
			return true;

	// aaaah, no deformable meshes found
	return false;
}


// set the facial setup for a given LOD level
void Actor::SetFacialSetup(const int lodLevel, const shared_ptr<FacialSetup> &setup) {
	assert(lodLevel < mFacialSetups.size());
	mFacialSetups[lodLevel] = setup;
}


// get the facial setup for a given LOD level
shared_ptr<FacialSetup> Actor::GetFacialSetup(const int lodLevel) const {
	assert(lodLevel < mFacialSetups.size());
	return mFacialSetups[lodLevel];
}


// remove all facial setups
void Actor::RemoveAllFacialSetups() {
	// for all LODs
	for (auto setup : mFacialSetups)
		setup = nullptr;

	// remove all modifiers from the stacks for each lod in all nodes
	for (auto curNode : mNodes) {

		// process all LOD levels
		for (int lod = 0; lod < curNode->GetNumMeshDeformerStacks(); lod++) {
			// if we have a modifier stack
			if (curNode->GetMeshDeformerStack(lod).get() != nullptr) {
				// remove all smart mesh morph deformers from that mesh deformer stack
				curNode->GetMeshDeformerStack(lod)->RemoveAllDeformersByType(SmartMeshMorphDeformer::TYPE_ID);
			}
		}
	}
}

// calculate the mesh based axis aligned bounding box
RCore::AABB Actor::CalcMeshBasedAABB(const int lodLevel) {
	// initialize the bounding box
	RCore::AABB result = RCore::AABB();

	// process all nodes
	for (auto* curNode : mNodes) {

		// get the mesh
		auto mesh = curNode->GetMesh(lodLevel);
		if (mesh == nullptr)
			continue;

		// get the worldspace matrix
		Matrix worldTM = curNode->GetWorldTM();

		// get the position data
		Vector3* positions = mesh->GetPositions();

		// process all vertices
		const int numVerts = mesh->GetNumVertices();
		for (int v = 0; v < numVerts; v++) {
			result.Encapsulate(positions[v] * worldTM);
		}
	}

	return result;
}


// calculate the collision mesh based axis aligned bounding box
RCore::AABB Actor::CalcCollisionMeshBasedAABB(const int lodLevel) {
	// initialize the bounding box
	RCore::AABB result = RCore::AABB();

	// process all nodes
	for (auto* curNode : mNodes) {

		// get the mesh
		auto mesh = curNode->GetCollisionMesh(lodLevel);
		if (mesh == nullptr)
			continue;

		// get the worldspace matrix
		Matrix worldTM = curNode->GetWorldTM();

		// get the position data
		Vector3* positions = mesh->GetPositions();

		// process all vertices
		const int numVerts = mesh->GetNumVertices();
		for (int v = 0; v < numVerts; v++) {
			result.Encapsulate(positions[v] * worldTM);
		}
	}

	return result;
}


// find an attachment by name
Actor * Actor::FindAttachmentByName(const std::string &name) const {
	for (auto* attachment : mAttachments)
		if (attachment->GetName().compare(name) == 0)
			return attachment;

	return nullptr;
}


// find the attachment pointer by id
Actor * Actor::FindAttachmentByID(const int id) const {
	for (auto* attachment : mAttachments)
		if (attachment->GetID() == id)
			return attachment;

	return nullptr;
}


// find the attachment number by name
int Actor::FindAttachmentNumberByName(const std::string &name) const {
	auto it = std::find_if(mAttachments.begin(), mAttachments.end(), [&name] (const Actor* attachment) {
		return attachment->GetName().compare(name) == 0;
	});

	return (it == mAttachments.end()) ? -1 : (it - mAttachments.begin());
}


// find the attachment number by id
int Actor::FindAttachmentNumberByID(const int id) {
	auto it = std::find_if(mAttachments.begin(), mAttachments.end(), [&id] (const Actor* attachment) {
		return attachment->GetID() == id;
	});

	return (it == mAttachments.end()) ? -1 : (it - mAttachments.begin());
}


// find the attachment number by pointer
int Actor::FindAttachmentNumberByPointer(Actor *attachment) const {
	auto it = std::find(mAttachments.begin(), mAttachments.end(), attachment);

	return (it == mAttachments.end()) ? -1 : (it - mAttachments.begin());
}


// resolve the parent/child relationships
void Actor::RelinkHierarchy() {
	// remove all childs from the actor itself
	RemoveAllChilds();

	// iterate through all nodes and relink the child/parent relationship
	for (auto node : mNodes) {

		// remove all childs from the current node
		node->RemoveAllChilds();

		// check if it is valid
		if (!node)
			continue;

		// retrieve the parent node
		Node * parent = node->GetParent();

		// do searches if the parent node is valid
		bool foundParent = false;
		if (parent) {
			// iterate through all nodes and try to find the node's parent by name
			for (auto searchNode : mNodes) {

				// if the node name (so also its ID) is equal to the original parent name, it means we have found the parent
				if (parent->GetID() == searchNode->GetID()) {
					// assign the new parent to the node and break the loop
					node->SetParent(searchNode);

					// add the new child node
					if (!searchNode->IsNodeChild(node))
						searchNode->AddChild(node);

					foundParent = true;
					break;
				}
			}
		}

		// if there has no parent been found, parent it to the actor and make it a root node
		if (!foundParent) {
			// assign the new parent to the node
			node->SetParent(this);

			// add the new child node
			if (!this->IsNodeChild(node))
				this->AddChild(node);
		}


/*		// copy the child array and remove all the childs from the given node
		vector<Node*> childs = node->mChilds;
		node->RemoveAllChilds();
		// retrieve the number of childs of the given node
		const int numChilds = childs.size();
		// iterate through all childs and relink them
		for (int k = 0; k < numChilds; k++) {
			// retrieve the current child
			Node* child = childs[k];
			bool foundChild = false;
			// iterate through all nodes and try to find the node's child by name
			for (int l = 0; l < numNodes; l++) {
				// retrieve the current search node
				Node* searchNode = mNodes[l];
				// if the node name (so also its ID) is equal to the original child name, it means we have found the child
				if (child->GetID() == searchNode->GetID()) {
					// add the new child to the node
					node->AddChild(searchNode);
					foundChild = true;
					break;
				}
			}
			if (!foundChild)
				LOG("Actor::RerootSkeleton: No valid child found.");
		}
*/
	}
}


// remove all controllers
void Actor::RemoveAllControllers() {
	for (auto* controller : mControllers)
		delete controller;
	mControllers.clear();
	mControllers.shrink_to_fit();
}


// remove all post controllers
void Actor::RemoveAllPostControllers() {
	for (auto* controller : mPostControllers)
		delete controller;
	mPostControllers.clear();
	mPostControllers.shrink_to_fit();
}


// check if the material is used by a mesh of this actor
bool Actor::IsMaterialUsed(const int lodLevel, const int index) {
	// retrieve the given material smart pointer
	auto material = mMaterials[lodLevel][index];

	// iterate through all nodes of the actor and check its meshes
	for (auto* node : mNodes) {

		// retrieve the number of meshes (depending on number of level of details) the node has got
		const size_t numMeshes = node->GetNumMeshes();

		// check if the level of detail is not bigger than the number of meshes, else we would be out of array range
		if (lodLevel >= numMeshes)
			continue;

		// get the the mesh for the given level of detail from the node
		auto mesh = node->GetMesh(lodLevel);

		// check if the mesh is valid
		if (!mesh)
			continue;

		// iterate through the sub meshes
		for (int s = 0; s < mesh->GetNumSubMeshes(); s++) {
			// retrieve the current sub mesh
			auto subMesh = mesh->GetSubMesh(s);

			// get the current search material based on the given level of detail, the sub mesh's material number and the node's material offset
			auto searchMaterial = mMaterials[lodLevel][(subMesh->GetMaterial() + node->GetMaterialOffset())];

			// check if the search material and the material smart pointer passed as parameter point to the same material object in memory
			// if they are equal return true, this means that the given material is used by a mesh of the actor
			if (material.get() == searchMaterial.get())
				return true;
		}
	}

	// return false, this means that no mesh uses the given material
	return false;
}


// remove the given material and reassign all material numbers of the submeshes
void Actor::RemoveMaterial(const size_t lodLevel, const int index) {
	assert(lodLevel < mMaterials.size());

	// first of all remove the given material
	mMaterials[lodLevel].erase(mMaterials[lodLevel].begin() + index);

	// now we have to reassign the material numbers of the submeshes since the material order has now been changed
	// iterate through all nodes of the actor
	for (auto* node : mNodes) {

		// retrieve the number of meshes (depending on number of level of details) the node has got
		const size_t numMeshes = node->GetNumMeshes();

		// check if the level of detail is not bigger than the number of meshes, else we would be out of array range
		if (lodLevel >= numMeshes)
			continue;

		// get the the mesh for the given level of detail from the node
		auto mesh = node->GetMesh(lodLevel);

		// check if the mesh is valid
		if (!mesh)
			continue;

		// flag which indicates whether one of the submeshes' material number is bigger than the one which has been removed
		// only the one with bigger material numbers have to be reset, since the smaller ones still point to their true materials
		bool isIndexLower = false;

		// iterate through the sub meshes
		for (int s = 0; s < mesh->GetNumSubMeshes(); s++) {
			// retrieve the current sub mesh
			SubMesh * subMesh = mesh->GetSubMesh(s);

			// check if one of the submeshes' material number is bigger than the one which has been removed
			if (index < (subMesh->GetMaterial() + node->GetMaterialOffset()))
				isIndexLower = true;
		}

		// one of the sub meshes of this mesh has got a bigger material index number than the one which has been removed
		// this means the meshes would now point to a wrong material or their index would be even out of range
		// to fix that we simply decrease their material offset so that they point to the right materials again
		if (isIndexLower)
			node->SetMaterialOffset(node->GetMaterialOffset() - 1);
	}
}


// remove all materials which aren't used by the actor
void Actor::RemoveUnusedMaterials() {
	// iterate through all level of details and do the checks
	for (int l = 0; l < mNumLODs; l++) {
		int m = 0;
		// since the amount of materials change during this operation we cannot do this with a for loop, we need something more dynamic
		while (m < mMaterials[l].size()) {
			// check wheter the current search material is used by the actor
			if (!IsMaterialUsed(l, m)) {
				// the mesh isn't used by a mesh, ready to remove it
				RemoveMaterial(l, m);
			} else {
				// this one is used by a mesh, let it in and increase the looping variable so that we can check the next material
				m++;
			}
		}
	}
}


// try to find the repositioning node automatically
Node* Actor::FindBestRepositioningNode() {
	// reset the repositioning node
	Node * result = nullptr;

	// the maximum number of children of a root node, the node with the most children
	// will become our repositioning node
	int maxNumChilds = 0;

	// traverse through all root nodes
	for (auto* rootNode : mChilds) {

		// get the number of child nodes recursively
		const int numChildNodes = rootNode->GetNumChildNodesRecursive();

		// if the number of child nodes of this node is bigger than the current max number
		// this is our new candidate for the repositioning node
		if (numChildNodes > maxNumChilds)
			result = rootNode;
	}

	return result;
}


// extract a bone list
void Actor::ExtractBoneList(const int lodLevel, std::set<Node*> &outBoneList) {
	// clear the existing items
	outBoneList.clear();

	// for all nodes
	for (auto* node : mNodes) {
		Mesh * mesh = node->GetMesh(lodLevel).get();

		// skip nodes without meshes
		if (mesh == nullptr)
			continue;

		// find the skinning information, if it doesn't exist, skip to the next node
		SkinningInfoVertexAttributeLayer * skinningLayer = (SkinningInfoVertexAttributeLayer *)mesh->FindSharedVertexAttributeLayer(SkinningInfoVertexAttributeLayer::TYPE_ID);
		if (skinningLayer == nullptr)
			continue;

		// iterate through all skinning data
		const int numOrgVerts = mesh->GetNumOrgVertices();
		for (int v = 0; v < numOrgVerts; ++v) {
			// for all influences for this vertex
			const auto numInfluences = skinningLayer->GetNumInfluences(v);
			for (int i = 0; i < numInfluences; ++i) {
				// get the bone
				Node * bone = skinningLayer->GetInfluence(v, i).GetBone();

				// check if it is already in the bone list, if not, add it
				outBoneList.insert(bone);
			}
		}
	}
}

// set the motion system
void Actor::SetMotionSystem(MotionSystem *motionSystem) {
	if (mMotionSystem != nullptr)
		delete mMotionSystem;

	mMotionSystem = motionSystem;
}


void Actor::RemoveNode(const int nr, const bool delMem) {
	assert(nr < mNodes.size());

	if (delMem)
		delete mNodes[nr];

	mNodes.erase(mNodes.begin() + nr);
}


void Actor::RestoreInitialPose() {
	for (auto* node : mNodes)
		node->SetToOriginalOrientation();
}


void Actor::UpdateNodeStacks(const double timePassedInSeconds) {
	for (auto* node : mNodes) {

		// update the meshes
		if (mCurrentLOD < node->GetNumMeshDeformerStacks()) {
			if (node->GetMeshDeformerStack(mCurrentLOD))
				node->GetMeshDeformerStack(mCurrentLOD)->Update(this, node, timePassedInSeconds);
		}

		// update the collision meshes
		if (mCurrentLOD < node->GetNumCollisionMeshDeformerStacks()) {
			if (node->GetCollisionMeshDeformerStack(mCurrentLOD))
				node->GetCollisionMeshDeformerStack(mCurrentLOD)->Update(this, node, timePassedInSeconds);
		}
	}
}


void Actor::DeleteAllNodes() {
	// remove all nodes from this actor
	for (auto* node : mNodes) {
		delete node;
	}
	mNodes.clear();
}


// search for a node on a specific name, and return a pointer to it
Node* Actor::GetNodeByName(const char *name) const {
	for (auto* node : mNodes)
		if (node->GetName().compare(name) == 0)	// TODO: optimize
			return node;

	return nullptr;
}


// search on name, non case sensitive
Node* Actor::GetNodeByNameNoCase(const char *name) const {
	for (auto* node : mNodes) {
#ifdef _WIN32
		if (_stricmp(node->GetName().c_str(), name) == 0)
#else
		if (strcasecmp(node->GetName().c_str(), name) == 0)
#endif
			return node;
	}

	return nullptr;
}


// search for a node on ID
Node* Actor::GetNodeByID(const int id) const {
	for (auto* node : mNodes) {
		if (node->GetID() == id)
			return node;
	}

	return nullptr;
}


// search for a node by name, and returns it's number in the array (case sensitive), returns -1 when not found
int Actor::GetNodeNumberByName(const char* name) const {
	const size_t numNodes = mNodes.size();
	for (size_t i = 0; i < numNodes; i++)
		if (mNodes[i]->GetName() == name)
			return (int)i;

	return -1;
}


// get a material
shared_ptr<Material> Actor::GetMaterial(const int lodLevel, const int nr) const {
	assert(lodLevel < mMaterials.size());
	assert(nr < mMaterials[lodLevel].size());
	return mMaterials[lodLevel][nr];
}


// get a material by name
shared_ptr<Material> Actor::GetMaterialByName(const int lodLevel, const std::string & name) const {
	assert(lodLevel < mMaterials.size());

	// search through all materials
	for (int i = 0; i < mMaterials[lodLevel].size(); i++)
		if (mMaterials[lodLevel][i].get()->GetName().compare(name))
			return mMaterials[lodLevel][i];

	// no material found
	return nullptr;
}


// set a material
void Actor::SetMaterial(const size_t lodLevel, const int nr, const shared_ptr<Material>& mat) {
	assert(lodLevel < mMaterials.size());
	assert(nr < mMaterials[lodLevel].size());
	mMaterials[lodLevel][nr] = mat;
}


// remove the given controller
void Actor::RemoveController(const int nr, const bool delFromMem) {
	assert(nr < mControllers.size());

	// remove the controller from the stack
	mControllers.erase(mControllers.begin() + nr);

	// delete the controller from memory
	if (delFromMem)
		delete mControllers[nr];
}


// remove the given controller
void Actor::RemoveController(Controller *controller, const bool delFromMem) {
	mControllers.erase(std::remove(mControllers.begin(), mControllers.end(), controller), mControllers.end());
}


// update all controllers if it is enabled
void Actor::UpdateControllers(const double timePassedInSeconds) {
	for (auto* controller : mControllers) {
		if (controller->IsEnabled())
			controller->Update(timePassedInSeconds);
	}
}


// remove the given post controller
void Actor::RemovePostController(const int nr, const bool delFromMem) {
	assert(nr < mPostControllers.size());

	// delete the post controller from memory
	if (delFromMem)
		delete mPostControllers[nr];

		// remove the post controller from the stack
	mPostControllers.erase(mPostControllers.begin() + nr);
}


// remove the given post controller
void Actor::RemovePostController(Controller *controller, const bool delFromMem) {
	mPostControllers.erase(std::remove(mPostControllers.begin(), mPostControllers.end(), controller), mPostControllers.end());
}


// update all post controllers if it is enabled
void Actor::UpdatePostControllers(const double timePassedInSeconds) {
	for (auto* controller : mPostControllers) {
		if (controller->IsEnabled())
			controller->Update(timePassedInSeconds);
	}
}


// set the repositioning mask based on some boolean values
void Actor::SetRepositioningMask(const bool positionEnabled, const bool rotationEnabled, const bool scaleEnabled) {
	int mask = 0;

	// create the mask
	if (positionEnabled)	mask |= REPOSITION_POSITION;
	if (rotationEnabled)	mask |= REPOSITION_ROTATION;
	if (scaleEnabled)		mask |= REPOSITION_SCALE;

	// set it
	mRepositioningMask = (ERepositioningMask)mask;
}

} // namespace
