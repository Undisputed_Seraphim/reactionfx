#pragma once

#include <vector>

#include "RCore.h"

#include "Node.h"

namespace ReActionFX {

// forward declarations.
class JacobianIKData;

/**
 * The JacobianIKConstraint class.
 * Base class of a specific jacobian ik constraint which is applied to an actor using the jacobian IK controller.
 */
class JacobianIKConstraint {
public:
	/**
	 * Default constructor.
	 */
	JacobianIKConstraint() {}

	/**
	 * Destructor.
	 */
	~JacobianIKConstraint() {}

	/**
	 * Clone the IK constraint.
	 * @return A pointer to a newly created exact copy of the IK constraint.
	 */
	virtual JacobianIKConstraint* Clone() = 0;

	/**
	 * Update the jacobian ik constraint.
	 * @param timeDeltaInSeconds The time passed in seconds.
	 */
	virtual void Update(const float timeDeltaInSeconds) = 0;

	/**
	 * Return the number of functions needed to apply the constraint.
	 * @return The number of functions.
	 */
	virtual int GetNumFunc() const = 0;

	/**
	 * Return the specified value for the functions.
	 * @param values An array where the values will be placed.
	 */
	virtual void GetValues(std::vector<float>& values) = 0;

	/**
	 * Return the value for the functions using the actual state of the actor.
	 * @param results An array where the values will be placed.
	 */
	virtual void GetResults(std::vector<float>& results) = 0;

	/**
	 * Calculate the constraint functions gradients and store them in the given matrix.
	 * @param matrix The matrix where the gradients are stored.
	 */
	virtual void CalcGradient(RCore::NMatrix& matrix) = 0;

   /**
	* Return the node the constraint is applied to.
	* @return The pointer to the node to which the constraint is applied to.
	*/
	Node* GetNode() {
		return mNode;
	}

   /**
	* Return a pointer to the node the IK is applied from.
	* @return A pointer to the base node.
	*/
	Node* GetBaseNode() {
		return mBaseNode;
	}

   /**
	* Set the node the constraint is applied to.
	* @param node A pointer to the node to which the constraint is applied to.
	*/
	void SetNode(Node* node) {
		mNode = node;
	}

   /**
	* Set the pointer to a node the IK is applied from.
	* @param node A pointer to the base node.
	*/
	void SetBaseNode(Node* node) {
		mBaseNode = node;
	}

   /**
	* Set the jacobian ik data to work with.
	* @param data A Pointer to the jacobian ik data.
	*/
	void SetIKData(JacobianIKData* data) {
		mIKData = data;
	}

protected:
	Node*			mBaseNode;		/* Node from which IK is applied. */
	Node*			mNode;			/* Constrained node. */
	JacobianIKData*	mIKData;		/* Pointer to JacobianIKData class. It�s needed to get information about nodes parameters and derivatives. */
};

} // namespace