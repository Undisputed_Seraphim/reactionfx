#pragma once

#include "RCore.h"

#include "Interpolator.h"
#include "HermiteInterpolators.h"

namespace ReActionFX {

/**
 * The TCB spline interpolator class.
 * TCB stands for Tension, Continuity and Bias. These are properties used to describe
 * the behavior of the spline. It basically is a specialized form of the Hermite spline.
 * When there is no TCB information set (when there never has been a call to GetTCB())
 * the interpolation will just be the same as the HermiteInterpolator method.
 * In other words, in that case the TCB values will all be equal to zero.
 */
template <class ReturnType, class StorageType>
class TCBSplineInterpolator : public HermiteInterpolator<ReturnType, StorageType> {
public:
	// the type ID, returned by GetType()
	enum { TYPE_ID = 0x00000004 };

	/**
	 * The TCB information for each key.
	 * All attributes inside the struct must be in range of [0..1].
	 */
	struct TCBInfo {
		float	mTension;		/* The tension. */
		float	mContinuity;	/* The continuity. */
		float	mBias;			/* The bias. */

		TCBInfo() : mTension(0), mContinuity(0), mBias(0) {}
	};


	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	TCBSplineInterpolator()
		: HermiteInterpolator<ReturnType, StorageType>(), mTCBs(nullptr), mNumTCBs(0) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	TCBSplineInterpolator(KeyTrack<ReturnType, StorageType>* interpolationSource)
		: HermiteInterpolator<ReturnType, StorageType>(interpolationSource), mTCBs(nullptr), mNumTCBs(0) {}

	/**
	 * Destructor.
	 */
	~TCBSplineInterpolator();

	/**
	 * Initialize the interpolator.
	 * In this case this will calculate the tangents for the keys.
	 */
	virtual void Init();

	/**
	 * Returns the unique interpolator type ID.
	 * This can be used to determine what interpolator is used.
	 * @result The unique type identification number of this interpolator class.
	 */
	virtual int GetType() const { return TYPE_ID; }

	/**
	 * Returns the type string of this interpolator class.
	 * This can be the class name, or a description of the interpolator.
	 * @result The string containing the description or class name of this interpolator.
	 */
	virtual const char* GetTypeString() const { return "TCBSplineInterpolator"; }

	/**
	 * Returns the given TCB information for a given key with both read and write access.
	 * When this method detects that there isn't any TCB information allocated yet, it will
	 * automatically allocate new memory. So you do not have to bother about memory management.
	 * The number of TCBInfo structs is always equal to the number which is returned by
	 * the method KeyTrack<ReturnType, StorageType>::GetNumKeys().
	 * @param keyNr The key number to get the TCB information from.
	 */
	inline TCBInfo&	GetTCB(const int keyNr);


protected:
	int			mNumTCBs;
	TCBInfo*	mTCBs;
};


/**
 * The TCB spline interpolator class used for quaternions.
 * The reason why there is a special class for quaternions is that they
 * need some additional checks in order to perform the interpolation correctly.
 */
template <class StorageType>
class TCBSplineQuaternionInterpolator : public TCBSplineInterpolator<RCore::Quaternion, StorageType> {

public:
	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	TCBSplineQuaternionInterpolator() : TCBSplineInterpolator<RCore::Quaternion, StorageType>(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	TCBSplineQuaternionInterpolator(KeyTrack<RCore::Quaternion, StorageType>* interpolationSource) : TCBSplineInterpolator<RCore::Quaternion, StorageType>(interpolationSource) {}

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	RCore::Quaternion Interpolate(const int startKey, const double timeValue) const;
};


// destructor
template <class ReturnType, class StorageType>
TCBSplineInterpolator<ReturnType, StorageType>::~TCBSplineInterpolator() {
	// get rid of the allocated TCB data
	if (mTCBs)
		free(mTCBs);
}


template <class ReturnType, class StorageType>
inline typename TCBSplineInterpolator<ReturnType, StorageType>::TCBInfo& TCBSplineInterpolator<ReturnType, StorageType>::GetTCB(const int keyNr) {
	assert(this->mInterpolationSource);
	assert(keyNr < this->mInterpolationSource->GetNumKeys());

	// if there is no TCB information or the number of TCBs is different from the number of keys
	if (mTCBs == nullptr || mNumTCBs != this->mInterpolationSource->GetNumKeys()) {
		// (re)allocate the data, so we have enough TCB elements
		mTCBs = (TCBInfo*)realloc(this->mTCBInfo, sizeof(TCBInfo) * this->mInterpolationSource->GetNumKeys());

		// update the number of TCBs
		mNumTCBs = this->mInterpolationSource->GetNumKeys();
	}

	return mTCBs[keyNr];
}

// initialize the interpolator
template <class ReturnType, class StorageType>
void TCBSplineInterpolator<ReturnType, StorageType>::Init() {
	assert(this->mInterpolationSource);	// make sure we have a key track assigned

									// get rid of existing tangents
	if (this->mTangents)
		free(this->mTangents);

	// allocate the new tangents
	const int numKeys = this->mInterpolationSource->GetNumKeys();
	this->mTangents = malloc(numKeys * sizeof(ReturnType));

	// calculate all tangents
	for (int i = 0; i < numKeys; i++) {
		// if there is no previous or next key
		if (i == 0 || i == numKeys - 1) {
			ReturnType tangent;
			memset(&tangent, 0, sizeof(ReturnType));
			this->mTangents[i] = tangent;
		} else	// if there is a previous or next key
		{
			const ReturnType& prevValue = this->mInterpolationSource->GetKey(i - 1)->GetValue();
			const ReturnType& value = this->mInterpolationSource->GetKey(i)->GetValue();
			const ReturnType& nextValue = this->mInterpolationSource->GetKey(i + 1)->GetValue();

			// if there is TCB information
			if (mTCBs != nullptr) {
				// get the TCB values
				const float tension = mTCBs[i].mTension;
				const float continuity = mTCBs[i].mContinuity;
				const float bias = mTCBs[i].mBias;

				// if the tension, bias and continuity are equal to zero, we can use standard hermite interpolation
				//if (tension==0.0 && bias==0.0 && continuity==0.0)
				//mTangents[i] = (0.5 * (value - prevValue)) + (0.5 * (nextValue - value));	// normal hermite interpolation
				//else
				this->mTangents[i] = (((1.0 - tension)*(1.0 + bias)*(1.0 + continuity))*0.5) * (value - prevValue) + (((1.0 - tension)*(1.0 - bias)*(1.0 - continuity))*0.5) * (nextValue - value);
			} else	// there is no TCB info, so just use standard hermite interpolation
				this->mTangents[i] = (0.5 * (value - prevValue)) + (0.5 * (nextValue - value));
		}
	}
}


// the interpolation method
template <class StorageType>
RCore::Quaternion TCBSplineQuaternionInterpolator<StorageType>::Interpolate(const int startKey, const double timeValue) const {
	// get the quaternions
	RCore::Quaternion a = this->mInterpolationSource->GetKey(startKey)->GetValue();
	RCore::Quaternion b = this->mInterpolationSource->GetKey(startKey + 1)->GetValue();

	// check if both quaternions are on the same hypersphere or not, if not, invert one
	if (a.Dot(b) < 0.0)
		a = -a;

	//				[ 2 -2  1  1]	[a]
	//[u3 u2 u 1]	[-3  3 -2 -1]	[b]
	//				[ 0  0  1  0]	[ta]
	//				[ 1  0  0  0]	[tb]

	// interpolate, and take the exponent of that, which is the interpolated quaternion
	double t = timeValue;
	double t2 = t * t;
	double t3 = t2 * t;

	return ((2 * t3 + -3 * t2 + 1) * a.LogN() +
		(-2 * t3 + 3 * t2)		* b.LogN() +
			(t3 + -2 * t2 + t)	* this->mTangents[startKey] +
			(t3 + -t2)		* this->mTangents[startKey + 1]).Exp().Normalize();
}
} // namespace
