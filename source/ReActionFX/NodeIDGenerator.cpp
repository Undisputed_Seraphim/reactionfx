#include "NodeIDGenerator.h"

namespace ReActionFX {

int NodeIDGenerator::GenerateIDForString(const std::string & nodeName) {
	for (int i = 0; i < mNodeNames.size(); i++) {
		if (mNodeNames[i] == nodeName)
			return i;
	}

	mNodeNames.push_back(nodeName);

	return (int)mNodeNames.size() - 1;
}


const std::string& NodeIDGenerator::GetNodeName(const int id) {
	assert(id < mNodeNames.size());
	return mNodeNames[id];
}

} // namespace