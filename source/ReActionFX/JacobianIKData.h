#pragma once

#include <vector>

#include "RCore.h"

#include "JacobianIKConstraint.h"
#include "JacobianIKActorDOF.h"

namespace ReActionFX {

/**
 * The jacobian ik data class.
 * Holds a jacobian ik constraint array and a pointer to the Actor
 * the IK is applied to. It creates an instance of the Actor for
 * calculation issues.
 */
class JacobianIKData {
public:
	/**
	 * Default constructor.
	 */
	JacobianIKData() :mJacobianActorDOF(nullptr), mActorInstance(nullptr) {}

	/**
	 * Destructor.
	 */
	~JacobianIKData();

	/**
	 * Get the number of constraints the ik solver has to apply.
	 * @return The number of constraints.
	 */
	size_t GetNumConstraints() const {
		return mConstraints.size();
	}

	/**
	 * Return a pointer to the actor to which the constraints are applied to.
	 * @return A pointer to the actor to which the constraints are applied to.
	 */
	Actor* GetActor() const {
		return mActor;
	}

	Actor* GetActorInstance() const {
		return mActorInstance;
	}

	/**
	 * Set the actor to which the constraints are applied to.
	 * @param actor A pointer to the actor to which the constraints are applied to.
	 */
	void SetActor(Actor* actor);

	/**
	 * Add a constraint which represents the goal to reach.
	 * @param constraint A pointer to the constraint. Could be a pointer to an instance of the
	 *					 JacobianIKFixedPoint or JacobianIKRelativePoint class.
	 */
	void AddConstraint(JacobianIKConstraint* constraint) {
		mConstraints.push_back(constraint);
		constraint->SetIKData(this);
	}

	/**
	 * Remove the given constraint from the constraints stack.
	 * @param constraint A pointer to the constraint.
	 */
	void RemoveConstraint(JacobianIKConstraint* constraint) {
		mConstraints.erase(std::remove(mConstraints.begin(), mConstraints.end(), constraint), mConstraints.end());
	}

	/**
	 * Get a given jacobian ik constraint by index.
	 * @return A pointer to the jacobian ik constraint. NULL if the function fails.
	 */
	JacobianIKConstraint* GetConstraint(const int index) const {
		return mConstraints.at(index);
	}

	/**
	 * Get the node of the given constraint to which the constraint is refered to.
	 * @param index The index of the constraint to get the node from.
	 * @return A pointer to the node to which the constraint is refered to.
	 */
	Node* GetConstraintNode(const int index) const {
		return mConstraints[index]->GetNode();
	}

	/**
	 * Calculate the derivative of the node position respect to the given node DOF.
	 * @param node A pointer to the node.
	 * @param DOFIndex Index of the node DOF.
	 * @return A vector with the 3 coordinates derivatives.
	 */
	RCore::Vector3 CalcPosDer(Node* node, const int DOFIndex);

	/**
	 * Get the number of DOFs in the IK system.
	 * @return The number of DOFs.
	 */
	int GetNumDOF() const {
		return mJacobianActorDOF->GetNumDOF();
	}

	/**
	 * Get the number of DOFs of the given node.
	 * @param node Pointer to the node.
	 * @return The number of DOFs.
	 */
	int GetNumDOF(Node* node) const {
		return ((NodeDOF*)(node->GetAttributeByType(NodeDOF::TYPE_ID)))->GetNumDOF();
	}

	/**
	 * Fill in the given array with the DOF variables from the actor's actual pose.
	 * The array will be cleared before values are inserted.
	 * @param values The array where the values are going to be inserted.
	 */
	void GetDOFValues(std::vector<float>& values);

	/**
	 * Return true if the given DOF has influence in the node position.
	 * @param node Pointer to the node.
	 * @param DOFIndex The DOF index.
	 * @return True if the given DOF has incluence in the node position.
	 */
	bool GetInfluence(Node* node, const int DOFIndex) const {
		return mJacobianActorDOF->GetInfluence(node, DOFIndex);
	}

	/**
	 * Return the node index in the actor nodes array.
	 * @param node Pointer to the node.
	 * @return The node index in actor's array.
	 */
	int GetNodeID(Node* node) const {
		return ((NodeDOF*)(node->GetAttributeByType(NodeDOF::TYPE_ID)))->GetNodeID();
	}

	/**
	 * Apply the values in the given array to the DOF on the actor instance.
	 * @param values Vector which contains the values.
	 */
	void SetValues(std::vector<float>& values) {
		mJacobianActorDOF->SetValues(values);
	}

	/**
	 * Update all constraints which are managed by the jacobian ik data.
	 * @param timeDeltaInSeconds The time delta since the last frame in seconds.
	 */
	void UpdateConstraints(const float timeDeltaInSeconds);

	/**
	 * Apply the DOF values resulted by the IK.
	 * @param solution Vector with the DOF values
	 */
	void Apply(std::vector<float>& solution);

	/**
	 * Initialize actor instance with actor pose.
	 */
	void InitInstance();

	/**
	 * Return the index of the first DOF of the node.
	 * @param node The node where to search the first DOF.
	 * @return The index of the first DOF.
	 */
	int GetNode1stDOF(Node* node) const {
		return mJacobianActorDOF->GetNode1stDOF(GetNodeID(node));
	}

	/**
	 * Return max value for the DOF 'index'.
	 * @param index The DOF index
	 * @return The maximum value of the given DOF.
	 **/
	float GetMax(const int index);

	/**
	 * Return min value for the DOF 'index'.
	 * @param index The DOF index
	 * @return The minimum value of the given DOF.
	 **/
	float GetMin(const int index);

private:
	std::vector<JacobianIKConstraint*>	mConstraints;		/* The constraint array. */
	JacobianIKActorDOF*					mJacobianActorDOF;	/* The jacobian ik actor DOF of the actor to which the constraints are applied to. */
	Actor*								mActorInstance;		/* Actor instance for calculation issues. */
	Actor*								mActor;				/* The actor to which the jacobian ik data belongs to. */
};

} // namespace