#pragma once

#include <fstream>

#include "RCore.h"

namespace ReActionFX {

/**
 * The image resolution checker class extracts the width and height from an image file.
 * Simply call GetResoltuion(), it will automatically take the correct function
 * for the given image file.
 */
class ImageResChecker {
public:
	/**
	 * Default constructor.
	 */
	ImageResChecker() {}

	/**
	 * Destructor.
	 */
	virtual ~ImageResChecker() {}

   /**
	* Extract the resolution from an image file.
	* @param filename The file name of the image file.
	* @param outWidth Pointer to an integer to which the function will write the image's width.
	* @param outHeight Pointer to an integer to which the function will write the image's height.
	* @return True if the function has extracted the resolution successfully, false if not.
	*/
	virtual bool GetResolution(const std::string& filename, uint32_t& outWidth, uint32_t& outHeight);


protected:
	/**
	 * Extract resolution from a BMP file.
	 * @param filename The filename of the image file.
	 * @param outWidth Pointer to an integer to which the function will write the image's width.
	 * @param outHeight Pointer to an integer to which the function will write the image's height.
	 * @return True if the function has extracted the resolution successfully, false if not.
	 */
	bool GetBMPRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight);

	/**
	 * Extract resolution from a TGA file.
	 * @param filename The filename of the image file.
	 * @param outWidth Pointer to an integer to which the function will write the image's width.
	 * @param outHeight Pointer to an integer to which the function will write the image's height.
	 * @return True if the function has extracted the resolution successfully, false if not.
	 */
	bool GetTGARes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight);

	/**
	 * Extract resolution from a JPG file.
	 * @param filename The filename of the image file.
	 * @param outWidth Pointer to an integer to which the function will write the image's width.
	 * @param outHeight Pointer to an integer to which the function will write the image's height.
	 * @return True if the function has extracted the resolution successfully, false if not.
	 */
	bool GetJPGRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight);

	/**
	 * Extract resolution from a PNG file.
	 * @param filename The filename of the image file.
	 * @param outWidth Pointer to an integer to which the function will write the image's width.
	 * @param outHeight Pointer to an integer to which the function will write the image's height.
	 * @return True if the function has extracted the resolution successfully, false if not.
	 */
	bool GetPNGRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight);

	/**
	 * Extract resolution from a Photoshop PSD file.
	 * @param filename The filename of the image file.
	 * @param outWidth Pointer to an integer to which the function will write the image's width.
	 * @param outHeight Pointer to an integer to which the function will write the image's height.
	 * @return True if the function has extracted the resolution successfully, false if not.
	 */
	bool GetPSDRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight);
};

} // namespace