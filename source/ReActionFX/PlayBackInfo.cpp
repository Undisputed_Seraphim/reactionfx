#include "PlayBackInfo.h"
#include "PlayModeForward.h"
#include "PlayModeBackward.h"

using namespace RCore;

namespace ReActionFX {

// constructor
PlayBackInfo::PlayBackInfo() {
	mBlendInTime = 0.3f;
	mBlendOutTime = 0.3f;
	mPlaySpeed = 1.0f;
	mTargetWeight = 1.0f;
	mNumLoops = FOREVER;
	mBlendMode = BLENDMODE_OVERWRITE;
	mPlayMode = new PlayModeForward();
	mPlayMask = PLAYMASK_POSROT;
	mPlayNow = true;
	mMix = false;
	mRepositionAfterLoop = false;
	mPriorityLevel = 0;
}


// set a new play mode
void PlayBackInfo::SetPlayMode(PlayMode* mode) {
	// check if we are dealing with a valid play mode
	if (!mode)
		return;

	// get rid of the old play mode
	if (mPlayMode)
		delete mPlayMode;

	mPlayMode = mode;
}

} // namespace