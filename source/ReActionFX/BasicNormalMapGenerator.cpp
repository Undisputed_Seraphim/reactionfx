#include "BasicNormalMapGenerator.h"
#include "Mesh.h"
#include "SubMesh.h"
#include "StandardMaterial.h"

#include <utility>

using namespace RCore;
using std::vector;

namespace ReActionFX {

// constructor
BasicNormalMapGenerator::BasicNormalMapGenerator() : NormalMapGenerator() {
	mTotalProgress = 0.0f;
	mPartProgress = 0.0f;
	mResChecker = new ImageResChecker();
}

// the main method, which generates a set of normal maps from a low and high detail model
void BasicNormalMapGenerator::Generate(Actor* lowDetail, Actor* highDetail, bool tangentSpace, bool postFilter) {
	// get rid of the previous normal maps
	ReleaseNormalMaps();

	// reset some member values
	mTotalFaces = 0;
	mTotalProgress = 0;
	mPartProgress = 0;

	// generate an array of normal map parts
	vector<NormalMapPart>	parts;
	const size_t numNodes = lowDetail->GetNumNodes();
	for (int n = 0; n < numNodes; n++) {
		Node* curNode = lowDetail->GetNode(n);
		auto curMesh = curNode->GetMesh(0);

		// skip if there is no mesh
		if (curMesh == nullptr) continue;

		// get the first UV set
		UVVertexAttributeLayer* uvLayer = curMesh->GetUVLayer(0);

		// we cannot generate if the uv data isn't there
		if (uvLayer == nullptr) continue;

		for (int s = 0; s < curMesh->GetNumSubMeshes(); s++) {
			SubMesh* subMesh = curMesh->GetSubMesh(s);

			std::string  mapName;
			int material = subMesh->GetMaterial();
			auto baseMaterial = lowDetail->GetMaterial(0, material);

			if (baseMaterial->GetType() == StandardMaterial::TYPE_ID) {
				auto mat = std::dynamic_pointer_cast<StandardMaterial>(baseMaterial);

				// if there are no texure layers, simply give it the name of the material
				if (mat->GetNumLayers() == 0) {
					mapName = mat->GetName();
				} else {
					// if there are map layers, try to find the diffuse layer
					int layerNr = mat->FindLayer(StandardMaterialLayer::LAYERTYPE_DIFFUSE);
					if (layerNr == -1)
						mapName = mat->GetName();
					else
						mapName = mat->GetLayer(layerNr)->GetFilename();
				}
			} else
				mapName = baseMaterial->GetName();

			// add the part
			NormalMapPart newPart;
			newPart.mMaterialNr = material;
			newPart.mMesh = curMesh.get();
			newPart.mNode = curNode;
			newPart.mSubMesh = subMesh;
			newPart.mDiffuseName = mapName;
			parts.push_back(newPart);

			mTotalFaces += subMesh->GetNumIndices() / 3;

			// find the normalmap for this part
			bool found = false;
			for (int i = 0; i < mNormalMaps.size() && !found; i++) {
				if (mNormalMaps[i]->GetName() == parts.back().mDiffuseName) {
					parts.back().mNormalMap = mNormalMaps[i];
					found = true;
				}
			}

			// if no normalmap found, create one
			if (!found) {
				const int RES = mDefaultRes;

				uint32_t width, height;
				if (mResChecker != nullptr) {
					if (!mResChecker->GetResolution(mapName, width, height)) {
						width = RES;
						height = RES;
					} else {
						width *= mRatio;
						height *= mRatio;
					}
				} else {
					width = RES;
					height = RES;
				}

				RCore::LOG("Allocating normalmap '%s' (%dx%d)", mapName.data(), width, height);
				NormalMap* normalMap = new NormalMap(width, height, mapName);
				mNormalMaps.push_back(normalMap);
				parts.back().mNormalMap = normalMap;
			}
		}
	}


	int curFace = 0;

	// process all normalmaps
	for (auto& part : parts) {
		Node* curNode = part.mNode;
		Mesh* curMesh = part.mMesh;

		// make sure we have generated our tangent info
		curMesh->CalcTangents(0);	// use the first UV set

		// get some pointers to direct mesh data
		int*			indices = curMesh->GetIndices();
		RCore::Vector3*	positions = curMesh->GetPositions();
		RCore::Vector3*	normalData = curMesh->GetNormals();
		RCore::Vector4*	tangentData = curMesh->GetTangents();

		// get the first UV set
		UVVertexAttributeLayer* uvLayer = curMesh->GetUVLayer(0);
		RCore::Vector2*			uvData = nullptr;

		if (uvLayer)
			uvData = uvLayer->GetUVs();

		// for all submeshes
		SubMesh* subMesh = part.mSubMesh;

		// get the index offset of the submesh
		const int startIndex = subMesh->GetStartIndex();

		// find the normalmap number
		mCurNormalMap = std::find(mNormalMaps.begin(), mNormalMaps.end(), part.mNormalMap) - mNormalMaps.begin();
		assert(mCurNormalMap < mNormalMaps.size());

		// for all faces in the submesh of the lowpoly model
		const int numIndices = subMesh->GetNumIndices();
		for (int f = 0; f < numIndices; f += 3) {
			// get the face indices
			const int indexA = indices[startIndex + f];
			const int indexB = indices[startIndex + f + 1];
			const int indexC = indices[startIndex + f + 2];

			// get the face positions
			RCore::Vector3 fPos[3];
			fPos[0] = positions[indexA];
			fPos[1] = positions[indexB];
			fPos[2] = positions[indexC];

			RCore::DVector3 pos[3];
			pos[0].Set(fPos[0].x, fPos[0].y, fPos[0].z);
			pos[1].Set(fPos[1].x, fPos[1].y, fPos[1].z);
			pos[2].Set(fPos[2].x, fPos[2].y, fPos[2].z);

			// get the face uvs
			RCore::DVector2 uv[3];
			uv[0] = RCore::DVector2(uvData[indexA].x, uvData[indexA].y);
			uv[1] = RCore::DVector2(uvData[indexB].x, uvData[indexB].y);
			uv[2] = RCore::DVector2(uvData[indexC].x, uvData[indexC].y);

			// get the normals
			RCore::Vector3 fNormals[3];
			fNormals[0] = normalData[indexA];
			fNormals[1] = normalData[indexB];
			fNormals[2] = normalData[indexC];

			RCore::DVector3 normals[3];
			normals[0].Set(fNormals[0].x, fNormals[0].y, fNormals[0].z);
			normals[1].Set(fNormals[1].x, fNormals[1].y, fNormals[1].z);
			normals[2].Set(fNormals[2].x, fNormals[2].y, fNormals[2].z);

			// get the tangents
			RCore::Vector4 fTangents[3];
			fTangents[0] = tangentData[indexA];
			fTangents[1] = tangentData[indexB];
			fTangents[2] = tangentData[indexC];

			RCore::DVector4 tangents[3];
			tangents[0].Set(fTangents[0].x, fTangents[0].y, fTangents[0].z, fTangents[0].w);
			tangents[1].Set(fTangents[1].x, fTangents[1].y, fTangents[1].z, fTangents[1].w);
			tangents[2].Set(fTangents[2].x, fTangents[2].y, fTangents[2].z, fTangents[2].w);

			// process the current face
			ProcessFace(pos, uv, normals, tangents, part.mNormalMap, highDetail, curNode, tangentSpace, postFilter);

			curFace++;
			mPartProgress = ((f + 1) / (float)numIndices) * 100.0f;
			mTotalProgress = ((curFace + 1) / (float)mTotalFaces) * 100.0f;
		} // for all faces (indices)
	} // for all normalmaps parts


	for (auto* normalMap : mNormalMaps) {

		// fix the errors, and fill the empty pixels
		normalMap->FixErrors();

		// fill the empty pixels
		normalMap->FillEmptyPixels();

		// now it is time to blur the image
		if (postFilter)
			normalMap->Blur();

		// renormalize the image
		normalMap->Normalize();

		// if bumpmap output
		//normalMap->FixBump();
	}
}


void BasicNormalMapGenerator::ProcessFace(RCore::DVector3 pos[3], RCore::DVector2 uv[3], RCore::DVector3 normals[3], RCore::DVector4 tangents[3], NormalMap* normalMap, Actor* highDetail, Node* node, bool tangentSpace, bool postFilter) {
	// calculate the poly normal
	RCore::DVector3 polyNormal = (pos[1] - pos[0]).Cross(pos[2] - pos[0]).Normalize();

	// calculate the top most vertex of the 2d triangle
	int	top = 0;
	if (uv[1].y < uv[0].y)		top = 1;
	if (uv[2].y < uv[top].y)	top = 2;

	// sort the first
	if (top > 0) {
		std::swap(pos[0], pos[top]);
		std::swap(uv[0], uv[top]);
		std::swap(normals[0], normals[top]);
		std::swap(tangents[0], tangents[top]);
	}

	// sort the remaining two
	if (uv[2].y < uv[1].y) {
		std::swap(pos[1], pos[2]);
		std::swap(uv[1], uv[2]);
		std::swap(normals[1], normals[2]);
		std::swap(tangents[1], tangents[2]);
	}

	// output normalmap resolution
	const int XRES = normalMap->GetWidth();
	const int YRES = normalMap->GetHeight();

	// calculate normalmap space vertex positions
	int x1 = uv[0].x * XRES;
	int y1 = uv[0].y * YRES;
	int x2 = uv[1].x * XRES;
	int y2 = uv[1].y * YRES;
	int x3 = uv[2].x * XRES;
	int y3 = uv[2].y * YRES;

	// calculate normalmap space interpolation steps
	double iX1toX2step = (x2 - x1) / (double)(y2 - y1);
	double iX1toX3step = (x3 - x1) / (double)(y3 - y1);
	double iX2toX3step = (x3 - x2) / (double)(y3 - y2);

	// calculate worldspace interpolation steps
	RCore::DVector3 pX1toX2step = (pos[1] - pos[0]) / (double)(y2 - y1);
	RCore::DVector3 pX1toX3step = (pos[2] - pos[0]) / (double)(y3 - y1);
	RCore::DVector3 pX2toX3step = (pos[2] - pos[1]) / (double)(y3 - y2);

	// calculate tangent and normal interpolation steps
	RCore::DVector4 tX1toX2step = (tangents[1] - tangents[0]) / (double)(y2 - y1);
	RCore::DVector4 tX1toX3step = (tangents[2] - tangents[0]) / (double)(y3 - y1);
	RCore::DVector4 tX2toX3step = (tangents[2] - tangents[1]) / (double)(y3 - y2);
	RCore::DVector3 nX1toX2step = (normals[1] - normals[0]) / (double)(y2 - y1);
	RCore::DVector3 nX1toX3step = (normals[2] - normals[0]) / (double)(y3 - y1);
	RCore::DVector3 nX2toX3step = (normals[2] - normals[1]) / (double)(y3 - y2);

	// process upper part of the triangle
	double iX1toX2 = x1;
	double iX1toX3 = x1;
	RCore::DVector3 pX1toX2 = pos[0];
	RCore::DVector3 pX1toX3 = pos[0];
	RCore::DVector3 nX1toX2 = normals[0];
	RCore::DVector3 nX1toX3 = normals[0];
	RCore::DVector4 tX1toX2 = tangents[0];
	RCore::DVector4 tX1toX3 = tangents[0];
	int y;
	for (y = y1; y < y2; y++) {
		// calculate the start and end of the scanline
		int start, end;
		RCore::DVector3 startPos, endPos, startNormal, endNormal;
		RCore::DVector4 startTangent, endTangent;
		if ((int)iX1toX2 > (int)iX1toX3) {
			start = (int)iX1toX3;
			startPos = pX1toX3;
			startTangent = tX1toX3;
			startNormal = nX1toX3;
			end = (int)iX1toX2;
			endPos = pX1toX2;
			endTangent = tX1toX2;
			endNormal = nX1toX2;
		} else {
			start = (int)iX1toX2;
			startPos = pX1toX2;
			startTangent = tX1toX2;
			startNormal = nX1toX2;
			end = (int)iX1toX3;
			endPos = pX1toX3;
			endTangent = tX1toX3;
			endNormal = nX1toX3;
		}

		double edgeLength = end - start;
		if (edgeLength > 0) {
			// calculate the worldspace position step when we increase one texel in the normalmap
			RCore::DVector3 posStep = (endPos - startPos) / edgeLength;
			RCore::DVector3 nStep = (endNormal - startNormal) / edgeLength;
			RCore::DVector4 tStep = (endTangent - startTangent) / edgeLength;

			// if we are in height range of the normalmap
			if (y >= 0 && y < YRES) {
				// process all texels on the scanline
				for (int i = start; i <= end; i++) {
					// if we are inside width range in the normalmap
					if (i >= 0 && i < XRES) {
						//RCore::DVector3 startPosD(startPos.x, startPos.y, startPos.z);
						//RCore::DRay rayD(startPos + polyNormal * 1000.0, startPos - polyNormal * 1000.0f);
						RCore::DRay rayD(startPos - startNormal * 0.001f, startPos + startNormal * 1000.0);
						Sample(rayD, highDetail, normalMap, i, y, node, startNormal, startTangent, tangentSpace, postFilter);
					}

					// go to the next worldspace position on the scanline
					startPos += posStep;
					startNormal += nStep;
					startTangent += tStep;
				}
			}
		}

		// update interpolation process
		iX1toX2 += iX1toX2step;	pX1toX2 += pX1toX2step; nX1toX2 += nX1toX2step;	tX1toX2 += tX1toX2step;
		iX1toX3 += iX1toX3step;	pX1toX3 += pX1toX3step;	nX1toX3 += nX1toX3step;	tX1toX3 += tX1toX3step;
	}


	// process lower part of the triangle
	double iX2toX3 = x2;
	RCore::DVector3 pX2toX3 = pos[1];
	RCore::DVector3 nX2toX3 = normals[1];
	RCore::DVector4 tX2toX3 = tangents[1];
	for (y = y2; y <= y3; y++) {
		// calculate the start and the end of the scanline
		int start, end;
		RCore::DVector3 startPos, endPos, startNormal, endNormal;
		RCore::DVector4 startTangent, endTangent;
		if ((int)iX2toX3 > (int)iX1toX3) {
			start = (int)iX1toX3;
			startPos = pX1toX3;
			startNormal = nX1toX3;
			startTangent = tX1toX3;
			end = (int)iX2toX3;
			endPos = pX2toX3;
			endNormal = nX2toX3;
			endTangent = tX2toX3;
		} else {
			start = (int)iX2toX3;
			startPos = pX2toX3;
			startNormal = nX2toX3;
			startTangent = tX2toX3;
			end = (int)iX1toX3;
			endPos = pX1toX3;
			endNormal = nX1toX3;
			endTangent = tX1toX3;
		}

		double edgeLength = end - start;
		if (edgeLength > 0) {
			// calculate how much to step in worldspace when we step one texel
			RCore::DVector3 posStep = (endPos - startPos) / edgeLength;
			RCore::DVector3 nStep = (endNormal - startNormal) / edgeLength;
			RCore::DVector4 tStep = (endTangent - startTangent) / edgeLength;

			// if we are in height range of the normalmap image
			if (y >= 0 && y < YRES) {
				// sample the scanline
				for (int i = start; i <= end; i++) {
					// if we are in width range of the normalmap image
					if (i >= 0 && i < XRES) {
						//RCore::DVector3 startPosD(startPos.x, startPos.y, startPos.z);
						//RCore::DRay rayD(startPos + polyNormal * 1000.0, startPos - polyNormal * 1000.f);
						RCore::DRay rayD(startPos - startNormal * 0.001f, startPos + startNormal * 1000.0);
						Sample(rayD, highDetail, normalMap, i, y, node, startNormal, startTangent, tangentSpace, postFilter);
					}

					// next 3D position on the scanline
					startPos += posStep;
					startNormal += nStep;
					startTangent += tStep;
				}
			}
		}

		// update interpolation process
		iX2toX3 += iX2toX3step;	pX2toX3 += pX2toX3step; nX2toX3 += nX2toX3step; tX2toX3 += tX2toX3step;
		iX1toX3 += iX1toX3step;	pX1toX3 += pX1toX3step;	nX1toX3 += nX1toX3step;	tX1toX3 += tX1toX3step;
	}
}


void BasicNormalMapGenerator::Sample(const RCore::DRay& ray, Actor* highDetail, NormalMap* normalMap, const int x, const int y, Node* node, RCore::DVector3& normal, RCore::DVector4& tangent, bool tangentSpace, bool postFilter) {
	// if this pixel is already in use, we can skip it
	if (normalMap->IsFree(x, y) == false)
		return;

	// find the same node in the high detail model
	Node* highNode = highDetail->GetNodeByID(node->GetID());
	assert(highNode);	// if not found, it means the actors are not the same

	RCore::DVector3 pos, pos1, pos2, normal1, normal2;
	double baryU = 0, baryV = 0, baryU1, baryU2, baryV1, baryV2;
	double dist1 = FLT_MAX;
	double dist2 = FLT_MAX;
	int startIndex = 0, startIndex1, startIndex2;
	bool normalFound = true;

	RCore::DRay ray2(ray.GetOrigin(), ray.GetOrigin() - ray.GetDirection() * 1000.0);
	normal1 = ray.GetDirection();
	normal2 = ray2.GetDirection();

	if (CalcIntersection(ray, highNode, &pos1, &baryU1, &baryV1, &startIndex1)) {
		dist1 = (pos1 - ray.GetOrigin()).SquareLength();

		// calculate the normal at the intersection point
		Mesh* mesh = highNode->GetMesh(0).get();
		RCore::Vector3* normals = mesh->GetNormals();
		int* indices = mesh->GetIndices();
		RCore::Vector3 normalA = normals[indices[startIndex1 + 0]];
		RCore::Vector3 normalB = normals[indices[startIndex1 + 1]];
		RCore::Vector3 normalC = normals[indices[startIndex1 + 2]];

		RCore::DVector3 dNormalA(normalA.x, normalA.y, normalA.z);
		RCore::DVector3 dNormalB(normalB.x, normalB.y, normalB.z);
		RCore::DVector3 dNormalC(normalC.x, normalC.y, normalC.z);

		normal1 = RCore::BarycentricInterpolate<RCore::DVector3>(baryU1, baryV1, dNormalA, dNormalB, dNormalC);
		normal1.Normalize();

		if (CalcIntersection(ray2, highNode, &pos2, &baryU2, &baryV2, &startIndex2)) {
			dist2 = (pos2 - ray2.GetOrigin()).SquareLength();

			// calculate the normal at the intersection point
			Mesh* mesh = highNode->GetMesh(0).get();
			RCore::Vector3* normals = mesh->GetNormals();
			int* indices = mesh->GetIndices();
			RCore::Vector3 normalA = normals[indices[startIndex2 + 0]];
			RCore::Vector3 normalB = normals[indices[startIndex2 + 1]];
			RCore::Vector3 normalC = normals[indices[startIndex2 + 2]];

			RCore::DVector3 dNormalA(normalA.x, normalA.y, normalA.z);
			RCore::DVector3 dNormalB(normalB.x, normalB.y, normalB.z);
			RCore::DVector3 dNormalC(normalC.x, normalC.y, normalC.z);

			normal2 = RCore::BarycentricInterpolate<RCore::DVector3>(baryU2, baryV2, dNormalA, dNormalB, dNormalC);
			normal2.Normalize();

			double dot1 = normal.Dot(normal1);
			double dot2 = normal.Dot(normal2);

			if (dot1 < 0 && dot2 > 0) {
				baryU = baryU2;
				baryV = baryV2;
				startIndex = startIndex2;
			} else {
				if ((dot1 >= 0 && dot2 >= 0) || (dot1 <= 0 && dot2 <= 0)) {
					if (dist1 < dist2) {
						baryU = baryU1;
						baryV = baryV1;
						startIndex = startIndex1;
					} else {
						baryU = baryU2;
						baryV = baryV2;
						startIndex = startIndex2;
					}
				} else {
					// dot2 is negative, dot1 is positive
					if (dot2 <= 0 && dot1 >= 0) {
						baryU = baryU1;
						baryV = baryV1;
						startIndex = startIndex1;
					}
				}
			}
		} else {
			baryU = baryU1;
			baryV = baryV1;
			startIndex = startIndex1;
		}
	} else {
		if (CalcIntersection(ray2, highNode, &pos1, &baryU1, &baryV1, &startIndex1)) {
			baryU = baryU1;
			baryV = baryV1;
			startIndex = startIndex1;
		} else {
			normalFound = false;
		}
	}

	// if the normal has been found
	if (normalFound) {
		// get the mesh of the high detail node
		Mesh* mesh = highNode->GetMesh(0).get();
		assert(mesh);

		// get the original normal data (in object space)
		RCore::Vector3* normals = mesh->GetNormals();

		// get the face indices
		int* indices = mesh->GetIndices();

		// get the normals of the vertices of the face
		RCore::Vector3 normalA = normals[indices[startIndex + 0]];
		RCore::Vector3 normalB = normals[indices[startIndex + 1]];
		RCore::Vector3 normalC = normals[indices[startIndex + 2]];

		RCore::DVector3 dNormalA(normalA.x, normalA.y, normalA.z);
		RCore::DVector3 dNormalB(normalB.x, normalB.y, normalB.z);
		RCore::DVector3 dNormalC(normalC.x, normalC.y, normalC.z);

		// calculate the interpolated normal
		RCore::DVector3 interpolatedNormal = RCore::BarycentricInterpolate<RCore::DVector3>(baryU, baryV, dNormalA, dNormalB, dNormalC);

		RCore::DVector3 outNormal;
		if (tangentSpace) {
			RCore::DVector3 finalTangent(tangent.x, tangent.y, tangent.z);

			// calculate the binormal
			RCore::DVector3 biNormal = tangent.w * normal.Cross(finalTangent);

			// transform the interpolated normal into tangent space 
			outNormal.x = interpolatedNormal.Dot(finalTangent);
			outNormal.y = interpolatedNormal.Dot(biNormal);
			outNormal.z = interpolatedNormal.Dot(normal);
			outNormal.Normalize();
		} else
			outNormal = interpolatedNormal.Normalize();

		// write the encoded normal into the normalmap
		normalMap->EncodeNormal(x, y, outNormal);
/*
		if (tangentSpace)	// heightmap output test
		{
			float dist;
			float sign;
			if (startIndex == startIndex1)
			{
				dist = dist1;
				sign = 1;
			}
			else
			{
				dist = dist2;
				sign = -1;
			}

			float length = std::sqrt(dist) * sign;

			int value = length * 255;

			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].r = value;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].g = value;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].b = value;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].isFree = false;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].x = x;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].y = y;
			normalMap->GetPixelData()[x+y*normalMap->GetWidth()].dist = 0;
		}*/
	} else // the normal has not been found, so write the interpolated lowpoly normal
	{
		RCore::DVector3 outNormal;

		if (tangentSpace) {
			RCore::DVector3 finalTangent(tangent.x, tangent.y, tangent.z);

			// calculate the binormal
			RCore::DVector3 biNormal = tangent.w * normal.Cross(finalTangent);

			// transform the interpolated normal into tangent space 
			outNormal.x = normal.Dot(finalTangent);
			outNormal.y = normal.Dot(biNormal);
			outNormal.z = normal.Dot(normal);
			outNormal.Normalize();
		} else
			outNormal = normal;

		// write the encoded normal into the normalmap
		normalMap->EncodeNormal(x, y, outNormal);
	}
}



bool BasicNormalMapGenerator::CalcIntersection(const RCore::DRay& ray, Node* node, RCore::DVector3* outPos, double* outBaryU, double* outBaryV, int* outStartIndex) {
	Mesh* mesh = node->GetMesh(0).get();

	RCore::Vector3*	positions = mesh->GetPositions();
	int*			indices = mesh->GetIndices();
	RCore::DVector3	newOrigin = ray.GetOrigin();
	RCore::DVector3	newDest = ray.GetDest();

	double			closestDist = FLT_MAX;
	bool			hasIntersected = false;
	int				closestStartIndex = 0;
	RCore::DVector3	intersectionPoint;
	RCore::DVector3	closestIntersect;
	double			dist, baryU, baryV, closestBaryU = 0, closestBaryV = 0;

	// loop through all triangles
	const int numIndices = mesh->GetNumIndices();
	for (int i = 0; i < numIndices; i += 3) {
		RCore::Vector3 fPosA = positions[indices[i]];
		RCore::Vector3 fPosB = positions[indices[i + 1]];
		RCore::Vector3 fPosC = positions[indices[i + 2]];
		RCore::DVector3 posA(fPosA.x, fPosA.y, fPosA.z);
		RCore::DVector3 posB(fPosB.x, fPosB.y, fPosB.z);
		RCore::DVector3 posC(fPosC.x, fPosC.y, fPosC.z);

		// test the ray with the triangle (in object space)
		if (ray.Intersects(posA, posB, posC, &intersectionPoint, &baryU, &baryV)) {
			// calculate the squared distance between the intersection point and the ray origin
			dist = (intersectionPoint - newOrigin).SquareLength();

			// if it is the closest intersection point until now, record it as closest intersection
			if (dist < closestDist) {
				closestDist = dist;
				closestIntersect = intersectionPoint;
				hasIntersected = true;
				closestStartIndex = i;
				closestBaryU = baryU;
				closestBaryV = baryV;
			}
		}
	}

	// store the closest intersection point (in world space)
	if (hasIntersected) {
		if (outPos != nullptr)
			*outPos = closestIntersect;

		if (outStartIndex != nullptr)
			*outStartIndex = closestStartIndex;

		if (outBaryU)
			*outBaryU = closestBaryU;

		if (outBaryV)
			*outBaryV = closestBaryV;
	}

	// return the result
	return hasIntersected;
}


void BasicNormalMapGenerator::SetResChecker(ImageResChecker* checker) {
	if (mResChecker)
		delete mResChecker;

	mResChecker = checker;
}

} // namespace
