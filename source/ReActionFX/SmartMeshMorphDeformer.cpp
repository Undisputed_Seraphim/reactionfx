#include "SmartMeshMorphDeformer.h"
#include "MeshExpressionPart.h"
#include "Mesh.h"
#include "Node.h"
#include "SubMesh.h"
#include "Actor.h"

using namespace RCore;

namespace ReActionFX {

// clone this class
MeshDeformer* SmartMeshMorphDeformer::Clone(Mesh* mesh, Actor* actor) {
	// create the new cloned deformer
	SmartMeshMorphDeformer* result = new SmartMeshMorphDeformer(mMeshExpressionPart, mDeformDataNr, mesh);

	// return the result
	return result;
}


// the main method where all calculations are done
void SmartMeshMorphDeformer::Update(Actor* actor, Node* node, const double timeDelta) {
	// get the deform data and number of vertices to deform
	MeshExpressionPart::DeformData* deformData = mMeshExpressionPart->mDeformDatas[mDeformDataNr];
	const int numDeformVerts = deformData->mNumVerts;

	// this mesh deformer can't work on this mesh, because the deformdata number of vertices is bigger than the 
	// number of vertices inside this mesh!
	// and that would make it crash, which isn't what we want
	if (numDeformVerts > mMesh->GetNumVertices())
		return;

	// get the weight value and convert it to a range based weight value
	const float weight = mMeshExpressionPart->CalcRangedWeight(mMeshExpressionPart->GetWeight());

	// nothing to do when the weight is too small
	bool nearZero = InRange<float>(weight, -std::numeric_limits<MReal>::epsilon(), std::numeric_limits<MReal>::epsilon());

	// we are near zero, and the previous frame as well, so we can return
	if (nearZero && mLastNearZero)
		return;

	// update the flag
	if (nearZero)
		mLastNearZero = true;
	else
		mLastNearZero = false;	// we moved away from zero influence

	// some vars we need
	Vector3*	positions = mMesh->GetPositions();
	Vector3*	normals = mMesh->GetNormals();
	int*		vertexNumbers = deformData->mVertexNumbers;

	Compressed16BitVector3*	deltas = deformData->mDeltas;
	Compressed8BitVector3*	deltaNormals = deformData->mDeltaNormals;
	const float				minValue = deformData->mMinValue;
	const float				maxValue = deformData->mMaxValue;

	// process all touched vertices
	for (int v = 0; v < numDeformVerts; v++) {
		int vtxNr = vertexNumbers[v];
		positions[vtxNr] += deltas[v].ToVector3(minValue, maxValue) * weight;
		normals[vtxNr] += deltaNormals[v].ToVector3(-1.0f, 1.0f) * weight;
	}
}

} // namespace