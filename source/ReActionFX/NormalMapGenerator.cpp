#include "NormalMapGenerator.h"

using namespace RCore;

namespace ReActionFX {

// get rid of normal maps
void NormalMapGenerator::ReleaseNormalMaps() {
	for (auto normalMap : mNormalMaps)
		delete normalMap;
	mNormalMaps.clear();
	mNormalMaps.shrink_to_fit();
}

} // namespace