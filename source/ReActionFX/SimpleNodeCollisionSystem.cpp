#include "SimpleNodeCollisionSystem.h"
#include "Actor.h"
#include "Node.h"
#include "Mesh.h"
#include "SimpleMesh.h"

using namespace RCore;

namespace ReActionFX {

// returns a true if there is a collision, otherwise false
bool SimpleNodeCollisionSystem::IntersectsCollisionMesh(const int lodLevel, const Ray& ray) {
	// make sure the LOD level exists for this node
	assert(lodLevel < mNode->GetNumCollisionMeshes());

	// get the direct pointer to the mesh, from the smartpointer
	Mesh* mesh = mNode->GetCollisionMesh(lodLevel).get();

	// if there is no mesh, there cannot be an intersection
	if (mesh == nullptr)
		return false;

	// check intersection with the mesh
	if (mesh->Intersects(ray, *mNode))
		return true;

	// no intersection occured or the node isn't vaid
	return false;
}

// returns a true if there is a collision, otherwise false
bool SimpleNodeCollisionSystem::IntersectsCollisionMesh(const int lodLevel, const Ray& ray, Vector3* outIntersect, float* outBaryU, float* outBaryV, int* outStartIndex) {
	// make sure the LOD level exists for this node
	assert(lodLevel < mNode->GetNumCollisionMeshes());

	// get the direct pointer to the mesh, from the smartpointer
	Mesh* mesh = mNode->GetCollisionMesh(lodLevel).get();

	// if there is no mesh, there cannot be an intersection
	if (mesh == nullptr)
		return false;

	// check for an intersection with the mesh
	if (mesh->Intersects(ray, *mNode, outIntersect, outBaryU, outBaryV, outStartIndex))
		return true;

	// no intersection found or the node isn't vaid
	return false;
}

// checks for collision with the real meshes
bool SimpleNodeCollisionSystem::IntersectsMesh(const int lodLevel, const Ray& ray) {
	// make sure the LOD level exists for this node
	assert(lodLevel < mNode->GetNumMeshes());

	// get the direct pointer to the mesh, from the smartpointer
	Mesh* mesh = mNode->GetMesh(lodLevel).get();

	// if there is no mesh, there cannot be an intersection
	if (mesh == nullptr)
		return false;

	// check intersection with the mesh
	if (mesh->Intersects(ray, *mNode))
		return true;

	// no intersection occured or the node isn't vaid
	return false;
}

// checks for collision with the real meshes
bool SimpleNodeCollisionSystem::IntersectsMesh(const int lodLevel, const Ray& ray, Vector3* outIntersect, float* outBaryU, float* outBaryV, int* outStartIndex) {
	// make sure the LOD level exists for this node
	assert(lodLevel < mNode->GetNumMeshes());

	// get the direct pointer to the mesh, from the smartpointer
	Mesh* mesh = mNode->GetMesh(lodLevel).get();

	// if there is no mesh, there cannot be an intersection
	if (mesh == nullptr)
		return false;

	// check for an intersection with the mesh
	if (mesh->Intersects(ray, *mNode, outIntersect, outBaryU, outBaryV, outStartIndex))
		return true;

	// no intersection found or the node isn't vaid
	return false;
}

} // namespace