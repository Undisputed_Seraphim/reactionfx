#include "HwShaderBuffer.h"
#include "Mesh.h"
#include "Node.h"
#include "SkinningInfoVertexAttributeLayer.h"

namespace ReActionFX {

HwShaderBuffer::~HwShaderBuffer() {
	mVertices.clear();
	mIndices.clear();
	mPrimitives.clear();
	mBones.clear();
}

// returns true if the buffer can handle a face with the specified properties, otherwise false is returned
bool HwShaderBuffer::CanHandleFace(const int numInfluences, const std::set<Node*>& faceBones, const int materialNumber, const int maxBonesPerBuffer, const bool allowDifferentWeights) {
	// if the face hasn't got the same material and if the index buffer isn't full yet
	if (mMaterialNumber != materialNumber)
		return false;

	// check the weights / influences
	if (!allowDifferentWeights && numInfluences != mNumInfluences)
		return false;

	// non-skinned objects must be in a separate buffer
	if ((mNumInfluences == 0) && (numInfluences != 0))
		return false;

	// keep non-skinned objects out of this buffer
	if ((numInfluences == 0) && (mNumInfluences != 0) && allowDifferentWeights)
		return false;

	// calculate how many new bones would be added to this buffer
	int numNewBones = 0;
	for (auto faceBone : faceBones) {
		if (std::find(mBones.begin(), mBones.end(), faceBone) == mBones.end())
			numNewBones++;
	}

	// check if that would reach the maximum number of bones or not, if so, this buffer cannot handle this face
	if (mBones.size() + numNewBones > maxBonesPerBuffer)
		return false;

	// yeah, this buffer can handle it :)
	return true;
}


// add a given mesh face to the buffer
void HwShaderBuffer::AddFace(Mesh* mesh, const int startIndex) {
	// get some pointers
	int* indices = mesh->GetIndices();
	int* orgVerts = mesh->GetOrgVerts();

	// try to locate the skinning attribute information
	SkinningInfoVertexAttributeLayer* skinningLayer = (SkinningInfoVertexAttributeLayer*)mesh->FindSharedVertexAttributeLayer(SkinningInfoVertexAttributeLayer::TYPE_ID);

	if (mIndices.size() % 100 == 0)
		mIndices.reserve(mIndices.size() + 100);

	if (mVertices.size() % 100 == 0)
		mVertices.reserve(mVertices.size() + 100);

	if (mBones.size() % 20 == 0)
		mBones.reserve(mBones.size() + 20);

	// for all vertices of the face
	for (int i = 0; i < 3; ++i) {
		int vertexNumber = indices[startIndex + i];

		// find out if this vertex is already added
		bool vertexFound = false;
		for (int v = mStartVertex; v < mVertices.size() && !vertexFound; v++) {
			if (mVertices[v].mVertexNr == vertexNumber) {
				mIndices.push_back(v);
				vertexFound = true;
			}
		}

		// skip to the next vertex :)
		if (vertexFound) continue;

		// create the vertex
		HwShaderVertex vtx(mesh, vertexNumber);

		// if there is skinning info
		if (skinningLayer != NULL) {
			// get the original vertex number
			int orgVertex = orgVerts[vertexNumber];

			// add the influences
			const auto numInfluences = skinningLayer->GetNumInfluences(orgVertex);
			vtx.mInfluences.reserve(mNumInfluences);
			for (int s = 0; s < numInfluences; s++) {
				// get the weight and bone
				float weight = skinningLayer->GetInfluence(orgVertex, s).GetWeight();
				Node* bone = skinningLayer->GetInfluence(orgVertex, s).GetBone();

				//RCore::LOG("bone = %s", bone->GetNamePtr());

				// now find the bone number
				auto boneNumber = std::find(mBones.begin(), mBones.end(), bone) - mBones.begin();

				// if it doesn't exist yet, we need to add it
				if (boneNumber == -1) {
					mBones.push_back(bone);
					boneNumber = mBones.size() - 1;
				}

				// add the influence to the vertex
				vtx.AddInfluence(HwShaderInfluence(boneNumber, weight));
			}

			// add NULL influences, which have no influence, but in order to give the vertex the same amount of influences
			// as the buffer specifies. This saves some potential problems later on.
			int numExtraInfluences = mNumInfluences - (int)vtx.GetNumInfluences();
			for (int a = 0; a < numExtraInfluences; a++)
				vtx.AddInfluence(HwShaderInfluence(0, 0));
		}

		// add the vertex to the buffer
		mVertices.push_back(vtx);

		// add the index to the indices
		mIndices.push_back((int)mVertices.size() - 1);
	}
}


void HwShaderBuffer::Attach(const HwShaderBuffer& buffer) {
	// get the vertex and index offsets
	int vertexOffset = (int)mVertices.size();
	int indexOffset = (int)mIndices.size();

	// attach the vertices
//	mVertices += buffer.mVertices;
	mVertices.insert(mVertices.end(), buffer.mVertices.begin(), buffer.mVertices.end());

	// attach the indices and relink their values
	auto bufIndices = buffer.GetNumIndices();
	mIndices.reserve(mIndices.size() + bufIndices);
	for (int i = 0; i < bufIndices; i++) {
		int index = vertexOffset + buffer.GetIndex(i);
		mIndices.push_back(index);
	}

	// attach the bones
	auto numBones = buffer.GetNumBones();
	mBones.reserve(mBones.size() + numBones);
	for (int i = 0; i < numBones; i++) {
		// check if we already have this bone in our buffer
		int boneNr = std::find(mBones.begin(), mBones.end(), buffer.GetBone(i)) - mBones.begin();

		// add the bone if we don't have it yet
		if (boneNr == -1)
			mBones.push_back(buffer.GetBone(i));
	}

	// relink the bone numbers inside the vertex weights
	if (mNumInfluences > 0) {
		// process all new added vertices
		for (int v = vertexOffset; v < mVertices.size(); v++) {
			HwShaderVertex& vertex = mVertices[v];

			// process all vertex influences
			for (int i = 0; i < vertex.GetNumInfluences(); i++) {
				// get the original bone number and a pointer to the original bone
				int		boneNr = vertex.GetInfluence(i).GetBoneNumber();
				Node*	orgBone = buffer.GetBone(boneNr);

				// find the new bone number, and adjust it in the vertex
				int newBoneNr = std::find(mBones.begin(), mBones.end(), orgBone) - mBones.begin();
				vertex.GetInfluence(i).mBoneNr = newBoneNr;
			}
		}
	}

	// attach the primitives
	auto numPrimitives = buffer.GetNumPrimitives();
	mPrimitives.reserve(mPrimitives.size() + numPrimitives);
	for (int i = 0; i < numPrimitives; i++) {
		mPrimitives.push_back(buffer.GetPrimitive(i));
		mPrimitives.back().mStartIndex += indexOffset;
		mPrimitives.back().mStartVertex += vertexOffset;
	}
}


// optimize the primitives
void HwShaderBuffer::OptimizePrimitives() {
	// first sort the primitives on material
	std::sort(mPrimitives.begin(), mPrimitives.end());
/*
	int before = mPrimitives.size();

	//RCore::LOG("---------------");
	int lastPrim = -1;
	int after = 0;
	for (int i=0; i<mPrimitives.size(); i++)
	{
		if (lastPrim != mPrimitives[i].GetMaterialNr())
		{
			after++;
			lastPrim = mPrimitives[i].GetMaterialNr();
		}
		//RCore::LOG("#%d = %d", i, mPrimitives[i].GetMaterialNr());
	}

	if (before != after)
		RCore::LOG("Before = %d     After = %d", before, after);
*/

}


void HwShaderBuffer::StartPrimitive(Node* node, const int matNr) {
	mStartIndex = (int)mIndices.size();
	mStartNode = node;
	mStartVertex = (int)mVertices.size();
	mStartMatNr = matNr;
}


void HwShaderBuffer::EndPrimitive() {
	// if faces have been added
	if (mStartIndex != mIndices.size()) {
		// create a new primitive
		int numTriangles = ((int)mIndices.size() - mStartIndex) / 3;
		mPrimitives.push_back(HwShaderPrimitive(mStartIndex, numTriangles, mStartNode, mStartMatNr, 0, (int)mVertices.size()));
	}
}


} // namespace