#pragma once

#include <vector>

#include "RCore.h"

#include "JacobianIKData.h"

namespace ReActionFX {

// forward declarations
class Actor;

/**
 * The Jacobian Pseudo Inverse IK solver.
 * This solver is not really realtime, but more interactive.
 * It is a lot slower than the CCD solver and will mostly be used for non-game applications.
 */
class JacobianIKSolver {
public:
	/**
	 * Constructor.
	 * @param ikData Pointer to the jacobian ik data to use.
	 **/
	JacobianIKSolver(JacobianIKData * ikData)
		: mIKData(ikData), mNumFunc(0), mNumFuncC(nullptr), mFuncs(nullptr), mVars(nullptr), mTolerance(0.05f), mMaxIterations(200) {}

	/**
	 * Destructor.
	 */
	~JacobianIKSolver() {}

	/**
	 * Set the maximun difference between constraints functions and results.
	 * @param tolerance The tolerance value.
	 */
	void SetTolerance(const float tolerance) {
		mTolerance = tolerance;
	}

	/**
	* Calculate the ik solution and store it in the given array.
	* @param solution Vector of floats in which the solution gets stored.
	*/
	void CalcIK(std::vector<float>& solution);

private:
	/**
	 * Solve iterations for current state.
	 **/
	void Solve();

	/**
	 * Obtain Jacobian matrix for constraints functions from DOF values in mxSol.
	 * @param jacob
	 **/
	void Jacobian(RCore::NMatrix& jacob);

	/**
	 * Apply actual DOF values over the actor instance.
	 */
	void SetValues();

	/**
	 * Evaluate which DOFs are variables in solving.
	 **/
	void CVars();

	/**
	 * Evaluate the number of functions for each constraint.
	 **/
	void CNumFunc();

	/**
	 * Construct the member function array.
	 **/
	void CFuncs();

	/**
	 * Evaluate the constraint functions, there use to be positions.
	 **/
	void CPositions();

	/**
	 * Evaluate which variables are over limits.
	 **/
	std::vector<int> TestLimits(RCore::NMatrix& matrix);

	RCore::NMatrix		mxSolP;				/* Last solution values evaluated by constraint functions */
	RCore::NMatrix		mxSol;				/* Temporal DOFs values for desired values of constraints */
	std::vector<float>	mIPosition;			/* Starting values for the constraints functions */
	std::vector<float>	mFPosition;			/* Desired values for constraints functions */
	std::vector<float>	mSolution;			/* Solution for all the DOFs */
	JacobianIKData*		mIKData;			/* Group of constraints to solve */
	float				mTolerance;			/* Maximun allowed difference between desired values and obtained ones */
	int					mNumVar;			/* Number of DOFs IKSolver can adjust. It isn't the number of DOF of the actor. Not all the DOFs affect the kinematic.*/
	int					mNumDOFs;			/* Number of DOFs of the actor */
	int					mNumFunc;			/* Number of functions the constraints are applied through */
	int					mMaxIterations;		/* The maximum number of iterations (tradeoff between speed and accuracy) [default value=200]. */
	int*				mNumFuncC;			/* Number of functions introduced for each constraint */
	int*				mFuncs;				/* Constraint which introduces each function */
	bool*				mVars;				/* mVars[i] = true if the i DOF is implied in IK */
};

} // namespace