#include "NodeLimitAttribute.h"

namespace ReActionFX {

NodeAttribute* NodeLimitAttribute::Clone() {
	NodeLimitAttribute* clone = new NodeLimitAttribute();

	// copy limits
	clone->mTranslationMin = mTranslationMin;
	clone->mTranslationMax = mTranslationMax;
	clone->mRotationMin = mRotationMin;
	clone->mRotationMax = mRotationMax;
	clone->mScaleMin = mScaleMin;
	clone->mScaleMax = mScaleMax;
	clone->mLimitFlags = mLimitFlags;

	// return the cloned limit
	return clone;
}


void NodeLimitAttribute::EnableLimit(const ELimitType limitType, const bool flag) {
	if (flag)
		mLimitFlags |= limitType;
	else
		mLimitFlags ^= limitType;
}


void NodeLimitAttribute::ToggleLimit(const ELimitType limitType) {
	bool enable = !IsLimited(limitType);
	EnableLimit(limitType, enable);
}

} // namespace