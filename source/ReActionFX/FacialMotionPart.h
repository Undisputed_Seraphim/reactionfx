#pragma once
#include "RCore.h"

#include "KeyTrack.h"

namespace ReActionFX {

/**
 * The facial motion part, which is an individual part of a bigger facial motion.
 * Each facial motion part works on an expression part, which is a part of the face, which can be animated, such as
 * eye blinking. Each facial motion part contains a keytrack, which represents the animation data for the given expression part.
 * Next to that the facial motion part contains a unique ID. All expression parts having the same ID will need to have
 * this motion part to be applied on them. If you want to manually link an expression part to a facial motion part, you
 * can pass as parameter to the constructor of the motion part the value returned by ExpressionPart::GetID().
 */
class FacialMotionPart {
public:
	/**
	 * The constructor.
	 * @param id The ID of the expression part (ExpressionPart) where you want to link the keyframe data to.
	 */
	FacialMotionPart(const int id) : mID(id) {}

	/**
	 * The destructor.
	 */
	~FacialMotionPart() {}

	/**
	 * Set the new ID of the motion part, which basically relinks it to another expression part.
	 * @param id The new ID.
	 */
	void SetID(const int id) { mID = id; }

   /**
	* Get the ID of this motion part.
	* Expression parts having the same ID will need this motion part to be applied to them.
	* @result The ID of this motion part.
	*/
	int GetID() const { return mID; }

   /**
	* Get a pointer to the keytrack, which contains the animation data.
	* @result A reference to the keytrack.
	*/
	KeyTrack<float, RCore::Compressed8BitFloat>& GetKeyTrack() { return mKeyTrack; }

private:
	KeyTrack<float, RCore::Compressed8BitFloat>	mKeyTrack;	/* The keytrack containing the animation data for a given expression part. */
	int	mID;		/* The ID of the ExpressionPart where this keytrack should be used on. */
};

} // namespace