#include "JacobianIKRelativePoint.h"
#include "JacobianIKData.h"

using namespace RCore;

namespace ReActionFX {

void JacobianIKRelativePoint::GetValues(std::vector<float>& values) {
	// the required value for the constraint is the node position plus the defined displacement
	values.push_back(mPosition.x + mDisplacement.x);
	values.push_back(mPosition.y + mDisplacement.y); // ahm yes ... (Benny)
	values.push_back(mPosition.z + mDisplacement.z);
}


void JacobianIKRelativePoint::GetResults(std::vector<float>& results) {
	// retrieve the node's world position
	Vector3 position = mNode->GetWorldPos();

	// the constraint result is the node position
	results.push_back(position.x);
	results.push_back(position.y); // ahm yes ... (Benny)
	results.push_back(position.z);
}


void JacobianIKRelativePoint::CalcGradient(NMatrix& matrix) {
	// retrieve the number of DOFs from the ik data
	const int numDOFs = mIKData->GetNumDOF();

	matrix.SetSize(3, numDOFs);

	// the derivative of the result is the derivative of the position
	// the gradient is the derivative of each coordinate (in each column) respect to each DOF (in each row)
	for (int i = 0; i < numDOFs; i++) {
		Vector3 v = mIKData->CalcPosDer(mNode, i);
		matrix(0, i) = v.x;
		matrix(1, i) = v.y;
		matrix(2, i) = v.z;
	}
}


int JacobianIKRelativePoint::GetNumFunc() const {
	// one function for each coordinate
	return 3;
}

} // namespace