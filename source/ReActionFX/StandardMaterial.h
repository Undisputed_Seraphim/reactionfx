﻿#pragma once

#include <algorithm>
#include "RCore.h"
#include "Material.h"

namespace ReActionFX {

/**
 * The material layer class.
 * A material layer is a texture layer in a material.
 * Examples of layers can be, bumpmaps, diffuse maps, opacity maps, specular maps, etc.
 */
class StandardMaterialLayer {
public:
	/**
	 * Standard supported layer types.
	 */
	enum {
		LAYERTYPE_UNKNOWN = 0,		/* An unknown layer type. */
		LAYERTYPE_AMBIENT = 1,		/* Ambient layer. */
		LAYERTYPE_DIFFUSE = 2,		/* Diffuse layer. */
		LAYERTYPE_SPECULAR = 3,		/* Specular layer. */
		LAYERTYPE_OPACITY = 4,		/* Opacity layer. */
		LAYERTYPE_BUMP = 5,			/* Bumpmap layer. */
		LAYERTYPE_SELFILLUM = 6,	/* Self illumination layer. */
		LAYERTYPE_SHINE = 7,		/* Shininess layer. */
		LAYERTYPE_SHINESTRENGTH = 8,/* Shininess strength layer. */
		LAYERTYPE_FILTERCOLOR = 9,	/* Filter color layer. */
		LAYERTYPE_REFLECT = 10,		/* Reflection layer. */
		LAYERTYPE_REFRACT = 11,		/* Refraction layer. */
		LAYERTYPE_ENVIRONMENT = 12	/* Environment map layer. */
	};

	/**
	 * Default constructor.
	 * The layer type will be set to LAYERTYPE_UNKNOWN, the amount will be set to 1, and the
	 * filename will stay uninitialized (empty).
	 */
	StandardMaterialLayer() : mLayerTypeID(LAYERTYPE_UNKNOWN), mAmount(1.0f), mUOffset(0.0f), mVOffset(0.0f), mUTiling(1.0f), mVTiling(1.0f), mRotationRadians(0.0f) {}

	/**
	 * Extended constructor.
	 * @param layerType The layer type.
	 * @param filename The filename, must be without extension or path.
	 * @param amount The amount value, which identifies how intense or active the layer is. The normal range is [0..1].
	 */
	StandardMaterialLayer(const int layerType, const std::string & filename, const float amount = 1.0f) : mLayerTypeID(layerType),
		mFilename(filename), mAmount(amount), mUOffset(0.0f), mVOffset(0.0f), mUTiling(1.0f), mVTiling(1.0f), mRotationRadians(0.0f) {}

	/**
	 * The destructor.
	 */
	virtual ~StandardMaterialLayer() {}

	/**
	 * Get the filename of the layer.
	 * @result The filename of the texture of the layer, without path or extension.
	 */
	const std::string& GetFilename() const {
		return mFilename;
	}

	/**
	 * Set the filename of the texture of the layer.
	 * @param filename The filename, must be without extension or path.
	 */
	void SetFilename(const std::string &filename) {
		mFilename = filename;
	}

	/**
	 * Set the amount value.
	 * The amount identifies how active or intense the layer is.
	 * The normal range of the value is [0..1].
	 */
	void SetAmount(const float amount) {
		mAmount = amount;
	}

	/**
	 * Get the amount.
	 * The amount identifies how active or intense the layer is.
	 * The normal range of the value is [0..1].
	 */
	float GetAmount() const {
		return mAmount;
	}

	/**
	 * Get the layer type.
	 * @result The layer type ID.
	 */
	int GetType() const {
		return mLayerTypeID;
	}

	/**
	 * Set the layer type.
	 * @param layerType The layer type.
	 */
	void SetType(const int typeID) {
		mLayerTypeID = typeID;
	}

	/**
	 * Creates an exact copy of this layer.
	 * @result A pointer to an exact copy of the layer.
	 */
	virtual StandardMaterialLayer* Clone();

	/**
	 * Set the u offset (horizontal texture shift).
	 * @param The u offset.
	 */
	void SetUOffset(const float uOffset) {
		mUOffset = uOffset;
	}

	/**
	 * Set the v offset (vertical texture shift).
	 * @param The v offset.
	 */
	void SetVOffset(const float vOffset) {
		mVOffset = vOffset;
	}

	/**
	 * Set the horizontal tiling factor.
	 * @param The horizontal tiling factor.
	 */
	void SetUTiling(const float uTiling) {
		mUTiling = uTiling;
	}

	/**
	 * Set the vertical tiling factor.
	 * @param The vertical tiling factor.
	 */
	void SetVTiling(const float vTiling) {
		mVTiling = vTiling;
	}

	/**
	 * Set the texture rotation.
	 * @param The texture rotation in radians.
	 */
	void SetRotationRadians(const float rotationRadians) {
		mRotationRadians = rotationRadians;
	}

	/**
	 * Get the u offset (horizontal texture shift).
	 * @result The u offset.
	 */
	float GetUOffset() const {
		return mUOffset;
	}

	/**
	 * Get the v offset (vertical texture shift).
	 * @result The v offset.
	 */
	float GetVOffset() const {
		return mVOffset;
	}

	/**
	 * Get the horizontal tiling factor.
	 * @result The horizontal tiling factor.
	 */
	float GetUTiling() const {
		return mUTiling;
	}

	/**
	 * Get the vertical tiling factor.
	 * @result The vertical tiling factor.
	 */
	float GetVTiling() const {
		return mVTiling;
	}

	/**
	 * Get the texture rotation.
	 * @result The texture rotation in radians.
	 */
	float GetRotationRadians() const {
		return mRotationRadians;
	}


protected:
	std::string 	mFilename;			/* The filename of the texture, without extension or path. */
	int				mLayerTypeID;		/* The layer type. See the enum for some possibilities. */
	float			mAmount;			/* The amount value, between 0 and 1. This can for example represent how intens the layer is. */
	float			mUOffset;			/* u offset (horizontal texture shift). */
	float			mVOffset;			/* v offset (vertical texture shift). */
	float			mUTiling;			/* Horizontal tiling factor. */
	float			mVTiling;			/* Vertical tiling factor. */
	float			mRotationRadians;	/* Texture rotation in radians. */

}; // class


/**
 * The material class.
 * This class describes the material properties of renderable surfaces.
 * Every material can have a set of material layers, which contain the texture information.
 */
class StandardMaterial : public Material {
public:
	enum { TYPE_ID = 0x00000001 };

	enum class TransparencyType : char {
		ADDITIVE = 'S',
		FILTER = 'F',
		NONE = 'N',
		SUBTRACTIVE = 'S',
		UNKNOWN = 'U'
	};

	/**
	 * Constructor.
	 * @param name The name of the material.
	 */
	StandardMaterial(const std::string & name);

	/**
	 * Destructor.
	 */
	virtual ~StandardMaterial() {
		RemoveAllLayers();
	}

	/**
	 * Set the ambient color.
	 * @param The ambient color.
	 */
	void SetAmbient(const RCore::RGBAColor& ambient) {
		mAmbient = ambient;
	}

	/**
	 * Set the diffuse color.
	 * @param The diffuse color.
	 */
	void SetDiffuse(const RCore::RGBAColor& diffuse) {
		mDiffuse = diffuse;
	}

	/**
	 * Set the specular color.
	 * @param The specular color.
	 */
	void SetSpecular(const RCore::RGBAColor& specular) {
		mSpecular = specular;
	}

	/**
	 * Set the self illumination color.
	 * @param The self illumination color.
	 */
	void SetEmissive(const RCore::RGBAColor& emissive) {
		mEmissive = emissive;
	}

	/**
	 * Set the shine.
	 * @param The shine.
	 */
	void SetShine(const float shine) {
		mShine = shine;
	}

	/**
	 * Set the shine strength.
	 * @param The shine strength.
	 */
	void SetShineStrength(const float shineStrength) {
		mShineStrength = shineStrength;
	}

	/**
	 * Set the opacity amount (opacity) [1.0=full opac, 0.0=full transparent].
	 * @param The opacity amount.
	 */
	void SetOpacity(const float opacity) {
		mOpacity = opacity;
	}

	/**
	 * Set the index of refraction.
	 * @param The index of refraction.
	 */
	void SetIOR(const float ior) {
		mIOR = ior;
	}

	/**
	 * Set double sided flag.
	 * @param True if it is double sided, false if not.
	 */
	void SetDoubleSided(const bool doubleSided) {
		mDoubleSided = doubleSided;
	}

	/**
	 * Set the wireframe flag.
	 * @param True if wireframe rendering is enabled, false if not.
	 */
	void SetWireFrame(const bool wireFrame) {
		mWireFrame = wireFrame;
	}

	/**
	 * Set the transparency type.
	 * @param Upper or lower case character that represents the transparency type.
	 */
	void SetTransparencyType(const char transparencyType = 'U');

	/**
	 * Get the ambient color.
	 * @result The ambient color.
	 */
	const RCore::RGBAColor& GetAmbient() const {
		return mAmbient;
	}

	/**
	 * Get the diffuse color.
	 * @result The diffuse color.
	 */
	const RCore::RGBAColor& GetDiffuse() const {
		return mDiffuse;
	}

	/**
	 * Get the specular color.
	 * @result The specular color.
	 */
	const RCore::RGBAColor& GetSpecular() const {
		return mSpecular;
	}

	/**
	 * Get the self illumination color.
	 * @result The self illumination color.
	 */
	const RCore::RGBAColor& GetEmissive() const {
		return mEmissive;
	}

	/**
	 * Get the shine.
	 * @result The shine.
	 */
	float GetShine() const {
		return mShine;
	}

	/**
	 * Get the shine strength.
	 * @result The shine strength.
	 */
	float GetShineStrength() const {
		return mShineStrength;
	}

	/**
	 * Get the opacity amount [1.0=full opac, 0.0=full transparent].
	 * @result The opacity amount.
	 */
	float GetOpacity() const {
		return mOpacity;
	}

	/**
	 * Get the index of refraction.
	 * @result The index of refraction.
	 */
	float GetIOR() const {
		return mIOR;
	}

	/**
	 * Get double sided flag.
	 * @result True if it is double sided, false if not.
	 */
	bool GetDoubleSided() const {
		return mDoubleSided;
	}

	/**
	 * Get the wireframe flag.
	 * @result True if wireframe rendering is enabled, false if not.
	 */
	bool GetWireFrame() const {
		return mWireFrame;
	}

	/**
	* Get the transparency type.
	* @result F=filter / S=substractive / A=additive / U=unknown
	*/
	char GetTransparencyType() const {
		return static_cast<char>(mTransparencyType);
	}

	/**
	 * Add a given layer to this material.
	 * @param layer The layer to add.
	 */
	void AddLayer(std::shared_ptr<StandardMaterialLayer> layer) {
		mLayers.push_back(layer);
	}

	/**
	 * Get the number of texture layers in this material.
	 * @result The number of layers.
	 */
	size_t GetNumLayers() const {
		return mLayers.size();
	}

	/**
	 * Get a specific layer.
	 * @param nr The material layer number to get.
	 * @result A pointer to the material layer.
	 */
	std::shared_ptr<StandardMaterialLayer> GetLayer(const int nr) const {
		return mLayers.at(nr);
	}

   /**
	* Remove a specified material layer (also deletes it from memory).
	* @param nr The material layer number to remove.
	*/
	void RemoveLayer(const int nr) {
		mLayers.erase(mLayers.begin() + nr);
	}

   /**
	* Remove a specified material layer (also deletes it from memory).
	* @param layer A pointer to the layer to remove.
	*/
	void RemoveLayer(std::shared_ptr<StandardMaterialLayer> layer) {
		std::remove(mLayers.begin(), mLayers.end(), layer);
	}

	/**
	 * Removes all material layers from this material (includes deletion from memory).
	 */
	void RemoveAllLayers() {
		mLayers.clear();
	}

   /**
	* Find the layer number which is of the given type.
	* If you for example want to search for a diffuse layer, you make a call like:
	*
	* int layerNumber = material->FindLayer( StandardMaterialLayer::LAYERTYPE_DIFFUSE );
	*
	* This will return a value the layer number, which can be accessed with the GetLayer(layerNumber) method.
	* A value of -1 will be returned in case no layer of the specified type could be found.
	* @param layerType The layer type you want to search on, for a list of valid types, see the enum inside StandardMaterialLayer.
	* @result Returns the layer number or -1 when it could not be found.
	*/
	int FindLayer(const int layerType) const;

   /**
	* Creates a clone of the material, including it's layers.
	* @result A pointer to an exact clone (copy) of the material.
	*/
	virtual std::shared_ptr<Material> Clone();

	/**
	 * Get the unique type ID of this type of material.
	 * @result The unique ID of this material type.
	 */
	virtual int GetType() const { return TYPE_ID; }

	/**
	 * Get the string that is a description or the class name of this material type.
	 * @result The string containing the description or class name.
	 */
	virtual const char* GetTypeString() const { return "StandardMaterial"; }

protected:
	std::vector<std::shared_ptr<StandardMaterialLayer>> mLayers;	/*StandardMaterial layers. */
	RCore::RGBAColor	mAmbient;			/* Ambient color. */
	RCore::RGBAColor	mDiffuse;			/* Diffuse color. */
	RCore::RGBAColor	mSpecular;			/* Specular color. */
	RCore::RGBAColor	mEmissive;			/* Self illumination color. */
	float				mShine;				/* The shine value, from the phong component (the power). */
	float				mShineStrength;		/* Shine strength. */
	float				mOpacity;			/* The opacity amount [1.0=full opac, 0.0=full transparent]. */
	float				mIOR;				/* Index of refraction. */
	bool				mDoubleSided;		/* Double sided?. */
	bool				mWireFrame;			/* Render in wireframe?. */
	TransparencyType	mTransparencyType;	/* F=filter / S=substractive / A=additive / U=unknown */

	// added by cogito
	// 2.0에서는 Material에 있?E클래스인데 2.1에서 이곳으로 옮겨옴 - 박주?E
	// ?EE2.0의 Material클래스는 2.1에서 StandardMaterial로 바?E
public:
	unsigned long		mShaderMask;
	int					mAlphaRef;
	int					mSrcBlend;
	int					mDstBlend;
	unsigned long		mTexFxn;
	float				mTexFxnUParm, mTexFxnVParm, mTexFxnSub0Parm;

}; // class

} // namespace