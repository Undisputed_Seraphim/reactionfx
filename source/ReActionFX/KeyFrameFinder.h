#pragma once

namespace ReActionFX {

// forward declaration
template <class ReturnType, class StorageType> class KeyTrack;

/**
 * The keyframe finder.
 * This is used to quickly locate the two keys inside the keytrack to interpolate between.
 * For example if there is a keyframe at every second, and we want to calculate a value at
 * time 5.6, then we need to interpolate between the key with the time 5 and the key with the time 6.
 * This class basically uses a kd-tree to perform the search. It quickly culls out parts of the keys
 * which cannot be the one which we are searching for. This means the search times are linear, no matter
 * how many keys we are searching.
 */
template <class ReturnType, class StorageType>
class KeyFrameFinder {
public:
	/**
	 * The constructor.
	 */
	KeyFrameFinder() {}

	/**
	 * The destructor.
	 */
	~KeyFrameFinder() {}

	/**
	 * Locates the key number to use for interpolation at a given time value.
	 * You will interpolate between the returned keyframe number and the one after that.
	 * @param timeValue The time value you want to calculate a value at.
	 * @param keyTrack The keytrack to perform the search on.
	 * @result The key number, or -1 when no valid key could be found.
	 */
	static int FindKey(const float timeValue, KeyTrack<ReturnType, StorageType>* keyTrack);

private:
	/**
	 * The recursive main search method, which does all the work.
	 * @param timeValue The time value to use for the search.
	 * @param low The first keyframe number to start the search for.
	 * @param high The high keyframe number to start the search for.
	 * @param keyTrack The keytrack to perform the search on.
	 * @result The key number.
	 */
	static int FindKeyNumber(const float timeValue, const int low, const int high, KeyTrack<ReturnType, StorageType>* keyTrack);
};


// returns the keyframe number to use for interpolation
template <class ReturnType, class StorageType>
int KeyFrameFinder<ReturnType, StorageType>::FindKey(const float timeValue, KeyTrack<ReturnType, StorageType>* keyTrack) {
	// if we haven't got any keys, return -1, which means no key found
	if (keyTrack->GetNumKeys() == 0)
		return -1;

	// get the mid point where we will perform the split
	const int midPoint = (int)keyTrack->GetNumKeys() >> 1;

	// set the splitting time
	if (timeValue <= keyTrack->GetKey(midPoint)->GetTime())
		return FindKeyNumber(timeValue, 0, midPoint, keyTrack);
	else
		return FindKeyNumber(timeValue, midPoint, (int)keyTrack->GetNumKeys(), keyTrack);
}


// find the keyframe number inside the key track
template <class ReturnType, class StorageType>
int KeyFrameFinder<ReturnType, StorageType>::FindKeyNumber(const float timeValue, const int low, const int high, KeyTrack<ReturnType, StorageType>* keyTrack) {
	const int midKey = (low + high) >> 1;
	const int numKeys = midKey - low;

	// if there are only the given number of keys to check, perform the search
	if (numKeys < 10) {
		const auto maxKeys = keyTrack->GetNumKeys() - 1;

		// check all keys in our range
		for (int i = low; i <= high; i++) {
			// if it's not the last key
			if (i < maxKeys) {
				// if the is between the current and next key, that means we found it!
				if (keyTrack->GetKey(i)->GetTime() <= timeValue && keyTrack->GetKey(i + 1)->GetTime() >= timeValue)
					return i;
			}
		}

		// the result has not been found
		return -1;
	}

	// recursively split
	if (timeValue <= keyTrack->GetKey(midKey)->GetTime())
		return FindKeyNumber(timeValue, low, midKey, keyTrack);
	else
		return FindKeyNumber(timeValue, midKey, high, keyTrack);
}

} // namespace