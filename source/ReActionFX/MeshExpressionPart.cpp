#include "Node.h"
#include "Actor.h"
#include "ExpressionPart.h"
#include "MeshExpressionPart.h"
#include "Mesh.h"
#include "SmartMeshMorphDeformer.h"

using namespace RCore;

namespace ReActionFX {

MeshExpressionPart::MeshExpressionPart(const bool captureTransforms, const bool captureMeshDeforms, Actor* parentActor, Actor* pose, const std::string & name, const bool delPoseFromMem) : ExpressionPart(parentActor, name) {
	mCaptureTransforms = captureTransforms;
	mCaptureMeshDeforms = captureMeshDeforms;

	// init the expression part
	InitFromPose(pose, delPoseFromMem);
}


// destructor
MeshExpressionPart::~MeshExpressionPart() {
	// get rid of all the deform datas
	for (auto* deformData : mDeformDatas) {
		delete deformData;
	}
	mDeformDatas.clear();
}


// initialize the expression part from a given actor pose
void MeshExpressionPart::InitFromPose(Actor* pose, bool delPoseFromMem) {
	assert(mActor);
	assert(pose);

	// filter all changed meshes
	if (mCaptureMeshDeforms) {
		const size_t numPoseNodes = pose->GetNumNodes();
		for (size_t i = 0; i < numPoseNodes; i++) {
			// find if the pose node also exists in the actor where we apply this to
			Node* curNode = pose->GetNode(i);
			Node* orgNode = mActor->GetNodeByID(curNode->GetID());

			//LOG("orgNode = '%s'", orgNode ? orgNode->GetNamePtr() : "<none>");

			// skip this node if it doesn't exist in the actor we apply this to
			if (orgNode == nullptr)
				continue;

			// get the meshes
			Mesh* orgMesh = orgNode->GetMesh(0).get();
			Mesh* curMesh = curNode->GetMesh(0).get();

			//LOG("Checking meshes");

			// if one of the nodes has no mesh, we can skip this node
			if (orgMesh == nullptr || curMesh == nullptr)
				continue;

			// both nodes have a mesh, lets check if they have the same number of vertices as well
			const int numOrgVerts = orgMesh->GetNumVertices();
			const int numCurVerts = curMesh->GetNumVertices();

			// if the number of original vertices is not equal to the number of current vertices, we can't use this mesh
			if (numOrgVerts != numCurVerts)
				continue;

			// check if the mesh has differences
			int numDifferent = 0;
			Vector3* orgPositions = orgMesh->GetOrgPositions();
			Vector3* curPositions = curMesh->GetOrgPositions();

			Vector3* orgNormals = orgMesh->GetOrgNormals();
			Vector3* curNormals = curMesh->GetOrgNormals();

			AABB box;
			for (int v = 0; v < numOrgVerts; v++) {
				// calculate the delta vector between the two positions
				Vector3 deltaVec = curPositions[v] - orgPositions[v];

				// check if the vertex positions are different, so if there are mesh changes
				if (deltaVec.Length() > std::numeric_limits<MReal>::epsilon()) {
					box.Encapsulate(deltaVec);
					numDifferent++;
				}
			}

			// go to the next node and mesh if there is no difference
			if (numDifferent == 0)
				continue;

			//LOG("MeshExpressionPart::Init() - orgMesh = %d verts   -   targetMesh = %d verts", numOrgVerts, numCurVerts);


			// calculate the minimum value of the box
			float minValue = box.GetMin().x;
			minValue = std::fmin(minValue, box.GetMin().y);
			minValue = std::fmin(minValue, box.GetMin().z);

			// calculate the minimum value of the box
			float maxValue = box.GetMax().x;
			maxValue = std::fmax(maxValue, box.GetMax().y);
			maxValue = std::fmax(maxValue, box.GetMax().z);

			// make sure the values won't be too small
			if (maxValue - minValue < 1.0f) {
				if (minValue < 0 && minValue > -1)
					minValue = -1;

				if (maxValue > 0 && maxValue < 1)
					maxValue = 1;
			}

			// create the deformation data
			DeformData* deformData = new DeformData(orgNode, numDifferent);

			deformData->mMinValue = minValue;
			deformData->mMaxValue = maxValue;

			int curVertex = 0;
			for (int v = 0; v < numOrgVerts; v++) {
				// calculate the delta vector between the two positions
				Vector3 deltaVec = curPositions[v] - orgPositions[v];

				// check if the vertex positions are different, so if there are mesh changes
				//if (deltaVec.SquareLength() > std::numeric_limits<MReal>::epsilon())
				if (deltaVec.Length() > std::numeric_limits<MReal>::epsilon()) {
					Vector3 deltaNormal = curNormals[v] - orgNormals[v];
					deformData->mVertexNumbers[curVertex] = v;
					deformData->mDeltas[curVertex].FromVector3(deltaVec, minValue, maxValue);
					deformData->mDeltaNormals[curVertex].FromVector3(deltaNormal, -1.0f, 1.0f);
					curVertex++;
				}
			}

			//LOG("Deformdata for '%s' with %d vertices", orgNode->GetNamePtr(), numDifferent);

			// add the deform data
			mDeformDatas.push_back(deformData);

			//-------------------------------
			// create the mesh deformer
			auto stack = orgNode->GetMeshDeformerStack(0);

			// create the stack if it doesn't yet exist
			if (stack.get() == nullptr) {
				stack = std::make_shared<MeshDeformerStack>(orgMesh);
				orgNode->SetMeshDeformerStack(stack, 0);
			}

			// add the skinning deformer to the stack
			auto deformer = new SmartMeshMorphDeformer(this, (int)mDeformDatas.size() - 1, orgMesh);
			stack->InsertDeformer(0, deformer);
			//-------------------------------

			// make sure we end up with the same number of different vertices, otherwise the two loops
			// have different detection on if a vertex has changed or not
			assert(curVertex == numDifferent);
		}
	}

	//--------------------------------------------------------------------------------------------------

	if (mCaptureTransforms) {
		// check for transformation changes
		const size_t numPoseNodes = pose->GetNumNodes();
		for (size_t i = 0; i < numPoseNodes; i++) {
			Node* orgNode = mActor->GetNodeByID(pose->GetNode(i)->GetID());

			// skip it if it doesn't exist (so we won't crash)
			if (orgNode == nullptr)
				continue;

			Node* nodeA = pose->GetNode(i);
			Node* nodeB = orgNode;

			RCore::Vector3		posA = nodeA->GetOrgPos();
			RCore::Vector3		posB = nodeB->GetOrgPos();
			RCore::Quaternion	rotA = nodeA->GetOrgRot();
			RCore::Quaternion	rotB = nodeB->GetOrgRot();
			RCore::Vector3		scaleA = nodeA->GetOrgScale();
			RCore::Vector3		scaleB = nodeB->GetOrgScale();

			bool changed = false;

			float posDif = (posA - posB).SquareLength();
			if (posDif > 0)
				posDif = std::sqrt(posDif);

			// check if the position changed
			if (posDif > 0.0001f)
				changed = true;

			// check if the rotation changed
			if (!changed) {
				float rotDif = (rotA - rotB).SquareLength();
				if (rotDif > 0)
					rotDif = std::sqrt(rotDif);

				if (rotDif > 0.0001f)
					changed = true;
			}

			// check if the scale changed
			if (!changed) {
				float scaleDif = (scaleA - scaleB).SquareLength();
				if (scaleDif > 0)
					scaleDif = std::sqrt(scaleDif);

				if (scaleDif > 0.0001f)
					changed = true;
			}

			// if this node changed transformation
			if (changed) {
				// create a transform object form the node in the pose
				Transformation transform;
				transform.mPosition = nodeA->GetOrgPos() - orgNode->GetOrgPos();
				transform.mRotation = nodeA->GetOrgRot();
				transform.mScale = nodeA->GetOrgScale() - orgNode->GetOrgScale();

				// add the new node/transform pair to the expression part
				AddTransformation(orgNode->GetID(), transform);
			}
		}

		//LOG("Num transforms = %d", mTransforms.size());
	}

	// delete the pose actor when wanted
	if (delPoseFromMem)
		delete pose;
}


// restore all nodes touched by this expression part by putting them back in their original transformation
void MeshExpressionPart::RestoreActor(Actor* actor) {
	// restore all nodes affected by this expression part to their original transformations
	for (auto transform : mTransforms) {
		Node* node = actor->GetNodeByID(transform.first);
		assert(node != nullptr);
		if (node)
			node->SetToOriginalOrientation();
	}
}


// apply the relative transformation to the specified node
// store the result in the position, rotation and scale parameters
void MeshExpressionPart::ApplyTransformation(Actor* actor, Node* node, RCore::Vector3& position, RCore::Quaternion& rotation, RCore::Vector3& scale) {
	// get the weight value and convert it to a range based weight value
	const float weight = CalcRangedWeight(mCurWeight);

	// calculate the new transformations for all nodes of this expression part
	for (auto transform : mTransforms) {
		const int id = transform.first;
		if (node->GetID() == id) {
			// get the source node and target transformation
			Node* orgNode = actor->GetNodeByID(id);
			assert(orgNode);
			Transformation&	targetTransform = transform.second;

			// calc new position (delta based targetTransform)
			position += targetTransform.mPosition * weight;

			//------------------
			// linear interpolate the quaternions, allowing negative weights as well
			// the Quaternion::Lerp function doesn't allow negative weights, so we have to do it like this
			RCore::Quaternion a = orgNode->GetOrgRot();
			RCore::Quaternion b = targetTransform.mRotation;

			// check if both quaternions are on the same hypersphere or not, if not, invert one
			if (a.Dot(b) < 0.0)
				a = -a;

			// take the logarithm
			a = a.LogN();
			b = b.LogN();

			// interpolate, and take the exponent of that, which is the interpolated quaternion
			Quaternion rot = RCore::LinearInterpolate<Quaternion>(a, b, weight).Exp().Normalize();
			//------------------

			//RCore::Quaternion rot		= orgNode->GetOrgRot().Lerp( targetTransform.mRotation, weight );	// the target rotation
			RCore::Quaternion invRot = orgNode->GetOrgRot().Inversed();									// inversed original rotation
			rotation = rotation * (invRot * rot);										// add the delta rotation

			// calc new scale (delta based targetTransform)
			scale += targetTransform.mScale * weight;

			break;
		}
	}
}


// check if this expression part influences the specified node or not
bool MeshExpressionPart::Influences(Node* node) const {
	// check if there is a deform data object, which works on the specified node
	for (auto data : mDeformDatas) {
		if (data->mNode == node)
			return true;
	}

	// check all transforms
	for (auto transform : mTransforms) {
		// if the node name is equal to the transform name, it means the node will be modified
		if (transform.first == node->GetID())
			return true;
	}

	// this expression part doesn't influence the given node
	return false;
}


// apply the deformations to a given actor
void MeshExpressionPart::Apply(Actor* actor) {
	// get the weight value and convert it to a range based weight value
	const float weight = CalcRangedWeight(mCurWeight);

	// calculate the new transformations for all nodes of this expression part
	for (auto transform : mTransforms) {
		Node* node = actor->GetNodeByID(transform.first);
		if (node == nullptr) continue;

		Transformation&	targetTransform = transform.second;

		// calc new position (delta based targetTransform)
		node->SetLocalPos(node->GetLocalPos() + (targetTransform.mPosition * weight));

		// calc new rotation (non-delta based targetTransform)
		//RCore::Quaternion rot	= node->GetOrgRot().Lerp( targetTransform.mRotation, mCurWeight );	// the target rotation

		//------------------
		// linear interpolate the quaternions, allowing negative weights as well
		// the Quaternion::Lerp function doesn't allow negative weights, so we have to do it like this
		RCore::Quaternion a = node->GetOrgRot();
		RCore::Quaternion b = targetTransform.mRotation;

		// check if both quaternions are on the same hypersphere or not, if not, invert one
		if (a.Dot(b) < 0.0)
			a = -a;

		// take the logarithm
		a = a.LogN();
		b = b.LogN();

		// interpolate, and take the exponent of that, which is the interpolated quaternion
		Quaternion rot = RCore::LinearInterpolate<Quaternion>(a, b, weight).Exp().Normalize();
		//------------------

		RCore::Quaternion invRot = node->GetOrgRot().Inversed();			// inversed original rotation
		node->SetLocalRot(node->GetLocalRot() * (invRot * rot));		// add the delta rotation

		// calc new scale (delta based targetTransform)
		node->SetLocalScale(node->GetLocalScale() + (targetTransform.mScale * weight));
	}
}




void MeshExpressionPart::AddTransformation(const int nodeID, const Transformation& transform) {
	std::pair<int, Transformation> newPair(nodeID, transform);
	mTransforms.push_back(newPair);
}


// get the given transformation
const std::pair<int, MeshExpressionPart::Transformation>& MeshExpressionPart::GetTransformation(const int nr) {
	assert(nr >= 0 && nr < mTransforms.size());
	return mTransforms[nr];
}


//---------------------------------------------------
// DeformData
//---------------------------------------------------

// constructor
MeshExpressionPart::DeformData::DeformData(Node* node, int numVerts) {
	mNode = node;
	mNumVerts = numVerts;
	mVertexNumbers = (int*)malloc(numVerts * sizeof(int));
	mDeltas = (RCore::Compressed16BitVector3*)malloc(numVerts * sizeof(RCore::Compressed16BitVector3));
	mDeltaNormals = (RCore::Compressed8BitVector3*)malloc(numVerts * sizeof(RCore::Compressed8BitVector3));
	mMinValue = -10.0f;
	mMaxValue = +10.0f;
}


// destructor
MeshExpressionPart::DeformData::~DeformData() {
	free(mVertexNumbers);
	free(mDeltas);
	free(mDeltaNormals);
}

} // namespace