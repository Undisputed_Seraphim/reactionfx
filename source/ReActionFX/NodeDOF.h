#pragma once

#include "RCore.h"

#include "NodeAttribute.h"

namespace ReActionFX {

/**
 * The NodeDOF class.
 * It specifies which rotations can be applied to a node
 * and the order in which it must be done.
 */
class NodeDOF : public NodeAttribute {
public:
	// the type ID, returned by GetType()
	enum { TYPE_ID = 0xCADA0001 };

	// the axis indices
	enum CanonicalAxis {
		AxisX = 0,
		AxisY = 1,
		AxisZ = 2,
		AxisNONE = -1
	};

	/**
	 * Constructor setting rotation.
	 * @param rotOrderAxis1 The first rotation axis.
	 * @param rotOrderAxis2 The second rotation axis.
	 * @param rotOrderAxis3 The third rotation axis.
	 */
	NodeDOF(const CanonicalAxis rotOrderAxis1, const CanonicalAxis rotOrderAxis2 = AxisNONE, const CanonicalAxis rotOrderAxis3 = AxisNONE);

	/**
	 * Default constructor. It returs a NodeDOF based on Euler's.
	 */
	NodeDOF();

	/**
	 * Destructor.
	 */
	~NodeDOF() {}

	/**
	 * Set the rotation order and the axis.
	 * @param rotOrderAxis1 The first rotation axis.
	 * @param rotOrderAxis2 The second rotation axis.
	 * @param rotOrderAxis3 The third rotation axis.
	 */
	void SetRotationOrder(const CanonicalAxis rotOrderAxis1, const CanonicalAxis rotOrderAxis2, const CanonicalAxis rotOrderAxis3);

	/**
	 * Set the NodeDOF based on Euler's.
	 */
	void SetEulerRotation();

   /**
	* Set the limits for the i-th DOF.
	*/
	void SetLimits(const int index, const float max, const float min);

	/**
	 * Returns the max value for the node DOF
	 */
	float GetMaxValue(const int index) const {
		return mMaxValue[index];
	}

	/**
	 * Returns the min value for the node DOF
	 */
	float GetMinValue(const int index) const {
		return mMinValue[index];
	}

	/**
	 * Get the attribute type.
	 * @return The attribute ID.
	 */
	 // return the type identification number of the node DOF
	int GetType() const {
		return TYPE_ID;
	}

   /**
	* Get the attribute type as a string.
	* This string should contain the name of the class.
	* @return The string containing the type name.
	*/
	// return the type identification string of the node DOF
	const char* GetTypeString() const {
		return "NodeDOF";
	}

   /**
	* Clone the node attribute.
	* @return Returns a pointer to a newly created exact copy of the node attribute.
	*/
	NodeAttribute* Clone();

	/**
	 * Returns the number of DOFs (rotations) applied to de LM_Node.
	 * @return The number of DOFs.
	 */
	int GetNumDOF() const {
		return mNumDOF;
	}

	/**
	 * Set the 'id' for the node. It's reffered to the order in actor`s array.
	 */
	void SetNodeID(const int id) {
		mNodeID = id;
	}

	/**
	 * Returns the node 'id'
	 */
	int GetNodeID() const {
		return mNodeID;
	}

	/**
	 * Returns the i-th angle value. It must be previously calculated.
	 * @return The amgle value.
	 */
	float GetAngleValue(const int index) const {
		return mAngles[index];
	}

	/**
	 * Returns the i-th DOF axis. It must be previously calculated.
	 * @return The DOF axis vector.
	 */
	RCore::Vector3 GetAxis(const int index) const {
		return mAxis[index];
	}

	/**
	 * Returns the rotation angles values from actor's actual pose.
	 * @return A float array containing the values.
	 */
	void CalcValues(RCore::Matrix& localM, const RCore::Vector3& localS);

	/**
	 * Sets the angles values to those given by 'v1', 'v2' and 'v3'.
	 */
	void SetValues(const float v1, const float v2, const float v3) {
		mAngles[0] = v1;
		mAngles[1] = v2;
		mAngles[2] = v3;
	}

	/**
	 * Calculates DOFs rotation axis from parent world matrix
	 * and knowing angles values.
	 */
	void CalcAxis(RCore::Matrix& parentM);

	/**
	 * Returns the rotation matrix using those angles values.
	 */
	RCore::Matrix GetMatrix(const float v1, const float v2, const float v3);

private:
	int				mNumDOF;				/* Number of DOFs for the node. */
	int				mNodeID;				/* */
	CanonicalAxis	mRotationOrder[3];		/* Axis for each rotation and order in which those are applied. */
	bool			mEuler;					/* This boolean is true if the DOFs are base on Euler rotation angles. */
	float			mMaxValue[3];			/* Maximun value for each possible DOF. */
	float			mMinValue[3];			/* Minimun value for each possible DOF. */
	float			mAngles[3];				/* Internal use. */
	RCore::Vector3	mAxis[3];				/* Internal use. */

	/**
	 * Returns true if these are the rotation settings.
	 * @param r1 1st rotation.
	 * @param r2 2nd rotation.
	 * @param r3 3th rotation.
	 */
	bool Rotation(const int r1, int r2, int r3) const {
		return (!mEuler&&
				mRotationOrder[0] == r1 &&
				mRotationOrder[1] == r2 &&
				mRotationOrder[2] == r3);
	}

	bool Rotation(const int r1, int r2) const {
		return (!mEuler&&
				mRotationOrder[0] == r1 &&
				mRotationOrder[1] == r2);
	}

	RCore::Matrix RotationMatrix(const int i, const float v) const;
};

} // namespace