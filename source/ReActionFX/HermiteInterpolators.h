#pragma once

#include "RCore.h"

#include "Interpolator.h"

namespace ReActionFX {

/**
 * The Hermite interpolator class.
 * A hermite is a spline which goes through its control points.
 * This means that you will get smooth motion, going through all keys you specified.
 */
template <class ReturnType, class StorageType>
class HermiteInterpolator : public Interpolator<ReturnType, StorageType> {
public:
	// the type ID, returned by GetType()
	enum { TYPE_ID = 0x00000003 };

	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	HermiteInterpolator() : Interpolator<ReturnType, StorageType>(), mTangents(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	HermiteInterpolator(KeyTrack<ReturnType, StorageType>* interpolationSource) : Interpolator<ReturnType, StorageType>(interpolationSource), mTangents(nullptr) {}

	/**
	 * Destructor.
	 */
	~HermiteInterpolator();

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	virtual ReturnType Interpolate(const int startKey, const double timeValue) const;

	/**
	 * Initialize the interpolator.
	 * In this case this will calculate the tangents for the keys.
	 */
	virtual void Init();

	/**
	 * Returns the unique interpolator type ID.
	 * This can be used to determine what interpolator is used.
	 * @result The unique type identification number of this interpolator class.
	 */
	virtual int GetType() const { return TYPE_ID; }

	/**
	 * Returns the type string of this interpolator class.
	 * This can be the class name, or a description of the interpolator.
	 * @result The string containing the description or class name of this interpolator.
	 */
	virtual const char* GetTypeString() const { return "HermiteInterpolator"; }

	/**
	 * Get the tangent for a given key, with both read and write access.
	 * Be sure to call this only AFTER the Init method has been called.
	 * Otherwise the tangent data won't yet be allocated, or the number of tangents might
	 * be different from the number of keys inside the keytrack.
	 * @param keyNr The key number to get the tangent from.
	 * @result A reference to the tangent for the given key, including write access.
	 */
	inline ReturnType& GetTangent(const int keyNr);


protected:
	ReturnType*		mTangents;	/* The tangent vectors, one for each key. */
};



/**
 * The hermite interpolator class used for quaternions.
 * The reason why there is a special class for quaternions is that they
 * need some additional checks in order to perform the interpolation correctly.
 */
template <class StorageType>
class HermiteQuaternionInterpolator : public HermiteInterpolator<RCore::Quaternion, StorageType> {

public:
	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	HermiteQuaternionInterpolator() : HermiteInterpolator<RCore::Quaternion, StorageType>(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	HermiteQuaternionInterpolator(KeyTrack<RCore::Quaternion, StorageType>* interpolationSource) : HermiteInterpolator<RCore::Quaternion, StorageType>(interpolationSource) {}

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	RCore::Quaternion Interpolate(const int startKey, const double timeValue) const;
};


template <class ReturnType, class StorageType>
HermiteInterpolator<ReturnType, StorageType>::~HermiteInterpolator() {
	if (mTangents)
		free(mTangents);
}


// get a given tangent
template <class ReturnType, class StorageType>
inline ReturnType& HermiteInterpolator<ReturnType, StorageType>::GetTangent(const int keyNr) {
	assert(this->mInterpolationSource);
	assert(keyNr < this->mInterpolationSource->GetNumKeys());
	assert(mTangents);
	return mTangents[keyNr];
}


// initialize the interpolator
template <class ReturnType, class StorageType>
void HermiteInterpolator<ReturnType, StorageType>::Init() {
	assert(this->mInterpolationSource);	// make sure we have a keytrack assigned

									// allocate the new tangents
	const int numKeys = (int)this->mInterpolationSource->GetNumKeys();
	mTangents = (ReturnType*)realloc(mTangents, sizeof(ReturnType) * numKeys);

	// calculate all tangents
	for (int i = 0; i < numKeys; i++) {
		// if there is no previous or next key
		if (i == 0 || i == numKeys - 1) {
			ReturnType tangent;
			memset(&tangent, 0, sizeof(ReturnType));
			mTangents[i] = tangent;
		} else	// if there is a previous or next key
		{
			const ReturnType& prevValue = this->mInterpolationSource->GetKey(i - 1)->GetValue();
			const ReturnType& value = this->mInterpolationSource->GetKey(i)->GetValue();
			const ReturnType& nextValue = this->mInterpolationSource->GetKey(i + 1)->GetValue();
			this->mTangents[i] = (0.5 * (value - prevValue)) + (0.5 * (nextValue - value));
		}
	}
}


// hermite interpolation
template <class ReturnType, class StorageType>
ReturnType HermiteInterpolator<ReturnType, StorageType>::Interpolate(const int startKey, const double timeValue) const {
	//				[ 2 -2  1  1]	[a]
	//[u3 u2 u 1]	[-3  3 -2 -1]	[b]
	//				[ 0  0  1  0]	[ta]
	//				[ 1  0  0  0]	[tb]

	// precalc u, u2 and u3
	double t = timeValue;
	double t2 = t * t;
	double t3 = t2 * t;

	return	(2 * t3 + -3 * t2 + 1) * this->mInterpolationSource->GetKey(startKey)->GetValue() +
		(-2 * t3 + 3 * t2)		* this->mInterpolationSource->GetKey(startKey + 1)->GetValue() +
		(t3 + -2 * t2 + t)	* this->mTangents[startKey] +
		(t3 + -t2)		* this->mTangents[startKey + 1];
}

//-------------------------------------------


// the interpolation method
template <class StorageType>
RCore::Quaternion HermiteQuaternionInterpolator<StorageType>::Interpolate(const int startKey, const double timeValue) const {
	// get the two quaternions
	RCore::Quaternion a = this->mInterpolationSource->GetKey(startKey)->GetValue();
	RCore::Quaternion b = this->mInterpolationSource->GetKey(startKey + 1)->GetValue();

	// check if both quaternions are on the same hypersphere or not, if not, invert one
	if (a.Dot(b) < 0.0)
		a = -a;

	//				[ 2 -2  1  1]	[a]
	//[u3 u2 u 1]	[-3  3 -2 -1]	[b]
	//				[ 0  0  1  0]	[ta]
	//				[ 1  0  0  0]	[tb]

	// interpolate, and take the exponent of that, which is the interpolated quaternion
	double t = timeValue;
	double t2 = t * t;
	double t3 = t2 * t;

	return ((2 * t3 + -3 * t2 + 1) * a.LogN() +
		(-2 * t3 + 3 * t2)		* b.LogN() +
			(t3 + -2 * t2 + t)	* this->mTangents[startKey] +
			(t3 + -t2)		* this->mTangents[startKey + 1]).Exp().Normalize();
}

} // namespace
