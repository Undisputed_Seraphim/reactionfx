#pragma once

#include "RCore.h"

#include "KeyTrack.h"


namespace ReActionFX {

/**
 * A motion part.
 * This is an animated part of a complete animation. For example an animated
 * bone. An example could be the animation of the 'upper arm'. A complete set of motion parts
 * together form a complete motion (for example a walk motion).
 */
class MotionPart {
public:
	/**
	 * Default constructor.
	 */
	MotionPart() {}

	/**
	 * Constructor.
	 * @param name The name of the motion part.
	 */
	MotionPart(const std::string & name) : mName(name) {}

	/**
	 * Destructor.
	 */
	~MotionPart() {}

	/**
	 * Returns the position keyframing track.
	 * @result The position keyframing track.
	 */
	KeyTrack<RCore::Vector3, RCore::Vector3>& GetPosTrack() {
		return mPosTrack;
	}

	/**
	 * Returns the rotation keyframing track.
	 * @result The rotation keyframing track.
	 */
	KeyTrack<RCore::Quaternion, RCore::Compressed16BitQuaternion>& GetRotTrack() {
		return mRotTrack;
	}

	/**
	 * Returns the scaling keyframing track.
	 * @result The scaling keyframing track.
	 */
	KeyTrack<RCore::Vector3, RCore::Vector3>& GetScaleTrack() {
		return mScaleTrack;
	}

	/**
	 * Returns the name, in form of a C/C++ character buffer.
	 * @result A pointer to the null terminated character buffer, containing the  name of this motion part.
	 */
	const char* GetName() const {
		return mName.data();
	}

	/**
	 * Returns the original pose position of this part.
	 * @result The original pose position of this part.
	 */
	const RCore::Vector3& GetPosePos() const {
		return mPosePos;
	}

	/**
	 * Returns the original pose rotation of this part.
	 * @result The original pose rotation of this part.
	 */
	const RCore::Quaternion& GetPoseRot() const {
		return mPoseRot;
	}

	/**
	 * Returns the original pose scaling of this part.
	 * @result The original pose scaling of this part.
	 */
	const RCore::Vector3& GetPoseScale() const {
		return mPoseScale;
	}

	/**
	 * Set the original pose position.
	 * @param pos The original pose position.
	 */
	void SetPosePos(const RCore::Vector3& pos) {
		mPosePos = pos;
	}

	/**
	 * Set the original pose rotation.
	 * @param pos The original pose rotation.
	 */
	void SetPoseRot(const RCore::Quaternion& rot) {
		mPoseRot = rot;
	}

	/**
	 * Set the original pose scaling.
	 * @param pos The original pose scaling.
	 */
	void SetPoseScale(const RCore::Vector3& scale) {
		mPoseScale = scale;
	}


private:
	std::string 									mName;			/* The name of the motion part. */
	KeyTrack<RCore::Quaternion, RCore::Compressed16BitQuaternion>	mRotTrack;	/* The rotation key track. */
	KeyTrack<RCore::Vector3, RCore::Vector3>		mPosTrack;		/* The position key track. */
	KeyTrack<RCore::Vector3, RCore::Vector3>		mScaleTrack;	/* The scale key track. */
	RCore::Quaternion								mPoseRot;		/* The original base pose rotation. */
	RCore::Vector3									mPosePos;		/* The original base pose position. */
	RCore::Vector3									mPoseScale;		/* The original base pose scale. */
};

} // namespace
