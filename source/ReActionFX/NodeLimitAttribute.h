#pragma once

#include "RCore.h"

#include "NodeAttribute.h"

namespace ReActionFX {

/**
 * The node/joint limit attribute class.
 * This class describes position, rotation and scale limits of a given node.
 * These values can be used during calculations of controllers, such as Inverse Kinematics solvers.
 * However, when a node has limits assigned to them, this does not mean these limits are used.
 * Controllers are not forced to use these limits, so adjusting values might not have any influence if they are
 * not supported by the specific controller.
 */
class NodeLimitAttribute : public NodeAttribute {
public:
	// the unique type ID of this node attribute
	enum { TYPE_ID = 0x07000002 };

	/**
	 * The limit activation flags.
	 * They specify what components are limited.
	 */
	enum ELimitType {
		TRANSLATIONX = 1 << 0,	/* Position limit on the x axis. */
		TRANSLATIONY = 1 << 1,	/* Position limit on the y axis. */
		TRANSLATIONZ = 1 << 2,	/* Position limit on the z axis. */
		ROTATIONX = 1 << 3,		/* Rotation limit on the x axis. */
		ROTATIONY = 1 << 4,		/* Rotation limit on the y axis. */
		ROTATIONZ = 1 << 5,		/* Rotation limit on the z axis. */
		SCALEX = 1 << 6,		/* Scale limit on the x axis. */
		SCALEY = 1 << 7,		/* Scale limit on the y axis. */
		SCALEZ = 1 << 8			/* Scale limit on the z axis. */
	};

	/**
	 * The constructor.
	 */
	NodeLimitAttribute() : NodeAttribute() { mLimitFlags = 0; mTranslationMax.Set(0, 0, 0); mRotationMax.Set(0, 0, 0); mScaleMax.Set(1, 1, 1); }

	/**
	 * The destructor.
	 */
	virtual ~NodeLimitAttribute() {}

	/**
	 * Get the attribute type.
	 * @result The attribute ID.
	 */
	int	GetType() const { return TYPE_ID; }

	/**
	 * Get the attribute type as a string.
	 * This string should contain the name of the class.
	 * @result The string containing the type name.
	 */
	const char* GetTypeString() const { return "NodeLimitAttribute"; }

	/**
	 * Clone the node attribute.
	 * @result Returns a pointer to a newly created exact copy of the node attribute.
	 */
	NodeAttribute* Clone();

	/**
	 * Set the minimum translation values, and automatically enable the limit to be true.
	 * @param translateMin The minimum translation values.
	 */
	void SetTranslationMin(const RCore::Vector3& translateMin) {
		mTranslationMin = translateMin;
	}

	/**
	 * Set the maximum translation values, and automatically enable the limit to be true.
	 * @param translateMax The maximum translation values.
	 */
	void SetTranslationMax(const RCore::Vector3& translateMax) {
		mTranslationMax = translateMax;
	}

	/**
	 * Set the minimum rotation angles, and automatically enable the limit to be true.
	 * The rotation angles must be in radians.
	 * @param rotationMin The minimum rotation angles.
	 */
	void SetRotationMin(const RCore::Vector3& rotationMin) {
		mRotationMin = rotationMin;
	}

	/**
	 * Set the maximum rotation angles, and automatically enable the limit to be true.
	 * The rotation angles must be in radians.
	 * @param rotationMax The maximum rotation angles.
	 */
	void SetRotationMax(const RCore::Vector3& rotationMax) {
		mRotationMax = rotationMax;
	}

	/**
	 * Set the minimum scale values, and automatically enable the limit to be true.
	 * @param scaleMin The minimum scale values.
	 */
	void SetScaleMin(const RCore::Vector3& scaleMin) {
		mScaleMin = scaleMin;
	}

	/**
	 * Set the maximum scale values, and automatically enable the limit to be true.
	 * @param scaleMax The maximum scale values.
	 */
	void SetScaleMax(const RCore::Vector3& scaleMax) {
		mScaleMax = scaleMax;
	}

	/**
	 * Get the minimum translation values.
	 * @return The minimum translation values.
	 */
	const RCore::Vector3& GetTranslationMin() const {
		return mTranslationMin;
	}

	/**
	 * Get the maximum translation values.
	 * @return The maximum translation values.
	 */
	const RCore::Vector3& GetTranslationMax() const {
		return mTranslationMax;
	}

	/**
	 * Get the minimum rotation values.
	 * The rotation angles are in RADs.
	 * @return The minimum rotation values.
	 */
	const RCore::Vector3& GetRotationMin() const {
		return mRotationMin;
	}

	/**
	 * Get the maximum rotation values.
	 * The rotation angles are in RADs.
	 * @return The maximum rotation values.
	 */
	const RCore::Vector3& GetRotationMax() const {
		return mRotationMax;
	}

	/**
	 * Get the minimum scale values.
	 * @return The minimum scale values.
	 */
	const RCore::Vector3& GetScaleMin() const {
		return mScaleMin;
	}

	/**
	 * Get the maximum scale values.
	 * @return The maximum scale values.
	 */
	const RCore::Vector3& GetScaleMax() const {
		return mScaleMax;
	}

	/**
	 * Enable or disable the limit for the specified limit type.
	 * @param limitType The limit type to enable or disable.
	 * @param flag True to enable the specified limit, false to disable it.
	 */
	void EnableLimit(const ELimitType limitType, const bool flag = true);

	/**
	 * Toggle limit state.
	 * @param limitType The limit type to toggle.
	 */
	void ToggleLimit(const ELimitType limitType);

	/**
	 * Determine if the specified limit is enabled or disabled.
	 * @param limitType The limit type to check.
	 * @return True if specified limit is limited, false it not.
	 */
	bool IsLimited(const ELimitType limitType) const {
		return (mLimitFlags & limitType);
	}


protected:
	RCore::Vector3		mTranslationMin;	/* The minimum translation values. */
	RCore::Vector3		mTranslationMax;	/* The maximum translation values. */
	RCore::Vector3		mRotationMin;		/* The minimum rotation values. */
	RCore::Vector3		mRotationMax;		/* The maximum rotation values. */
	RCore::Vector3		mScaleMin;			/* The minimum scale values. */
	RCore::Vector3		mScaleMax;			/* The maximum scale values. */
	unsigned short		mLimitFlags;		/* The limit type activation flags. */
};

} // namespace