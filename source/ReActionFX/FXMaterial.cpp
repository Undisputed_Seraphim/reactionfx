#include "FXMaterial.h"

using namespace RCore;

namespace ReActionFX {

// create and return a clone of the material
std::shared_ptr<Material> FXMaterial::Clone() {
	// create the new material
	auto clone = std::make_shared<FXMaterial>(mName);

	for (const auto& param : stringParams)
		clone->AddStringParameter(param.first, param.second);

	for (const auto& param : colorParams)
		clone->AddColorParameter(param.first, param.second);

	for (const auto& param : floatParams)
		clone->AddFloatParameter(param.first, param.second);

	for (const auto& param : intParams)
		clone->AddIntParameter(param.first, param.second);


	// return the cloned fx material
	return clone;
}

bool FXMaterial::SetIntParameter(const std::string &name, const int value) {
	if (intParams.find(name) == intParams.end())
		return false;

	intParams[name] = value;

	return true;
}

bool FXMaterial::SetFloatParameter(const std::string &name, const float value) {
	if (floatParams.find(name) == floatParams.end())
		return false;

	floatParams[name] = value;

	return true;
}

bool FXMaterial::SetColorParameter(const std::string &name, const RCore::RGBAColor &value) {
	if (colorParams.find(name) == colorParams.end())
		return false;

	colorParams[name] = value;

	return true;
}

bool FXMaterial::SetStringParameter(const std::string &name, const std::string &value) {
	if (stringParams.find(name) == stringParams.end())
		return false;

	stringParams[name] = value;

	return true;
}

} // namespace