#include <cmath>
#include "Node.h"
#include "NodeLimitAttribute.h"
#include "LookAtConstraint.h"

using namespace RCore;

namespace ReActionFX {

// constructor
LookAtConstraint::LookAtConstraint(Actor* actor, Node* node, Node* target)
	: Constraint(actor, node, target), mGoal(Vector3(0.0f, 0.0f, 0.0f)), mInterpolationSpeed(1.0f) {
	assert(mNode != nullptr);

	// we need to keep a copy of the rotation
	mRotationQuat = Quaternion::FromMatrix(node->GetWorldTM().Normalized()).Normalized();

	mPostRotation.Identity();
	mPreRotation.Identity();

	// set the limits accoring to the model's current limits if it has them set, otherwise just use default values
	NodeLimitAttribute* limit = (NodeLimitAttribute*)mNode->GetAttributeByType(NodeLimitAttribute::TYPE_ID);

	// set the limits
	if (limit != nullptr)
		SetEulerConstraints(limit->GetRotationMin(), limit->GetRotationMax());
	else {
		mEllipseOrientation.Identity();
		mEllipseRadii.Set(1.0f, 1.0f);
		mMinMaxTwist.Set(0.0f, 0.0f);
	}

	// disable the constraints
	mConstraintType = CONSTRAINT_NONE;
}


// update the constraint
void LookAtConstraint::Update(const float timeDeltaInSeconds) {
	// calculate the new forward vector
	Vector3 forward;
	if (mTarget)
		forward = (mTarget->GetWorldPos() - mNode->GetWorldPos()).Normalized();
	else
		forward = (mGoal - mNode->GetWorldPos()).Normalized();

	Vector3 up;
	Node* parent = mNode->GetParent();
	Matrix parentWorldTM(true);
	if (parent != mActor) {
		parentWorldTM = parent->GetWorldTM().Normalized();

		// get the up vector from the parent if it exsists 
		up = (mPreRotation * parentWorldTM).GetUp().Normalized();
	} else // otherwise use the up vector of the node itself 
		up = (mPreRotation * mNode->GetWorldTM().Normalized()).GetUp().Normalized();

	// caclculate the right and up vector we wish to use
	Vector3 right = up.Cross(forward).Normalized();
	up = forward.Cross(right).Normalized();

	// build the destination rotation matrix
	Matrix destRotation(true);
	destRotation.SetRight(right);
	destRotation.SetUp(up);
	destRotation.SetForward(forward);

	// at this point, destRotation contains the world space orientation that we would like our node do be using

	// if we want to apply constraints
	if (mConstraintType != CONSTRAINT_NONE) {
		// find the destination relative to the node's parent if the node has one 
		if (parent != mActor)
			destRotation = destRotation * parentWorldTM.Inversed();

		// now destRotation contains the exact orientation we wish to apply constraints to (relative to the identity matrix)

		// multiply by the inverse of the ellipse's orientation, and rotate our coordinate system into 'ellipse space'
		destRotation = mEllipseOrientation.Inversed() * destRotation;

		// calculate the swing and twist
		SwingAndTwist sAndt(destRotation);

		// apply swing constraints
		if (mConstraintType == CONSTRAINT_ELLIPSE)		// constraint using an ellipse
		{
			sAndt.ConstrainSwingWithEllipse(mEllipseRadii);
		} else
			if (mConstraintType == CONSTRAINT_RECTANGLE)	// constraint using a rectangle
			{
				// the rectangle method will be a lot faster than the ellipse, but doesn't look anywhere near as good
				// this should probably only be used for distant models using a lower LOD

				// treat the ellipse's radii as bounds for a rectangle
				sAndt.ConstrainSwingWithRectangle(mEllipseRadii);
			}

			// apply twist constraints
		sAndt.ConstrainTwist(mMinMaxTwist);

		// convert the swing and twist back to a matrix
		destRotation = sAndt.ToMatrix();

		// rotate back out of ellipse space
		destRotation = mEllipseOrientation * destRotation;

		// if we converted our coord system into the space relative to the parent, then convert it back to world space
		if (parent != mActor)
			destRotation = destRotation * parentWorldTM;
	}

	// apply the post rotation patrix
	destRotation = mPostRotation * destRotation;

	// interpolate between the current rotation and the destination rotation
	float speed = mInterpolationSpeed * timeDeltaInSeconds * 6.0f;
	speed = RCore::Clamp<float>(speed, 0.0f, 1.0f);
	mRotationQuat = mRotationQuat.Slerp(Quaternion::FromMatrix(destRotation), speed);

	// create the rotation matrix from the rotation quat
	destRotation = mRotationQuat.ToMatrix();

	// scale the matrix and put it back in the right position
	Vector3 scale = mNode->GetWorldScale();
	destRotation.Scale(scale.x, scale.y, scale.z);
	destRotation.SetTranslation(mNode->GetWorldPos());

	// update the matrix with forward kinematics
	mNode->RecursiveUpdateWorldTM(&destRotation);
}


// Calculate the Ellipse radii and the twist value from the min and max euler angles
void LookAtConstraint::SetEulerConstraints(const Vector3& minVals, const Vector3& maxVals) {
	// twist is z
	mMinMaxTwist.x = minVals.z;
	mMinMaxTwist.y = maxVals.z;

	// Offset is the average
	float xOffset = (minVals.x + maxVals.x) * 0.5;
	float yOffset = (minVals.y + maxVals.y) * 0.5;

	// these lines are the same as above
	mEllipseRadii.x = maxVals.x - xOffset;
	mEllipseRadii.y = maxVals.y - yOffset;

	// build the apropriate matrices
	Matrix xOffsetMat, yOffsetMat, zRot;
	xOffsetMat.SetRotationMatrixX(xOffset);
	yOffsetMat.SetRotationMatrixY(yOffset);
	mEllipseOrientation = xOffsetMat * yOffsetMat;

	// TODO: check if this is required
	// basically just swaps the x and y axii
	zRot.SetRotationMatrixZ(-M_PI_2);
	mEllipseOrientation = zRot * mEllipseOrientation;
}


// clone the constraint
Controller* LookAtConstraint::Clone(Actor* actor) {
	Node* newNode = actor->GetNodeByID(mNode->GetID());
	assert(newNode != nullptr);

	// create the new constraint and copy its members
	LookAtConstraint* newController = new LookAtConstraint(actor, newNode, mTarget);
	newController->mPreRotation = mPreRotation;
	newController->mPostRotation = mPostRotation;
	newController->mEllipseOrientation = mEllipseOrientation;
	newController->mRotationQuat = mRotationQuat;
	newController->mGoal = mGoal;
	newController->mEllipseRadii = mEllipseRadii;
	newController->mMinMaxTwist = mMinMaxTwist;
	newController->mInterpolationSpeed = mInterpolationSpeed;
	newController->mConstraintType = mConstraintType;

	return newController;
}

} // namespace