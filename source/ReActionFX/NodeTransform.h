#pragma once

#include "RCore.h"

namespace ReActionFX {

/**
 * A node transformation contains the orientation, position and scale of a node (used mainly internally inside ReActionFX).
 */
class NodeTransform {
public:
	RCore::Quaternion	mRotation;		/* The rotation. */
	RCore::Vector3		mPosition;		/* The position. */
	RCore::Vector3		mScale;			/* The scale. */
};

} // namespace