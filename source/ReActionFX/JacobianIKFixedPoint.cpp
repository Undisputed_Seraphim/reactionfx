#include "JacobianIKFixedPoint.h"
#include "JacobianIKData.h"

using namespace RCore;

namespace ReActionFX {

// return in 'values' the requested position for the constrained node
void JacobianIKFixedPoint::GetValues(std::vector<float>& values) {
	values.push_back(mPosition.x);
	values.push_back(mPosition.y); // ahm yes ... (Benny)
	values.push_back(mPosition.z);
}


void JacobianIKFixedPoint::GetResults(std::vector<float>& results) {
	// retrieve the node's world position
	Vector3 position = mNode->GetWorldPos();

	// the constraint result is the node position
	results.push_back(position.x);
	results.push_back(position.y); // ahm yes ... (Benny)
	results.push_back(position.z);
}


void JacobianIKFixedPoint::CalcGradient(NMatrix& matrix) {
	const int numDOFs = mIKData->GetNumDOF();

	matrix.SetSize(3, numDOFs);

	// the derivative of the result is the derivative of the position
	// the gradient is the derivative of each coordinate (in each column) respect to each DOF (in each row)
	for (int i = 0; i < numDOFs; i++) {
		Vector3 v = mIKData->CalcPosDer(mNode, i);
		matrix(0, i) = v.x;
		matrix(1, i) = v.y;
		matrix(2, i) = v.z;
	}
}

} // namespace