#pragma once

#include <vector>

#include "RCore.h"

#include "NodeDOF.h"
#include "Actor.h"

namespace ReActionFX {

/**
 * The JacobianIKActorDOF class.
 * Contains information about the DOFs applied on the nodes of the actor. It obtains the total number of DOFs, which
 * DOF is from each node and which of them gets influence over each node. This attribute will be stored only once per actor
 * and not on node level.
 */
class JacobianIKActorDOF {
public:
	/**
	 * Default constructor.
	 */
	JacobianIKActorDOF() :mNumDOF(0), mActor(nullptr) {}

	/**
	 * Destructor.
	 */
	~JacobianIKActorDOF() {}

	/**
	 * Update the attributes of the actor. The actor must point to the owner of the attribute instance.
	 * @param actor Pointer to the actor which owns this attribute.
	 */
	void Update(Actor* actor);

	/**
	 * Update the attributes of the actor data.
	 */
	void Update();

	/**
	 * Get the number of DOFs applied to the actor.
	 * @return The number of DOFs.
	 */
	inline int GetNumDOF() const {
		return mNumDOF;
	}

	/**
	 * Get an array which contains the actual DOF values.
	 * @return The array which includes the values.
	 */
	//std::vector<float>& GetValues();

	/**
	 * Return true if the given DOF has influence in the position.
	 * @param node A pointer to the node to check.
	 * @param DOFIndex The index of the DOF.
	 */
	bool GetInfluence(Node* node, const int DOFIndex);

	/**
	 * Calculate the angle values using the actual actor pose.
	 */
	void CalcAnglesValues();

	/**
	 * Calculate the rotation axis for each DOF.
	 */
	void CalcDOFAxis();

	/**
	 * Get the node that includes the given node DOF.
	 * @param DOFIndex The index of the node DOF.
	 * @return A pointer to the node which includes the given DOF attribute. NULL if the node hasn't been found.
	 */
	Node* GetDOFNode(const int DOFIndex);

	/**
	 * Return a pointer to the actor to which this actor DOF belongs to.
	 * @return A pointer to the actor to which this actor DOF belongs to.
	 */
	Actor* GetActor() {
		return mActor;
	}

	/**
	 * Get the rotation axis for the given node DOF.
	 * @param DOFIndex The index of the node DOF.
	 * @return The axis vector for the given node DOF.
	 */
	RCore::Vector3 GetDOFAxis(const int DOFIndex);

	/**
	 * Apply the given DOF values to the actor.
	 * @param values An array with the values.
	 */
	void SetValues(const std::vector<float>& values);

	/**
	 * Return the index of the first DOF referred to any of the node's rotations.
	 * @param index The index of the node.
	 * @return The index to the first node DOF.
	 */
	int GetNode1stDOF(const int index) {
		return mNode1stDOF[index];
	}

private:
	/**
	 * Calculate the influence of each DOF in each node position and store it in the influence matrix.
	 */
	void CalcInfluence();

	RCore::TNMatrix<bool>	mInfluence;		/* The influence matrix. */
	std::vector<int>		mNode1stDOF;	/* Vector containing the index for the 1st DOF of each node. */
	int						mNumDOF;		/* The total number of DOFs. */
	Actor*					mActor;			/* The pointer to the Actor the DOFs are applied to. */
};

} // namespace