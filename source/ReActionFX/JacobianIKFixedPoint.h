#pragma once

#include <vector>

#include "RCore.h"

#include "JacobianIKConstraint.h"

namespace ReActionFX {

/**
 * The JacobianIKFixedPoint class.
 * Defines the constraints applied to an Actor using IK.
 * This constraint specifies a point where a node must be.
 */
class JacobianIKFixedPoint : public JacobianIKConstraint {
public:
	/**
	 * Default constructor.
	 */
	JacobianIKFixedPoint();

	/**
	 * Constructor.
	 * @param position The goal which the solver tries to reach.
	 */
	JacobianIKFixedPoint(const RCore::Vector3& position) {
		SetPosition(position);
	}

	/**
	 * Constructor giving the goal values.
	 * @param x The x component of the goal which the solver tries to reach.
	 * @param y The y component of the goal which the solver tries to reach.
	 * @param z The z component of the goal which the solver tries to reach.
	 */
	JacobianIKFixedPoint(const float x, const float y, const float z) {
		SetPosition(RCore::Vector3(x, y, z));
	}

	/**
	 * Destructor
	 */
	~JacobianIKFixedPoint() {}

	/**
	 * Clone the IK constraint.
	 * @return A pointer to a newly created exact copy of the IK constraint.
	 */
	JacobianIKConstraint* Clone() {
		return new JacobianIKFixedPoint(mPosition);
	}

	/**
	 * Update the jacobian ik constraint.
	 * @param timeDeltaInSeconds The time passed in seconds.
	 */
	void Update(const float timeDeltaInSeconds) {}

	/**
	 * Return the number of functions needed to apply the constraint.
	 * @return The number of functions.
	 */
	int GetNumFunc() const {
		return 3; // one function for each coordinate
	}

	/**
	 * Return the specified value for the functions.
	 * @param values An array where the values will be placed.
	 */
	void GetValues(std::vector<float>& values);

	/**
	 * Return the value for the functions using the actual state of the actor.
	 * @param results An array where the values will be placed.
	 */
	void GetResults(std::vector<float>& results);

	/**
	 * Calculate the constraint functions gradients and store them in the given matrix.
	 * @param matrix The matrix where the gradients are stored.
	 */
	void CalcGradient(RCore::NMatrix& matrix);

   /**
	* Sets the position where the node must be placed.
	* @param position The goal which the solver tries to reach.
	*/
	void SetPosition(const RCore::Vector3& position) {
		mPosition = position;
	}

private:
	RCore::Vector3 mPosition; /* The position where the node must be placed by IK */
};

} // namespace 