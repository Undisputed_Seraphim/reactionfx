#include "ExpressionPartController.h"

namespace ReActionFX {

// set the expression part to control
void ExpressionPartController::SetExpressionPart(ExpressionPart* expressionPart) {
	// disable manual weight control for the last controlled expression part if the controller is already assigned to one
	if (mExpressionPart)
		expressionPart->SetEnableManualMode(false);

	// assign the new expression part to the controller
	mExpressionPart = expressionPart;

	// enable manual weight control for the given expression part if it is valid
	if (mExpressionPart)
		mExpressionPart->SetEnableManualMode(true);
}

} // namespace