#include "FacialLayerPass.h"
#include "MotionLayerSystem.h"
#include "FacialMotion.h"
#include "Actor.h"
#include "FacialSetup.h"

using namespace RCore;

namespace ReActionFX {

// the main function that processes the pass
void FacialLayerPass::Process() {
	// get the facial setup of the actor
	Actor* actor = mMotionSystem->GetActor();
	int curLOD = actor->GetCurrentLOD();
	FacialSetup* facialSetup = actor->GetFacialSetup(curLOD).get();
	if (facialSetup == nullptr)
		return;

	// reset the weights
	for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
		// get the expression part
		ExpressionPart* expPart = facialSetup->GetExpressionPart(i);
		if (expPart->IsInManualMode()) continue;
		expPart->SetWeight(expPart->CalcZeroInfluenceWeight());
	}

	for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
		// get the expression part
		ExpressionPart* expPart = facialSetup->GetPhoneme(i);
		if (expPart->IsInManualMode()) continue;
		expPart->SetWeight(expPart->CalcZeroInfluenceWeight());
	}

	// get the root layer of the motion system
	MotionLayer* rootLayer = mMotionSystem->GetMotionTree();
	if (rootLayer == nullptr)
		return;


	// find the last layer
	MotionLayer* curLayer = rootLayer;
	while (curLayer->GetDestLayer() != nullptr)
		curLayer = curLayer->GetDestLayer();


	// now curLayer contains the bottom layer
	// we can now work our way up back to the root layer again
	while (curLayer) {
		// get the motion instance from the layer
		MotionInstance* motionInstance = curLayer->GetSource();

		// get the weight from the motion instance
		const float weight = motionInstance->GetWeight();
		const float finalWeight = RCore::CalcCosineInterpolationWeight(weight);

		// if we are dealing with a facial motion here
		if (motionInstance->GetMotion()->GetType() == FacialMotion::TYPE_ID) {
			// get the facial motion we are dealing with
			FacialMotion* motion = (FacialMotion*)motionInstance->GetMotion();

			// for all expression parts
			for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
				// get the expression part
				ExpressionPart* expPart = facialSetup->GetExpressionPart(i);

				// don't process expresion parts that are controlled manually
				if (expPart->IsInManualMode()) continue;

				// get the weight for this expression part at the given playback times
				const int motionPartNr = motion->FindExpMotionPart(expPart->GetID());
				float value;

				// if the motion part cannot be found, we 'disable' this slider
				if (motionPartNr == -1) {
					if (motionInstance->IsMixing())
						value = expPart->GetWeight();
					else
						value = expPart->CalcZeroInfluenceWeight();
				} else {
					// blend the weight
					auto keyTrack = motion->GetExpKeyTrack(motionPartNr);
					value = keyTrack.GetValueAtTime(motionInstance->_GetCurrentTime());
				}

				// calculate and set the new slider weight
				const float inWeight = expPart->GetWeight();
				const float newWeight = finalWeight * value + inWeight * (1.0f - finalWeight);
				expPart->SetWeight(newWeight);
			}


			// for all phonemes
			for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
				// get the expression part
				ExpressionPart* expPart = facialSetup->GetPhoneme(i);

				// don't process expresion parts that are controlled manually
				if (expPart->IsInManualMode()) continue;

				// get the weight for this expression part at the given playback times
				const int motionPartNr = motion->FindPhoMotionPart(expPart->GetID());
				float value;

				// if the motion part cannot be found, we 'disable' this slider
				if (motionPartNr == -1) {
					if (motionInstance->IsMixing())
						value = expPart->GetWeight();
					else
						value = expPart->CalcZeroInfluenceWeight();
				} else {
					// blend the weight
					auto keyTrack = motion->GetPhoKeyTrack(motionPartNr);
					value = keyTrack.GetValueAtTime(motionInstance->_GetCurrentTime());
				}

				// calculate and set the new slider weight
				const float inWeight = expPart->GetWeight();
				const float newWeight = finalWeight * value + inWeight * (1.0f - finalWeight);
				expPart->SetWeight(newWeight);
			}
		} else	// no facial motion
		{
			// for all expression partsfacialSetup->GetNumExpressionParts()
			for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
				// get the expression part
				ExpressionPart* expPart = facialSetup->GetExpressionPart(i);

				// don't process expresion parts that are controlled manually
				if (expPart->IsInManualMode()) continue;

				// calculate and set the new slider weight
				const float inWeight = expPart->GetWeight();
				const float newWeight = inWeight * (1.0f - finalWeight);
				expPart->SetWeight(newWeight);
			}

			// for all phonemes
			for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
				// get the expression part
				ExpressionPart* expPart = facialSetup->GetPhoneme(i);

				// don't process expresion parts that are controlled manually
				if (expPart->IsInManualMode()) continue;

				// calculate and set the new slider weight
				const float inWeight = expPart->GetWeight();
				const float newWeight = inWeight * (1.0f - finalWeight);
				expPart->SetWeight(newWeight);
			}
		}

		// go to the layer above the current one (towards the root layer)
		curLayer = curLayer->GetParent();
	}
}

} // namespace