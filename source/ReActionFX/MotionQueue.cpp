#include "MotionQueue.h"
#include "Actor.h"
#include "MotionInstance.h"
#include "MotionLayer.h"
#include "MotionSystem.h"

using namespace RCore;

namespace ReActionFX {

// constructor
MotionQueue::MotionQueue(Actor* actor, MotionSystem* motionSystem) {
	assert(actor != nullptr && motionSystem != nullptr);

	mActor = actor;
	mMotionSystem = motionSystem;
}


// remove a given entry from the queue
void MotionQueue::RemoveEntry(const int nr) {
	mMotionSystem->RemoveMotionInstance(mEntries[nr].mMotion);
	mEntries.erase(mEntries.begin() + nr);
}


// update the motion queue
void MotionQueue::Update() {
	// if there are entries in the queue
	if (GetNumEntries() == 0)
		return;

	// if there is only one entry in the queue, we can start playing it immediately
	if (!mMotionSystem->IsPlaying()) {
		// get the entry from the queue to play next
		MotionQueue::QueueEntry queueEntry = GetFirstEntry();

		// remove it from the queue
		RemoveFirstEntry();

		// start the motion on the queue
		mMotionSystem->StartMotion(queueEntry.mMotion, &queueEntry.mPlayInfo);

		// get out of this method, nothing more to do :)
		return;
	}

	// find the first non mixing motion
	MotionInstance* motionInst = mMotionSystem->FindFirstNonMixingMotionInst();

	// if there isn't a non mixing motion
	if (motionInst == nullptr) return;

	// start the next motion
	if (motionInst->HasEnded() || motionInst->IsStopping()) {
		// get the entry from the queue
		MotionQueue::QueueEntry queueEntry = GetFirstEntry();

		// remove it from the queue
		RemoveFirstEntry();

		// start the motion
		mMotionSystem->StartMotion(queueEntry.mMotion, &queueEntry.mPlayInfo);
	}
}

} // namespace