#include "ImageResChecker.h"

using namespace RCore;

namespace ReActionFX {

// extract the resolution
bool ImageResChecker::GetResolution(const std::string& filename, uint32_t& outWidth, uint32_t& outHeight) {
	std::ifstream file;

	// extract the resolutions
	file.open(filename + ".bmp", std::ios_base::in | std::ios_base::binary);
	if (file) {
		if (!GetBMPRes(file, outWidth, outHeight)) { file.close(); return false; }
		file.close();
		return true;
	}

	file.open(filename + ".jpg", std::ios_base::in | std::ios_base::binary);
	if (file) {
		if (!GetJPGRes(file, outWidth, outHeight)) { file.close(); return false; }
		file.close();
		return true;
	}

	file.open(filename + ".png", std::ios_base::in | std::ios_base::binary);
	if (file) {
		if (!GetPNGRes(file, outWidth, outHeight)) { file.close(); return false; }
		file.close();
		return true;
	}

	file.open(filename + ".tga", std::ios_base::in | std::ios_base::binary);
	if (file) {
		if (!GetTGARes(file, outWidth, outHeight)) { file.close(); return false; }
		file.close();
		return true;
	}

	file.open(filename + ".psd", std::ios_base::in | std::ios_base::binary);
	if (file) {
		if (!GetPSDRes(file, outWidth, outHeight)) { file.close(); return false; }
		file.close();
		return true;
	}

	// no texture found
	return false;
}


// extract the resolution from a bmp file
bool ImageResChecker::GetBMPRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight) {
	RCore::LOG("Extracting BMP resolution...");

	if (!file.seekg(18)) return false;
	if (!file.read((char*)&outWidth, 4)) return false;
	if (!file.read((char*)&outHeight, 4)) return false;

	RCore::LOG("Resolution : %dx%d", outWidth, outHeight);

	return true;
}


// extract the resolution of a tga file
bool ImageResChecker::GetTGARes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight) {
	RCore::LOG("Extracting TGA resolution...");

	if (!file.seekg(12)) return false;
	short width, height;
	if (!file.read((char*)&width, 2)) return false;
	if (!file.read((char*)&height, 2)) return false;
	RCore::LOG("Resolution : %dx%d", width, height);

	outWidth = width;
	outHeight = height;

	return true;
}


// extract the resolution from a jpg file
bool ImageResChecker::GetJPGRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight) {
	RCore::LOG("Extracting JPG resolution...");

	if (0xFF != file.get()) {
		if (0xD8 != file.get()) {
			RCore::LOG("no res found!");
			return false;
		}
	}

	char seg = 0xFF;
	while (file.get(seg) && (seg == 0xFF)) {
		if (seg != 0xFF) {
			if ((seg == 0xC0) || (seg == 0xC1)) {
				char dummy[3];
				file.read(dummy, 3);
				uint16_t width, height;
				file.read((char*)&height, 2);
				file.read((char*)&width, 2);

				// Convert the endianness.
#if defined (_MSC_VER)
				outWidth = _byteswap_ushort(width);
				outHeight = _byteswap_ushort(height);
#else
				outWidth = __builtin_bswap16(width);
				outHeight = __builtin_bswap16(height);
#endif
				RCore::LOG("Resolution : %dx%d", width, height);
				return true;
			} else {
				if (seg != 0x01 && seg != 0xD0 && seg != 0xD1 && seg != 0xD2 && seg != 0xD3 && seg != 0xD4 && seg != 0xD5 && seg != 0xD6 && seg != 0xD7) {
					uint16_t len;
					file.read((char*)&len, 2);

					// Convert the endianness.
#if defined (_MSC_VER)
					len = _byteswap_ushort(len);
#else
					len = __builtin_bswap16(len);
#endif
					file.seekg(len - 2);
					file.get(seg);
				} else
					seg = 0xFF;
			}
		}
	}

	RCore::LOG("no res found!");
	return false;
}


// extract the resolution from a png file
bool ImageResChecker::GetPNGRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight) {
	RCore::LOG("Extracting PNG resolution...");

	uint32_t width, height;
	if (!file.seekg(16)) return false;
	if (!file.read((char*)&width, 4)) return false;
	if (!file.read((char*)&height, 4)) return false;

	// convert from big to little endian
	// TODO: don't do this conversion on hardware which already is big endian
#if defined (_MSC_VER)
	outWidth = _byteswap_ulong(width);
	outHeight = _byteswap_ulong(height);
#else
	outWidth = __builtin_bswap32(width);
	outHeight = __builtin_bswap32(height);
#endif

	RCore::LOG("Resolution : %dx%d", outWidth, outHeight);

	return true;
}


// extract the resolution from a psd file
bool ImageResChecker::GetPSDRes(std::ifstream& file, uint32_t& outWidth, uint32_t& outHeight) {
	RCore::LOG("Extracting PSD resolution...");

	uint32_t width, height;
	if (!file.seekg(14)) return false;
	if (!file.read((char*)&height, 4)) return false;
	if (!file.read((char*)&width, 4)) return false;

	// convert from big to little endian
	// TODO: don't do this conversion on hardware which already is big endian
#if defined (_MSC_VER)
	outWidth = _byteswap_ulong(width);
	outHeight = _byteswap_ulong(height);
#else
	outWidth = __builtin_bswap32(width);
	outHeight = __builtin_bswap32(height);
#endif

	RCore::LOG("Resolution : %dx%d", outWidth, outHeight);

	return true;
}

} // namespace