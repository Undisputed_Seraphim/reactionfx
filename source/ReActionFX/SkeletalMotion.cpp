#include "SkeletalMotion.h"
#include "Node.h"
#include "Actor.h"

using namespace RCore;

namespace ReActionFX {

// get rid of all parts
void SkeletalMotion::DeleteParts() {
	for (auto* part : mParts) {
		delete part;
	}
	mParts.clear();
}

NodeTransform SkeletalMotion::CalcNodeTransform(Actor* actor, Node* node, const float timeValue) {
/*	NodeTransform result;

	// search for the motion link
	MotionLink* motionLink = node->SearchMotionLink(this);
	if (motionLink == NULL)
	{
		result.mPosition = node->GetLocalPos();
		result.mRotation = node->GetLocalRot();
		result.mScale	 = node->GetLocalScale();
		return result;
	}

	// get the current time and the motion part
	MotionPart* motionPart = motionLink->GetMotionPart();

	// get position
	if (motionPart->GetPosTrack().GetNumKeys() > 0)
		result.mPosition = motionPart->GetPosTrack().GetValueAtTime( timeValue );
	else
		result.mPosition = motionPart->GetPosePos();

	// get rotation
	if (motionPart->GetRotTrack().GetNumKeys() > 0)
		result.mRotation = motionPart->GetRotTrack().GetValueAtTime( timeValue );
	else
		result.mRotation = motionPart->GetPoseRot();

	// get scale
	if (motionPart->GetScaleTrack().GetNumKeys() > 0)
		result.mScale = motionPart->GetScaleTrack().GetValueAtTime( timeValue );
	else
		result.mScale = motionPart->GetPoseScale();

	// return the result
	return result;*/

	NodeTransform		result;
	RCore::Vector3		Pos;		/// The position.
	RCore::Vector3		Scl;		/// The scale.

	// search for the motion link
	MotionLink* motionLink = node->SearchMotionLink(this);
	if (motionLink == nullptr) {
		result.mPosition = node->GetLocalPos();
		result.mRotation = node->GetLocalRot();
		result.mScale = node->GetLocalScale();
		return result;
	}

	// get the current time and the motion part
	MotionPart* motionPart = motionLink->GetMotionPart();

	// get rotation
	if (motionPart->GetRotTrack().GetNumKeys() > 0)
		result.mRotation = motionPart->GetRotTrack().GetValueAtTime(timeValue);
	else
		result.mRotation = motionPart->GetPoseRot();

	// rotŰ
	bool forceUsePose = false;

	if (node->IsRootNode() || node->GetParent()->IsRootNode() || node->IsPropNode()) {
		forceUsePose = true;
	}

	RCore::Vector3 basePos, baseScl;
	if (forceUsePose) {
		basePos = motionPart->GetPosePos();
		baseScl = motionPart->GetPoseScale();
	} else {
		basePos = node->GetOrgPos();
		baseScl = node->GetOrgScale();
	}

	// get position
	if (motionPart->GetPosTrack().GetNumKeys() > 0) {
		Pos = motionPart->GetPosTrack().GetValueAtTime(timeValue);
		result.mPosition = (basePos + (Pos - motionPart->GetPosePos()));
	} else {
		result.mPosition = basePos;
	}

	// get scale
	if (motionPart->GetScaleTrack().GetNumKeys() > 0) {
		Scl = motionPart->GetScaleTrack().GetValueAtTime(timeValue);
		result.mScale = (baseScl + (Scl - motionPart->GetPoseScale()));
	} else {
		result.mScale = baseScl;
	}

	// return the result
	return result;
}


// creates the  motion links in the nodes of the given actor
void SkeletalMotion::CreateMotionLinks(Actor* actor, MotionInstance* instance) {
	//LOG("*** Creating links for motion '%s'", instance->GetMotion()->GetName());

	// build a quick array of nodes, copy the array info from the node array of the actor
	auto nodeList = actor->GetNodes();

	// try to link the motion parts with the nodes
	for (auto* part : mParts) {

		//bool hasKeys = (part->GetPosTrack().GetNumKeys()>0 || part->GetRotTrack().GetNumKeys()>0 || part->GetScaleTrack().GetNumKeys()>0);

		for (auto iter = nodeList.begin(); iter != nodeList.end(); iter++) {
			Node *node = *iter;

			// if the name of the motion part is the same as the name of the node
			if (strcmp(part->GetName(), node->GetName().c_str()) == 0) {
				node->AddMotionLink(MotionLink(part, instance));
				std::iter_swap(iter, nodeList.end() - 1);
				nodeList.pop_back();
				break;
			}
		}
	}
}


// get the maximum time value and update the stored one
void SkeletalMotion::UpdateMaxTime() {
	// for all motion parts
	for (auto* part : mParts) {
		// check the position track
		if (part->GetPosTrack().GetLastTime() > mMaxTime)
			mMaxTime = part->GetPosTrack().GetLastTime();

		// check the rotation track
		if (part->GetRotTrack().GetLastTime() > mMaxTime)
			mMaxTime = part->GetRotTrack().GetLastTime();

		// check the scale track
		if (part->GetScaleTrack().GetLastTime() > mMaxTime)
			mMaxTime = part->GetScaleTrack().GetLastTime();
	}
}


// make the motion loopable
void SkeletalMotion::MakeLoopable(const float fadeTime) {
	// make all tracks of all motin parts loopable
	for (auto* part : mParts) {
		part->GetPosTrack().MakeLoopable(fadeTime);
		part->GetRotTrack().MakeLoopable(fadeTime);
		part->GetScaleTrack().MakeLoopable(fadeTime);
	}

	// update the maximum time
	UpdateMaxTime();
}

} // namespace