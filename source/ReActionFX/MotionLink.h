#pragma once

#include "RCore.h"
#include "MotionInstance.h"
#include "MotionPart.h"

namespace ReActionFX {

/**
 * A motion link.
 * This class links the nodes with the motion parts.
 * Not directly by this class, but it helps forming the link, so that we know what MotionPart effects what Node.
 * MotionLinks are stored inside the Node objects.
 */
class MotionLink {
public:
	/**
	 * Constructor.
	 * @param part The motion part.
	 * @param instance The Motion instance.
	 */
	MotionLink(MotionPart* part, MotionInstance* instance) : mMotionPart(part), mMotionInstance(instance) {}

	/**
	 * Destructor.
	 */
	~MotionLink() {}

	/**
	 * Returns the motion part.
	 * @return The motion part.
	 */
	MotionPart* GetMotionPart() const {
		return mMotionPart;
	}

   /**
	* Returns the motion info.
	* @return The motion info.
	*/
	MotionInstance* GetMotionInstance() const {
		return mMotionInstance;
	}


private:
	MotionPart*		mMotionPart;		/* The motion part. */
	MotionInstance*	mMotionInstance;	/* The motion instance. */
};

} // namespace