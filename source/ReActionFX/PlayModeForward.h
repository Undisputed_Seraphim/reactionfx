#pragma once

#include "RCore.h"
#include "PlayMode.h"

namespace ReActionFX {

/**
 * The forward playback mode.
 * This playback mode plays the motion instance in a forward direction, so how you would expect if you
 * play a motion the normal way.
 */
class PlayModeForward : public PlayMode {
public:
	enum { TYPE_ID = 0x00700001 };

	/**
	 * Default constructor.
	 */
	PlayModeForward() : PlayMode() {}

	/**
	 * Destructor.
	 */
	~PlayModeForward() {}

	/**
	 * Returns the type identification number of the play mode class.
	 * @return The type identification number.
	 */
	int GetType() const { return TYPE_ID; }

	/**
	 * Gets the type as a description. This for example could be "PlayModeForward" or "PlayModePingPong".
	 * @return The string containing the type of the play mode.
	 */
	const char* GetTypeString() const { return "PlayModeForward"; }

	/**
	 * Update the motion instance time value and keep track of the current number loops.
	 * Motion instance's time value will be reset when the motion has ended and the loop number will
	 * be increased.
	 * @param timeDelta The time passed in seconds.
	 */
	void UpdateMotionInstance(const double timeDelta);

	/**
	 * Returns the start time of the motion, when it is looped.
	 * A forward playing mode would have a start time of 0, because when it passed the end of the motion, it
	 * should again start at the front. A backward playing motion however will need to return the maximum time
	 * of the motion, which is returned by GetMaxTime().
	 * @result The start time of the motion, when it is looped.
	 */
	float GetStartTime() const { return 0.0f; }
};

} // namespace