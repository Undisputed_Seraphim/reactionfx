#include "MeshDeformerStack.h"
#include "Mesh.h"
#include "Actor.h"

using namespace RCore;

namespace ReActionFX {

// destructor
MeshDeformerStack::~MeshDeformerStack() {
	// iterate through the deformers and delete their objects
	for (auto* deformer : mDeformers) {
		delete deformer;
	}

	// reset
	mMesh = nullptr;
}


void MeshDeformerStack::Update(Actor* actor, Node* node, const double timeDelta) {
	// if we have deformers in the stack
	if (mDeformers.size() > 0) {
		// reset all output vertex data to the original vertex data
		mMesh->ResetToOriginalData();

		// iterate through the deformers and update them
		for (auto deformer : mDeformers) {
			if (deformer->IsEnabled())
				deformer->Update(actor, node, timeDelta);
		}
	}
}


// reinitialize mesh deformers
void MeshDeformerStack::ReInitializeDeformers() {
	// if we have deformers in the stack
	// iterate through the deformers and reinitialize them
	for (auto deformer : mDeformers)
		deformer->ReInitialize();
}


MeshDeformerStack* MeshDeformerStack::Clone(Mesh* mesh, Actor* actor) {
	// create the clone passing the mesh pointer
	MeshDeformerStack* newStack = new MeshDeformerStack(mesh);

	// clone all deformers
	for (auto deformer : mDeformers)
		newStack->AddDeformer(deformer->Clone(mesh, actor));

	// return a pointer to the clone
	return newStack;
}


MeshDeformer* MeshDeformerStack::GetDeformer(const int nr) const {
	assert(nr < mDeformers.size());
	return mDeformers[nr];
}


// remove all the deformers of a given type
int MeshDeformerStack::RemoveAllDeformersByType(const int deformerTypeID) {
	int numRemoved = 0;
	for (int a = 0; a < mDeformers.size();) {
		MeshDeformer* deformer = mDeformers[a];
		if (deformer->GetType() == deformerTypeID) {
			RemoveDeformer(deformer);
			delete deformer;
			numRemoved++;
		} else
			a++;
	}

	return numRemoved;
}


// remove all the deformers
void MeshDeformerStack::RemoveAllDeformers() {
	for (auto deformer : mDeformers) {
		RemoveDeformer(deformer);
		delete deformer;
	}
}


// enabled or disable all controllers of a given type
int MeshDeformerStack::EnableAllDeformersByType(const int deformerTypeID, const bool enabled) {
	int numChanged = 0;
	for (auto deformer : mDeformers) {
		if (deformer->GetType() == deformerTypeID) {
			deformer->SetEnabled(enabled);
			numChanged++;
		}
	}

	return numChanged;
}

// check if the stack contains a deformer of a specified type
bool MeshDeformerStack::HasDeformerOfType(const int deformerTypeID) const {
	for (auto deformer : mDeformers) {
		if (deformer->GetType() == deformerTypeID)
			return true;
	}

	return false;
}

} // namespace