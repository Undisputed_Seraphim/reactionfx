#pragma once

#include <memory>
#include <unordered_map>
#include "RCore.h"
#include "Material.h"

namespace ReActionFX {

/**
 * The FX material class.
 * This class stores a couple of parameters, which each have a name and a value.
 * The types of parameters stored are: int parameters, float parameters, color parameters and string parameters.
 * The string parameters can be used to represent texture filenames.
 * FX materials can be exported from 3D Studio Max when using the "DirectX Material" of 3D Studio Max 6.
 * All FX parameters and their values can be setup by the artist, and are stored inside this class, which you can
 * use again to set the values for a given effect file that you are using to render some objects.
 */
class FXMaterial : public Material {
public:
	enum { TYPE_ID = 0x00000002 };

	/**
	 * The constructor.
	 * @param name The name of the material.
	 */
	FXMaterial(const std::string & name) : Material(name) {}

	/**
	 * The destructor.
	 */
	virtual ~FXMaterial() {}

	/**
	 * Get the unique material type ID.
	 * @result The material type identification number.
	 */
	virtual int GetType() const {
		return TYPE_ID;
	}

	/**
	 * Get the type identification string.
	 * This can be a description or the class name of the material.
	 * @result A pointer to the string containing the name.
	 */
	virtual const char* GetTypeString() const {
		return "FXMaterial";
	}

	/**
	 * Create a clone of the material.
	 * @result A smart-pointer to the cloned material.
	 */
	virtual std::shared_ptr<Material> Clone();

	/**
	 * Get a given integer parameter by its name.
	 * @param nr The index of the integer parameter to return.
	 * @return The given integer parameter.
	 */
	int GetIntParameter(const std::string &name) {
		return intParams[name];
	}

	/**
	 * Get a given float parameter by its name.
	 * @param nr The index of the float parameter to return.
	 * @return The given float parameter.
	 */
	float GetFloatParameter(const std::string &name) {
		return floatParams[name];
	}

	/**
	 * Get a given color parameter by its name.
	 * @param nr The index of the color parameter to return.
	 * @return The given color parameter.
	 */
	RCore::RGBAColor GetColorParameter(const std::string &name) {
		return colorParams[name];
	}

	/**
	 * Get a given string parameter by its name.
	 * @param nr The index of the string parameter to return.
	 * @return The given string parameter.
	 */
	std::string& GetStringParameter(const std::string &name) {
		return stringParams[name];
	}

	/**
	 * Get the number of float parameters.
	 * @return The number of float parameters.
	 */
	size_t GetNumFloatParameters() const {
		return floatParams.size();
	}

	/**
	 * Get the number of integer parameters.
	 * @return The number of integer parameters.
	 */
	size_t GetNumIntParameters() const {
		return intParams.size();
	}

	/**
	 * Get the number of color parameters.
	 * @return The number of color parameters.
	 */
	size_t GetNumColorParameters() const {
		return colorParams.size();
	}

	/**
	 * Get the number of string parameters.
	 * @return The number of string parameters.
	 */
	size_t GetNumStringParameters() const {
		return stringParams.size();
	}

	/**
	 * Add an integer parameter to the material.
	 * @param name The name of the parameter to add.
	 * @param value The integer value which will be assigned to the new parameter.
	 */
	void AddIntParameter(const std::string &name, const int value) {
		intParams.insert(std::make_pair(name, value));
	}

	/**
	 * Add an float parameter to the material.
	 * @param name The name of the parameter to add.
	 * @param value The float value which will be assigned to the new parameter.
	 */
	void AddFloatParameter(const std::string &name, const float value) {
		floatParams.insert(std::make_pair(name, value));
	}

	/**
	 * Add an color parameter to the material.
	 * @param name The name of the parameter to add.
	 * @param value The color value which will be assigned to the new parameter.
	 */
	void AddColorParameter(const std::string &name, const RCore::RGBAColor &value) {
		colorParams.insert(std::make_pair(name, value));
	}

	/**
	 * Add an string parameter to the material.
	 * @param name The name of the parameter to add.
	 * @param value The string value which will be assigned to the new parameter.
	 */
	void AddStringParameter(const std::string &name, const std::string &value) {
		stringParams.insert(std::make_pair(name, value));
	}

	/**
	 * Set the value of the given integer parameter by name.
	 * @param name The name of the integer parameter.
	 * @param value The integer value to set.
	 * @return Returns true when the parameter value has been set successfully. False is returned when there is no parameter with the specified name.
	 */
	bool SetIntParameter(const std::string &name, const int value);

   /**
	* Set the value of the given float parameter by name.
	* @param name The name of the float parameter.
	* @param value The floating point value to set.
	* @return Returns true when the parameter value has been set successfully. False is returned when there is no parameter with the specified name.
	*/
	bool SetFloatParameter(const std::string &name, const float value);

   /**
	* Set the value of the given color parameter by name.
	* @param name The name of the color parameter.
	* @param value The color value to set.
	* @return Returns true when the parameter value has been set successfully. False is returned when there is no parameter with the specified name.
	*/
	bool SetColorParameter(const std::string &name, const RCore::RGBAColor &value);

   /**
	* Set the value of the given string parameter by name.
	* @param name The name of the string parameter.
	* @param value The string value to set.
	* @return Returns true when the parameter value has been set successfully. False is returned when there is no parameter with the specified name.
	*/
	bool SetStringParameter(const std::string &name, const std::string &value);

   /**
	* Set the effect file name.
	* @param filename The filename of the effect file to use.
	*/
	void SetEffectFile(const std::string &filename) {
		mEffectFile = filename;
	}

	/**
	 * Get the effect file name.
	 * @return The filename of the effect file to use.
	 */
	const std::string& GetEffectFile() const {
		return mEffectFile;
	}

protected:
	std::unordered_map<std::string, std::string>		stringParams;
	std::unordered_map<std::string, RCore::RGBAColor>	colorParams;
	std::unordered_map<std::string, float>				floatParams;
	std::unordered_map<std::string, int>				intParams;
	std::string 										mEffectFile;	/* The effect filename. */
};

} // namespace