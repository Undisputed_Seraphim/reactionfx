#pragma once

#include <vector>

#include "RCore.h"

#include "Controller.h"
#include "JacobianIKSolver.h"

namespace ReActionFX {

/**
 * The jacobian ik controller class.
 * The jacobian pseudo inverse controller inherits from the base controller. It must be added as a postcontroller.
 * It applies the added constraints using jacobian pseudo inverse solver.
 */
class JacobianIKController : public Controller {
public:
	// the unique type ID of this controller
	enum { TYPE_ID = 0xCADA006 };

	/**
	 * Constructor.
	 * @param actor The actor to which the controller gets applied to.
	 */
	JacobianIKController(Actor* actor);

	/**
	 * Destructor.
	 */
	~JacobianIKController();

	/**
	 * Update the controller. This is the main method that performs the IK and updates the nodes.
	 * @param timeDeltaInSeconds The time passed in seconds, since the last update call.
	 */
	void Update(const float timeDeltaInSeconds);

	/**
	 * Clone this controller.
	 * @param actor The actor to apply the cloned version on.
	 * @result A pointer to the cloned controller.
	 */
	Controller* Clone(Actor* actor);

	/**
	 * Get access to the ik data.
	 * @return A pointer to the ik data.
	 */
	JacobianIKData* GetIKData() {
		return mData;
	}

	/**
	 * Return the actor instance node. It�s useful for applying constraints.
	 * @param name The name of the node to get.
	 * @return A pointer to the searched node.
	 **/
	Node* GetNodeByName(const char* name) {
		return mData->GetActorInstance()->GetNodeByName(name);
	}

	/**
	 * Get the unique controller type ID.
	 * @return The controller type identification number.
	 */
	int GetType() const {
		return TYPE_ID;
	}

	/**
	 * Get the type identification string.
	 * This can be a description or the class name of the controller.
	 * @return A pointer to the string containing the name.
	 */
	const char* GetTypeString() const {
		return "JacobianIKController";
	}

private:
	JacobianIKData*		mData;		/* The jacobian ik data, e.g. the constraints. */
	JacobianIKSolver*	mSolver;	/* The jacobian pseudo inverse solver. */
	std::vector<float>	mSolution;	/* The calculated solution from the last update call. */
};

} // namespace