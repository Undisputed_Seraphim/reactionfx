#include "NodePhysicsAttribute.h"

namespace ReActionFX {

NodeAttribute* NodePhysicsAttribute::Clone() {
	// create the clone
	NodePhysicsAttribute* clone = new NodePhysicsAttribute();

	// copy the attributes
	clone->mMass = mMass;
	clone->mPhysicsObject = (mPhysicsObject != nullptr) ? mPhysicsObject->Clone() : nullptr;

	// return the clone
	return clone;
};

void NodePhysicsAttribute::SetPhysicsObject(PhysicsObject* object) {
	if (mPhysicsObject)
		delete mPhysicsObject;

	mPhysicsObject = object;
}

} // namespace