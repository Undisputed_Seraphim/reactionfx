#include <algorithm>
#include "FacialSetup.h"
#include "ExpressionPart.h"

using namespace RCore;

namespace ReActionFX {

// destructor
FacialSetup::~FacialSetup() {
	RemoveAllExpressionParts();
	RemoveAllPhonemes();
}


// remove an expression part
void FacialSetup::RemoveExpressionPart(const int nr, const bool delFromMem) {
	if (delFromMem)
		delete mExpressionParts[nr];

	mExpressionParts.erase(mExpressionParts.begin() + nr);
}


// remove an expression part
void FacialSetup::RemoveExpressionPart(ExpressionPart* part, const bool delFromMem) {
	std::remove(mExpressionParts.begin(), mExpressionParts.end(), part);

	if (delFromMem && part != nullptr)
		delete part;
}


// remove all expression parts
void FacialSetup::RemoveAllExpressionParts() {
	// remove all expression parts
	for (auto* expressionPart : mExpressionParts) {
		delete expressionPart;
	}
	mExpressionParts.clear();
	mExpressionParts.shrink_to_fit();
}


// get an expression part by ID
ExpressionPart* FacialSetup::GetExpressionPartByID(const int id) const {
	auto it = std::find_if(mExpressionParts.begin(), mExpressionParts.end(), [&id] (const ExpressionPart* part) {
		return part->GetID() == id;
	});

	return (it == mExpressionParts.end()) ? nullptr : *(it);
}


// get an expression part by ID
int FacialSetup::GetExpressionPartNumberByID(const int id) const {
	auto it = std::find_if(mExpressionParts.begin(), mExpressionParts.end(), [&id] (const ExpressionPart* part) {
		return part->GetID() == id;
	});

	return (it == mExpressionParts.end()) ? -1 : (mExpressionParts.begin() - it);
}

//-----------------

// remove a phoneme
void FacialSetup::RemovePhoneme(const int nr, const bool delFromMem) {
	if (delFromMem)
		delete mPhonemes[nr];

	mPhonemes.erase(mPhonemes.begin() + nr);
}


// remove a phoneme
void FacialSetup::RemovePhoneme(ExpressionPart* phoneme, const bool delFromMem) {
	std::remove(mPhonemes.begin(), mPhonemes.end(), phoneme);

	if (delFromMem && phoneme != nullptr)
		delete phoneme;
}


// remove all phonemes
void FacialSetup::RemoveAllPhonemes() {
	while (mPhonemes.size()) {
		delete mPhonemes.back();
		mPhonemes.pop_back();
	}
	for (auto* phoneme : mPhonemes) {
		delete phoneme;
	}
	mPhonemes.clear();
	mPhonemes.shrink_to_fit();
}


// get a phoneme by ID
ExpressionPart* FacialSetup::GetPhonemeByID(const int id) const {
	auto it = std::find_if(mPhonemes.begin(), mPhonemes.end(), [&id] (const ExpressionPart* part) {
		return part->GetID() == id;
	});

	return (it == mPhonemes.end()) ? nullptr : *(it);
}


// get a phoneme by ID
int FacialSetup::GetPhonemeNumberByID(const int id) const {
	auto it = std::find_if(mPhonemes.begin(), mPhonemes.end(), [&id] (const ExpressionPart* part) {
		return part->GetID() == id;
	});

	return (it == mPhonemes.end()) ? -1 : (it - mPhonemes.begin());
}

} // namespace