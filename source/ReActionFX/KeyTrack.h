#pragma once

#include <algorithm>

#include "RCore.h"

#include "KeyFrame.h"
#include "Interpolator.h"
#include "KeyFrameFinder.h"

namespace ReActionFX {

/**
 * The keyframe track.
 * This is a class holding a set of keyframes and contains methods for automatic playback.
 */
template <class ReturnType, class StorageType>
class KeyTrack {
public:
	/// Keyframe track loop modes.
	enum EKeyTrackLoopMode {
		KEYTRACK_NO_LOOP = 0x000000,	/*No looping, stops when track finished. */
		KEYTRACK_LOOP = 0x000001		/*Loop when track finished. */
	};

	/**
	 * Default constructor.
	 */
	KeyTrack()
		: mInitialized(false), mCurrentTime(0.0f), mLoopMode(KEYTRACK_LOOP), mPlaySpeed(1.0f),
		mIsPlaying(false), mForward(true), mInterpolator(nullptr) {}

	/**
	 * Constructor.
	 * @param nrKeys The number of keyframes which the keytrack contains (preallocates this amount of keyframes).
	 */
	KeyTrack(const int nrKeys) :mInitialized(false), mLoopMode(KEYTRACK_LOOP), mCurrentTime(0.0f), mPlaySpeed(1.0f),
		mIsPlaying(false), mForward(true), mInterpolator(nullptr) {
		mKeys.Resize(nrKeys);
	}

	/**
	 * Destructor.
	 */
	~KeyTrack();

	/**
	 * Set the keytrack loop mode.
	 * @param mode The looping mode to use.
	 */
	void SetLoopMode(EKeyTrackLoopMode mode) {
		mLoopMode = mode;
	}

	/**
	 * Get the current loop mode of the keytrack.
	 * @result The loop mode.
	 */
	EKeyTrackLoopMode GetLoopMode() {
		return mLoopMode;
	}

	/**
	 * Start automatic playback.
	 */
	void Play() {
		mIsPlaying = true;
	}

	/**
	 * Stop automatic playback.
	 */
	void Stop() {
		mIsPlaying = false;
	}

	/**
	 * Rewind the keyframer to the first key.
	 */
	void Rewind() {
		mCurrentTime = GetFirstTime();
	}

	/**
	 * Set the playback speed, where 1.0 is normal playback speed. If you want to play backwards, do NOT use negative values, but use SetPlayForwards(false) instead.
	 * @param speed The playback speed.
	 */
	void SetPlaySpeed(const float speed) {
		assert(speed >= 0.0f);
		mPlaySpeed = speed;
	}

	/**
	 * Set the play direction.
	 * @param forward If set to true, we will play forwards, otherwise backwards.
	 */
	void SetPlayForwards(const bool forward) {
		mForward = forward;
	}

	/**
	 * Check if we are automatically playing or not.
	 * @result Returns true when we are playing, otherwise false.
	 */
	bool GetPlaying() {
		return mIsPlaying;
	}

	/**
	 * Check if we are playing forwards or backwards.
	 * @result Returns true when we are playing forwards, otherwise false.
	 */
	bool GetPlayForwards() {
		return mForward;
	}

	/**
	 * Get the play speed factor, where 1.0 would be normal speed and 2.0 would be
	 * twice the original speed and 0.2 would be slowmotion.
	 * @result The playspeed factor.
	 */
	float GetPlaySpeed() {
		return mPlaySpeed;
	}

	/**
	 * Return the current time in the automatic playback.
	 * @result The current time, in seconds.
	 */
	float GetTime() {
		return mCurrentTime;
	}

	/**
	 * Initialize all keyframes in this keytrack.
	 * Call this after all keys are added and setup, otherwise the interpolation won't work and nothing will happen.
	 */
	void Init();

	/**
	 * Update the playback time if in automatic playback mode.
	 * @param timePassed The time passed, in seconds.
	 */
	void Update(const double timePassed);

	/**
	 * Add a key to the track (at the back).
	 * @param time The time of the keyframe, in seconds.
	 * @param value The value of the keyframe.
	 */
	void AddKey(const float time, const ReturnType& value);

   /**
	* Adds a key to the track, and automatically detects where to place it.
	* NOTE: you will have to call the Init() method again when you finished adding keys.
	* @param time The time value of the keyframe, in seconds.
	* @param value The value of the keyframe at that time.
	*/
	void AddKeySorted(const float time, const ReturnType& value);

   /**
	* Remove a given keyframe, by number.
	* Do not forget that you have to re-initialize the keytrack after you have removed one or more
	* keyframes. The reason for this is that the possible tangents inside the interpolators have to be
	* recalculated when the key structure has changed.
	* @param keyNr The keyframe number, must be in range of [0..GetNumKeys()-1].
	*/
	void RemoveKey(const int keyNr) {
		mKeys.erase(mKeys.begin() + keyNr);
	}

   /**
	* clear all keys.
	*/
	void ClearKeys() {
		mKeys.clear();
		mInitialized = false;
	}

/**
 * Set the current play time, so the offset in the animation.
 * @param newTimeValue The new time value, in seconds.
 */
	void SetTime(const float newTimeValue);

	/**
	 * Set the current play time, so the offset in the animation. Normalized in this
	 * case means from 0.0 to 1.0. The maximum time of this animation is then 1.0.
	 * @param newTimeValue The new normalized time value, in seconds.
	 */
	void SetTimeNormalized(const float normalizedTimeValue) {
		mCurrentTime = normalizedTimeValue * mKeys.back().GetTime();
	}

	/**
	 * Return the current time in the automatic playback. Normalized in this
	 * case means from 0.0 to 1.0. The maximum time of this animation is then 1.0.
	 * @result The normalized current time, in seconds.
	 */
	float GetTimeNormalized() {
		return mCurrentTime / mKeys.back().GetTime();
	}

	/**
	 * Get the interpolated value at the current time (use this when using automatic playback).
	 * @result Returns the value at the current time.
	 */
	ReturnType GetCurrentValue() {
		return GetValueAtTime(mCurrentTime);
	}

	/**
	 * Return the interpolated value at the specified time.
	 * @param currentTime The time in seconds.
	 * @result Returns the value at the specified time.
	 */
	ReturnType GetValueAtTime(const float currentTime);

	/**
	 * Get a given keyframe.
	 * @param nr The index, so the keyframe number.
	 * @result A pointer to the keyframe.
	 */
	KeyFrame<ReturnType, StorageType>* GetKey(const int nr) {
		assert(nr < mKeys.size());
		return &mKeys[nr];
	}

	/**
	 * Returns the first keyframe.
	 * @result A pointer to the first keyframe.
	 */
	KeyFrame<ReturnType, StorageType>* GetFirstKey() {
		if (mKeys.empty()) return nullptr;
		return &mKeys.front();
	}

   /**
	* Returns the last keyframe.
	* @result A pointer to the last keyframe.
	*/
	KeyFrame<ReturnType, StorageType>* GetLastKey() {
		if (mKeys.empty()) return nullptr;
		return &mKeys.back();
	}

   /**
	* Returns the time value of the first keyframe.
	* @result The time value of the first keyframe, in seconds.
	*/
	float GetFirstTime() {
		if (mKeys.empty()) return 0.0f;
		return mKeys.front().GetTime();
	}

   /**
	* Return the time value of the last keyframe.
	* @result The time value of the last keyframe, in seconds.
	*/
	float GetLastTime() {
		if (mKeys.empty()) return 0.0f;
		return mKeys.back().GetTime();
	}

   /**
	* Returns the number of keyframes in this track.
	* @result The number of currently stored keyframes.
	*/
	size_t GetNumKeys() {
		return mKeys.size();
	}

	/**
	 * Find a key at a given time.
	 * @param curTime The time of the key.
	 * @return A pointer to the keyframe.
	 */
	KeyFrame<ReturnType, StorageType>* FindKey(const float curTime);

   /**
	* Find a key number at a given time.
	* You will need to interpolate between this and the next key.
	* @param curTime The time to retreive the key for.
	* @result Returns the key number or -1 when not found.
	*/
	int FindKeyNumber(const float curTime) {
		return KeyFrameFinder<ReturnType, StorageType>::FindKey(curTime, this);
	}

	/**
	 * Set the interpolator to be used for interpolation of the keyframes.
	 * The existing interpolator for this keytrack will automatically be deleted from memory.
	 * @param newInterpolator The interpolator to use.
	 */
	void SetInterpolator(Interpolator<ReturnType, StorageType>* newInterpolator);

	/**
	 * Get the currently used interpolator.
	 * @result A pointer to the interpolator.
	 */
	Interpolator<ReturnType, StorageType>* GetInterpolator() {
		return mInterpolator;
	}

	/**
	 * Make the keytrack loopable, by adding a new keyframe at the end of the keytrack.
	 * This added keyframe will have the same value as the first keyframe.
	 * @param fadeTime The relative offset after the last keyframe. If this value is 0.5, it means it will add
	 *                 a keyframe half a second after the last keyframe currently in the keytrack.
	 */
	void MakeLoopable(const float fadeTime = 0.3f);

protected:
	std::vector<KeyFrame<ReturnType, StorageType>>		mKeys;			/* The collection of keys which form the track. */
	Interpolator<ReturnType, StorageType>*				mInterpolator;	/* The interpolator to use. */
	EKeyTrackLoopMode		mLoopMode;			/* The loop mode [default=LOOP]. */
	float					mCurrentTime;		/* The current time [default=0.0f]. */
	float					mPlaySpeed;			/* Playback speed [default=1.0f]. */
	bool					mForward;			/* Is playing forward? */
	bool					mIsPlaying;			/* Automatic playing enabled? [default=false]. */
	bool					mInitialized;		/* Are we initialized? */

	/**
	 * Get the time value when looping is enabled. You input a value and it outputs the value in range of the keys.
	 * @param newTime The new time value you want to use.
	 * @param minTime The minimum time of the keyframes.
	 * @param maxTime The maximum time of the keyframes.
	 * @param forward Play forwards?
	 * @result The time value between 'minTime' and 'maxTime'.
	 */
	float GetLoopTime(const float newTime, const float minTime, const float maxTime, const bool forward);
};


template <class ReturnType, class StorageType>
KeyTrack<ReturnType, StorageType>::~KeyTrack() {
	ClearKeys();

	if (mInterpolator != nullptr)
		delete mInterpolator;
}


template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::Init() {
	// check all key time values, so we are sure the first key start at time 0
	if (mKeys.size() > 0) {
		// get the time value of the first key, which is our minimum time
		float minTime = mKeys[0].GetTime();

		// if it's not equal to zero, we have to correct it (and all other keys as well)
		if (minTime > 0) {
			std::for_each(mKeys.begin(), mKeys.end(), [minTime] (KeyFrame<ReturnType, StorageType> &key) {
				key.SetTime(key.GetTime() - minTime);
			});
		}
	}

	// initialize the interpolator
	if (mInterpolator)
		mInterpolator->Init();

	mInitialized = true;
}


template <class ReturnType, class StorageType>
inline void KeyTrack<ReturnType, StorageType>::AddKey(const float time, const ReturnType& value) {
#ifdef _DEBUG
	if (mKeys.size() > 0)
		assert(time >= mKeys.back().GetTime());
#endif

	// you have to re-init after you added a key
	mInitialized = false;
	// not the first key, so add on the end
	mKeys.push_back(KeyFrame<ReturnType, StorageType>(time, value));
}


template <class ReturnType, class StorageType>
inline KeyFrame<ReturnType, StorageType>* KeyTrack<ReturnType, StorageType>::FindKey(const float curTime) {
	// find the key number
	const int keyNumber = KeyFrameFinder<ReturnType, StorageType>::FindKey(curTime, this);

	// if no key was found
	if (keyNumber == -1)
		return nullptr;

	// return the pointer to the key
	return mKeys[keyNumber];
}


// update playback time etc
template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::Update(const double timePassed) {
	// if we aren't in automatic playback mode, we have nothing to do here! :)
	if (!mIsPlaying) return;

	// calculate the new delta time
	float deltaTime = (timePassed * mPlaySpeed);

	// if we are playing backwards
	if (!mForward)
		deltaTime = -deltaTime;

	// calculate the new current time, however it can still be out of range
	float newTime = mCurrentTime + deltaTime;

	// set the new curent time, automatically clamps given timevalue in a valid range
	SetTime(newTime);
}


// returns the time when looping, it kindof recursively gets the new time
// imagine there is one frame which takes 10 seconds long, while the total track animation time
// is only 7 seconds. this function will return 3 in this case, since after the 10 second frame
// the animation should be at 3 seconds, and cannot just be set to 0, because that will introduce
// out-of-sync animations
template <class ReturnType, class StorageType>
float KeyTrack<ReturnType, StorageType>::GetLoopTime(const float newTime, const float minTime, const float maxTime, const bool forward) {
	float range = maxTime - minTime; assert(range >= 0.0f);
	if (range == 0.0f) return 0.0f;

	float result = std::abs(newTime);

	while (result > range)
		result -= range;

	if (forward)
		return result;
	else
		return maxTime + result;
}


// set the new time value
template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::SetTime(const float newTimeValue) {
	float minTime = GetFirstTime();
	float maxTime = GetLastTime();
	float newTime = newTimeValue;

	// if we are in a normal looping mode
	if (mLoopMode == KEYTRACK_LOOP) {
		if (mForward) {
			if (newTime > maxTime)
				newTime = GetLoopTime(newTime, minTime, maxTime, true);

			if (newTime < minTime)
				newTime = minTime;
		} else {
			if (newTime < minTime)
				newTime = GetLoopTime(newTime, minTime, maxTime, false);

			if (newTime > maxTime)
				newTime = maxTime;
		}
	} else {
		// clamp the time into valid ranges
		if (newTime < minTime)
			newTime = minTime;
		else
			if (newTime > maxTime)
				newTime = maxTime;
	}

	// set the current time
	mCurrentTime = newTime;
}


// returns the interpolated value at a given time
template <class ReturnType, class StorageType>
ReturnType KeyTrack<ReturnType, StorageType>::GetValueAtTime(const float currentTime) {
	assert(mInterpolator != nullptr);
	assert(currentTime >= 0.0);
	assert(mKeys.size() > 0);

	// find the first key to start interpolating from (between this one and the next)
	const int keyNumber = KeyFrameFinder<ReturnType, StorageType>::FindKey(currentTime, this);

	// if no key could be found
	if (keyNumber == -1) {
		// if there are no keys at all, simply return an empty object
		if (mKeys.size() == 0) {
			// return an empty object
			//ReturnType result;
			//memset(&result, 0, sizeof(ReturnType));
			//return result;
			return ReturnType();
		}

		// return the last key
		return mKeys.back().GetValue();
	}

	// check if we didn't reach the end of the track
	if ((keyNumber + 1) > (mKeys.size() - 1)) {
		return mKeys.back().GetValue();
	}

	// calculate the time value in range of [0..1]
	float timeValue = (currentTime - mKeys[keyNumber].GetTime()) / (mKeys[keyNumber + 1].GetTime() - mKeys[keyNumber].GetTime());

	// interpolate between the two keys
	return mInterpolator->Interpolate(keyNumber, timeValue);
}


// adds a keyframe with automatic sorting support
template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::AddKeySorted(const float time, const ReturnType& value) {
	// if there are no keys yet, add it
	if (mKeys.size() == 0) {
		mKeys.push_back(KeyFrame<ReturnType, StorageType>(time, value));
		return;
	}

	// get the keyframe time
	const float keyTime = time;

	// if we must add it at the end
	if (keyTime >= mKeys.back().GetTime()) {
		mKeys.push_back(KeyFrame<ReturnType, StorageType>(time, value));
		return;
	}

	// if we have to add it in the front
	if (keyTime < mKeys.front().GetTime()) {
		mKeys.insert(mKeys.begin(), KeyFrame<ReturnType, StorageType>(time, value));
		return;
	}

	// quickly find the location to insert, and insert it
	int place = KeyFrameFinder<ReturnType, StorageType>::FindKey(keyTime, this);
	mKeys.insert(mKeys.begin() + place + 1, KeyFrame<ReturnType, StorageType>(time, value));
}


template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::SetInterpolator(Interpolator<ReturnType, StorageType>* newInterpolator) {
	// get rid of the existing interpolator
	if (mInterpolator)
		delete mInterpolator;

	// set the new interpolator
	mInterpolator = newInterpolator;
	mInterpolator->SetKeyTrack(this);
}


template <class ReturnType, class StorageType>
void KeyTrack<ReturnType, StorageType>::MakeLoopable(const float fadeTime) {
	assert(fadeTime > 0);

	if (mKeys.size() == 0)
		return;

	float lastTime = GetLastKey()->GetTime();
	ReturnType firstValue = GetFirstKey()->GetValue();
	AddKey(lastTime + fadeTime, firstValue);

	// re-init the track
	Init();
}

} // namespace
