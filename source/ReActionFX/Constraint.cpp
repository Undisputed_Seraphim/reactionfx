#include "PositionConstraint.h"
#include "RotationConstraint.h"
#include "ScaleConstraint.h"
#include "TransformConstraint.h"

namespace ReActionFX {

void PositionConstraint::Update(const float timeDeltaInSeconds) {
	assert(mTarget != nullptr);
	assert(mNode != nullptr);

	mNode->SetLocalPos(mTarget->GetLocalPos());
}

void RotationConstraint::Update(const float timeDeltaInSeconds) {
	assert(mTarget != nullptr);
	assert(mNode != nullptr);

	// damped following
	// if it is directly at the target, so without any delays, then the damp value is 0
	// otherwise the constraint has no influence, so damping is 1
	mNode->SetLocalRot(mNode->GetLocalRot().Slerp(mTarget->GetLocalRot(), 1.0f - mDamping));
}

void ScaleConstraint::Update(const float timeDeltaInSeconds) {
	assert(mTarget != nullptr);
	assert(mNode != nullptr);

	mNode->SetLocalScale(mTarget->GetLocalScale());
}

void TransformConstraint::Update(const float timeDeltaInSeconds) {
	assert(mTarget != nullptr);
	assert(mNode != nullptr);

	mNode->SetLocalPos(mTarget->GetLocalPos());
	mNode->SetLocalRot(mTarget->GetLocalRot());
	mNode->SetLocalScale(mTarget->GetLocalScale());
}

} // namespace