#pragma once

#include "RCore.h"

#include "Interpolator.h"

namespace ReActionFX {

/**
 * The Bezier interpolator class.
 * This class will interpolate on a bezier curve, which does not go
 * through the control points. The advantage of this is that it will result in very smooth results.
 */
template <class ReturnType, class StorageType>
class BezierInterpolator : public Interpolator<ReturnType, StorageType> {
public:
	// the type ID, returned by GetType()
	enum { TYPE_ID = 0x00000002 };

	/**
	 * Default Constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	BezierInterpolator() : Interpolator<ReturnType, StorageType>(), mControlPoints(nullptr), mNumControlPoints(0) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	BezierInterpolator(KeyTrack<ReturnType, StorageType>* interpolationSource)
		: Interpolator<ReturnType, StorageType>(interpolationSource), mControlPoints(nullptr), mNumControlPoints(0) {}

	/**
	 * Destructor.
	 */
	~BezierInterpolator();

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	virtual ReturnType Interpolate(const int startKey, const double timeValue) const;

	/**
	 * Initialize the interpolator.
	 */
	virtual void Init();

	/**
	 * Returns the unique interpolator type ID.
	 * This can be used to determine what interpolator is used.
	 * @result The unique type identification number of this interpolator class.
	 */
	virtual int GetType() const { return TYPE_ID; }

	/**
	 * Returns the type string of this interpolator class.
	 * This can be the class name, or a description of the interpolator.
	 * @result The string containing the description or class name of this interpolator.
	 */
	virtual const char* GetTypeString() const { return "BezierInterpolator"; }

	/**
	 * Returns the given control point for a given key with both read and write access.
	 * When this method detects that there isn't any control point information allocated yet, it will
	 * automatically allocate new memory. So you do not have to bother about memory management.
	 * The number of control points is always equal to the number which is returned by
	 * the method KeyTrack<ReturnType, StorageType>::GetNumKeys().
	 * @param keyNr The key number to get the control point for.
	 */
	inline ReturnType& GetControlPoint(const int keyNr);


protected:
	ReturnType*	mControlPoints;		/* The control points. */
	int			mNumControlPoints;	/* The total number of control points. */
};


/**
 * The bezier interpolator class used for quaternions.
 * The reason why there is a special class for quaternions is that they
 * need some additional checks in order to perform the interpolation correctly.
 */
template <class StorageType>
class BezierQuaternionInterpolator : public BezierInterpolator<RCore::Quaternion, StorageType> {

public:
	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	BezierQuaternionInterpolator() : BezierInterpolator<RCore::Quaternion, StorageType>(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	BezierQuaternionInterpolator(KeyTrack<RCore::Quaternion, StorageType>* interpolationSource) : BezierInterpolator<RCore::Quaternion, StorageType>(interpolationSource) {}

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	RCore::Quaternion Interpolate(const int startKey, const double timeValue) const;
};



// destructor
template <class ReturnType, class StorageType>
BezierInterpolator<ReturnType, StorageType>::~BezierInterpolator() {
	if (mControlPoints)
		free(mControlPoints);
}


// initialize the interpolator
template <class ReturnType, class StorageType>
void BezierInterpolator<ReturnType, StorageType>::Init() {
	// allocate the new control points
	const int numKeys = this->mInterpolationSource->GetNumKeys();
	mControlPoints = (ReturnType*)realloc(mControlPoints, sizeof(ReturnType) * numKeys);
	mNumControlPoints = numKeys;

	// setup temp control points
	//for (int i=0; i<numKeys; i++)
	//mControlPoints[i] = mInterpolationSource->GetKey(i)->GetValue();
}


template <class ReturnType, class StorageType>
inline ReturnType& BezierInterpolator<ReturnType, StorageType>::GetControlPoint(const int keyNr) {
	assert(this->mInterpolationSource);
	assert(keyNr < this->mInterpolationSource->GetNumKeys());

	// if there is no control point information or the number of control points is different from the number of keys
	if (mControlPoints == nullptr || mNumControlPoints != this->mInterpolationSource->GetNumKeys()) {
		// (re)allocate the data, so we have enough control points
		mControlPoints = (ReturnType*)realloc(mControlPoints, sizeof(ReturnType) * this->mInterpolationSource->GetNumKeys());

		// update the number of TCBs
		mNumControlPoints = this->mInterpolationSource->GetNumKeys();
	}

	return mControlPoints[keyNr];
}


// the bezier interpolation
template <class ReturnType, class StorageType>
ReturnType BezierInterpolator<ReturnType, StorageType>::Interpolate(const int startKey, const double timeValue) const {
	double t = timeValue;
	double t2 = timeValue * timeValue;
	double t3 = t2 * timeValue;

	return (-1 * t3 + 3 * t2 + -3 * t + 1) * this->mInterpolationSource->GetKey(startKey)->GetValue() +
		(3 * t3 + -6 * t2 + 3 * t) * mControlPoints[startKey] +
		(-3 * t3 + 3 * t2) * mControlPoints[startKey + 1] +
		(t3)* this->mInterpolationSource->GetKey(startKey + 1)->GetValue();
}


// the interpolation method
template <class StorageType>
RCore::Quaternion BezierQuaternionInterpolator<StorageType>::Interpolate(const int startKey, const double timeValue) const {
	// get the quaternions
	RCore::Quaternion a = this->mInterpolationSource->GetKey(startKey)->GetValue();
	RCore::Quaternion b = this->mInterpolationSource->GetKey(startKey + 1)->GetValue();

	// check if both quaternions are on the same hypersphere or not, if not, invert one
	if (a.Dot(b) < 0.0)
		a = -a;

	double t = timeValue;
	double t2 = timeValue * timeValue;
	double t3 = t2 * timeValue;

	return	((-1 * t3 + 3 * t2 + -3 * t + 1) * a.LogN() +
		(3 * t3 + -6 * t2 + 3 * t) * this->mControlPoints[startKey] +
			 (-3 * t3 + 3 * t2) * this->mControlPoints[startKey + 1] +
			 (t3)* b.LogN()).Exp().Normalize();
}

} // namespace
