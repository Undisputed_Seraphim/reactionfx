#include "MotionSystem.h"
#include "Actor.h"

using namespace RCore;

namespace ReActionFX {

// constructor
MotionSystem::MotionSystem(Actor* actor) {
	assert(actor != nullptr);

	mActor = actor;
	mMotionQueue = nullptr;

	// create the motion queue
	mMotionQueue = new MotionQueue(mActor, this);
}


// destructor
MotionSystem::~MotionSystem() {
	// delete the motion infos
	for (auto* instance : mMotionInstances) {
		delete instance;
	}

	// get rid of the motion queue
	if (mMotionQueue != nullptr)
		delete mMotionQueue;
}


// play a given motion on an actor, and return the instance
MotionInstance* MotionSystem::PlayMotion(Motion* motion, PlayBackInfo* info) {
	// check some things
	assert(motion != nullptr);
	assert(info != nullptr);

	if (motion == nullptr)
		return nullptr;
/*
	// if we want to play a motion which will loop forever (so never ends) and we want to put it on the queue
	if (info->mNumLoops==FOREVER && info->mPlayNow==false)
	{
		// if there is already a motion on the queue, this means the queue would end up in some kind of deadlock
		// because it has to wait until the current motion is finished with playing, before it would start this motion
		// and since that will never happen, the queue won't be processed anymore...
		// so we may simply not allow this to happen.
		if (mMotionQueue->GetNumEntries() > 0)
			throw Exception("Cannot schedule this LOOPING motion to be played later, because there are already motions queued. If we would put this motion on the queue, all motions added later on to the queue will never be processed because this motion is a looping (so never ending) one.", HERE);
	}*/

	// make sure we always mix when using additive blending
	if (info->mBlendMode == BLENDMODE_ADDITIVE && info->mMix == false) {
		assert(1 == 0);	// this shouldn't happen actually, please make sure you always mix additive motions
		info->mMix = true;
	}

	// create the motion instance and add the motion info the this actor
	MotionInstance* motionInst = CreateMotionInstance(motion, info);

	// if we want to play it immediately (so if we do NOT want to schedule it for later on)
	if (info->mPlayNow) {
		// start the motion for real! :)
		StartMotion(motionInst, info);
	} else {
		// schedule the motion, by adding it to the back of the motion queue
		mMotionQueue->AddEntry(MotionQueue::QueueEntry(motionInst, info));
		motionInst->Pause();
	}

	// return the pointer to the motion info
	return motionInst;
}


// create the motion instance and add the motion info the this actor
MotionInstance* MotionSystem::CreateMotionInstance(Motion* motion, PlayBackInfo* info) {
	// create the motion instance
	MotionInstance* motionInst = new MotionInstance(motion, mActor);

	// set the motion instance properties
	motionInst->SetPlayMode(info->GetPlayMode());
	motionInst->SetFadeTime(info->mBlendOutTime);
	motionInst->SetMixMode(info->mMix);
	motionInst->SetMaxLoops(info->mNumLoops);
	motionInst->SetBlendMode(info->mBlendMode);
	motionInst->SetPlaySpeed(info->mPlaySpeed);
	motionInst->SetWeight(info->mTargetWeight, info->mBlendInTime);
	motionInst->SetPriorityLevel(info->mPriorityLevel);
	motionInst->SetPlayMask(info->mPlayMask);
	motionInst->SetRepositionAfterLoop(info->mRepositionAfterLoop);

	return motionInst;
}


// remove a given motion instance
bool MotionSystem::RemoveMotionInstance(MotionInstance* instance) {
	bool isSuccess = true;

	// for all nodes which are part of the actor
	const size_t numNodes = mActor->GetNumNodes();
	for (size_t i = 0; i < numNodes; i++) {
		// remove the motion link for the given motion instance, in the given node
		mActor->GetNode(i)->RemoveLink(instance);
	}

	// remove the motion instance from the actor
	auto a = std::remove(mMotionInstances.begin(), mMotionInstances.end(), instance);
	if (a == mMotionInstances.end())
		isSuccess = false;

	// delete the motion instance from memory
	delete instance;

	// return if it all worked :)
	return isSuccess;
}


// update motion queue and instances
void MotionSystem::Update(const double timePassed, const bool updateNodes) {
	// update the motion queue
	mMotionQueue->Update();

	// update the motions
	UpdateMotionInstances(timePassed);
}


// apply the motion mask
void MotionSystem::ApplyMotionMask(NodeTransform* nodeTransform, MotionInstance* instance, Node* node) {
	EPlayMask mask = instance->GetPlayMask();

	// restore position
	if ((mask & PLAYMASK_POSITION) == false)
		nodeTransform->mPosition = node->GetLocalPos();

	// restore rotation
	if ((mask & PLAYMASK_ROTATION) == false)
		nodeTransform->mRotation = node->GetLocalRot();

	// restore scale
	if ((mask & PLAYMASK_SCALE) == false)
		nodeTransform->mScale = node->GetLocalScale();
}

// remove the given motion
void MotionSystem::RemoveMotion(const int nr, const bool deleteMem) {
	assert(nr < mMotionInstances.size());

	if (deleteMem)
		delete mMotionInstances[nr];

	mMotionInstances.erase(mMotionInstances.begin() + nr);
}


// remove the given motion
void MotionSystem::RemoveMotion(MotionInstance* motion, const bool delMem) {
	assert(motion);

	int nr = std::find(mMotionInstances.begin(), mMotionInstances.end(), motion) - mMotionInstances.begin();
	assert(nr != -1);

	if (nr == -1)
		return;

	RemoveMotion(nr, delMem);
}


// update the motion infos
void MotionSystem::UpdateMotionInstances(const double timePassed) {
	// update all the motion infos
	for (int i = 0; i < mMotionInstances.size(); i++)
		mMotionInstances[i]->Update(timePassed);
}


// check if the given motion instance still exists within the actor, so if it hasn't been deleted from memory yet
bool MotionSystem::IsValidMotionInstance(MotionInstance *instance) const {
	// if it's a null pointer, just return
	if (instance == nullptr)
		return false;

	// for all motion instances currently playing in this actor
	for (int i = 0; i < mMotionInstances.size(); i++) {
		// check if this one is the one we are searching for, if so, return that it is still valid
		if (mMotionInstances[i] == instance)
			return true;
	}

	// it's not found, this means it has already been deleted from memory and is not valid anymore
	return false;
}


// check if there is a motion instance playing, which is an instance of a specified motion
bool MotionSystem::IsPlayingMotion(Motion* motion) const {
	// if the motion is nullptr, return false
	if (motion == nullptr)
		return false;

	// for all motion instances currently playing in this actor
	for (int i = 0; i < mMotionInstances.size(); i++) {
		// check if the motion instance is an instance of the motion we are searching for
		if (mMotionInstances[i]->GetMotion() == motion)
			return true;
	}

	// it's not found, this means it has already been deleted from memory and is not valid anymore
	return false;
}


// set a new motion queue
void MotionSystem::SetMotionQueue(MotionQueue* motionQueue) {
	if (mMotionQueue != nullptr)
		delete mMotionQueue;

	mMotionQueue = motionQueue;
}


// add a motion queue to the motion system
void MotionSystem::AddMotionQueue(MotionQueue* motionQueue) {
	assert(motionQueue != nullptr);

	// copy entries from the given queue to the motion system's one
	for (int i = 0; i < motionQueue->GetNumEntries(); i++)
		mMotionQueue->AddEntry(motionQueue->GetEntry(i));

	// get rid of the given motion queue
	delete motionQueue;
}

} // namespace