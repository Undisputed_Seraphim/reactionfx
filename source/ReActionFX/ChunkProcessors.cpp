#include "ChunkProcessors.h"
#include "Importer.h"

#include "Node.h"
#include "Motion.h"
#include "SkeletalMotion.h"
#include "MotionPart.h"
#include "Actor.h"
#include "Mesh.h"
#include "MeshDeformerStack.h"
#include "SoftSkinDeformer.h"
#include "SimpleMesh.h"
#include "SubMesh.h"
#include "Material.h"
#include "StandardMaterial.h"
#include "FXMaterial.h"
#include "SkinningInfoVertexAttributeLayer.h"
#include "SoftSkinManager.h"
#include "LinearInterpolators.h"
#include "UVVertexAttributeLayer.h"
#include "NodeLimitAttribute.h"
#include "NodePhysicsAttribute.h"
#include "MeshExpressionPart.h"
#include "FacialMotionPart.h"
#include "FacialMotion.h"
#include "NodeIDGenerator.h"
#include "HermiteInterpolators.h"
#include "SmartMeshMorphDeformer.h"

using namespace RCore;

using std::shared_ptr;
using std::vector;

namespace ReActionFX {

bool NodeChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");
		return false;
	}

	// read the node header
	file->read((char*)&mNodeHeader, sizeof(LMA_Node));

	if (GetLogging()) {
		LOG("- Node name = '%s'", mNodeHeader.mName);
		LOG("    + Parent = '%s'", mNodeHeader.mParent);
		LOG("    + Position:    x=%f, y=%f, z=%f", (float)mNodeHeader.mLocalPos.mX, (float)mNodeHeader.mLocalPos.mY, (float)mNodeHeader.mLocalPos.mZ);
		LOG("    + Orientation: x=%f, y=%f, z=%f, w=%f", (float)mNodeHeader.mLocalQuat.mX, (float)mNodeHeader.mLocalQuat.mY, (float)mNodeHeader.mLocalQuat.mZ, (float)mNodeHeader.mLocalQuat.mW);
		LOG("    + Scale:       x=%f, y=%f, z=%f", (float)mNodeHeader.mLocalScale.mX, (float)mNodeHeader.mLocalScale.mY, (float)mNodeHeader.mLocalScale.mZ);
		LOG("    + Inverse Matrix: %f, \t%f, \t%f, \t%f", (float)mNodeHeader.mInvBoneTM.m[0], (float)mNodeHeader.mInvBoneTM.m[1], (float)mNodeHeader.mInvBoneTM.m[2], (float)mNodeHeader.mInvBoneTM.m[3]);
		LOG("                      %f, \t%f, \t%f, \t%f", (float)mNodeHeader.mInvBoneTM.m[4], (float)mNodeHeader.mInvBoneTM.m[5], (float)mNodeHeader.mInvBoneTM.m[6], (float)mNodeHeader.mInvBoneTM.m[7]);
		LOG("                      %f, \t%f, \t%f, \t%f", (float)mNodeHeader.mInvBoneTM.m[8], (float)mNodeHeader.mInvBoneTM.m[9], (float)mNodeHeader.mInvBoneTM.m[10], (float)mNodeHeader.mInvBoneTM.m[11]);
		LOG("                      %f, \t%f, \t%f, \t%f", (float)mNodeHeader.mInvBoneTM.m[12], (float)mNodeHeader.mInvBoneTM.m[13], (float)mNodeHeader.mInvBoneTM.m[14], (float)mNodeHeader.mInvBoneTM.m[15]);
	}

	// create the new node
	Node* node = new Node(mNodeHeader.mName);

	// add it to the actor
	actor->AddNode(node);

	// set the transformation
	node->SetLocalPos(Vector3(mNodeHeader.mLocalPos.mX, mNodeHeader.mLocalPos.mY, mNodeHeader.mLocalPos.mZ));
	node->SetLocalScale(Vector3(mNodeHeader.mLocalScale.mX, mNodeHeader.mLocalScale.mY, mNodeHeader.mLocalScale.mZ));
	node->SetLocalRot(Quaternion(mNodeHeader.mLocalQuat.mX, mNodeHeader.mLocalQuat.mY, mNodeHeader.mLocalQuat.mZ, mNodeHeader.mLocalQuat.mW));
	node->UpdateLocalTM();

	node->SetOrgPos(node->GetLocalPos());
	node->SetOrgRot(node->GetLocalRot());
	node->SetOrgScale(node->GetLocalScale());

	// set the inverse world space matrix
	Matrix mat(true);
	mat.m44[0][0] = mNodeHeader.mInvBoneTM.m[0 + 0 * 4];	mat.m44[0][1] = mNodeHeader.mInvBoneTM.m[1 + 0 * 4];	mat.m44[0][2] = mNodeHeader.mInvBoneTM.m[2 + 0 * 4];//	mat.m44[0][2]=mNodeHeader.mInvBoneTM.m[3+0*4];
	mat.m44[1][0] = mNodeHeader.mInvBoneTM.m[0 + 1 * 4];	mat.m44[1][1] = mNodeHeader.mInvBoneTM.m[1 + 1 * 4];	mat.m44[1][2] = mNodeHeader.mInvBoneTM.m[2 + 1 * 4];//	mat.m44[1][2]=mNodeHeader.mInvBoneTM.m[3+1*4];
	mat.m44[2][0] = mNodeHeader.mInvBoneTM.m[0 + 2 * 4];	mat.m44[2][1] = mNodeHeader.mInvBoneTM.m[1 + 2 * 4];	mat.m44[2][2] = mNodeHeader.mInvBoneTM.m[2 + 2 * 4];//	mat.m44[2][2]=mNodeHeader.mInvBoneTM.m[3+2*4];
	mat.m44[3][0] = mNodeHeader.mInvBoneTM.m[0 + 3 * 4];	mat.m44[3][1] = mNodeHeader.mInvBoneTM.m[1 + 3 * 4];	mat.m44[3][2] = mNodeHeader.mInvBoneTM.m[2 + 3 * 4];//	mat.m44[3][2]=mNodeHeader.mInvBoneTM.m[3+3*4];
	node->SetInvBoneTM(mat);

	// add a parent link
	if (mNodeHeader.mParent[0] != '\0')
		AddLink(node, mNodeHeader.mParent);

	// get the shared data
	Importer::SharedHierarchyInfo* sharedHierarchyInfo = (Importer::SharedHierarchyInfo*)mLMAImporter->FindSharedData(Importer::SharedHierarchyInfo::TYPE_ID);

	// if we haven't found it, something is wrong in the implementation of the shared data or this processor
	assert(sharedHierarchyInfo);

	// if we found it, add it
	if (sharedHierarchyInfo) {
		sharedHierarchyInfo->mNodes.push_back(node);
		sharedHierarchyInfo->mLastNode = node;
	} else {
		delete node;
		if (GetLogging())
			LOG("Can't find shared data.");
		return false;
	}

	return true;
}


void NodeChunkProcessor1::AddLink(Node* node, char* parent) {
	// create a new link
	Importer::SharedHierarchyInfo::Link link;
	link.mNode = node;
	link.mParent = parent;

	// get the shared data
	Importer::SharedHierarchyInfo* sharedHierarchyInfo = (Importer::SharedHierarchyInfo*)mLMAImporter->FindSharedData(Importer::SharedHierarchyInfo::TYPE_ID);

	if (sharedHierarchyInfo)
		sharedHierarchyInfo->mLinks.push_back(link);
	else {
		if (GetLogging())
			LOG("Can't find shared data.");
	}
}

//==================================================================================================

bool MeshChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		LOG_ERROR("Passed actor seems to be not valid!");
		return false;
	}

	// read the mesh header
	LMA_Mesh meshHeader;
	file->read((char*)&meshHeader, sizeof(LMA_Mesh));

	if (GetLogging()) {
		LOG("- Mesh");
		LOG("    + NodeNumber    = %d", meshHeader.mNodeNumber);
		LOG("    + NumOrgVerts   = %d", meshHeader.mNumOrgVerts);
		LOG("    + NumVerts      = %d", meshHeader.mTotalVerts);
		LOG("    + NumIndices    = %d", meshHeader.mTotalIndices);
		LOG("    + NumSubMeshes  = %d", meshHeader.mNumSubMeshes);
	}

	// create the smartpointer to the mesh object
	// we use a smartpointer here, because of the reference counting system needed for shared meshes.
	auto mesh = std::make_shared<Mesh>(meshHeader.mTotalVerts, meshHeader.mTotalIndices / 3, meshHeader.mNumOrgVerts);

	// link the mesh to the correct node
	actor->GetNode(meshHeader.mNodeNumber)->SetMesh(mesh, 0);

	LMA_SubMesh subMesh;
	LMA_SubMeshVertex vertex;
	int index;

	// add an uv layer
	const int numVerts = mesh->GetNumVertices();
	UVVertexAttributeLayer* uvLayer = new UVVertexAttributeLayer(numVerts);
	mesh->AddVertexAttributeLayer(uvLayer);

	// get the arrays
	Vector3* positions = mesh->GetPositions();
	Vector3* normals = mesh->GetNormals();
	Vector3* orgPos = mesh->GetOrgPositions();
	Vector3* orgNormals = mesh->GetOrgNormals();
	int*	 orgVerts = mesh->GetOrgVerts();
	int*	 indices = mesh->GetIndices();
	Vector2* uvData = uvLayer->GetUVs();

	int vertexOffset = 0;
	int indexOffset = 0;
	int startVertex = 0;

	// read all submeshes
	for (int i = 0; i < meshHeader.mNumSubMeshes; i++) {
		// read the submesh header
		file->read((char*)&subMesh, sizeof(LMA_SubMesh));

		if (GetLogging()) {
			LOG("    - Reading SubMesh");
			LOG("       + Material ID   = %d", subMesh.mMatID);
			LOG("       + NumOfIndices  = %d", subMesh.mNumIndices);
			LOG("       + NumOfVertices = %d", subMesh.mNumVerts);
			LOG("       + NumOfUVSets   = %d", subMesh.mNumUVSets);
		}

		// create and add the submesh
		SubMesh *lmSubMesh = new SubMesh(mesh.get(), vertexOffset, indexOffset, subMesh.mNumVerts, subMesh.mNumIndices, subMesh.mMatID);
		mesh->AddSubMesh(lmSubMesh);

		// read the vertices
		for (int v = 0; v < subMesh.mNumVerts; v++) {
			// read the vertex
			file->read((char*)&vertex, sizeof(LMA_SubMeshVertex));

			// set the vertex data
			positions[vertexOffset] = Vector3(vertex.mPos.mX, vertex.mPos.mY, vertex.mPos.mZ);
			normals[vertexOffset] = Vector3(vertex.mNormal.mX, vertex.mNormal.mY, vertex.mNormal.mZ);
			orgPos[vertexOffset] = positions[vertexOffset];
			orgNormals[vertexOffset] = normals[vertexOffset];
			orgVerts[vertexOffset] = vertex.mOrgVtx;
			uvData[vertexOffset] = Vector2(vertex.mUV.mU, vertex.mUV.mV);

			// next vertex in the big mesh
			vertexOffset++;
		}

		// read the indices
		for (int a = 0; a < subMesh.mNumIndices; a++) {
			file->read((char*)&index, sizeof(int));
			indices[indexOffset] = startVertex + index;
			indexOffset++;
		}

		startVertex += subMesh.mNumVerts;
	}

	// calculate the tangents
	if (usePerPixelLighting)
		mesh->CalcTangents();

	return true;
}

//==================================================================================================


bool MeshChunkProcessor2::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		LOG_ERROR("Passed actor seems to be not valid!");
		return false;
	}

	// read the mesh header
	LMA_Mesh2 meshHeader;
	file->read((char*)&meshHeader, sizeof(LMA_Mesh2));

	if (GetLogging()) {
		LOG("- Mesh");
		LOG("    + NodeNumber      = %d", meshHeader.mNodeNumber);
		LOG("    + NumOrgVerts     = %d", meshHeader.mNumOrgVerts);
		LOG("    + NumVerts        = %d", meshHeader.mTotalVerts);
		LOG("    + NumIndices      = %d", meshHeader.mTotalIndices);
		LOG("    + NumSubMeshes    = %d", meshHeader.mNumSubMeshes);
		LOG("    + IsCollisionMesh = %d", meshHeader.mIsCollisionMesh);
	}

	// create the smartpointer to the mesh object
	// we use a smartpointer here, because of the reference counting system needed for shared meshes.
	auto mesh = std::make_shared<Mesh>(meshHeader.mTotalVerts, meshHeader.mTotalIndices / 3, meshHeader.mNumOrgVerts);

	// link the mesh to the correct node
	if (meshHeader.mIsCollisionMesh)
		actor->GetNode(meshHeader.mNodeNumber)->SetCollisionMesh(mesh, 0);
	else
		actor->GetNode(meshHeader.mNodeNumber)->SetMesh(mesh, 0);

	LMA_SubMesh subMesh;
	LMA_SubMeshVertex vertex;
	int index;

	// add an uv layer
	UVVertexAttributeLayer* uvLayer = new UVVertexAttributeLayer(mesh->GetNumVertices());
	mesh->AddVertexAttributeLayer(uvLayer);

	// get the arrays
	Vector3* positions = mesh->GetPositions();
	Vector3* normals = mesh->GetNormals();
	Vector3* orgPos = mesh->GetOrgPositions();
	Vector3* orgNormals = mesh->GetOrgNormals();
	int*	 orgVerts = mesh->GetOrgVerts();
	int*	 indices = mesh->GetIndices();
	Vector2* uvData = uvLayer->GetUVs();

	int vertexOffset = 0;
	int indexOffset = 0;
	int startVertex = 0;

	// read all submeshes
	for (int i = 0; i < meshHeader.mNumSubMeshes; i++) {
		// read the submesh header
		file->read((char*)&subMesh, sizeof(LMA_SubMesh));

		if (GetLogging()) {
			LOG("    - Reading SubMesh");
			LOG("       + Material ID   = %d", subMesh.mMatID);
			LOG("       + NumOfIndices  = %d", subMesh.mNumIndices);
			LOG("       + NumOfVertices = %d", subMesh.mNumVerts);
			LOG("       + NumOfUVSets   = %d", subMesh.mNumUVSets);
		}

		// create and add the submesh
		SubMesh* lmSubMesh = new SubMesh(mesh.get(), vertexOffset, indexOffset, subMesh.mNumVerts, subMesh.mNumIndices, subMesh.mMatID);
		mesh->AddSubMesh(lmSubMesh);

		// read the vertices
		for (int v = 0; v < subMesh.mNumVerts; v++) {
			// read the vertex
			file->read((char*)&vertex, sizeof(LMA_SubMeshVertex));

			// set the vertex data
			positions[vertexOffset] = Vector3(vertex.mPos.mX, vertex.mPos.mY, vertex.mPos.mZ);
			normals[vertexOffset] = Vector3(vertex.mNormal.mX, vertex.mNormal.mY, vertex.mNormal.mZ);
			orgPos[vertexOffset] = positions[vertexOffset];
			orgNormals[vertexOffset] = normals[vertexOffset];
			orgVerts[vertexOffset] = vertex.mOrgVtx;
			uvData[vertexOffset] = Vector2(vertex.mUV.mU, vertex.mUV.mV);

			// next vertex in the big mesh
			vertexOffset++;
		}

		// read the indices
		for (int a = 0; a < subMesh.mNumIndices; a++) {
			file->read((char*)&index, sizeof(int));
			indices[indexOffset] = startVertex + index;
			indexOffset++;
		}

		startVertex += subMesh.mNumVerts;
	}

	// calculate the tangents
	if (usePerPixelLighting)
		mesh->CalcTangents();

	return true;
}

//==================================================================================================


bool MeshChunkProcessor3::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		LOG_ERROR("Passed actor seems to be not valid!");
		return false;
	}

	// read the mesh header
	LMA_Mesh3 meshHeader;
	file->read((char*)&meshHeader, sizeof(LMA_Mesh3));

	if (GetLogging()) {
		LOG("- Mesh");
		LOG("    + NodeNumber      = %d", meshHeader.mNodeNumber);
		LOG("    + NumOrgVerts     = %d", meshHeader.mNumOrgVerts);
		LOG("    + NumVerts        = %d", meshHeader.mTotalVerts);
		LOG("    + NumIndices      = %d", meshHeader.mTotalIndices);
		LOG("    + NumSubMeshes    = %d", meshHeader.mNumSubMeshes);
		LOG("    + NumUVSets       = %d", meshHeader.mNumUVSets);
		LOG("    + IsCollisionMesh = %d", meshHeader.mIsCollisionMesh);
	}

	// create the smartpointer to the mesh object
	// we use a smartpointer here, because of the reference counting system needed for shared meshes.
	auto mesh = std::make_shared<Mesh>(meshHeader.mTotalVerts, meshHeader.mTotalIndices / 3, meshHeader.mNumOrgVerts);

	// link the mesh to the correct node
	if (meshHeader.mIsCollisionMesh)
		actor->GetNode(meshHeader.mNodeNumber)->SetCollisionMesh(mesh, 0);
	else
		actor->GetNode(meshHeader.mNodeNumber)->SetMesh(mesh, 0);

	LMA_SubMesh subMesh;
	LMA_SubMeshVertex2 vertex;
	int index;

	// add an uv layer
	vector<UVVertexAttributeLayer*> uvLayers;
	vector <Vector2*> uvDatas;

	for (int a = 0; a < meshHeader.mNumUVSets; a++) {
		UVVertexAttributeLayer* layer = new UVVertexAttributeLayer(mesh->GetNumVertices());
		mesh->AddVertexAttributeLayer(layer);
		uvLayers.push_back(layer);
		uvDatas.push_back(layer->GetUVs());
	}

	// get the arrays
	Vector3* positions = mesh->GetPositions();
	Vector3* normals = mesh->GetNormals();
	Vector3* orgPos = mesh->GetOrgPositions();
	Vector3* orgNormals = mesh->GetOrgNormals();
	int*	 orgVerts = mesh->GetOrgVerts();
	int*	 indices = mesh->GetIndices();

	int vertexOffset = 0;
	int indexOffset = 0;
	int startVertex = 0;

	// read all submeshes
	for (int i = 0; i < meshHeader.mNumSubMeshes; i++) {
		// read the submesh header
		file->read((char*)&subMesh, sizeof(LMA_SubMesh));

		if (GetLogging()) {
			LOG("    - Reading SubMesh");
			LOG("       + Material ID   = %d", subMesh.mMatID);
			LOG("       + NumOfIndices  = %d", subMesh.mNumIndices);
			LOG("       + NumOfVertices = %d", subMesh.mNumVerts);
		}

		// create and add the submesh
		SubMesh* lmSubMesh = new SubMesh(mesh.get(), vertexOffset, indexOffset, subMesh.mNumVerts, subMesh.mNumIndices, subMesh.mMatID);
		mesh->AddSubMesh(lmSubMesh);

		// read the vertices
		for (int v = 0; v < subMesh.mNumVerts; v++) {
			// read the vertex
			file->read((char*)&vertex, sizeof(LMA_SubMeshVertex2));

			// set the vertex data
			positions[vertexOffset] = Vector3(vertex.mPos.mX, vertex.mPos.mY, vertex.mPos.mZ);
			normals[vertexOffset] = Vector3(vertex.mNormal.mX, vertex.mNormal.mY, vertex.mNormal.mZ);
			orgPos[vertexOffset] = positions[vertexOffset];
			orgNormals[vertexOffset] = normals[vertexOffset];
			orgVerts[vertexOffset] = vertex.mOrgVtx;

			// read the UVs
			LMA_UV uv;
			for (int a = 0; a < meshHeader.mNumUVSets; a++) {
				file->read((char*)&uv, sizeof(LMA_UV));
				uvDatas[a][vertexOffset] = Vector2(uv.mU, uv.mV);
			}

			// next vertex in the big mesh
			vertexOffset++;
		}

		// read the indices
		for (int a = 0; a < subMesh.mNumIndices; a++) {
			file->read((char*)&index, sizeof(int));
			indices[indexOffset] = startVertex + index;
			indexOffset++;
		}

		startVertex += subMesh.mNumVerts;
	}

	// calculate the tangents
	if (usePerPixelLighting)
		mesh->CalcTangents();

	return true;
}

//==================================================================================================


bool MeshChunkProcessor10000::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		LOG_ERROR("Passed actor seems to be not valid!");
		return false;
	}

	// read the mesh header
	LMA_Mesh10000 meshHeader;
	file->read((char*)&meshHeader, sizeof(LMA_Mesh10000));

	if (GetLogging()) {
		LOG("- Mesh");
		LOG("    + NodeNumber      = %d", meshHeader.mNodeNumber);
		LOG("    + NumOrgVerts     = %d", meshHeader.mNumOrgVerts);
		LOG("    + NumVerts        = %d", meshHeader.mTotalVerts);
		LOG("    + NumIndices      = %d", meshHeader.mTotalIndices);
		LOG("    + NumSubMeshes    = %d", meshHeader.mNumSubMeshes);
		LOG("    + IsCollisionMesh = %d", meshHeader.mIsCollisionMesh);
	}

	// create the smartpointer to the mesh object
	// we use a smartpointer here, because of the reference counting system needed for shared meshes.
	auto mesh = std::make_shared<Mesh>(meshHeader.mTotalVerts, meshHeader.mTotalIndices / 3, meshHeader.mNumOrgVerts);

	// link the mesh to the correct node
	if (meshHeader.mIsCollisionMesh)
		actor->GetNode(meshHeader.mNodeNumber)->SetCollisionMesh(mesh, 0);
	else
		actor->GetNode(meshHeader.mNodeNumber)->SetMesh(mesh, 0);

	LMA_SubMesh subMesh;
	LMA_SubMeshVertex vertex;
	LMA_SubMeshVertex10000 vertex10000;
	int index;

	// add an uv layer
	UVVertexAttributeLayer* uvData = new UVVertexAttributeLayer(mesh->GetNumVertices());
	UVVertexAttributeLayer * uvData1 = nullptr;
	mesh->AddVertexAttributeLayer(uvData);
	if (strcmp(meshHeader.mShdMapName, "") != 0) {
		uvData1 = new UVVertexAttributeLayer(mesh->GetNumVertices());
		mesh->AddVertexAttributeLayer(uvData1);

		mesh->SetShdMapName(meshHeader.mShdMapName);
	}

	// get the arrays
	Vector3* positions = mesh->GetPositions();
	Vector3* normals = mesh->GetNormals();
	Vector3* orgPos = mesh->GetOrgPositions();
	Vector3* orgNormals = mesh->GetOrgNormals();
	int*	 orgVerts = mesh->GetOrgVerts();
	int*	 indices = mesh->GetIndices();

	int vertexOffset = 0;
	int indexOffset = 0;
	int startVertex = 0;

	// read all submeshes
	for (int i = 0; i < meshHeader.mNumSubMeshes; i++) {
		// read the submesh header
		file->read((char*)&subMesh, sizeof(LMA_SubMesh));

		if (GetLogging()) {
			LOG("    - Reading SubMesh");
			LOG("       + Material ID   = %d", subMesh.mMatID);
			LOG("       + NumOfIndices  = %d", subMesh.mNumIndices);
			LOG("       + NumOfVertices = %d", subMesh.mNumVerts);
			LOG("       + NumOfUVSets   = %d", subMesh.mNumUVSets);
		}

		// create and add the submesh
		SubMesh* lmSubMesh = new SubMesh(mesh.get(), vertexOffset, indexOffset, subMesh.mNumVerts, subMesh.mNumIndices, subMesh.mMatID);
		mesh->AddSubMesh(lmSubMesh);

		// read the vertices
		if (strcmp(meshHeader.mShdMapName, "") != 0) {
			for (int v = 0; v < subMesh.mNumVerts; v++) {
				// read the vertex
				file->read((char*)&vertex10000, sizeof(LMA_SubMeshVertex10000));

				// set the vertex data
				positions[vertexOffset] = Vector3(vertex10000.mPos.mX, vertex10000.mPos.mY, vertex10000.mPos.mZ);
				normals[vertexOffset] = Vector3(vertex10000.mNormal.mX, vertex10000.mNormal.mY, vertex10000.mNormal.mZ);
				orgPos[vertexOffset] = positions[vertexOffset];
				orgNormals[vertexOffset] = normals[vertexOffset];
				orgVerts[vertexOffset] = vertex10000.mOrgVtx;
				uvData->GetUVs()[vertexOffset] = Vector2(vertex10000.mUV0.mU, vertex10000.mUV0.mV);
				uvData1->GetUVs()[vertexOffset] = Vector2(vertex10000.mUV1.mU, vertex10000.mUV1.mV);

				// next vertex in the big mesh
				vertexOffset++;
			}
		} else {
			for (int v = 0; v < subMesh.mNumVerts; v++) {
				// read the vertex
				file->read((char*)&vertex, sizeof(LMA_SubMeshVertex));

				// set the vertex data
				positions[vertexOffset] = Vector3(vertex.mPos.mX, vertex.mPos.mY, vertex.mPos.mZ);
				normals[vertexOffset] = Vector3(vertex.mNormal.mX, vertex.mNormal.mY, vertex.mNormal.mZ);
				orgPos[vertexOffset] = positions[vertexOffset];
				orgNormals[vertexOffset] = normals[vertexOffset];
				orgVerts[vertexOffset] = vertex.mOrgVtx;
				uvData->GetUVs()[vertexOffset] = Vector2(vertex.mUV.mU, vertex.mUV.mV);

				// next vertex in the big mesh
				vertexOffset++;
			}
		}

		// read the indices
		for (int a = 0; a < subMesh.mNumIndices; a++) {
			file->read((char*)&index, sizeof(int));
			indices[indexOffset] = startVertex + index;
			indexOffset++;
		}

		startVertex += subMesh.mNumVerts;
	}

	// calculate the tangents
	if (usePerPixelLighting)
		mesh->CalcTangents();

	return true;
}

//==================================================================================================


bool SkinningInfoChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging()) LOG("Passed actor seems to be not valid!");
		return false;
	}

	if (GetLogging()) LOG("- Skinning Information");

	// read the node number this info belongs to
	int nodeNr;
	file->read((char*)&nodeNr, sizeof(int));

	if (GetLogging()) {
		LOG("   + Node number = %d", nodeNr);
		LOG("   + Node name   = %s", actor->GetNode(nodeNr)->GetNamePtr());
	}

	// get the mesh of the node
	bool colMesh = false;
	if (actor->GetNode(nodeNr)->GetCollisionMesh(0))
		colMesh = true;

	shared_ptr<Mesh> mesh = nullptr;
	if (colMesh)
		mesh = actor->GetNode(nodeNr)->GetCollisionMesh(0);
	else
		mesh = actor->GetNode(nodeNr)->GetMesh(0);

	if (GetLogging()) LOG("   + Mesh pointer = 0x%x", mesh);

	// add the skinning info to the mesh
	SkinningInfoVertexAttributeLayer* skinningLayer = new SkinningInfoVertexAttributeLayer(mesh->GetNumOrgVertices());
	mesh->AddSharedVertexAttributeLayer(skinningLayer);

	if (GetLogging()) LOG("   + Num org vertices = %d", mesh->GetNumOrgVertices());

	// read all the influences, for each vertex
	LMA_SkinInfluence influence;
	const int numOrgVerts = mesh->GetNumOrgVertices();
	for (int i = 0; i < numOrgVerts; i++) {
		// read the number of influences for this vertex
		char numInfluences;
		file->read((char*)&numInfluences, 1);

		if (GetLogging()) LOG("   + Num influences for vertex #%d = %d", i, numInfluences);

		// load the influences
		for (int w = 0; w < numInfluences; w++) {
			file->read((char*)&influence, sizeof(LMA_SkinInfluence));
			if (GetLogging()) LOG("     + %d - nodeNr = %d    - weight = %f", i, influence.mNodeNr, influence.mWeight);

			// add the influence to the skinning attribute
			skinningLayer->AddInfluence(i, actor->GetNode(influence.mNodeNr), influence.mWeight);
		}
	}

	// get the stack
	Node *node = actor->GetNode(nodeNr);

	if (colMesh) {
		auto stack = node->GetCollisionMeshDeformerStack(0);

		// create the stack if it doesn't yet exist
		if (stack == nullptr) {
			stack = std::make_shared<MeshDeformerStack>(node->GetCollisionMesh(0).get());
			node->SetCollisionMeshDeformerStack(stack, 0);
		}

		// add the skinning deformer to the stack
		SoftSkinDeformer* skinDeformer = SoftSkinManager::GetInstance().CreateDeformer(node->GetCollisionMesh(0).get());
		stack->AddDeformer(skinDeformer);
	} else {
		auto stack = node->GetMeshDeformerStack(0);

		// create the stack if it doesn't yet exist
		if (stack == nullptr) {
			stack = std::make_shared<MeshDeformerStack>(node->GetMesh(0).get());
			node->SetMeshDeformerStack(stack, 0);
		}

		// add the skinning deformer to the stack
		SoftSkinDeformer* skinDeformer = SoftSkinManager::GetInstance().CreateDeformer(node->GetMesh(0).get());
		stack->AddDeformer(skinDeformer);
	}

	return true;
}

//==================================================================================================
/*
bool CollisionMeshChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");
		return false;
	}

	if (GetLogging()) LOG("- Collision Mesh");

	LMA_CollisionMesh obj;
	if (!file->read((char*)&obj, sizeof(LMA_CollisionMesh))) return false;

	if (GetLogging()) LOG("    + NumVertices = %d", obj.mNumVertices);
	if (GetLogging()) LOG("    + NumFaces    = %d", obj.mNumFaces);

	// create the mesh
	//SimpleMesh *mesh = new SimpleMesh(obj.mNumVertices, obj.mNumFaces);
	RCore::Pointer<SimpleMesh> mesh(new SimpleMesh(obj.mNumVertices, obj.mNumFaces));

	// get pointers to the mesh data
	Vector3*	positions = mesh->GetPositions();
	int*		indices = mesh->GetIndices();

	// read the vertices
	LMA_Vector3 vertex;
	int i;
	for (i = 0; i < obj.mNumVertices; i++) {
		file->read((char*)&vertex, sizeof(LMA_Vector3));
		positions[i].Set(vertex.mX, vertex.mY, vertex.mZ);
		mesh->GetBoundingBox().Encapsulate(positions[i]);
	}

	// read the faces
	int indexA, indexB, indexC, curIndex = 0;
	for (i = 0; i < obj.mNumFaces; i++) {
		file->read((char*)&indexA, sizeof(int));
		file->read((char*)&indexB, sizeof(int));
		file->read((char*)&indexC, sizeof(int));
		indices[curIndex++] = indexA;
		indices[curIndex++] = indexB;
		indices[curIndex++] = indexC;
	}

	// set the collision mesh
	Importer::SharedHierarchyInfo* sharedHierarchyInfo = (Importer::SharedHierarchyInfo*)mLMAImporter->FindSharedData(SHAREDDATA_HIERARCHYINFO);

	if (sharedHierarchyInfo) {
		if (sharedHierarchyInfo->mLastNode != nullptr)
			sharedHierarchyInfo->mLastNode->SetCollisionMesh(mesh);
	} else {
		if (GetLogging())
			LOG("Can't find shared data.");
		return false;
	}

	// update the boundingsphere
	mesh->GetBoundingSphere() = BoundingSphere(mesh->GetBoundingBox().CalcMiddle(), 0);
	mesh->GetBoundingSphere().Encapsulate(mesh->GetBoundingBox().GetMin());
	mesh->GetBoundingSphere().Encapsulate(mesh->GetBoundingBox().GetMax());

	return true;
}
*/
//==================================================================================================

bool MaterialChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	LMA_Material material;
	file->read((char*)&material, sizeof(LMA_Material));

	// print material information
	if (GetLogging()) {
		LOG("- Material name = '%s'", material.mName);
		LOG("    + Ambient       : (%f, %f, %f)", material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB);
		LOG("    + Diffuse       : (%f, %f, %f)", material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB);
		LOG("    + Specular      : (%f, %f, %f)", material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB);
		LOG("    + Emissive      : (%f, %f, %f)", material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB);
		LOG("    + Shine         : %f", material.mShine);
		LOG("    + Shine strength: %f", material.mShineStrength);
		LOG("    + Opacity       : %f", material.mOpacity);
		LOG("    + IOR           : %f", material.mIOR);
		LOG("    + Double sided  : %d", material.mDoubleSided);
		LOG("    + WireFrame     : %d", material.mWireFrame);
		LOG("    + TransparencyTp: %c", material.mTransparencyType);
		LOG("    + *** Material offset/tiling/rotation NOT available ***");
	}

	// create the material
	auto mat = std::make_shared<StandardMaterial>(material.mName);

	// setup its properties
	mat->SetAmbient(RGBAColor(material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB));
	mat->SetDiffuse(RGBAColor(material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB));
	mat->SetSpecular(RGBAColor(material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB));
	mat->SetEmissive(RGBAColor(material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB));
	mat->SetShine(material.mShine);
	mat->SetShineStrength(material.mShineStrength);
	mat->SetOpacity(material.mOpacity);
	mat->SetIOR(material.mIOR);
	mat->SetDoubleSided(material.mDoubleSided);
	mat->SetWireFrame(material.mWireFrame);
	mat->SetTransparencyType(material.mTransparencyType);

	// add the material to the actor
	actor->AddMaterial(0, mat);

	return true;
}

//==================================================================================================

bool MaterialLayerChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}


	// read the layer from disk
	LMA_MaterialLayer matLayer;
	file->read((char*)&matLayer, sizeof(LMA_MaterialLayer));

	// convert the layer type
	int layerType;
	switch (matLayer.mMapType) {
	case LMA_LAYERID_DIFFUSE:		layerType = StandardMaterialLayer::LAYERTYPE_DIFFUSE;		break;
	case LMA_LAYERID_OPACITY:		layerType = StandardMaterialLayer::LAYERTYPE_OPACITY;		break;
	case LMA_LAYERID_SHINE:			layerType = StandardMaterialLayer::LAYERTYPE_SHINE;			break;
	case LMA_LAYERID_SHINESTRENGTH:	layerType = StandardMaterialLayer::LAYERTYPE_SHINESTRENGTH;	break;
	case LMA_LAYERID_SELFILLUM:		layerType = StandardMaterialLayer::LAYERTYPE_SELFILLUM;		break;
	case LMA_LAYERID_AMBIENT:		layerType = StandardMaterialLayer::LAYERTYPE_AMBIENT;		break;
	case LMA_LAYERID_SPECULAR:		layerType = StandardMaterialLayer::LAYERTYPE_SPECULAR;		break;
	case LMA_LAYERID_REFLECT:		layerType = StandardMaterialLayer::LAYERTYPE_REFLECT;		break;
	case LMA_LAYERID_REFRACT:		layerType = StandardMaterialLayer::LAYERTYPE_REFRACT;		break;
	case LMA_LAYERID_BUMP:			layerType = StandardMaterialLayer::LAYERTYPE_BUMP;			break;
	case LMA_LAYERID_FILTERCOLOR:	layerType = StandardMaterialLayer::LAYERTYPE_FILTERCOLOR;	break;
	case LMA_LAYERID_ENVIRONMENT:	layerType = StandardMaterialLayer::LAYERTYPE_ENVIRONMENT;	break;
	default:						layerType = StandardMaterialLayer::LAYERTYPE_UNKNOWN;
	};

	// create the layer
	auto layer = std::make_shared<StandardMaterialLayer>(layerType, matLayer.mTexture, matLayer.mAmount);


	// add the layer to the material
	auto mat = actor->GetMaterial(0, matLayer.mMaterialNumber);
	assert(mat->GetType() == StandardMaterial::TYPE_ID);
	auto stdMat = std::dynamic_pointer_cast<StandardMaterial>(mat);

	stdMat->AddLayer(layer);

	if (GetLogging()) {
		LOG("    - Material Layer");
		LOG("       + Texture  = '%s'", matLayer.mTexture);
		LOG("       + Material = '%s' (%d)", stdMat->GetName().c_str(), matLayer.mMaterialNumber);
		LOG("       + Amount   = %f", matLayer.mAmount);
		LOG("       + MapType  = %d", matLayer.mMapType);
	}

	return true;
}

//==================================================================================================

bool MotionPartChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!motion) {
		if (GetLogging())
			LOG_ERROR("Passed motion seems to be not valid!");

		return false;
	}

	if (GetLogging()) LOG("- Motion Part:");

	file->read((char*)&mMotionPart, sizeof(LMA_MotionPart));

	if (GetLogging()) {
		LOG("    + Name = '%s'", mMotionPart.mName);
		LOG("    + Pose Position: x=%f, y=%f, z=%f", mMotionPart.mPosePos.mX, mMotionPart.mPosePos.mY, mMotionPart.mPosePos.mZ);
		LOG("    + Pose Rotation: x=%f, y=%f, z=%f, w=%f", mMotionPart.mPoseRot.mX, mMotionPart.mPoseRot.mY, mMotionPart.mPoseRot.mZ, mMotionPart.mPoseRot.mW);
		LOG("    + Pose Scale:    x=%f, y=%f, z=%f", mMotionPart.mPoseScale.mX, mMotionPart.mPoseScale.mY, mMotionPart.mPoseScale.mZ);
	}

	// create the part, and add it to the motion
	MotionPart* motionPart = new MotionPart(mMotionPart.mName);

	motionPart->SetPosePos(Vector3(mMotionPart.mPosePos.mX, mMotionPart.mPosePos.mY, mMotionPart.mPosePos.mZ));
	motionPart->SetPoseScale(Vector3(mMotionPart.mPoseScale.mX, mMotionPart.mPoseScale.mY, mMotionPart.mPoseScale.mZ));
	motionPart->SetPoseRot(Quaternion(mMotionPart.mPoseRot.mX, mMotionPart.mPoseRot.mY, mMotionPart.mPoseRot.mZ, mMotionPart.mPoseRot.mW));

	if (motion->GetType() == SkeletalMotion::TYPE_ID) {
		// cast
		SkeletalMotion* skelMotion = (SkeletalMotion*)motion;
		skelMotion->AddPart(motionPart);
	}

	// set last node
	Importer::SharedMotionInfo* sharedMotionInfo = (Importer::SharedMotionInfo*)mLMAImporter->FindSharedData(Importer::SharedMotionInfo::TYPE_ID);

	if (sharedMotionInfo)
		sharedMotionInfo->mLastPart = motionPart;
	else {
		delete motionPart;
		if (GetLogging())
			LOG("Can't find shared data.");

		return false;
	}

	return true;
}

//==================================================================================================

bool AnimationChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!motion) {
		if (GetLogging())
			LOG("Passed motion seems to be not valid!");

		return false;
	}

	if (GetLogging()) LOG("    - Animation");

	file->read((char*)&mAnim, sizeof(LMA_Anim));

	if (GetLogging()) {
		LOG("       + NrKeys    = %d", mAnim.mNrKeys);
		LOG("       + AnimType  = %c", mAnim.mAnimType);
		LOG("       + IPType    = %c", mAnim.mIPType);
	}

	MotionPart* motionPart = nullptr;

	// find last motion part
	Importer::SharedMotionInfo* sharedMotionInfo = (Importer::SharedMotionInfo*)mLMAImporter->FindSharedData(Importer::SharedMotionInfo::TYPE_ID);

	if (sharedMotionInfo) {
		motionPart = sharedMotionInfo->mLastPart;
	} else {
		if (GetLogging())
			LOG("Can't find shared data.");

		return false;
	}

	// check if it is valid
	if (!motionPart) {
		if (GetLogging())
			LOG("Can't get last motion part.");

		return false;
	}

	// read the key data
	for (int i = 0; i < mAnim.mNrKeys; i++) {
		if (mAnim.mAnimType == 'P') { // position
			LMA_Vector3Key key;
			file->read((char*)&key, sizeof(LMA_Vector3Key));
			motionPart->GetPosTrack().AddKey(key.mTime, Vector3(key.mValue.mX, key.mValue.mY, key.mValue.mZ));
			if (GetLogging()) LOG("       + Translation keyframe: Nr=%i, x=%f, y=%f, z=%f", i, key.mValue.mX, key.mValue.mY, key.mValue.mZ);
		} else if (mAnim.mAnimType == 'R') { // rotation
			LMA_QuaternionKey key;
			file->read((char*)&key, sizeof(LMA_QuaternionKey));
			motionPart->GetRotTrack().AddKey(key.mTime, Quaternion(key.mValue.mX, key.mValue.mY, key.mValue.mZ, key.mValue.mW)/*.LogN()*/);
			if (GetLogging()) LOG("       + Rotation keyframe: Nr=%i, x=%f, y=%f, z=%f, w=%f", i, key.mValue.mX, key.mValue.mY, key.mValue.mZ, key.mValue.mW);
		} else if (mAnim.mAnimType == 'S') { // scaling
			LMA_Vector3Key key;
			file->read((char*)&key, sizeof(LMA_Vector3Key));
			motionPart->GetScaleTrack().AddKey(key.mTime, Vector3(key.mValue.mX, key.mValue.mY, key.mValue.mZ));
			if (GetLogging()) LOG("       + Scale keyframe: Nr=%i, x=%f, y=%f, z=%f", i, key.mValue.mX, key.mValue.mY, key.mValue.mZ);
		}
	}

	// init the track
	// position
	if (mAnim.mAnimType == 'P') {
		motionPart->GetPosTrack().SetInterpolator(new LinearInterpolator<Vector3, Vector3>());
		motionPart->GetPosTrack().Init();
	}

	// rotation
	if (mAnim.mAnimType == 'R') {
		motionPart->GetRotTrack().SetInterpolator(new LinearQuaternionInterpolator<Compressed16BitQuaternion>());
		motionPart->GetRotTrack().Init();
	}

	// scaling
	if (mAnim.mAnimType == 'S') {
		motionPart->GetScaleTrack().SetInterpolator(new LinearInterpolator<Vector3, Vector3>());
		motionPart->GetScaleTrack().Init();
	}

	return true;
}

//==================================================================================================

bool MaterialChunkProcessor2::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	LMA_Material2 material;
	file->read((char*)&material, sizeof(LMA_Material2));

	// print material information
	if (GetLogging()) {
		LOG("- Material name = '%s'", material.mName);
		LOG("    + Ambient       : (%f, %f, %f)", material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB);
		LOG("    + Diffuse       : (%f, %f, %f)", material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB);
		LOG("    + Specular      : (%f, %f, %f)", material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB);
		LOG("    + Emissive      : (%f, %f, %f)", material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB);
		LOG("    + Shine         : %f", material.mShine);
		LOG("    + Shine strength: %f", material.mShineStrength);
		LOG("    + Opacity       : %f", material.mOpacity);
		LOG("    + IOR           : %f", material.mIOR);
		LOG("    + Double sided  : %d", material.mDoubleSided);
		LOG("    + WireFrame     : %d", material.mWireFrame);
		LOG("    + TransparencyTp: %c", material.mTransparencyType);
	}

	// create the material
	auto mat = std::make_shared<StandardMaterial>(material.mName);

	// setup its properties
	mat->SetAmbient(RGBAColor(material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB));
	mat->SetDiffuse(RGBAColor(material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB));
	mat->SetSpecular(RGBAColor(material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB));
	mat->SetEmissive(RGBAColor(material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB));
	mat->SetShine(material.mShine);
	mat->SetShineStrength(material.mShineStrength);
	mat->SetOpacity(material.mOpacity);
	mat->SetIOR(material.mIOR);
	mat->SetDoubleSided(material.mDoubleSided);
	mat->SetWireFrame(material.mWireFrame);
	mat->SetTransparencyType(material.mTransparencyType);

	mat->mShaderMask = 0;
	mat->mAlphaRef = 127;
	mat->mSrcBlend = 4;
	mat->mDstBlend = 5;

	// add the material to the actor
	actor->AddMaterial(0, mat);

	return true;
}

//==================================================================================================

bool MaterialChunkProcessor3::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	LMA_Material3 material;
	file->read((char*)&material, sizeof(LMA_Material3));

	// print material information
	if (GetLogging()) {
		LOG("- Material name = '%s'", material.mName);
		LOG("    + Ambient       : (%f, %f, %f)", material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB);
		LOG("    + Diffuse       : (%f, %f, %f)", material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB);
		LOG("    + Specular      : (%f, %f, %f)", material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB);
		LOG("    + Emissive      : (%f, %f, %f)", material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB);
		LOG("    + Shine         : %f", material.mShine);
		LOG("    + Shine strength: %f", material.mShineStrength);
		LOG("    + Opacity       : %f", material.mOpacity);
		LOG("    + IOR           : %f", material.mIOR);
		LOG("    + Double sided  : %d", material.mDoubleSided);
		LOG("    + WireFrame     : %d", material.mWireFrame);
		LOG("    + TransparencyTp: %c", material.mTransparencyType);
	}

	// create the material
	auto mat = std::make_shared<StandardMaterial>(material.mName);

	// setup its properties
	mat->SetAmbient(RGBAColor(material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB));
	mat->SetDiffuse(RGBAColor(material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB));
	mat->SetSpecular(RGBAColor(material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB));
	mat->SetEmissive(RGBAColor(material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB));
	mat->SetShine(material.mShine);
	mat->SetShineStrength(material.mShineStrength);
	mat->SetOpacity(material.mOpacity);
	mat->SetIOR(material.mIOR);
	mat->SetDoubleSided(material.mDoubleSided);
	mat->SetWireFrame(material.mWireFrame);
	mat->SetTransparencyType(material.mTransparencyType);

	// add the material to the actor
	actor->AddMaterial(0, mat);

	return true;
}

//==================================================================================================

bool MaterialChunkProcessor10000::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	LMA_Material10000 material;
	file->read((char*)&material, sizeof(LMA_Material10000));

	// print material information
	if (GetLogging()) {
		LOG("- Material name = '%s'", material.mName);
		LOG("    + Ambient: r=%f g=%f b=%f", material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB);
		LOG("    + Diffuse: r=%f g=%f b=%f", material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB);
		LOG("    + Specular: r=%f g=%f b=%f", material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB);
		LOG("    + Emissive: r=%f g=%f b=%f", material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB);
		LOG("    + Shine: %f", material.mShine);
		LOG("    + ShineStrength: %f", material.mShineStrength);
		LOG("    + Opacity: %f", material.mOpacity);
		LOG("    + IndexOfRefraction: %f", material.mIOR);
		LOG("    + DoubleSided: %d", material.mDoubleSided);
		LOG("    + WireFrame: %d", material.mWireFrame);
		LOG("    + TransparencyType: %c", material.mTransparencyType);
		LOG("    + +");
		LOG("    + ShaderMask: %d", material.mShaderMask);
		LOG("    + AlphaRef: %d", material.mAlphaRef);
		LOG("    + SourceBlend: %d", material.mSrcBlend);
		LOG("    + DestBlend: %d", material.mDstBlend);
		LOG("    + TextureFxn: %d", material.mTexFxn);
		LOG("    + TextureFxn: U=%f, V=%f, Sub0=%f", material.mTexFxnUParm0, material.mTexFxnVParm0, material.mTexFxnSub0Parm0);
		LOG("    + Reserved: %s", material.mReserved);
	}

	// create the material
	auto mat = std::make_shared<StandardMaterial>(material.mName);

	// setup its properties
	mat->SetAmbient(RGBAColor(material.mAmbient.mR, material.mAmbient.mG, material.mAmbient.mB));
	mat->SetDiffuse(RGBAColor(material.mDiffuse.mR, material.mDiffuse.mG, material.mDiffuse.mB));
	mat->SetSpecular(RGBAColor(material.mSpecular.mR, material.mSpecular.mG, material.mSpecular.mB));
	mat->SetEmissive(RGBAColor(material.mEmissive.mR, material.mEmissive.mG, material.mEmissive.mB));
	mat->SetShine(material.mShine);
	mat->SetShineStrength(material.mShineStrength);
	mat->SetOpacity(material.mOpacity);
	mat->SetIOR(material.mIOR);
	mat->SetDoubleSided(material.mDoubleSided);
	mat->SetWireFrame(material.mWireFrame);
	mat->SetTransparencyType(material.mTransparencyType);

	mat->mShaderMask = material.mShaderMask;
	mat->mAlphaRef = material.mAlphaRef;
	mat->mSrcBlend = material.mSrcBlend;
	mat->mDstBlend = material.mDstBlend;
	mat->mTexFxn = material.mTexFxn;
	mat->mTexFxnUParm = material.mTexFxnUParm0;
	mat->mTexFxnVParm = material.mTexFxnVParm0;
	mat->mTexFxnSub0Parm = material.mTexFxnSub0Parm0;

	// add the material to the actor
	actor->AddMaterial(0, mat);

	return true;
}

//==================================================================================================

bool MaterialLayerChunkProcessor2::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	// read the layer from disk
	LMA_MaterialLayer2 matLayer;
	file->read((char*)&matLayer, sizeof(LMA_MaterialLayer2));

	// convert the layer type
	int layerType;
	switch (matLayer.mMapType) {
	case LMA_LAYERID_DIFFUSE:		layerType = StandardMaterialLayer::LAYERTYPE_DIFFUSE;			break;
	case LMA_LAYERID_OPACITY:		layerType = StandardMaterialLayer::LAYERTYPE_OPACITY;			break;
	case LMA_LAYERID_SHINE:			layerType = StandardMaterialLayer::LAYERTYPE_SHINE;				break;
	case LMA_LAYERID_SHINESTRENGTH:	layerType = StandardMaterialLayer::LAYERTYPE_SHINESTRENGTH;		break;
	case LMA_LAYERID_SELFILLUM:		layerType = StandardMaterialLayer::LAYERTYPE_SELFILLUM;			break;
	case LMA_LAYERID_AMBIENT:		layerType = StandardMaterialLayer::LAYERTYPE_AMBIENT;			break;
	case LMA_LAYERID_SPECULAR:		layerType = StandardMaterialLayer::LAYERTYPE_SPECULAR;			break;
	case LMA_LAYERID_REFLECT:		layerType = StandardMaterialLayer::LAYERTYPE_REFLECT;			break;
	case LMA_LAYERID_REFRACT:		layerType = StandardMaterialLayer::LAYERTYPE_REFRACT;			break;
	case LMA_LAYERID_BUMP:			layerType = StandardMaterialLayer::LAYERTYPE_BUMP;				break;
	case LMA_LAYERID_FILTERCOLOR:	layerType = StandardMaterialLayer::LAYERTYPE_FILTERCOLOR;		break;
	case LMA_LAYERID_ENVIRONMENT:	layerType = StandardMaterialLayer::LAYERTYPE_ENVIRONMENT;		break;
	default:						layerType = StandardMaterialLayer::LAYERTYPE_UNKNOWN;
	};

	// create the layer
	auto layer = std::make_shared<StandardMaterialLayer>(layerType, matLayer.mTexture, matLayer.mAmount);

	// add the layer to the material
	auto mat = actor->GetMaterial(0, matLayer.mMaterialNumber);
	assert(mat->GetType() == StandardMaterial::TYPE_ID);
	auto stdMat = std::dynamic_pointer_cast<StandardMaterial>(mat);
	stdMat->AddLayer(layer);

	// set the properties
	layer->SetRotationRadians(matLayer.mRotationRadians);
	layer->SetUOffset(matLayer.mUOffset);
	layer->SetVOffset(matLayer.mVOffset);
	layer->SetUTiling(matLayer.mUTiling);
	layer->SetVTiling(matLayer.mVTiling);

	if (GetLogging()) {
		LOG("    - Material Layer");
		LOG("       + Texture  = %s", matLayer.mTexture);
		LOG("       + Material = %s (number %d)", mat->GetName().data(), matLayer.mMaterialNumber);
		LOG("       + Amount   = %f", matLayer.mAmount);
		LOG("       + MapType  = %d", matLayer.mMapType);
		LOG("       + UOffset  = %f", matLayer.mUOffset);
		LOG("       + VOffset  = %f", matLayer.mVOffset);
		LOG("       + UTiling  = %f", matLayer.mUTiling);
		LOG("       + VTiling  = %f", matLayer.mVTiling);
		LOG("       + Rotation = %f (radians)", matLayer.mRotationRadians);
	}

	return true;
}

//==================================================================================================

bool LimitChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	// read the limit from disk
	LMA_Limit lmaLimit;
	file->read((char*)&lmaLimit, sizeof(LMA_Limit));

	// create our limit attribute
	NodeLimitAttribute* limit = new NodeLimitAttribute();

	// set translation minimums and maximums
	limit->SetTranslationMin(Vector3(lmaLimit.mTranslationMin.mX, lmaLimit.mTranslationMin.mY, lmaLimit.mTranslationMin.mZ));
	limit->SetTranslationMax(Vector3(lmaLimit.mTranslationMax.mX, lmaLimit.mTranslationMax.mY, lmaLimit.mTranslationMax.mZ));

	// set rotation minimums and maximums
	limit->SetRotationMin(Vector3(lmaLimit.mRotationMin.mX, lmaLimit.mRotationMin.mY, lmaLimit.mRotationMin.mZ));
	limit->SetRotationMax(Vector3(lmaLimit.mRotationMax.mX, lmaLimit.mRotationMax.mY, lmaLimit.mRotationMax.mZ));

	// set scale minimums and maximums
	limit->SetScaleMin(Vector3(lmaLimit.mScaleMin.mX, lmaLimit.mScaleMin.mY, lmaLimit.mScaleMin.mZ));
	limit->SetScaleMax(Vector3(lmaLimit.mScaleMax.mX, lmaLimit.mScaleMax.mY, lmaLimit.mScaleMax.mZ));

	// set activation flags
	limit->EnableLimit(NodeLimitAttribute::TRANSLATIONX, lmaLimit.mLimitFlags[0]);
	limit->EnableLimit(NodeLimitAttribute::TRANSLATIONY, lmaLimit.mLimitFlags[1]);
	limit->EnableLimit(NodeLimitAttribute::TRANSLATIONZ, lmaLimit.mLimitFlags[2]);
	limit->EnableLimit(NodeLimitAttribute::ROTATIONX, lmaLimit.mLimitFlags[3]);
	limit->EnableLimit(NodeLimitAttribute::ROTATIONY, lmaLimit.mLimitFlags[4]);
	limit->EnableLimit(NodeLimitAttribute::ROTATIONZ, lmaLimit.mLimitFlags[5]);
	limit->EnableLimit(NodeLimitAttribute::SCALEX, lmaLimit.mLimitFlags[6]);
	limit->EnableLimit(NodeLimitAttribute::SCALEY, lmaLimit.mLimitFlags[7]);
	limit->EnableLimit(NodeLimitAttribute::SCALEZ, lmaLimit.mLimitFlags[8]);

	// try to get the node to which the limit belongs to
	Node* node = actor->GetNode(lmaLimit.mNodeNumber);

	// add the limit attribute to the node
	node->AddAttribute(limit);

	// print limit information
	if (GetLogging()) {
		LOG("- Limits for node '%s'", node->GetName().c_str());
		LOG("    + TranslateMin = %.3f, %.3f, %.3f", lmaLimit.mTranslationMin.mX, lmaLimit.mTranslationMin.mY, lmaLimit.mTranslationMin.mZ);
		LOG("    + TranslateMax = %.3f, %.3f, %.3f", lmaLimit.mTranslationMax.mX, lmaLimit.mTranslationMax.mY, lmaLimit.mTranslationMax.mZ);
		LOG("    + RotateMin    = %.3f, %.3f, %.3f", lmaLimit.mRotationMin.mX, lmaLimit.mRotationMin.mY, lmaLimit.mRotationMin.mZ);
		LOG("    + RotateMax    = %.3f, %.3f, %.3f", lmaLimit.mRotationMax.mX, lmaLimit.mRotationMax.mY, lmaLimit.mRotationMax.mZ);
		LOG("    + ScaleMin     = %.3f, %.3f, %.3f", lmaLimit.mScaleMin.mX, lmaLimit.mScaleMin.mY, lmaLimit.mScaleMin.mZ);
		LOG("    + ScaleMax     = %.3f, %.3f, %.3f", lmaLimit.mScaleMax.mX, lmaLimit.mScaleMax.mY, lmaLimit.mScaleMax.mZ);
		LOG("    + LimitFlags   = POS[%d, %d, %d], ROT[%d, %d, %d], SCALE[%d, %d, %d]", lmaLimit.mLimitFlags[0], lmaLimit.mLimitFlags[1], lmaLimit.mLimitFlags[2], lmaLimit.mLimitFlags[3], lmaLimit.mLimitFlags[4], lmaLimit.mLimitFlags[5], lmaLimit.mLimitFlags[6], lmaLimit.mLimitFlags[7], lmaLimit.mLimitFlags[8]);
	}

	return true;
}

//==================================================================================================

bool PhysicsInfoChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	// read the physics info from disk
	LMA_PhysicsInfo lmaPhysicsInfo;
	file->read((char*)&lmaPhysicsInfo, sizeof(LMA_PhysicsInfo));

	// create our physics info attribute
	NodePhysicsAttribute* physicsAttribute = new NodePhysicsAttribute();

	// set the mass
	physicsAttribute->SetMass(lmaPhysicsInfo.mMass);

	// create the physics object
	switch (lmaPhysicsInfo.mPhysicsObjectType) {
		// box
	case 0:
	{
		// create our box and set the width, height and depth
		NodePhysicsAttribute::Box* box = new NodePhysicsAttribute::Box;
		box->SetWidth(lmaPhysicsInfo.mBoxWidth);
		box->SetHeight(lmaPhysicsInfo.mBoxHeight);
		box->SetDepth(lmaPhysicsInfo.mBoxDepth);
		physicsAttribute->SetPhysicsObject(box);
		break;
	}

	// sphere
	case 1:
	{
		// create our sphere and set the radius
		NodePhysicsAttribute::Sphere* sphere = new NodePhysicsAttribute::Sphere;
		sphere->SetRadius(lmaPhysicsInfo.mSphereRadius);
		physicsAttribute->SetPhysicsObject(sphere);
		break;
	}

	// cylinder
	case 2:
	{
		// create our cylinder and set height and radius
		NodePhysicsAttribute::Cylinder* cylinder = new NodePhysicsAttribute::Cylinder;
		cylinder->SetRadius(lmaPhysicsInfo.mCylinderRadius);
		cylinder->SetHeight(lmaPhysicsInfo.mCylinderHeight);
		physicsAttribute->SetPhysicsObject(cylinder);
		break;
	}

	default:
		if (GetLogging()) LOG("    + Physics object type = %d (unsupported)", lmaPhysicsInfo.mPhysicsObjectType);
	}

	// try to get the node to which the physics info belongs to
	Node* node = actor->GetNodeByName(lmaPhysicsInfo.mName);

	if (node) {
		// add the physics info attribute to the node
		node->AddAttribute(physicsAttribute);
	} else {
		std::string errorMsg = "Error getting node ";
		errorMsg.append(lmaPhysicsInfo.mName);
		if (GetLogging()) LOG_ERROR(errorMsg);

		// we haven't found the node to which the physics info belongs to
		// get rid of the physics attribute
		delete physicsAttribute;
	}

	return true;
}

//==================================================================================================

bool MeshExpressionPartChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (GetLogging()) LOG("- Reading mesh expression part");

	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be invalid!");

		return false;
	}

	// read the expression part from disk
	LMA_MeshExpressionPart lmaExpressionPart;
	file->read((char*)&lmaExpressionPart, sizeof(LMA_MeshExpressionPart));
	if (GetLogging()) {
		LOG("    + Name          = %s", lmaExpressionPart.mName);
		LOG("    + LOD Level     = %d", lmaExpressionPart.mLOD);
		LOG("    + RangeMin      = %d", lmaExpressionPart.mRangeMin);
		LOG("    + RangeMax      = %d", lmaExpressionPart.mRangeMax);
		LOG("    + NumDeforms    = %d", lmaExpressionPart.mNumDeformDatas);
		LOG("    + NumTransforms = %d", lmaExpressionPart.mNumTransformations);
		LOG("    + IsPhoneme     = %s", (lmaExpressionPart.mPhonemeChar == 0) ? "No" : "Yes");
		LOG("    + Phoneme Sets  = %d", lmaExpressionPart.mNumPhonemeSets);
	}

	// get the level of detail of the expression part
	int expressionPartLOD = lmaExpressionPart.mLOD;

	// get the char to which this expression(phoneme) belongs to
	char phonemeChar = lmaExpressionPart.mPhonemeChar;

	// check if the facial setup has already been created, if not create it
	if (actor->GetFacialSetup(expressionPartLOD) == nullptr) {
		// create the facial setup
		auto facialSetup = std::make_shared<FacialSetup>();

		// bind it
		actor->SetFacialSetup(expressionPartLOD, facialSetup);
	}

	// get the expression name
	std::string expressionName = lmaExpressionPart.mName;

	// create the mesh expression part
	MeshExpressionPart* expressionPart = new MeshExpressionPart(true, true, actor, expressionName);

	expressionPart->SetPhonemeCharacter(lmaExpressionPart.mPhonemeChar);
	//expressionPart->SetPhonemeSet((BonesExpressionPart::EPhonemeSet)lmaExpressionPart.mPhonemeSet);

	if (phonemeChar == 0) {
		// add the expression part to the facial setup
		actor->GetFacialSetup(expressionPartLOD)->AddExpressionPart(expressionPart);
	} else {
		// add the phoneme to the facial setup
		actor->GetFacialSetup(expressionPartLOD)->AddPhoneme(expressionPart);
	}

	// set the slider range
	expressionPart->SetRangeMin(lmaExpressionPart.mRangeMin);
	expressionPart->SetRangeMax(lmaExpressionPart.mRangeMax);

	// read all deform datas
	if (GetLogging()) LOG("    - Reading deform datas...");
	for (int i = 0; i < lmaExpressionPart.mNumDeformDatas; i++) {
		LMA_FacialDeformData lmaDeformData;
		file->read((char*)&lmaDeformData, sizeof(LMA_FacialDeformData));

		if (GetLogging()) {
			LOG("    + Deform data info:");
			LOG("       - Node               = %s", lmaDeformData.mNodeName);
			LOG("       - NumVertices        = %d", lmaDeformData.mNumVertices);
		}

		Node* deformNode = actor->GetNodeByName(lmaDeformData.mNodeName);
		assert(deformNode != nullptr);

		MeshExpressionPart::DeformData* deformData = new MeshExpressionPart::DeformData(deformNode, lmaDeformData.mNumVertices);
		expressionPart->AddDeformData(deformData);

		//-------------------------------
		// create the mesh deformer
		auto stack = deformNode->GetMeshDeformerStack(0);

		// create the stack if it doesn't yet exist
		if (stack == nullptr) {
			stack = std::make_shared<MeshDeformerStack>(deformNode->GetMesh(0).get());
			deformNode->SetMeshDeformerStack(stack, 0);
		}

		// add the skinning deformer to the stack
		SmartMeshMorphDeformer* deformer = new SmartMeshMorphDeformer(expressionPart, i, deformNode->GetMesh(0).get());
		stack->InsertDeformer(0, deformer);
		//-------------------------------

		// read the deltas
		if (GetLogging()) LOG("   + Reading deltas");
		const int numVerts = lmaDeformData.mNumVertices;

		// allocate
		LMA_Vector3* deltas = (LMA_Vector3*)malloc(numVerts * sizeof(LMA_Vector3));

		// read the deltas
		file->read((char*)deltas, numVerts * sizeof(LMA_Vector3));

		// calculate the bounding box around the deltas
		AABB box;
		for (int a = 0; a < numVerts; a++)
			box.Encapsulate(Vector3(deltas[a].mX, deltas[a].mY, deltas[a].mZ));

		// calculate the minimum value of the box
		float minValue = box.GetMin().x;
		minValue = std::min(minValue, box.GetMin().y);
		minValue = std::min<float>(minValue, box.GetMin().z);

		// calculate the minimum value of the box
		float maxValue = box.GetMax().x;
		maxValue = std::max(maxValue, box.GetMax().y);
		maxValue = std::max(maxValue, box.GetMax().z);

		// make sure the values won't be too small and wil lead to compression errors
		if (maxValue - minValue < 1.0f) {
			if (minValue < 0 && minValue > -1)
				minValue = -1;

			if (maxValue > 0 && maxValue < 1)
				maxValue = 1;
		}

		// update the deform data min and max
		deformData->mMinValue = minValue;
		deformData->mMaxValue = maxValue;

		// now set and compress the vectors
		for (int a = 0; a < numVerts; a++)
			deformData->mDeltas[a].FromVector3(Vector3(deltas[a].mX, deltas[a].mY, deltas[a].mZ), minValue, maxValue);

		// delete the temparory buffer
		free(deltas);


		// read the normals
		LMA_Vector3 delta;
		for (int a = 0; a < numVerts; a++) {
			file->read((char*)&delta, sizeof(LMA_Vector3));
			deformData->mDeltaNormals[a].FromVector3(Vector3(delta.mX, delta.mY, delta.mZ), -1.0f, 1.0f);
		}

		// read the vertex numbers
		if (GetLogging()) LOG("   + Reading vertex numbers");
		int vtxNr;
		for (int a = 0; a < numVerts; a++) {
			file->read((char*)&vtxNr, sizeof(int));
			deformData->mVertexNumbers[a] = vtxNr;
		}
	}


	// read the facial transformations
	if (GetLogging()) LOG("    - Reading transformations...");
	for (int i = 0; i < lmaExpressionPart.mNumTransformations; i++) {
		// read the facial transformation from disk
		LMA_FacialTransformation lmaFacialTransformation;
		file->read((char*)&lmaFacialTransformation, sizeof(LMA_FacialTransformation));

		// try to get node id, if the given node doesn't exist skip the transformation
		std::string  nodeName = lmaFacialTransformation.mNodeName;
		Node* node = actor->GetNodeByName(nodeName.c_str());
		if (!node) {
			LOG("Could not get node: %s", nodeName.c_str());
			continue;
		}

		// create our transformation
		MeshExpressionPart::Transformation transform;
		transform.mPosition = Vector3(lmaFacialTransformation.mPosition.mX, lmaFacialTransformation.mPosition.mY, lmaFacialTransformation.mPosition.mZ);
		transform.mScale = Vector3(lmaFacialTransformation.mScale.mX, lmaFacialTransformation.mScale.mY, lmaFacialTransformation.mScale.mZ);
		transform.mRotation = Quaternion(lmaFacialTransformation.mRotation.mX, lmaFacialTransformation.mRotation.mY, lmaFacialTransformation.mRotation.mZ, lmaFacialTransformation.mRotation.mW);

		// add the transformation to the bones expression part
		expressionPart->AddTransformation(node->GetID(), transform);
	}

	// read the phoneme sets
	if (GetLogging()) LOG("    - Reading phoneme sets...");
	std::vector<ExpressionPart::EPhonemeSet> phonemeSets;
	phonemeSets.reserve(lmaExpressionPart.mNumPhonemeSets);
	for (int i = 0; i < lmaExpressionPart.mNumPhonemeSets; i++) {
		int set;
		file->read((char*)&set, sizeof(int));
		phonemeSets[i] = (ExpressionPart::EPhonemeSet)set;
	}

	// set the read phoneme sets
	expressionPart->SetPhonemeSets(phonemeSets);

	return true;
}

//==================================================================================================

bool MeshExpressionPartChunkProcessor2::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (GetLogging()) LOG("- Reading mesh expression part [v2]");

	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be invalid!");

		return false;
	}

	// read the expression part from disk
	LMA_MeshExpressionPart2 lmaExpressionPart;
	file->read((char*)&lmaExpressionPart, sizeof(LMA_MeshExpressionPart2));
	if (GetLogging()) {
		LOG("    + Name          = %s", lmaExpressionPart.mName);
		LOG("    + LOD Level     = %d", lmaExpressionPart.mLOD);
		LOG("    + RangeMin      = %d", lmaExpressionPart.mRangeMin);
		LOG("    + RangeMax      = %d", lmaExpressionPart.mRangeMax);
		LOG("    + NumDeforms    = %d", lmaExpressionPart.mNumDeformDatas);
		LOG("    + NumTransforms = %d", lmaExpressionPart.mNumTransformations);
		LOG("    + IsPhoneme     = %s", (lmaExpressionPart.mPhonemeChar == 0) ? "No" : "Yes");
		LOG("    + Phoneme Sets  = %d", lmaExpressionPart.mNumPhonemeSets);
	}

	// get the level of detail of the expression part
	int expressionPartLOD = lmaExpressionPart.mLOD;

	// get the char to which this expression(phoneme) belongs to
	char phonemeChar = lmaExpressionPart.mPhonemeChar;

	// check if the facial setup has already been created, if not create it
	if (actor->GetFacialSetup(expressionPartLOD) == nullptr) {
		// create the facial setup
		auto facialSetup = std::make_shared<FacialSetup>();

		// bind it
		actor->SetFacialSetup(expressionPartLOD, facialSetup);
	}

	// get the expression name
	std::string  expressionName = lmaExpressionPart.mName;

	// create the mesh expression part
	MeshExpressionPart* expressionPart = new MeshExpressionPart(true, true, actor, expressionName);

	expressionPart->SetPhonemeCharacter(lmaExpressionPart.mPhonemeChar);
	//expressionPart->SetPhonemeSet((BonesExpressionPart::EPhonemeSet)lmaExpressionPart.mPhonemeSet);

	if (phonemeChar == 0) {
		// add the expression part to the facial setup
		actor->GetFacialSetup(expressionPartLOD)->AddExpressionPart(expressionPart);
	} else {
		// add the phoneme to the facial setup
		actor->GetFacialSetup(expressionPartLOD)->AddPhoneme(expressionPart);
	}

	// set the slider range
	expressionPart->SetRangeMin(lmaExpressionPart.mRangeMin);
	expressionPart->SetRangeMax(lmaExpressionPart.mRangeMax);

	// read all deform datas
	if (GetLogging()) LOG("    - Reading deform datas...");
	for (int i = 0; i < lmaExpressionPart.mNumDeformDatas; i++) {
		LMA_FacialDeformData2 lmaDeformData;
		file->read((char*)&lmaDeformData, sizeof(LMA_FacialDeformData2));

		if (GetLogging()) {
			LOG("    + Deform data info:");
			LOG("       - Node        = %s", lmaDeformData.mNodeName);
			LOG("       - NumVertices = %d", lmaDeformData.mNumVertices);
		}

		Node* deformNode = actor->GetNodeByName(lmaDeformData.mNodeName);
		assert(deformNode != nullptr);

		MeshExpressionPart::DeformData* deformData = new MeshExpressionPart::DeformData(deformNode, lmaDeformData.mNumVertices);
		expressionPart->AddDeformData(deformData);

		// set the min and max values, used to define the compression/quantitization range for the positions
		deformData->mMinValue = lmaDeformData.mMinValue;
		deformData->mMaxValue = lmaDeformData.mMaxValue;

		//-------------------------------
		// create the mesh deformer
		auto stack = deformNode->GetMeshDeformerStack(0);

		// create the stack if it doesn't yet exist
		if (stack == nullptr) {
			stack = std::make_shared<MeshDeformerStack>(deformNode->GetMesh(0).get());
			deformNode->SetMeshDeformerStack(stack, 0);
		}

		// add the skinning deformer to the stack
		SmartMeshMorphDeformer* deformer = new SmartMeshMorphDeformer(expressionPart, i, deformNode->GetMesh(0).get());
		stack->InsertDeformer(0, deformer);
		//-------------------------------

		// read the deltas
		if (GetLogging()) LOG("   + Reading deltas");
		const int numVerts = lmaDeformData.mNumVertices;

		// read the positions
		LMA_16BitVector3 deltaPos;
		for (int d = 0; d < numVerts; d++) {
			file->read((char*)&deltaPos, sizeof(LMA_16BitVector3));
			deformData->mDeltas[d] = Compressed16BitVector3(deltaPos.mX, deltaPos.mY, deltaPos.mZ);
		}

		// read the normals
		LMA_8BitVector3 delta;
		for (int d = 0; d < numVerts; d++) {
			file->read((char*)&delta, sizeof(LMA_8BitVector3));
			deformData->mDeltaNormals[d] = Compressed8BitVector3(delta.mX, delta.mY, delta.mZ);
		}

		// read the vertex numbers
		if (GetLogging()) LOG("   + Reading vertex numbers");
		int vtxNr;
		for (int a = 0; a < numVerts; a++) {
			file->read((char*)&vtxNr, sizeof(int));
			deformData->mVertexNumbers[a] = vtxNr;
		}
	}


	// read the facial transformations
	if (GetLogging()) LOG("    - Reading transformations...");
	for (int i = 0; i < lmaExpressionPart.mNumTransformations; i++) {
		// read the facial transformation from disk
		LMA_FacialTransformation lmaFacialTransformation;
		file->read((char*)&lmaFacialTransformation, sizeof(LMA_FacialTransformation));

		// try to get node id, if the given node doesn't exist skip the transformation
		std::string  nodeName = lmaFacialTransformation.mNodeName;
		Node* node = actor->GetNodeByName(nodeName.c_str());
		if (!node) {
			LOG("Could not get node: %s", nodeName.c_str());
			continue;
		}

		// create our transformation
		MeshExpressionPart::Transformation transform;
		transform.mPosition = Vector3(lmaFacialTransformation.mPosition.mX, lmaFacialTransformation.mPosition.mY, lmaFacialTransformation.mPosition.mZ);
		transform.mScale = Vector3(lmaFacialTransformation.mScale.mX, lmaFacialTransformation.mScale.mY, lmaFacialTransformation.mScale.mZ);
		transform.mRotation = Quaternion(lmaFacialTransformation.mRotation.mX, lmaFacialTransformation.mRotation.mY, lmaFacialTransformation.mRotation.mZ, lmaFacialTransformation.mRotation.mW);

		// add the transformation to the bones expression part
		expressionPart->AddTransformation(node->GetID(), transform);
	}

	// read the phoneme sets
	if (GetLogging()) LOG("    - Reading phoneme sets...");
	std::vector<ExpressionPart::EPhonemeSet> phonemeSets;
	phonemeSets.reserve(lmaExpressionPart.mNumPhonemeSets);
	for (int i = 0; i < lmaExpressionPart.mNumPhonemeSets; i++) {
		int set;
		file->read((char*)&set, sizeof(int));
		phonemeSets[i] = (ExpressionPart::EPhonemeSet)set;
	}

	// set the read phoneme sets
	expressionPart->SetPhonemeSets(phonemeSets);

	return true;
}

//==================================================================================================

bool ExpressionMotionPartChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!motion) {
		if (GetLogging())
			LOG("Passed motion seems to be not valid!");

		return false;
	}

	// cast motion
	FacialMotion* facialMotion = (FacialMotion*)motion;

	// get the expression motion part
	LMA_ExpressionMotionPart lmaExpressionPart;
	file->read((char*)&lmaExpressionPart, sizeof(LMA_ExpressionMotionPart));

	// create a new facial motion part and set it's id
	int id = NodeIDGenerator::GetInstance().GenerateIDForString(lmaExpressionPart.mName);
	FacialMotionPart* motionPart = new FacialMotionPart(id);

	// get the part's keytrack
	auto keytrack = motionPart->GetKeyTrack();

	// set the interpolator(always using hermite interpolation for facial animation)
	keytrack.SetInterpolator(new HermiteInterpolator<float, RCore::Compressed8BitFloat>());

	if (GetLogging()) {
		LOG("    - Expression Motion Part: %s", lmaExpressionPart.mName);
		LOG("       + Interpolation Type = Hermite");
		LOG("       + NrKeys             = %d", lmaExpressionPart.mNrKeys);
	}

	// add keyframes
	for (int i = 0; i < lmaExpressionPart.mNrKeys; i++) {
		LMA_UnsignedShortKey	lmaKey;

		// read the keyframe
		file->read((char*)&lmaKey, sizeof(LMA_UnsignedShortKey));

		// gives it into range of 0..1
		float normalizedValue = lmaKey.mValue / (float)65535;

		// add the keyframe to the keytrack
		keytrack.AddKey(lmaKey.mTime, normalizedValue);
	}

	keytrack.SetLoopMode(ReActionFX::KeyTrack<float, RCore::Compressed8BitFloat>::KEYTRACK_NO_LOOP);

	// init the keytrack
	keytrack.Init();

	// add the expression motion part
	facialMotion->AddExpMotionPart(motionPart);

	// each keytrack which stops before maxtime gets an ending key with time=maxTime.
	PhonemeMotionDataChunkProcessor1::SyncMotionTrackEnds(facialMotion);

	return true;
}

//==================================================================================================

bool PhonemeMotionDataChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	if (!motion) {
		if (GetLogging())
			LOG("Passed motion seems to be not valid!");

		return false;
	}

	// cast motion
	FacialMotion* facialMotion = (FacialMotion*)motion;

	// get the facial motion part
	LMA_PhonemeMotionData lmaPhonemePart;
	file->read((char*)&lmaPhonemePart, sizeof(LMA_PhonemeMotionData));

	if (GetLogging()) {
		LOG("    - Phoneme Motion Data");
		LOG("       + Interpolation Type = Hermite");
		LOG("       + NrKeys             = %d", lmaPhonemePart.mNrKeys);
		LOG("       + NumPhonemes        = %d", lmaPhonemePart.mNumPhonemes);
	}

	// get the number of phonemes
	int numPhonemes = lmaPhonemePart.mNumPhonemes;

	// get all phoneme names
	for (int i = 0; i < numPhonemes; i++) {
		char phonemeName[40];

		// read the name
		file->read(phonemeName, sizeof(char) * 40);
		if (GetLogging()) LOG("          - Phoneme             = %s", phonemeName);

		// create a new facial motion part and set it's id
		int id = NodeIDGenerator::GetInstance().GenerateIDForString(phonemeName);
		FacialMotionPart* motionPart = new FacialMotionPart(id);

		// get the part's keytrack
		auto keytrack = motionPart->GetKeyTrack();

		// set the interpolator(always using hermite interpolation for facial animation)
		keytrack.SetInterpolator(new HermiteInterpolator<float, RCore::Compressed8BitFloat>());

		keytrack.SetLoopMode(ReActionFX::KeyTrack<float, RCore::Compressed8BitFloat>::KEYTRACK_NO_LOOP);

		// add the phoneme motion part
		facialMotion->AddPhoMotionPart(motionPart);
	}

	// if assert fails, something strange has happened
	assert(numPhonemes == facialMotion->GetNumPhoMotionParts());

	// iterate through all keyframes
	for (int i = 0; i < lmaPhonemePart.mNrKeys; i++) {
		LMA_PhonemeKey lmaKey;

		// read the keyframe
		file->read((char*)&lmaKey, sizeof(LMA_PhonemeKey));

		//LOG("          - Key: Time=%f, PhoNum=%i, Power=%i", lmaKey.mTime, lmaKey.mPhonemeNumber, lmaKey.mPowerValue);

		// iterate through all phoneme keytracks and add the given keyframe
		for (int j = 0; j < facialMotion->GetNumPhoMotionParts(); j++) {
			// get the part's keytrack
			auto& keytrack = facialMotion->GetPhoKeyTrack(j);

			// if the first key isn't at the beginning of the animation, so at time=0.0
			// add a keyframe in front of them all
			if (i == 0 && lmaKey.mTime > 0.0) {
				// add it sorted since the keys are stored in the order the artist adds them in lmstudio
				keytrack.AddKeySorted(0, 0);
			}

			float keyTime = lmaKey.mTime;
			float keyValue;

			// check if this key belongs to the current phoneme
			// if yes set the power value, if no reset it to 0.0 (phoneme pose reset)
			if (j == lmaKey.mPhonemeNumber)
				keyValue = lmaKey.mPowerValue / (float)255.0f;
			else
				keyValue = 0;

			// add it sorted since the keys are stored in the order the artist adds them in lmstudio
			keytrack.AddKeySorted(keyTime, keyValue);
		}
	}

	// init all keytracks
	for (int i = 0; i < facialMotion->GetNumPhoMotionParts(); i++) {
		// get the part's keytrack
		auto& keytrack = facialMotion->GetPhoKeyTrack(i);

		// init the keytrack
		keytrack.Init();
	}

	// each keytrack which stops before maxtime gets an ending key with time=maxTime.
	PhonemeMotionDataChunkProcessor1::SyncMotionTrackEnds(facialMotion);

	return true;
}

//==================================================================================================

void PhonemeMotionDataChunkProcessor1::SyncMotionTrackEnds(FacialMotion* facialMotion) {
	// the maximum time of the animation
	float maxTime = FindMaxTime(facialMotion);

	// iterate through all phoneme keytracks
	for (int i = 0; i < facialMotion->GetNumPhoMotionParts(); i++) {
		// get the part's keytrack
		auto& keytrack = facialMotion->GetPhoKeyTrack(i);

		// add an ending key if the track stops before maxTime
		if (keytrack.GetNumKeys() > 0) {
			if (keytrack.GetLastKey()->GetTime() < maxTime) {
				// add it sorted since the keys are stored in the order the artist adds them in lmstudio
				keytrack.AddKeySorted(maxTime, 0);
			}
		}
	}

	// iterate through all expression keytracks
	for (int i = 0; i < facialMotion->GetNumExpMotionParts(); i++) {
		// get the part's keytrack
		auto& keytrack = facialMotion->GetExpKeyTrack(i);

		// add an ending key if the track stops before maxTime
		if (keytrack.GetNumKeys() > 0) {
			if (keytrack.GetLastKey()->GetTime() < maxTime) {
				// add it sorted since the keys are stored in the order the artist adds them in lmstudio
				keytrack.AddKeySorted(maxTime, 0);
			}
		}
	}
}

//==================================================================================================

float PhonemeMotionDataChunkProcessor1::FindMaxTime(FacialMotion* facialMotion) {
	// the maximum time of the animation
	float maxTime = 0.0;

	// iterate through all phoneme keytracks
	for (int i = 0; i < facialMotion->GetNumPhoMotionParts(); i++) {
		// get the part's keytrack
		auto keytrack = facialMotion->GetPhoKeyTrack(i);

		// check if the maxtime isn't the real maxtime anymore and replace if in that case
		if (keytrack.GetNumKeys() > 0)
			if (keytrack.GetLastKey()->GetTime() > maxTime)
				maxTime = keytrack.GetLastKey()->GetTime();
	}

	// iterate through all expression keytracks
	for (int i = 0; i < facialMotion->GetNumExpMotionParts(); i++) {
		// get the part's keytrack
		auto keytrack = facialMotion->GetExpKeyTrack(i);

		// check if the maxtime isn't the real maxtime anymore and replace if in that case
		if (keytrack.GetNumKeys() > 0)
			if (keytrack.GetLastKey()->GetTime() > maxTime)
				maxTime = keytrack.GetLastKey()->GetTime();
	}

	// return the maximum time of the facial animation
	return maxTime;
}

//==================================================================================================

bool FXMaterialChunkProcessor1::Process(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	// validate the actor
	if (!actor) {
		if (GetLogging())
			LOG("Passed actor seems to be not valid!");

		return false;
	}

	// read the header
	LMA_FXMaterial material;
	file->read((char*)&material, sizeof(LMA_FXMaterial));

	// print material information
	if (GetLogging()) {
		LOG("- Material name = '%s'", material.mName);
		LOG("  + Effect file       = '%s'", material.mEffectFile);
		LOG("  + Num int params    = %d", material.mNumIntParams);
		LOG("  + Num float params  = %d", material.mNumFloatParams);
		LOG("  + Num color params  = %d", material.mNumColorParams);
		LOG("  + Num bitmap params = %d", material.mNumBitmapParams);
	}

	// create the EMFX FX material and add it to the actor
	auto mat = std::make_shared<FXMaterial>(material.mName);

	actor->AddMaterial(0, mat);

	// setup the material values
	mat->SetEffectFile(material.mEffectFile);


	// read the int parameters
	if (GetLogging()) LOG("  + Parameters:");
	for (int i = 0; i < material.mNumIntParams; i++) {
		LMA_FXIntParameter fileParam;
		file->read((char*)&fileParam, sizeof(LMA_FXIntParameter));
		if (GetLogging()) LOG("    + int %s = %d", fileParam.mName, fileParam.mValue);
		mat->AddIntParameter(fileParam.mName, fileParam.mValue);
	}

	// read the float parameters
	for (int i = 0; i < material.mNumFloatParams; i++) {
		LMA_FXFloatParameter fileParam;
		file->read((char*)&fileParam, sizeof(LMA_FXFloatParameter));
		if (GetLogging()) LOG("    + float %s = %f", fileParam.mName, fileParam.mValue);
		mat->AddFloatParameter(fileParam.mName, fileParam.mValue);
	}

	// read the color parameters
	for (int i = 0; i < material.mNumColorParams; i++) {
		LMA_FXColorParameter fileParam;
		file->read((char*)&fileParam, sizeof(LMA_FXColorParameter));
		if (GetLogging()) LOG("    + color %s = (%f, %f, %f, %f)", fileParam.mName, fileParam.mR, fileParam.mG, fileParam.mB, fileParam.mA);
		mat->AddColorParameter(fileParam.mName, RCore::RGBAColor(fileParam.mR, fileParam.mG, fileParam.mB, fileParam.mA));
	}

	// read the bitmap parameters
	for (int i = 0; i < material.mNumBitmapParams; i++) {
		LMA_FXBitmapParameter fileParam;
		file->read((char*)&fileParam, sizeof(LMA_FXBitmapParameter));
		if (GetLogging()) LOG("    + string %s = %s", fileParam.mName, fileParam.mFilename);
		mat->AddStringParameter(fileParam.mName, fileParam.mFilename);
	}

	return true;
}

} // namespace ReActionFX