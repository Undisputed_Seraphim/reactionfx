#include <limits>
#include <unordered_set>
#include "FacialMotion.h"
#include "ExpressionPart.h"
#include "Actor.h"
#include "FacialSetup.h"

using namespace RCore;

namespace ReActionFX {

// destructor
FacialMotion::~FacialMotion() {
	// delete all expression motion parts
	for (auto* expMotionPart : mExpMotionParts) {
		delete expMotionPart;
	}
	mExpMotionParts.clear();

	// delete all phoneme motion parts
	for (auto* phoMotionPart : mPhoMotionParts) {
		delete phoMotionPart;
	}
	mPhoMotionParts.clear();
}


// restore all touched nodes inside the actor to their original transformations
void FacialMotion::RestoreActor(Actor* actor) {
	// check if we have a facial setup
	auto facialSetup = actor->GetFacialSetup(actor->GetCurrentLOD());
	if (facialSetup == nullptr)
		return;

	// process all expression parts
	for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++)
		facialSetup->GetExpressionPart(i)->RestoreActor(actor);

	// process all phonemes
	for (int i = 0; i < facialSetup->GetNumPhonemes(); i++)
		facialSetup->GetPhoneme(i)->RestoreActor(actor);
}


// create the motion links
void FacialMotion::CreateMotionLinks(Actor* actor, MotionInstance* instance) {
	// check if we have a facial setup
	auto facialSetup = actor->GetFacialSetup(0);
	if (facialSetup == nullptr)
		return;

	// first build a list of nodes which are influenced by all expression parts
	std::unordered_set<Node*> nodes;

	// traverse all nodes
	for (int n = 0; n < actor->GetNumNodes(); n++) {
		Node* curNode = actor->GetNode(n);

		// traverse all expression parts
		for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
			// if the expression part influences this node
			if (facialSetup->GetExpressionPart(i)->Influences(curNode)) {
				nodes.insert(curNode);
			}
		}

		// traverse all phonemes
		for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
			// if the phoneme influences this node
			if (facialSetup->GetPhoneme(i)->Influences(curNode)) {
				nodes.insert(curNode);
			}
		}
	}

	// so now we have a list of nodes which are influenced by this facial motion
	// now we can create motion links inside these nodes
	for (auto node : nodes) {
		// add the motion link, with a nullptr motion part, since we don't use the motion part for this motion		
		node->AddMotionLink(MotionLink(nullptr, instance));
		//LOG("Adding motion link for node %s", node->GetNamePtr());
	}
}


// get the maximum time value and update the stored one
void FacialMotion::UpdateMaxTime() {
	// check expression tracks for max value
	for (auto* motionPart : mExpMotionParts) {
		KeyTrack<float, RCore::Compressed8BitFloat>& keytrack = motionPart->GetKeyTrack();

		// check the two time values and return the biggest one
		mMaxTime = std::max(mMaxTime, keytrack.GetLastTime());
	}

	// check phoneme tracks for max value
	for (auto* motionPart : mPhoMotionParts) {
		KeyTrack<float, RCore::Compressed8BitFloat>& keytrack = motionPart->GetKeyTrack();

		// check the two time values and return the biggest one
		mMaxTime = std::max(mMaxTime, keytrack.GetLastTime());
	}
}



// the main function which returns the new node transformation after this motion has been processed
NodeTransform FacialMotion::CalcNodeTransform(Actor* actor, Node* node, const float timeValue) {
	// get the current local transformation of the node
	Vector3		position = node->GetLocalPos();
	Quaternion	rotation = node->GetLocalRot();
	Vector3		scale = node->GetLocalScale();

	// check if we have a facial setup
	FacialSetup* facialSetup = actor->GetFacialSetup(actor->GetCurrentLOD()).get();
	if (facialSetup != nullptr) {
		// pass them through all expression parts
		for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
			ExpressionPart* part = facialSetup->GetExpressionPart(i);

			// find the motion part number with the given ID
			int motionPartNr = FindExpMotionPart(part->GetID());
			if (motionPartNr == -1)
				continue;

			// get the keytrack
			KeyTrack<float, Compressed8BitFloat>& keyTrack = mExpMotionParts[motionPartNr]->GetKeyTrack();

			// update the weight and apply the transformation
			// don't process expresion parts that are controlled manually
			if (part->IsInManualMode() == false)
				part->SetWeight(keyTrack.GetValueAtTime(timeValue));

			// apply the transformation
			part->ApplyTransformation(actor, node, position, rotation, scale);
		}

		// pass them through all phonemes
		for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
			ExpressionPart* phoneme = facialSetup->GetPhoneme(i);

			// find the motion part number with the given ID
			int motionPartNr = FindPhoMotionPart(phoneme->GetID());
			if (motionPartNr == -1)
				continue;

			// get the keytrack
			KeyTrack<float, RCore::Compressed8BitFloat>& keyTrack = mPhoMotionParts[motionPartNr]->GetKeyTrack();

			// update the weight and apply the transformation
			if (phoneme->IsInManualMode() == false)
				phoneme->SetWeight(keyTrack.GetValueAtTime(timeValue));

			// apply the transformation
			phoneme->ApplyTransformation(actor, node, position, rotation, scale);
		}
	}

	// construct the resulting node transformation
	NodeTransform result;
	result.mPosition = position;
	result.mRotation = rotation;
	result.mScale = scale;

	// return the resulting node transform
	return result;
}


// apply the facial deformations to the actor
void FacialMotion::Apply(Actor* actor, const bool updateWeights) {
	// restore the actor
	RestoreActor(actor);

	// check if we have a facial setup
	FacialSetup* facialSetup = actor->GetFacialSetup(actor->GetCurrentLOD()).get();
	if (facialSetup == nullptr)
		return;

	// pass them through all expression parts
	for (int i = 0; i < facialSetup->GetNumExpressionParts(); i++) {
		ExpressionPart* expPart = facialSetup->GetExpressionPart(i);

		// if we need to extract and overwrite the weight value of the expression part
		if (updateWeights && (expPart->IsInManualMode() == false)) {
			int motionPartNr = FindExpMotionPart(expPart->GetID());

			if (motionPartNr != -1) {
				float weight = mExpMotionParts[motionPartNr]->GetKeyTrack().GetCurrentValue();
				expPart->SetWeight(weight);
			}
		}

		// apply the expression part
		expPart->Apply(actor);
	}

	// pass them through all phonemes
	for (int i = 0; i < facialSetup->GetNumPhonemes(); i++) {
		ExpressionPart* expPart = facialSetup->GetPhoneme(i);

		// if we need to extract and overwrite the weight value of the expression part
		if (updateWeights && (expPart->IsInManualMode() == false)) {
			int motionPartNr = FindPhoMotionPart(expPart->GetID());

			if (motionPartNr != -1) {
				float weight = mPhoMotionParts[motionPartNr]->GetKeyTrack().GetCurrentValue();
				expPart->SetWeight(weight);
			}
		}

		// apply the expression part
		expPart->Apply(actor);
	}
}


void FacialMotion::RemoveExpMotionPart(const int nr) {
	delete mExpMotionParts[nr];
	mExpMotionParts.erase(mExpMotionParts.begin() + nr);
}


int FacialMotion::FindExpMotionPart(const int id) const {
	auto it = std::find_if(mExpMotionParts.begin(), mExpMotionParts.end(), [&id] (const FacialMotionPart* part) {
		return part->GetID() == id;
	});

	return (it == mExpMotionParts.end()) ? -1 : (mExpMotionParts.begin() - it);
}


//-------

void FacialMotion::RemovePhoMotionPart(const int nr) {
	delete mPhoMotionParts[nr];
	mPhoMotionParts.erase(mPhoMotionParts.begin() + nr);
}


int FacialMotion::FindPhoMotionPart(const int id) const {
	auto it = std::find_if(mPhoMotionParts.begin(), mPhoMotionParts.end(), [&id] (const FacialMotionPart* part) {
		return part->GetID() == id;
	});

	return (it == mPhoMotionParts.end()) ? -1 : (mPhoMotionParts.begin() - it);
}


void FacialMotion::MakeLoopable(const float fadeTime) {
	// make the expression part tracks loopable
	for (auto* motionPart : mExpMotionParts)
		motionPart->GetKeyTrack().MakeLoopable(fadeTime);

	// make the phoneme tracks loopable
	for (auto* motionPart : mPhoMotionParts)
		motionPart->GetKeyTrack().MakeLoopable(fadeTime);

	// update the maximum time
	UpdateMaxTime();
}


} // namespace ReActionFX