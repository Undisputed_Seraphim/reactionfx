#pragma once

#include "RCore.h"

namespace ReActionFX {

/**
 * The keyframe class.
 * Each keyframe holds a value at a given time.
 * Interpolators can then calculate interpolated values between a set of keys, which are stored
 * inside a key track. This makes it possible to do keyframed animations.
 */
template <class ReturnType, class StorageType>
class KeyFrame {
public:
	/**
	 * Default constructor.
	 */
	KeyFrame() : mTime(0) {}

	/**
	 * Constructor which sets the time and value.
	 * @param time The time of the keyframe, in seconds.
	 * @param value The value at this time.
	 */
	KeyFrame(const float time, const ReturnType& value) : mTime(time) {
		SetValue(value);
	}

	/**
	 * Destructor.
	 */
	~KeyFrame() {}

	/**
	 * Return the time of the keyframe.
	 * @return The time, in seconds.
	 */
	float GetTime() const {
		return mTime;
	}

   /**
	* Return the value of the keyframe.
	* @return The value of the keyframe.
	*/
	ReturnType GetValue() const {
		return mValue;
	}

   /**
	* Set the time of the keyframe.
	* @param time The time of the keyframe, in seconds.
	*/
	void SetTime(const float time) {
		mTime = time;
	}

   /**
	* Set the value.
	* @param value The value.
	*/
	void SetValue(const ReturnType& value) {
		mValue = value;
	}

protected:
	StorageType	mValue;	/* The key value. */
	float		mTime;	/* Time in seconds. */
};


//--------------------------------------------------------------------------------------
// Partial Template Specialization for KeyFrames with the following template arguments:
// ReturnType:  Quaternion
// StorageType: Compressed8BitQuaternion
//--------------------------------------------------------------------------------------
// compress a quaternion
template<> inline
void KeyFrame<RCore::Quaternion, RCore::Compressed8BitQuaternion>::SetValue(const RCore::Quaternion& value) { mValue.FromQuaternion(value); }


// decompress into a quaternion
template<> inline
RCore::Quaternion KeyFrame<RCore::Quaternion, RCore::Compressed8BitQuaternion>::GetValue() const { return mValue.ToQuaternion(); }


//--------------------------------------------------------------------------------------
// Partial Template Specialization for KeyFrames with the following template arguments:
// ReturnType:  Quaternion
// StorageType: Compressed16BitQuaternion
//--------------------------------------------------------------------------------------
// compress a quaternion
template<> inline
void KeyFrame<RCore::Quaternion, RCore::Compressed16BitQuaternion>::SetValue(const RCore::Quaternion& value) { mValue.FromQuaternion(value); }


// decompress into a quaternion
template<> inline
RCore::Quaternion KeyFrame<RCore::Quaternion, RCore::Compressed16BitQuaternion>::GetValue() const { return mValue.ToQuaternion(); }


//--------------------------------------------------------------------------------------
// Partial Template Specialization for KeyFrames with the following template arguments:
// ReturnType:  float
// StorageType: Compressed8BitFloat
//--------------------------------------------------------------------------------------
// compress a float
template<> inline
void KeyFrame<float, RCore::Compressed8BitFloat>::SetValue(const float& value) { mValue.FromFloat(value, 0.0f, 1.0f); }


// decompress into a float
template<> inline
float KeyFrame<float, RCore::Compressed8BitFloat>::GetValue() const { return mValue.ToFloat(0.0f, 1.0f); }


//--------------------------------------------------------------------------------------
// Partial Template Specialization for KeyFrames with the following template arguments:
// ReturnType:  float
// StorageType: Compressed16BitFloat
//--------------------------------------------------------------------------------------
// compress a float
template<> inline
void KeyFrame<float, RCore::Compressed16BitFloat>::SetValue(const float& value) { mValue.FromFloat(value, 0.0f, 1.0f); }


// decompress into a float
template<> inline
float KeyFrame<float, RCore::Compressed16BitFloat>::GetValue() const { return mValue.ToFloat(0.0f, 1.0f); }


} // namespace