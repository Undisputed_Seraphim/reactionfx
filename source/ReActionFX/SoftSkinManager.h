#pragma once

#include "RCore.h"

#include "Mesh.h"
#include "SoftSkinDeformer.h"

namespace ReActionFX {

/**
 * The softskin manager.
 * This manager allows to create optimized softskin deformers, which will run as fast as possible
 * on the hardware of the user. In other words, specialised version of softskin deformers can be
 * created using this class. For example: if the hardware supports SSE, an SSE optimized softskin deformer
 * will be returned, instead of the normal C++ version.
 */
class SoftSkinManager {
	SINGLETON(SoftSkinManager);

public:
	/**
	 * The constructor.
	 * When constructed, the class checks if SSE is available on the hardware.
	 */
	SoftSkinManager() {}

	/**
	 * Creates the softskin deformer, by looking at the hardware capabilities.
	 * If SSE is present, an SSE optimized softskinner will be returned, otherwise a normal
	 * C++ version.
	 */
	SoftSkinDeformer* CreateDeformer(Mesh* mesh) {
		return new SoftSkinDeformer(mesh);
	}

};

} // namespace