#include "KeyTrack.h"
#include "Node.h"
#include "Actor.h"
#include "LinearInterpolators.h"
#include "ExpressionPart.h"
#include "NodeIDGenerator.h"

using namespace RCore;

namespace ReActionFX {

// the constructor
ExpressionPart::ExpressionPart(Actor* parentActor, const std::string & name) {
	mCurWeight = 0.0f;
	mActor = parentActor;
	mName = name;
	mCharacter = 0;
	mRangeMin = 0.0f;
	mRangeMax = 1.0f;
	mManualMode = false;

	// calculate the ID
	SetName(name);
}


// update the name and ID
void ExpressionPart::SetName(const std::string & name) {
	mName = name;
	mID = NodeIDGenerator::GetInstance().GenerateIDForString(name);
}


float ExpressionPart::CalcZeroInfluenceWeight() const {
	return std::fabs(mRangeMin) / std::fabs(mRangeMax - mRangeMin);
}


} // namespace