#include "Node.h"
#include "SimpleMesh.h"
#include "Mesh.h"
#include "MeshDeformerStack.h"
#include "Material.h"
#include "SimpleNodeCollisionSystem.h"
#include "NodeIDGenerator.h"

// use the Core namespace
using namespace RCore;

namespace ReActionFX {

// constructor
Node::Node(const std::string & name) : mName(name) {
	mParent = nullptr;
	mCollisionSystem = nullptr;

	mStacks.push_back(nullptr);
	mMeshes.push_back(nullptr);

	mColStacks.push_back(nullptr);
	mColMeshes.push_back(nullptr);

	mLocalTM.Identity();
	mWorldTM.Identity();
	mInvBoneTM.Identity();
	mLocalScale.Set(1.0, 1.0, 1.0);
	mLocalPos.Set(0.0, 0.0, 0.0);
	mWorldScale.Set(1.0, 1.0, 1.0);

	mOrgPos.Set(0, 0, 0);
	mOrgScale.Set(1, 1, 1);

	// reset the material offset, so that no offset will be used
	mMaterialOffset = 0;

	// pregenerate the collision system
	mCollisionSystem = new SimpleNodeCollisionSystem(this);

	// calculate the ID
	mID = NodeIDGenerator::GetInstance().GenerateIDForString(mName);

	if ((name == "Bip01 Prop1") || (name == "Bip01 Prop2"))
		m_isProp = true;
	else
		m_isProp = false;
}


// destructor
Node::~Node() {
	// get rid of all node attributes
	RemoveAllAttributes();

	// remove all meshes
	// let the smartpointer deal with deletion
	mMeshes.clear();

	// get rid of the stack
	// let the smartpointer deal with deletion
	mStacks.clear();

	// get rid of the collision meshes
	// smartpointer deletion
	mColMeshes.clear();

	// get rid of the collision mesh deformer stacks
	// smartpointer deletion
	mColStacks.clear();

	// delete the node collision system
	if (mCollisionSystem != nullptr)
		delete mCollisionSystem;

	// remove all child nodes
	RemoveAllChilds();
}


// process down the hierarchy
void Node::RecursiveHierarchyUpdate() {
	// update the local TM
	UpdateLocalTM();

	// update this node's transformations
	if (IsRootNode()) {
		mWorldTM = mLocalTM;
		mWorldScale = mLocalScale;
	} else {
		Vector3 parentScale = mParent->GetWorldScale();
		mWorldScale = Vector3(mLocalScale.x*parentScale.x, mLocalScale.y*parentScale.y, mLocalScale.z*parentScale.z);
		mWorldTM = mLocalTM * mParent->GetWorldTM();
	}

	// process the hierarchy for all childs
	for (auto* child : mChilds)
		child->RecursiveHierarchyUpdate();
}


// recursively update the world space matrix
void Node::RecursiveUpdateWorldTM(const Matrix* worldTM) {
	// if we have set a world matrix, then copy it, else calculate it
	if (worldTM != nullptr) {
		mWorldTM = *worldTM;

		// calculate the local TM
		if (mParent)
			mLocalTM = mWorldTM * mParent->GetWorldTM().Inversed();
		else
			mLocalTM = mWorldTM;
	} else {
		// if there is no parent, this really won't have any influence :)
		if (mParent == nullptr) return;

		// calculate the updated world space matrix
		mWorldTM = mLocalTM * mParent->GetWorldTM();
	}

	// recursively update the world matrices of the childs
	for (auto* child : mChilds)
		child->RecursiveUpdateWorldTM(nullptr);
}


// update the local transformation matrix (construct it from the mLocalQuat and mScale)
void Node::UpdateLocalTM() {
	mLocalTM = Matrix(mLocalPos, mLocalRot, mLocalScale);
}


// clone the node stacks
void Node::CloneNodeStacksFromNode(Node* sourceNode, Actor* actor) {
	// get rid of the existing stacks
	mStacks.clear();
	mStacks.shrink_to_fit();

	// clone all stacks
	for (int a = 0; a < sourceNode->mStacks.size(); a++) {
		if (sourceNode->mStacks[a].get() != nullptr)
			mStacks.push_back(std::shared_ptr<MeshDeformerStack>(sourceNode->mStacks[a].get()->Clone(mMeshes[a].get(), actor)));
		else
			mStacks.push_back(nullptr);
	}
}


// clone the collision node stacks
void Node::CloneNodeCollisionStacksFromNode(Node* sourceNode, Actor* actor) {
	// get rid of the existing collision stacks
	mColStacks.clear();
	mColStacks.shrink_to_fit();

	// clone all collision stacks
	for (int a = 0; a < sourceNode->mColStacks.size(); a++) {
		if (sourceNode->mColStacks[a] != nullptr)

			mColStacks.push_back(std::shared_ptr<MeshDeformerStack>(sourceNode->mColStacks[a]->Clone(mColMeshes[a].get(), actor)));
		else
			mColStacks.push_back(nullptr);
	}
}


// create a clone of this node
Node* Node::Clone(Actor* actor, ENodeCloneFlags flags) const {
	Node* result = new Node(mName);

	// copy attributes
	result->mLocalRot = mLocalRot;
	result->mLocalTM = mLocalTM;
	result->mWorldTM = mWorldTM;
	result->mInvBoneTM = mInvBoneTM;
	result->mLocalScale = mLocalScale;
	result->mLocalPos = mLocalPos;
	result->mWorldScale = mWorldScale;
	result->mParent = nullptr;
	result->mOrgPos = mOrgPos;
	result->mOrgRot = mOrgRot;
	result->mOrgScale = mOrgScale;
	result->mMaterialOffset = mMaterialOffset;

	// copy the meshes (well, the pointers, not the actual mesh data, since it is instanced)
	result->mMeshes = mMeshes;
	result->mColMeshes = mColMeshes;

	// copy the node attributes, if desired
	if (flags & CLONE_ATTRIBUTES) {
		for (auto* attribute : mAttributes)
			result->AddAttribute(attribute->Clone());
	}

	// clone node stacks if desired
	if (flags & CLONE_NODESTACKS) {
		result->CloneNodeStacksFromNode((Node*)this, actor);
	}

	// clone node collision stacks if desired
	if (flags & CLONE_NODECOLLISIONSTACKS) {
		result->CloneNodeCollisionStacksFromNode((Node*)this, actor);
	}


	// clone the node collision system
	if (mCollisionSystem)
		result->SetNodeCollisionSystem(mCollisionSystem->Clone(result));

	// return the resulting clone
	return result;
}


// removes all attributes
void Node::RemoveAllAttributes() {
	for (auto* attribute : mAttributes) {
		delete attribute;
	}
	mAttributes.clear();
}


// removes all meshes and stacks from this node, for all LOD levels.
void Node::RemoveMeshes() {
	mMeshes.clear();
	mStacks.clear();
}


// remove meshes for a given LOD, but keep the LOD levels intact
void Node::RemoveMeshesForLOD(const int lodLevel) {
	mMeshes[lodLevel] = nullptr;
	mStacks[lodLevel] = nullptr;
}


// removes all collision meshes and stacks from this node, for all LOD levels.
void Node::RemoveCollisionMeshes() {
	mColMeshes.clear();
	mColStacks.clear();
}


// remove collision meshes for a given LOD, but keep the LOD levels intact
void Node::RemoveCollisionMeshesForLOD(const int lodLevel) {
	mColMeshes[lodLevel] = nullptr;
	mColStacks[lodLevel] = nullptr;
}


// set new collision system
void Node::SetNodeCollisionSystem(NodeCollisionSystem* collisionSystem) {
	if (collisionSystem != nullptr) {
		// if there is a collision system delete it
		if (mCollisionSystem != nullptr) {
			delete mCollisionSystem;
		}

		mCollisionSystem = collisionSystem;
	}
}


// search for the motion part
MotionPart* Node::SearchMotionPart(Motion* motion) {
	for (auto& link : mLinks) {
		if (link.GetMotionInstance()->GetMotion() == motion)
			return link.GetMotionPart();
	}
	return nullptr;
}


// search for the motion link
MotionLink* Node::SearchMotionLink(Motion* motion) {
	for (auto& link : mLinks) {
		if (link.GetMotionInstance()->GetMotion() == motion)
			return &link;
	}
	return nullptr;
}


// search for the motion link
MotionLink* Node::SearchMotionLink(MotionInstance* instance) {
	for (auto& link : mLinks) {
		if (link.GetMotionInstance() == instance)
			return &link;
	}

	// Switch to this style one day to avoid "returning temporary" warnings.
	//std::vector<MotionLink>::iterator link = std::find_if(mLinks.begin(), mLinks.end(), [instance] (MotionLink &link) {
	//	return link.GetMotionInstance() == instance;
	//});

	return nullptr;
}


// check if the mesh at the given LOD is deformable
bool Node::HasDeformableMesh(const int lodLevel) const {
	assert(lodLevel < mMeshes.size());
	return (mMeshes[lodLevel].get() && mStacks[lodLevel].get() && mStacks[lodLevel].get()->GetNumDeformers() > 0);
}


// check if the collision mesh at the given LOD is deformable
bool Node::HasDeformableCollisionMesh(const int lodLevel) const {
	assert(lodLevel < mColMeshes.size());
	return (mColMeshes[lodLevel] && mColStacks[lodLevel] && mColStacks[lodLevel]->GetNumDeformers() > 0);
}


// check if there is any mesh available
bool Node::HasMesh() const {
	assert(mMeshes.size() > 0 && mColMeshes.size() > 0);
	return mMeshes[0] || mColMeshes[0];
}


// calculate the mesh based axis aligned bounding box
RCore::AABB Node::CalcMeshBasedAABB(const int lodLevel) {
	// get the mesh
	auto mesh = GetMesh(lodLevel);
	if (mesh == nullptr)
		return RCore::AABB();

	// get the position data
	Vector3* positions = mesh->GetPositions();

	RCore::AABB result = RCore::AABB();

	// process all vertices
	const int numVerts = mesh->GetNumVertices();
	for (int v = 0; v < numVerts; v++) {
		result.Encapsulate(positions[v] * mWorldTM);
	}

	return result;
}


// calculate the collision mesh based axis aligned bounding box
RCore::AABB Node::CalcCollisionMeshBasedAABB(const int lodLevel) {
	// get the mesh
	auto mesh = GetCollisionMesh(lodLevel);
	if (mesh == nullptr)
		return RCore::AABB();

	// get the position data
	Vector3* positions = mesh->GetPositions();

	RCore::AABB result = RCore::AABB();

	// process all vertices
	const int numVerts = mesh->GetNumVertices();
	for (int v = 0; v < numVerts; v++) {
		result.Encapsulate(positions[v] * mWorldTM);
	}

	return result;
}


// checks if the given node is a child of this node
bool Node::IsNodeChild(Node* node, const bool compareNames) {
	// iterate through all childs and compare them with the given node
	for (const auto& child : mChilds) {

		// the given node is already a child, return success
		if (compareNames) {
			if (node->GetName() == child->GetName())
				return true;
		} else {
			if (node == child)
				return true;
		}
	}

	// nothing found
	return false;
}


// get the total number of children
int Node::GetNumChildNodesRecursive() {
	// the number of total child nodes which include the childs of the childs, too
	int numTotalChilds = 0;

	// retrieve the number of child nodes of the actual node
	for (const auto& child : mChilds) {
		if (!child)
			continue;

		// get the number of children and add it to the total number
		numTotalChilds += child->GetNumChilds();

		// go recursive!
		numTotalChilds += child->GetNumChildNodesRecursive();
	}

	return numTotalChilds;
}


Node* Node::FindRoot() {
	if (mParent)
		return mParent->FindRoot();

	return this;
}

void Node::RemoveLink(MotionInstance *instance) {
	std::remove_if(mLinks.begin(), mLinks.end(), [&instance] (const MotionLink &link) {
		return link.GetMotionInstance() == instance;
	});
}

void Node::SetToOriginalOrientation() {
	SetLocalPos(mOrgPos);
	SetLocalRot(mOrgRot);
	SetLocalScale(mOrgScale);
}

NodeAttribute* Node::GetAttribute(const int attributeNr) {
	assert(attributeNr < mAttributes.size());

	return mAttributes[attributeNr];
}

int Node::FindAttributeNumber(const int attributeTypeID) const {
	auto attribute_it = std::find_if(mAttributes.begin(), mAttributes.end(), [attributeTypeID] (const NodeAttribute* attribute) {
		return attribute->GetType() == attributeTypeID;
	});

	return (attribute_it == mAttributes.end()) ? -1 : (attribute_it - mAttributes.begin());
}

NodeAttribute* Node::GetAttributeByType(const int attributeType) {
	for (const auto& attribute : mAttributes) {
		if (attribute->GetType() == attributeType)
			return attribute;
	}
	return nullptr;
}

} // namespace