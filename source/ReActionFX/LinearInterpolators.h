#pragma once

#include "RCore.h"

#include "Interpolator.h"

namespace ReActionFX {

/**
 * The linear interpolator class.
 * This class interpolates value on a linear way, so things are not curved.
 */
template <class ReturnType, class StorageType>
class LinearInterpolator : public Interpolator<ReturnType, StorageType> {
public:
	// the type ID, returned by GetType()
	enum { TYPE_ID = 0x00000001 };

	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	LinearInterpolator() : Interpolator<ReturnType, StorageType>(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	LinearInterpolator(KeyTrack<ReturnType, StorageType>* interpolationSource) : Interpolator<ReturnType, StorageType>(interpolationSource) {}

	/**
	 * Destructor.
	 */
	~LinearInterpolator() {}

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	virtual ReturnType Interpolate(const int startKey, const double timeValue) const;

	/**
	 * Initialize the interpolator.
	 * This will calculate the delta values to speed up interpolation.
	 */
	void Init() {}

	/**
	 * Returns the unique interpolator type ID.
	 * This can be used to determine what interpolator is used.
	 * @result The unique type identification number of this interpolator class.
	 */
	virtual int GetType() const { return TYPE_ID; }

	/**
	 * Returns the type string of this interpolator class.
	 * This can be the class name, or a description of the interpolator.
	 * @result The string containing the description or class name of this interpolator.
	 */
	virtual const char* GetTypeString() const { return "LinearInterpolator"; }
};


/**
 * The linear interpolator class used for quaternions.
 * The reason why there is a special class for quaternions is that they
 * need some additional checks in order to perform the interpolation correctly.
 */
template <class StorageType>
class LinearQuaternionInterpolator : public LinearInterpolator<RCore::Quaternion, StorageType> {

public:
	/**
	 * Default constructor.
	 * Do not forget to call SetKeyTrack() if you are not using the KeyTrack::SetInterpolator() method...
	 */
	LinearQuaternionInterpolator() : LinearInterpolator<RCore::Quaternion, StorageType>(nullptr) {}

	/**
	 * Constructor.
	 * @param interpolationSource The keytrack which will be used by the interpolator.
	 */
	LinearQuaternionInterpolator(KeyTrack<RCore::Quaternion, StorageType>* interpolationSource) : LinearInterpolator<RCore::Quaternion, StorageType>(interpolationSource) {}

	/**
	 * Interpolates between keyframe number startKey and the next one.
	 * Keep in mind that startKey must be in range of [0..GetInterpolationSource()->GetNumKeys()-2].
	 * @param startKey The keyframe to start interpolating at. Interpolation will most likely be done between
	 *                 the startKey and the one coming after that.
	 * @param timeValue A value in range of [0..1].
	 * @result Returns the interpolated value.
	 */
	RCore::Quaternion Interpolate(const int startKey, const double timeValue) const;
};


//-------------------------------------------------------------------------------------------------
// LinearInterpolator
//-------------------------------------------------------------------------------------------------

template <class ReturnType, class StorageType>
ReturnType LinearInterpolator<ReturnType, StorageType>::Interpolate(const int startKey, const double timeValue) const {
	KeyFrame<ReturnType, StorageType>*	firstKey = this->mInterpolationSource->GetKey(startKey);
	KeyFrame<ReturnType, StorageType>*	nextKey = this->mInterpolationSource->GetKey(startKey + 1);

	return (1.0 - timeValue) * firstKey->GetValue() + timeValue * nextKey->GetValue();
}

//--------------------------
// Quaternion interpolation
//--------------------------

// the interpolation method
template <class StorageType>
RCore::Quaternion LinearQuaternionInterpolator<StorageType>::Interpolate(const int startKey, const double timeValue) const {
	// get the quaternions
	RCore::Quaternion a = this->mInterpolationSource->GetKey(startKey)->GetValue();
	RCore::Quaternion b = this->mInterpolationSource->GetKey(startKey + 1)->GetValue();

	// check if both quaternions are on the same hypersphere or not, if not, invert one
	if (a.Dot(b) < 0.0)
		a = -a;

	// take the logarithm
	a = a.LogN();
	b = b.LogN();

	// interpolate, and take the exponent of that, which is the interpolated quaternion
	return ((1.0 - timeValue) * a + (timeValue * b)).Exp().Normalize();
}

} // namespace
