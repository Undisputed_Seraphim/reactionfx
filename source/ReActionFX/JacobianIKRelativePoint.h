#pragma once
#include "RCore.h"

#include "JacobianIKConstraint.h"

namespace ReActionFX {

/**
 * The JacobianIKRelativePoint class.
 * Defines the constraints applied to an Actor using IK.
 * This constraint specifies a point where a Node must be.
 */
class JacobianIKRelativePoint : public JacobianIKConstraint {
public:
	/**
	 * Default constructor.
	 */
	JacobianIKRelativePoint();

	/**
	 * Constructor.
	 * @param displacement Displacement vector which the solver tries to reach.
	 */
	JacobianIKRelativePoint(const RCore::Vector3& displacement) {
		SetDisplacement(displacement);
	}

	/**
	 * Constructor.
	 * @param x The x component of the displacement vector which the solver tries to reach.
	 * @param y The y component of the displacement vector which the solver tries to reach.
	 * @param z The z component of the displacement vector which the solver tries to reach.
	 */
	JacobianIKRelativePoint(const float x, const float y, const float z) {
		SetDisplacement(RCore::Vector3(x, y, z));
	}

	/**
	 * Destructor
	 */
	~JacobianIKRelativePoint() {}

	/**
	 * Clone the IK constraint.
	 * @return A pointer to a newly created exact copy of the IK constraint.
	 */
	JacobianIKConstraint* Clone() {
		return new JacobianIKRelativePoint(mDisplacement);
	}

	/**
	 * Update the jacobian ik constraint.
	 * @param timeDeltaInSeconds The time passed in seconds.
	 */
	void Update(const float timeDeltaInSeconds) {
		mPosition = mNode->GetWorldPos();
	}

	/**
	 * Return the number of functions needed to apply the constraint.
	 * @return The number of functions.
	 */
	int GetNumFunc() const;

	/**
	 * Return the specified value for the functions.
	 * @param values An array where the values will be placed.
	 */
	void GetValues(std::vector<float>& values);

	/**
	 * Return the value for the functions using the actual state of the actor.
	 * @param results An array where the values will be placed.
	 */
	void GetResults(std::vector<float>& results);

	/**
	 * Calculate the constraint functions gradients and store them in the given matrix.
	 * @param matrix The matrix where the gradients are stored.
	 */
	void CalcGradient(RCore::NMatrix& matrix);

   /**
	* Sets the position where the node must be placed.
	* @param v The vector that specifies the position.
	*/
	void SetDisplacement(const RCore::Vector3& displacement) {
		mDisplacement = displacement;
	}

private:
	RCore::Vector3 mPosition;		/* The position where the node must be placed by IK */
	RCore::Vector3 mDisplacement;
};

} // namespace