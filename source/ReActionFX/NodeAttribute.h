#pragma once

#include "RCore.h"

namespace ReActionFX {

/**
 * The node attribute base class.
 * Custom attributes can be attached to every node.
 * An example of an attribute could be physics properties.
 * In order to create your own node attribute, simply inherit a class from this base class.
 */
class NodeAttribute {
public:
	/**
	 * The constructor.
	 */
	NodeAttribute() {}

	/**
	 * The destructor.
	 */
	virtual ~NodeAttribute() {}

	/**
	 * Get the attribute type.
	 * @result The attribute ID.
	 */
	virtual int	GetType() const = 0;

	/**
	 * Get the attribute type as a string.
	 * This string should contain the name of the class.
	 * @result The string containing the type name.
	 */
	virtual const char* GetTypeString() const = 0;

	/**
	 * Clone the node attribute.
	 * @result Returns a pointer to a newly created exact copy of the node attribute.
	 */
	virtual NodeAttribute* Clone() = 0;
};

} // namespace