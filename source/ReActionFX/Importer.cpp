#include "Importer.h"

#include "Node.h"
#include "Motion.h"
#include "SkeletalMotion.h"
#include "FacialMotion.h"
#include "MotionPart.h"
#include "Actor.h"
#include "Mesh.h"
#include "MeshDeformerStack.h"
#include "SoftSkinDeformer.h"
#include "SimpleMesh.h"
#include "SubMesh.h"
#include "Material.h"
#include "SkinningInfoVertexAttributeLayer.h"
#include "SoftSkinManager.h"
#include "LinearInterpolators.h"

using namespace RCore;

namespace ReActionFX {

// constructor
Importer::Importer() {
	// register all standard chunks
	RegisterStandardChunks();

	// init some default values
	mLoggingActive = true;
	mLogDetails = false;
}


// destructor
Importer::~Importer() {
	// remove of all chunk processors
	for (auto* processor : mChunkProcessors) {
		delete processor;
	}

	// remove all shared data
	for (auto* sharedData : mSharedData) {
		delete sharedData;
	}

	// we're  not initialized anymore
	mStandardProcessorsInitialized = false;
}

//-------------------------------------------------------------------------------------------------

// try to load an actor from disk
Actor* Importer::LoadActor(const std::string& filename, const std::string& actorName, bool usePerPixelLighting) {
	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release the shared data
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load actor '%s' from file '%s'...", actorName.data(), filename.data());

	// try to open the file from disk
	std::ifstream f(filename, std::ios_base::binary);
	if (!f) {
		if (GetLogging())
			LOG("Failed open the file '%s' for actor '%s', actor not loaded!", filename.data(), actorName.data());
		return nullptr;
	}

	// create the actor reading from memory
	Actor *result = LoadActor(&f, actorName, usePerPixelLighting);

	// check if it worked :)
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load actor '%s' from file '%s'", actorName.data(), filename.data());
	}

	// return the result
	return result;
}

/*
// load an actor from memory
Actor* Importer::LoadActor(unsigned char* memoryStart, int lengthInBytes, const std::string& actorName, bool usePerPixelLighting) {
	DECLARE_FUNCTION(LoadActor)

	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release the shared data
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load actor '%s' from memory location 0x%x...", actorName.data(), memoryStart);

	// create the memory file
	MemoryFile memFile;
	memFile.open(memoryStart, lengthInBytes);

	// try to load the file
	Actor* result = LoadActor(&memFile, actorName, usePerPixelLighting);
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load actor '%s' from memory location 0x%x", actorName.data(), memoryStart);
	}

	// close the memory file again
	memFile.close();

	// return the result
	return result;
}*/


// try to load a LMA file
Actor* Importer::LoadActor(std::ifstream* f, const std::string& actorName, bool usePerPixelLighting) {
	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	assert(f);
	assert(f->is_open());

	if (GetLogging())
		LOG("- Trying to load actor '%s'...", actorName.data());

	// release some buffers
	ResetSharedData();

	// read the header
	LMA_Header header;
	f->read((char*)&header, sizeof(LMA_Header));

	mLMAHeader = header;

	// check FOURCC
	if (header.mFourcc[0] != 'L' || header.mFourcc[1] != 'M' || header.mFourcc[2] != 'A' || header.mFourcc[3] != ' ') {
		if (GetLogging())
			LOG("Importer::LoadActor() - Not a valid LMA file!");
		f->close();
		return nullptr;
	}

	if (header.mHiVersion != LMA_HIGHVERSION) {
		if (GetLogging())
			LOG("Importer::LoadActor() - Cannot read this version! Can only read LMA files with high version %d.x, and this is version %d.%d!", LMA_HIGHVERSION, header.mHiVersion, header.mLoVersion);
		f->close();
		return nullptr;
	}

	// no actor in this file
	if (!header.mActor) {
		if (GetLogging())
			LOG("Importer::LoadActor() - This file does NOT contain an actor, but a motion!");
		f->close();
		return nullptr;
	}

	// display some header info
	if (GetLogging() && GetLogDetails())
		LOG("  + Version = %d.%d", header.mHiVersion, header.mLoVersion);

	// create our actor
	Actor* actor = new Actor(actorName.c_str());

	// read the chunks
	while (ProcessChunk(f, actor, nullptr, usePerPixelLighting)) {
	}

	// update the hierarchy pointers
	UpdateHierarchy(actor);

	// update the character so that everything is up-to-date
	actor->Update(0.0, true);
	actor->UpdateBounds(0, Actor::BOUNDS_MESH_BASED, true);

	// reset all shared data again
	ResetSharedData();

	// close the file and return a pointer to the actor we loaded
	f->close();

	// return the created actor
	return actor;
}

//-------------------------------------------------------------------------------------------------

// try to load a motion from disk
Motion* Importer::LoadMotion(const std::string& filename, const std::string& motionName) {
	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release the shared data
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load motion '%s' from file '%s'...", motionName.data(), filename.data());

	// try to open the file from disk
	std::ifstream f(filename, std::ios_base::binary);
	if (!f) {
		if (GetLogging())
			LOG("Failed open the file '%s' for motion '%s', motion not loaded!", filename.data(), motionName.data());
		return nullptr;
	}

	// create the motion reading from memory
	Motion* result = LoadMotion(&f, motionName);

	// check if it worked :)
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load motion '%s' from file '%s'", motionName.data(), filename.data());
	}

	// return the result
	return result;
}

/*
Motion* Importer::LoadMotion(unsigned char* memoryStart, int lengthInBytes, const std::string& motionName) {
	DECLARE_FUNCTION(LoadMotion)

	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release some buffers
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load motion '%s' from memory location 0x%x...", motionName.data(), memoryStart);

	// create the memory file
	MemoryFile memFile;
	memFile.open(memoryStart, lengthInBytes);

	// try to load it
	Motion* result = LoadMotion(&memFile, motionName);

	// check if it worked
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load motion '%s' from memory location 0x%x", motionName.data(), memoryStart);
	}

	// return the result
	return result;
}*/


// try to load a motion from a LMA file
Motion* Importer::LoadMotion(std::ifstream *f, const std::string& motionName) {
	// register standard chunk processors if it hasn't been done yet
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	assert(f);
	assert(f->is_open());

	if (GetLogging())
		LOG("- Trying to load LMA motion file '%s'...", motionName.data());

	// release some buffers
	ResetSharedData();

	// read the header
	LMA_Header header;
	f->read((char*)&header, sizeof(LMA_Header));

	mLMAHeader = header;

	// check FOURCC
	if (header.mFourcc[0] != 'L' || header.mFourcc[1] != 'M' || header.mFourcc[2] != 'A' || header.mFourcc[3] != ' ') {
		if (GetLogging())
			LOG("Importer::LoadMotion() - Not a valid LMA file!");
		f->close();
		return nullptr;
	}

	// no actor in this file
	if (header.mActor) {
		if (GetLogging())
			LOG("Importer::Load() - This file does NOT contain a motion, but only an actor!");
		f->close();
		return nullptr;
	}

	// version check
	if (header.mHiVersion != LMA_HIGHVERSION) {
		if (GetLogging())
			LOG("Importer::LoadMotion() - Cannot read this version! Can only read LMA files of version %d.x, and this is version %d.%d!", LMA_HIGHVERSION, header.mHiVersion, header.mLoVersion);
		f->close();
		return nullptr;
	}

	// display some header info
	if (GetLogging() && GetLogDetails())
		LOG("  + Version         = %d.%d", header.mHiVersion, header.mLoVersion);

	// create our motion
	SkeletalMotion* motion = new SkeletalMotion(motionName.c_str());

	// read the chunks
	while (ProcessChunk(f, nullptr, motion, false)) {
	}

	// update the max time of the motion
	motion->UpdateMaxTime();

	// get rid of all shared data
	ResetSharedData();

	// close the file and return a pointer to the actor we loaded
	f->close();
	return motion;
}

//-------------------------------------------------------------------------------------------------

// try to load a facial motion from disk
FacialMotion* Importer::LoadFacialMotion(const std::string& filename, const std::string& motionName) {
	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release the shared data
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load facial motion '%s' from file '%s'...", motionName.data(), filename.data());

	// try to open the file from disk
	std::ifstream f(filename, std::ios_base::binary);
	if (!f) {
		if (GetLogging())
			LOG("Failed open the file '%s' for facial motion '%s', facial motion not loaded!", filename.data(), motionName.data());
		return nullptr;
	}

	// create the facial motion reading from memory
	FacialMotion* result = LoadFacialMotion(&f, motionName);

	// check if it worked :)
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load facial motion '%s' from file '%s'", motionName.data(), filename.data());
	}

	// return the result
	return result;
}

/*
FacialMotion* Importer::LoadFacialMotion(unsigned char *memoryStart, int lengthInBytes, const std::string& motionName) {
	DECLARE_FUNCTION(LoadFacialMotion)

	// register all standard chunks if this has not ye been done
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	// release some buffers
	ResetSharedData();

	if (GetLogging())
		LOG("Trying to load facial motion '%s' from memory location 0x%x...", motionName.data(), memoryStart);

	// create the memory file
	MemoryFile memFile;
	memFile.open(memoryStart, lengthInBytes);

	// try to load it
	FacialMotion* result = Importer::GetInstance().LoadFacialMotion(&memFile, motionName);

	// check if it worked
	if (result == nullptr) {
		if (GetLogging())
			LOG("Failed to load facial motion '%s' from memory location 0x%x", motionName.data(), memoryStart);
	}

	// return the result
	return result;
}*/


// try to load a facial motion from a LMF file
FacialMotion* Importer::LoadFacialMotion(std::ifstream *f, const std::string& motionName) {
	// register standard chunk processors if it hasn't been done yet
	if (!mStandardProcessorsInitialized)
		RegisterStandardChunks();

	assert(f);
	assert(f->is_open());

	if (GetLogging())
		LOG("- Trying to load LMF facial motion file '%s'...", motionName.data());

	// release some buffers
	ResetSharedData();

	// read the header
	LMF_Header header;
	f->read((char*)&header, sizeof(LMF_Header));

	// fill in lma header which will be used by the ProcessChunk routine to detect the correct processors
	mLMAHeader.mHiVersion = header.mHiVersion;
	mLMAHeader.mLoVersion = header.mLoVersion;

	// check FOURCC
	if (header.mFourcc[0] != 'L' || header.mFourcc[1] != 'M' || header.mFourcc[2] != 'F' || header.mFourcc[3] != ' ') {
		if (GetLogging())
			LOG("Importer::LoadFacialMotion() - Not a valid LMF file!");
		f->close();
		return nullptr;
	}

	// version check
	if (header.mHiVersion != LMA_HIGHVERSION) {
		if (GetLogging())
			LOG("Importer::LoadFacialMotion() - Cannot read this version! Can only read LMF files of version %d.x, and this is version %d.%d!", LMA_HIGHVERSION, header.mHiVersion, header.mLoVersion);
		f->close();
		return nullptr;
	}

	// display some header info
	if (GetLogging() && GetLogDetails())
		LOG("  + Version         = %d.%d", header.mHiVersion, header.mLoVersion);

	// create our motion
	FacialMotion* facialMotion = new FacialMotion(motionName.c_str());

	// read the chunks
	while (ProcessChunk(f, nullptr, facialMotion, false)) {
	}

	// update the max time of the motion
	facialMotion->UpdateMaxTime();

	// get rid of all shared data
	ResetSharedData();

	// close the file and return a pointer to the actor we loaded
	f->close();
	return facialMotion;
}

//-------------------------------------------------------------------------------------------------

// retrieve the low and high version of an lma file
void Importer::GetLMAFileVersion(int& lowVersion, int& highVersion, const std::string& fileName) {
	// reset the version number
	lowVersion = highVersion = -1;

	// check for a valid name
	if (fileName.empty())
		return;

	// get the chunk header
	LMA_Header header;
	std::ifstream file(fileName, std::ios_base::binary);
	file.read((char*)&header, sizeof(LMA_Header));
	file.close();

	highVersion = header.mHiVersion;
	lowVersion = header.mLoVersion;
}


// return number of the given chunks
int Importer::GetNumChunks(const int chunkType, std::ifstream* file) const {
	int chunkCounter = 0;

	// check if the file is valid, else return error
	if (!file)
		return -1;

	assert(file->is_open());

	// get the offset position so that we can reset the file afterwards
	int offsetPos = file->tellg();

	// construct the chunk iterator
	std::ifstream* iteratorFile = file;
	ChunkIterator iter(chunkType, iteratorFile);

	// since chunks can have a dynamic size, try to get it
	int chunkSize = 0;

	iteratorFile = iter.Next(chunkSize);

	while (iteratorFile) {
		// skip the chunk
		iteratorFile->seekg(chunkSize, iteratorFile->cur);

		chunkCounter++;
		//LOG("Increasing chunk counter.");

		iteratorFile = iter.Next(chunkSize);
	}

	// reset the offset position, nothing has happened ;)
	file->seekg(offsetPos, file->cur);

	return chunkCounter;
}


// return the size of the given chunk
int Importer::GetChunkSize(const int chunkType, const int index, std::ifstream* file) const {
	// check if the file is valid, else return error
	if (!file)
		return -1;

	assert(file->is_open());

	// since chunks can have a dynamic size, try to get it
	int chunkSize = -1;

	// check if the index is in range
	if (index >= 0 && index < GetNumChunks(chunkType, file)) {
		// get the offset position so that we can reset the file afterwards
		int offsetPos = file->tellg();

		// construct the chunk iterator
		std::ifstream* iteratorFile = file;
		ChunkIterator iter(chunkType, iteratorFile);

		int chunkCounter = 0;
		iteratorFile = iter.Next(chunkSize);

		while (iteratorFile) {
			// that's our chunk
			if (index == chunkCounter) {
				// nothing more to do, leave loop
				break;
			}

			// skip the chunk
			iteratorFile->seekg(chunkSize, iteratorFile->cur);

			// new chunk of the given type found, increase counter
			chunkCounter++;

			iteratorFile = iter.Next(chunkSize);
		}

		// reset the offset position, nothing has happened ;)
		file->seekg(offsetPos);
	}

	return chunkSize;
}


// return the the seek position of the given chunk in bytes
int Importer::GetSeekPositionByChunk(const int chunkType, const int index, std::ifstream* file) const {
	int chunkSeekPosition = -1;

	// check if the file is valid, else return error
	if (!file)
		return -1;

	assert(file->is_open());

	// check if the index is in range
	if (index >= 0 && index < GetNumChunks(chunkType, file)) {
		// get the offset position so that we can reset the file afterwards
		int offsetPos = file->tellg();

		// construct the chunk iterator
		std::ifstream* iteratorFile = file;
		ChunkIterator iter(chunkType, iteratorFile);

		// since chunks can have a dynamic size, try to get it
		int chunkSize = -1;

		int chunkCounter = 0;
		for (iteratorFile = iter.Next(chunkSize); !iter.IsDone() && iteratorFile != nullptr; iteratorFile = iter.Next(chunkSize)) {
			// check if the file is still valid
			if (iteratorFile) {
				// that's our chunk
				if (index == chunkCounter) {
					// get the seek position of the given chunk
					chunkSeekPosition = iteratorFile->tellg();

					// nothing more to do, leave loop
					break;
				}

				// skip the chunk
				iteratorFile->seekg(chunkSize, iteratorFile->cur);

				// new chunk of the given type found, increase counter
				chunkCounter++;
			}
		}

		// reset the offset position, nothing has happened ;)
		file->seekg(offsetPos);
	}

	return chunkSeekPosition;
}


// remove the given chunk from the file
bool Importer::RemoveChunk(const int chunkType, const int index, const std::string& fileName) const {
	// check for a valid name
	if (fileName.empty())
		return false;

	// get the chunk header size (could be the new or the old version)
	int chunkHeaderSize = 0;
	LMA_Header header;
	std::ifstream file(fileName, std::ios_base::binary);
	file.read((char*)&header, sizeof(LMA_Header));
	file.close();

	if (header.mHiVersion == 1 && header.mLoVersion == 0)
		chunkHeaderSize = sizeof(LMA_ChunkOld);
	else
		chunkHeaderSize = sizeof(LMA_Chunk);

	// try to open the file from disk
	std::ifstream inputFile(fileName, std::ios_base::binary | std::ios_base::ate);
	if (!inputFile) {
		if (GetLogging())
			LOG("Importer::RemoveChunk: Failed open the file '%s'", fileName.data());
		return false;
	}

	// get the file size
	int inputFileSize = inputFile.tellg();
	if (inputFileSize <= 0)
		return false;
	//LOG("Input File Size: %i", inputFileSize);
	inputFile.seekg(0);

	// get the seek position of the given chunk
	int chunkSeekPosition = GetSeekPositionByChunk(chunkType, index, &inputFile);
	int chunkSize = GetChunkSize(chunkType, index, &inputFile);
	//LOG("Chunk seekp Position: %i", chunkSeekPosition);
	//LOG("Chunk Size: %i", chunkSize);

	// return if doesn't exist
	if (chunkSeekPosition == -1)
		return false;


	// create file buffer
	char* inputData = (char*)malloc(inputFileSize);

	// read in data
	inputFile.read(inputData, inputFileSize);

	// close the input file
	inputFile.close();


	// output buffer
	int outputFileSize = inputFileSize - chunkHeaderSize - chunkSize;
	//LOG("Header Size: %i", sizeof(LMA_Chunk));
	//LOG("Chunk Size: %i", chunkSize);
	char* outputData = (char*)malloc(outputFileSize);
	//LOG("Output File Size: %i", outputFileSize);

	int i;
	int outputIndex = 0;
	// copy data before the chunk(before chunk header, since it isn't needed anymore too
	int beforeSize = chunkSeekPosition - chunkHeaderSize;
	//LOG("Before Size: %i", beforeSize);
	for (i = 0; i < beforeSize; i++) {
		outputData[outputIndex] = inputData[i];
		outputIndex++;
	}
	// copy data after the chunk
	//LOG("Copy data after the chunk: i=%i to %i", chunkSeekPosition+chunkSize, inputFileSize);
	for (i = chunkSeekPosition + chunkSize; i < inputFileSize; i++) {
		outputData[outputIndex] = inputData[i];
		outputIndex++;
	}

	// try to open the file from disk
	std::ofstream outputFile(fileName, std::ios_base::binary);
	if (!outputFile) {
		if (GetLogging())
			LOG("Importer::RemoveChunk: Failed open the file '%s'. Check if file is write protected.", fileName.data());

		free(inputData);
		free(outputData);

		return false;
	}

	// write output data
	outputFile.write(outputData, outputFileSize);

	// close the output file
	outputFile.close();

	// get rid of our file buffers
	free(inputData);
	free(outputData);

	// everything went right
	return true;
}


// remove all chunks of the given type from file
bool Importer::RemoveAllChunks(const int chunkType, const std::string& fileName) const {
	bool result = false;

	// try to open the file from disk
	std::ifstream file(fileName, std::ios_base::binary);
	if (!file) {
		if (GetLogging())
			LOG("Importer::RemoveAllChunks: Failed open the file '%s'", fileName.data());
		return result;
	}

	// get number of given chunks
	int numSpecificChunks = GetNumChunks(chunkType, &file);
	//LOG("Number Of Chunks To Delete: %i", numSpecificChunks);

	// close the file
	file.close();

	// remove all chunks
	for (int i = 0; i < numSpecificChunks; i++) {
		bool chunkResult = RemoveChunk(chunkType, 0, fileName);
		//LOG("Removing Chunk: %i", i);

		if (!chunkResult) {
			result = false;
			LOG("Error removing Chunk: %i", i);
		}
	}

	return result;
}


// replace the given chunk by another one
bool Importer::ReplaceChunk(const int chunkType, const int index, char* chunkData, const std::string& fileName) const {
	// check for a valid name
	if (fileName.empty())
		return false;

	// try to open the file from disk
	std::ifstream inputFile(fileName, std::ios_base::binary | std::ios_base::ate);
	if (!inputFile) {
		if (GetLogging())
			LOG("Importer::RemoveChunk: Failed open the file '%s'", fileName.data());
		return false;
	}
	// get the file size
	int fileSize = inputFile.tellg();
	if (fileSize <= 0)
		return false;
	//LOG("Input File Size: %i", inputFileSize);
	inputFile.seekg(0);

	// get the seek position of the given chunk
	int chunkSeekPosition = GetSeekPositionByChunk(chunkType, index, &inputFile);
	//LOG("Chunk seekp Position: %i", chunkSeekPosition);
	int chunkSize = GetChunkSize(chunkType, index, &inputFile);
	//LOG("Chunk Size: %i", chunkSize);

	// return if it doesn't exist
	if (chunkSeekPosition == -1)
		return false;


	// create file buffer
	char* inputData = (char*)malloc(fileSize);

	// read in data
	inputFile.read(inputData, fileSize);

	// close the input file
	inputFile.close();

	// replace chunk data
	for (int i = 0; i < chunkSize; i++)
		inputData[chunkSeekPosition + i] = chunkData[i];
	//LOG("Overwriting bytes from %i to %i", chunkSeekPosition, chunkSeekPosition + chunkSize);

	// try to open the file from disk
	std::ofstream outputFile(fileName, std::ios_base::binary);
	if (!outputFile) {
		if (GetLogging())
			LOG("Importer::RemoveChunk: Failed open the file '%s'", fileName.data());

		free(inputData);
		return false;
	}

	// write output data
	outputFile.write(inputData, fileSize);

	// close the output file
	outputFile.close();

	// get rid of our file buffer
	free(inputData);

	// everything went right
	return true;
}


// insert chunk to the given file at a given position
bool Importer::InsertChunk(const int filePosition, const LMA_Chunk& chunkHeader, const int chunkSize, char* chunkData, const std::string& fileName) const {
	// check for a valid name
	if (fileName.empty() || chunkData == nullptr)
		return false;

	// try to open the file from disk
	std::ifstream inputFile(fileName, std::ios_base::binary | std::ios_base::ate);
	if (!inputFile) {
		if (GetLogging())
			LOG("Importer::InsertChunk: Failed open the file '%s'", fileName.data());
		return false;
	}

	// get the file size
	int fileSize = inputFile.tellg();
	if (fileSize <= 0)
		return false;
	//LOG("Input File Size: %i", inputFileSize);
	inputFile.seekg(0);

	// create file buffer
	char* inputData = (char*)malloc(fileSize);

	// read in data
	inputFile.read(inputData, fileSize);

	// close the input file
	inputFile.close();

	// try to open the file from disk
	std::ofstream outputFile(fileName, std::ios_base::binary);
	if (!outputFile || filePosition >= fileSize) {
		if (GetLogging())
			LOG("Importer::RemoveChunk: Failed open the file '%s'", fileName.data());

		free(inputData);
		return false;
	}

	// write output data
	outputFile.write(inputData, filePosition);
	outputFile.write((char*)&chunkHeader, sizeof(LMA_Chunk));
	outputFile.write(chunkData, chunkSize);
	outputFile.write(inputData + filePosition, fileSize - filePosition);

	// close the output file
	outputFile.close();

	// get rid of our file buffer
	free(inputData);

	// everything went right
	return true;
}

//-------------------------------------------------------------------------------------------------

// register a new chunk processor
void Importer::RegisterChunkProcessor(ChunkProcessor* processorToRegister) {
	assert(processorToRegister);
	mChunkProcessors.push_back(processorToRegister);
}


// add shared data object to the importer
void Importer::AddSharedData(SharedData* sharedData) {
	assert(sharedData);
	mSharedData.push_back(sharedData);
}


// search for shared data
Importer::SharedData* Importer::FindSharedData(const int type) {
	for (auto data : mSharedData)
		if (data->GetType() == type)
			return data;

	// nothing found
	return nullptr;
}


void Importer::SetLogDetails(const bool detailLoggingActive) {
	mLogDetails = detailLoggingActive;
	for (auto processor : mChunkProcessors)
		processor->SetLogging((mLoggingActive&&detailLoggingActive));
}


// update parent and childs
void Importer::UpdateHierarchy(Actor *actor) {
	// find the shared data
	SharedHierarchyInfo* sharedHierarchyInfo = (SharedHierarchyInfo*)FindSharedData(SharedHierarchyInfo::TYPE_ID);

	// if no shared hierarchy info is found
	if (!sharedHierarchyInfo)
		return;

	// get the data
	auto links = sharedHierarchyInfo->mLinks;
	auto nodes = sharedHierarchyInfo->mNodes;

	// Set parents
	for (auto& link : links) {
		bool found = false;
		for (auto* node : nodes) {
			if (found) break;
			if (node->GetName().compare(link.mParent) == 0) {
				link.mNode->SetParent(node);
				found = true;
			}
		}

		if (!found)
			if (GetLogging())
				LOG("Failed to resolve parent node '%s'", link.mParent);
	}

	// Set children
	for (auto node : nodes) {
		if (node->IsRootNode()) continue;
		for (auto node2 : nodes) {
			if (node->GetParent() == node2)
				node2->AddChild(node);
		}
	}

	// Link parents to the actor
	for (auto node : nodes) {
		if (node->IsRootNode()) {
			node->SetParent(actor);
			actor->AddChild(node);
		}
	}
}


// reset shared objects so that the importer is ready for use again
void Importer::ResetSharedData() {
	for (auto data : mSharedData) {
		if (!data)
			continue;
		data->Reset();
	}
}


// find chunk, return nullptr if it hasn't been found
ChunkProcessor* Importer::FindChunk(const int chunkID, const int version) {
	for (auto processor : mChunkProcessors)
		if (processor->GetChunkID() == chunkID && processor->GetVersion() == version)
			return processor;

	// nothing found
	return nullptr;
}


// register basic chunk processors
void Importer::RegisterStandardChunks() {
	// version 1
	RegisterChunkProcessor(new NodeChunkProcessor1(this));
	RegisterChunkProcessor(new MeshChunkProcessor1(this));
	RegisterChunkProcessor(new SkinningInfoChunkProcessor1(this));
	//RegisterChunkProcessor(new CollisionMeshChunkProcessor1(this));
	RegisterChunkProcessor(new MaterialChunkProcessor1(this));
	RegisterChunkProcessor(new MaterialLayerChunkProcessor1(this));
	RegisterChunkProcessor(new MotionPartChunkProcessor1(this));
	RegisterChunkProcessor(new AnimationChunkProcessor1(this));
	RegisterChunkProcessor(new LimitChunkProcessor1(this));
	RegisterChunkProcessor(new PhysicsInfoChunkProcessor1(this));
	RegisterChunkProcessor(new MeshExpressionPartChunkProcessor1(this));
	RegisterChunkProcessor(new ExpressionMotionPartChunkProcessor1(this));
	RegisterChunkProcessor(new PhonemeMotionDataChunkProcessor1(this));
	RegisterChunkProcessor(new FXMaterialChunkProcessor1(this));

	// version 2
	RegisterChunkProcessor(new MaterialChunkProcessor2(this));
	RegisterChunkProcessor(new MaterialLayerChunkProcessor2(this));
	RegisterChunkProcessor(new MeshChunkProcessor2(this));
	RegisterChunkProcessor(new MeshExpressionPartChunkProcessor2(this));

	// version 3
	RegisterChunkProcessor(new MaterialChunkProcessor3(this));
	RegisterChunkProcessor(new MeshChunkProcessor3(this));

	// version 10000
	RegisterChunkProcessor(new MeshChunkProcessor10000(this));
	RegisterChunkProcessor(new MaterialChunkProcessor10000(this));

	// create standard shared objects
	AddSharedData(new SharedHierarchyInfo());
	AddSharedData(new SharedMotionInfo());

	mStandardProcessorsInitialized = true;
}


// find the chunk processor and read in the chunk
bool Importer::ProcessChunk(std::ifstream* file, Actor* actor, Motion* motion, bool usePerPixelLighting) {
	// if we have reached the end of the file, exit
	if (file->eof())
		return false;

	LMA_ChunkOld	chunkOld = {0, 0};		// initialization to prevent a warning
	LMA_Chunk		chunk = {0, 0, 0};		// initialization to prevent a warning

	// try to read the chunk header
	if (mLMAHeader.mHiVersion == 1 && mLMAHeader.mLoVersion == 0) {
		if (!file->read((char*)&chunkOld, sizeof(LMA_ChunkOld)))
			return false; // failed reading chunk
	} else {
		if (!file->read((char*)&chunk, sizeof(LMA_Chunk)))
			return false; // failed reading chunk
	}

	// try to find the chunk processor which can process this chunk
	ChunkProcessor* processor = nullptr;

	if (mLMAHeader.mHiVersion == 1 && mLMAHeader.mLoVersion == 0)
		processor = FindChunk(chunkOld.mChunkID, 1); // using version 1
	else
		processor = FindChunk(chunk.mChunkID, chunk.mVersion);

	// if we cannot find the chunk, skip the chunk
	if (processor == nullptr) {
		if (mLMAHeader.mHiVersion == 1 && mLMAHeader.mLoVersion == 0) {
			if (GetLogging())
				LOG("Importer::ProcessChunk() - Unknown chunk (ID=%d  Size=%d bytes), skipping...", chunkOld.mChunkID, chunkOld.mSizeInBytes);
			file->seekg(chunkOld.mSizeInBytes, file->cur);
		} else {
			if (GetLogging())
				LOG("Importer::ProcessChunk() - Unknown chunk (ID=%d  Size=%d bytes), skipping...", chunk.mChunkID, chunk.mSizeInBytes);
			file->seekg(chunk.mSizeInBytes, file->cur);
		}

		return true;
	}

	// try to process the chunk
	return processor->Process(file, actor, motion, usePerPixelLighting);
}


// check if the given file contains an actor
bool Importer::IsActor(const std::string& fileName) {
	// check for a valid name
	if (fileName.empty())
		return false;

	// get the chunk header
	std::ifstream file(fileName, std::ios_base::binary);
	if (!file)
		return false;

	// try to read the header
	LMA_Header header;
	if (!file.read((char*)&header, sizeof(LMA_Header))) {
		file.close();
		return false;
	}

	// close the file again
	file.close();

	// check if it is really an LMA file
	if (header.mFourcc[0] != 'L' || header.mFourcc[1] != 'M' || header.mFourcc[2] != 'A' || header.mFourcc[3] != ' ')
		return false;

	// check if we are dealing with an actor
	return header.mActor;
}


// check if the given file contains a motion
bool Importer::IsMotion(const std::string& fileName) {
	// check for a valid name
	if (fileName.empty())
		return false;

	// get the chunk header
	std::ifstream file(fileName, std::ios_base::binary);
	if (!file)
		return false;

	// try to read the header
	LMA_Header header;
	if (!file.read((char*)&header, sizeof(LMA_Header))) {
		file.close();
		return false;
	}

	// close the file again
	file.close();

	// check if it is really an LMA file
	if (header.mFourcc[0] != 'L' || header.mFourcc[1] != 'M' || header.mFourcc[2] != 'A' || header.mFourcc[3] != ' ')
		return false;

	// check if we are dealing with a motion
	return !header.mActor;
}


// constructor
ChunkIterator::ChunkIterator(const int chunkFilterType, std::ifstream* file) {
	// set the file to iterate through
	mFile = file;

	// set chunk type
	mChunkFilterType = chunkFilterType;

	// read the header
	mFile->read((char*)&mLMAHeader, sizeof(LMA_Header));
}


// step forward
std::ifstream* ChunkIterator::Next(int& chunkSize) {
	// if not check if there still is a chunk of the iterator's type
	LMA_ChunkOld	chunkOld = {0, 0};		// initialization to prevent a warning
	LMA_Chunk		chunk = {0, 0, 0};		// initialization to prevent a warning

	// we are done...
	if (IsDone())
		return nullptr;

	// try to read the chunk header
	if (mLMAHeader.mHiVersion == 1 && mLMAHeader.mLoVersion == 0) {
		mFile->read((char*)&chunkOld, sizeof(LMA_ChunkOld));

		if (mFile) {
			//LOG("Read failed. Return!");
			return nullptr;
		}

		if (chunkOld.mChunkID == mChunkFilterType) {
			chunkSize = chunkOld.mSizeInBytes;
			//LOG("Chunk Found.");
			return mFile;
		} else {
			//LOG("Skipping: Size: %i, SeekPos: %i", chunkOld.mSizeInBytes, mFile->tellg());
			mFile->seekg(chunkOld.mSizeInBytes, mFile->cur);
		}
	} else {
		mFile->read((char*)&chunk, sizeof(LMA_Chunk));

		if (mFile) {
			//LOG("Read failed. Return!");
			return nullptr;
		}

		if (chunk.mChunkID == mChunkFilterType) {
			chunkSize = chunk.mSizeInBytes;
			//LOG("Chunk Found.");
			return mFile;
		} else {
			//LOG("Skipping: Size: %i, SeekPos: %i", chunk.mSizeInBytes, mFile->tellg());
			mFile->seekg(chunk.mSizeInBytes, mFile->cur);
		}
	}

	// go again
	if (!IsDone())
		return Next(chunkSize);

	// if all hasn't helped return nullptr
	return nullptr;
}


// end of iteration ?
bool ChunkIterator::IsDone() {
	// if we have reached the end of the file, return false
	if (mFile->eof())
		return true;

	// still chunks to process
	return false;
}


} // namespace