#pragma once

#include <fstream>

namespace RCore {

/// Incomplete class declaration.
class LogManager;

/**
 * LogFile class for writing debug/log data to files.
 * Should not be used directly, but through the LogManager class.
 */
class LogFile : public std::ofstream {

public:
	/**
	 * The importance of a logged message.
	 */
	enum ELogMessageLevel {
		LOW = 0x00,	/*Low logging priority */
		NORMAL = 0x01, /*Normal logging priority (default) */
		HIGH = 0x02	/*High logging priority */
	};

	/**
	 * Constructor. Called by LogManager.
	 * @param name The filename of the log file.
	 */
	LogFile(const std::string & name);

	/**
	 * Destructor.
	 */
	~LogFile();

	/**
	 * Logs a message to log file.
	 * @param message The message to write into the log file.
	 * @param logMessageLevel The log message priority.
	 * @param source The message source.
	 */
	void LogMessage(const std::string & message, const ELogMessageLevel& logMessageLevel = LogFile::NORMAL, const std::string & source = "");

	/// Friend class declaration.
	friend class LogManager;
};

} // namespace