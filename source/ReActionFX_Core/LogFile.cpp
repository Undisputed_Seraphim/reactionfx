#include <sstream>
#include "LogFile.h"

namespace RCore {

LogFile::LogFile(const std::string & name) {
	open(name);

}

LogFile::~LogFile() {
	close();
}

void LogFile::LogMessage(const std::string & message, const ELogMessageLevel& logMessageLevel, const std::string & source) {
	std::ostringstream oss;
	oss << ((logMessageLevel == HIGH) ? "HIGH: " : (logMessageLevel == NORMAL) ? "" : "");
	oss << message << std::endl;

	std::string buffer = oss.str();

	if (is_open()) {
		write((char*)buffer.data(), buffer.size());
		flush();
	}
}

} // namespace