#pragma once

namespace RCore {

/**
 * The class containing methods to solve quadric, cubic and quartic equations.
 */
class RootSolver {
public:
	/**
	 * Solve a quadric equation.
	 * @param c The coefficients.
	 * @param s The values that will contain the solutions (output).
	 * @result The number of roots.
	 */
	static int SolveQuadratic(float c[3], float s[2]);

	/**
	 * Solve a cubic equation.
	 * @param c The coefficients.
	 * @param s The values that will contain the solutions (output).
	 * @result The number of roots.
	 */
	static int SolveCubic(float c[4], float s[3]);

	/**
	 * Solve a quartic equation.
	 * @param c The coefficients.
	 * @param s The values that will contain the solutions (output).
	 * @result The number of roots.
	 */
	static int SolveQuartic(float c[5], float s[4]);
};

} // namespace