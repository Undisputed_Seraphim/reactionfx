#pragma once

#include <cmath>
#include <cstring>
#include <limits>
#include "Algorithms.h"
#include "LogManager.h"
#include "PlaneEq.h"
#include "Vector.h"

namespace RCore {

// forward declaration
template <class T> class TQuaternion;

/**
 * A 4x4 matrix template/class.
 * Matrices can for example be used to transform points or vectors.
 * Transforming means moving to another coordinate system. With matrices you can do things like: translate (move), rotate and scale.
 * One single matrix can store a translation, rotation and scale. If we have only a rotation inside the matrix, this means that if we
 * multiply the matrix with a vector, the vector will rotate by the rotation inside the matrix! The cool thing is that you can also
 * concatenate matrices. In other words, you can multiply matrices with eachother. If you have a rotation matrix, like described above, and
 * also have a translation matrix, then multiplying these two matrices with eachother will result in a new matrix, which contains both the
 * rotation and the translation! So when you multiply this resulting matrix with a vector, it will both translate and rotate.
 * But, does it first rotate and then translate in the rotated space? Or does it first translate and then rotate?
 * For example if you want to rotate a planet, while it's moving, you want to rotate it in it's local coordinate system. Like when you spin
 * a globe you can have on your desk. So where it spins around it's own center. However, if the planet is at location (10,10,10) in 3D space for example
 * it is also possible that rotates around the origin in world space (0,0,0). The order of multiplication between matrices matters.
 * This means that (matrixA * matrixB) does not have to not result in the same as (matrixB * matrixA).
 *
 * Here is some information about how the matrices are stored internally:
 *
 * [00 01 02 03] // m16 offsets <br>
 * [04 05 06 07] <br>
 * [08 09 10 11] <br>
 * [12 13 14 15] <br>
 *
 * [00 01 02 03] // m44 offsets --> [row][column] <br>
 * [10 11 12 13] <br>
 * [20 21 22 23] <br>
 * [30 31 32 33] <br>
 *
 * [Xx Xy Xz 0]	// right <br>
 * [Yx Yy Yz 0]	// up <br>
 * [Zx Zy Zz 0]	// forward <br>
 * [Tx Ty Tz 1]	// translation <br>
 *
 */

template <class T>
class TMatrix {

public:
	/**
	 * Default constructor.
	 * @param identity If set to true, the constructed matrix will be initialized as identity matrix. In case of false (default) no data is initialized.
	 */
	TMatrix<T>(bool identity = false) { if (identity) Identity(); }

	/**
	 * Copy constructor.
	 * @param m The matrix to copy the data from.
	 */
	TMatrix<T>(const TMatrix<T>& m);

	/**
	 * Construct the matrix from a position, rotation and scale.
	 * @param position The translation part of the matrix.
	 * @param rotation The rotation of the matrix, in form of a quaternion.
	 * @param scale The scale of the matrix.
	 */
	TMatrix<T>(const TVector3<T>& position, const TQuaternion<T>& rotation, const TVector3<T>& scale);

	/**
	 * Construct the matrix from a position and rotation.
	 * @param position The translation part of the matrix.
	 * @param rotation The rotation of the matrix, in form of a quaternion.
	 */
	TMatrix<T>(const TVector3<T>& position, const TQuaternion<T>& rotation);

	/**
	 * Sets the matrix to identity.
	 * When a matrix is an identity matrix it will have no influence.
	 */
	void Identity();

	/**
	 * Makes the matrix a scaling matrix.
	 * Values of 1.0 would have no influence on the scale. Values of for example 2.0 would scale up by a factor of two.
	 * @param sx The scaling along the x-axis.
	 * @param sy The scaling along the y-axis.
	 * @param sz The scaling along the z-axis.
	 */
	void SetScaleMatrix(const T sx, const T sy, const T sz);

	/**
	 * Makes this matrix a translation matrix.
	 * @param tx The translation along the x-axis, in units.
	 * @param ty The translation along the y-axis, in units.
	 * @param tz The translation along the z-axis, in units.
	 */
	void SetTranslationMatrix(const T tx, const T ty, const T tz);

	/**
	 * Makes the matrix an rotation matrix along the x-axis.
	 * @param angle The angle to rotate around this axis, in radians.
	 */
	void SetRotationMatrixX(const T angle);

	/**
	 * Makes the matrix a rotation matrix along the y-axis.
	 * @param angle The angle to rotate around this axis, in radians.
	 */
	void SetRotationMatrixY(const T angle);

	/**
	 * Makes the matrix a rotation matrix along the z-axis.
	 * @param angle The angle to rotate around this axis, in radians.
	 *
	 */
	void SetRotationMatrixZ(const T angle);

	/**
	 * Makes the matrix a rotation matrix around the x, y and z axis.
	 * @param yaw The angle to rotate around the y-axis, in radians.
	 * @param pitch The angle to rotate around the x-axis, in radians.
	 * @param roll, The angle to rotate around teh z-axis, in radians.
	 */
	//void SetRotationMatrixXYZ(const T yaw, const T pitch, const T roll);

	/**
	 * Makes the matrix a rotation matrix around a given axis with a given angle.
	 * @param axis The axis to rotate around.
	 * @param angle The angle to rotate around this axis, in radians.
	 */
	void SetRotationMatrixAxisAngle(const TVector3<T>& axis, const T angle);

	/**
	 * Makes the matrix a rotation matrix given the euler angles.
	 * @param anglevec The vector containing the angles for each axis, in radians, so (pitch, yaw, roll) as (x,y,z).
	 */
	void SetRotationMatrixEuler(const TVector3<T>& anglevec);

	/**
	 * Inverse rotate a vector with this matrix.
	 * This means that the vector will be multiplied with the inverse rotation of this matrix.
	 * @param v The vector to rotate.
	 * @result The rotated vector.
	 */
	TVector3<T>	InverseRot(const TVector3<T>& v);

	/**
	 * Multiply this matrix with another matrix and stores the result in itself.
	 * @param right The matrix to multiply with.
	 */
	void MultMatrix(const TMatrix<T>& right);

	/**
	 * Multiply this matrix with another matrix, but only multiply the 3x4 part.
	 * So treat the other matrix as 4x3 matrix instead of 4x4 matrix. Stores the result in itself.
	 * @param right The matrix to multiply with.
	 */
	void MultMatrix3x4(const TMatrix<T>& right);

	/**
	 * Multiply this matrix with the 3x3 rotation part of the other given matrix, and modify itself.
	 * @param right The matrix to multiply with.
	 */
	void MultMatrix3x3(const TMatrix<T>& right);

	/**
	 * Multiply this matrix with the inverse of the 3x3 rotation part of another given matrix.
	 * In other words, multiply this matrix with an inverse rotation matrix.
	 * @param right The matrix to multiply with.
	 */
	void MultInverseMatrix3x3(const TMatrix<T>& right);

	/**
	 * Transpose the matrix (swap rows with columns).
	 */
	void Transpose();

	/**
	 * Transpose the 3x3 rotation part of the matrix.
	 * Leaves the translation on place.
	 */
	void TransposeRot();

	/**
	 * Transpose the translation 1x3 translation part.
	 * Leaves the rotation in tact.
	 */
	void TransposeTranslation();

	/**
	 * Adjoint this matrix.
	 */
	void Adjoint();

	/**
	 * Inverse this matrix.
	 */
	void Inverse();

	/**
	 * Makes this the inverse tranpose version of this matrix.
	 * The inverse transpose is the transposed version of the inverse.
	 */
	inline void InverseTranspose();

	/**
	 * Returns the inverse transposed version of this matrix.
	 * The inverse transpose is the transposed version of the inverse.
	 * @result The inverse transposed version of this matrix.
	 */
	inline TMatrix<T> InverseTransposed() const;

	/**
	 * Orthonormalize this matrix (to prevent skewing or other errors).
	 * This normalizes the x, y and z vectors of the matrix.
	 * It makes sure that the axis vectors are perpendicular to eachother.
	 */
	void OrthoNormalize();

	/**
	 * Normalizes the matrix, which means that all axis vectors (right, up, forward)
	 * will be made of unit length.
	 */
	void Normalize();

	/**
	 * Returns a normalized version of this matrix.
	 * @result The normalized version of this matrix, where the  right, up and forward vectors are of unit length.
	 */
	inline TMatrix<T> Normalized() const;

	/**
	 * Scale this matrix.
	 * @param sx The factor to scale with along the x-axis.
	 * @param sy The factor to scale with along the y-axis.
	 * @param sz The factor to scale with along the z-axis.
	 */
	void Scale(const T sx, const T sy, const T sz);

	/**
	 * Rotate this matrix around the x-axis.
	 * @param angle The rotation in radians.
	 */
	void RotateX(const T angle);

	/**
	 * Rotate this matrix around the y-axis.
	 * @param angle The rotation in radians.
	 */
	void RotateY(const T angle);

	/**
	 * Rotate this matrix around the z-axis.
	 * @param angle The rotation in radians.
	 */
	void RotateZ(const T angle);

	/**
	 * Rotate this matrix around the x, y and z axis.
	 * @param yaw The rotation around the y-axis, in radians.
	 * @param pitch The rotation around the x-axis, in radians.
	 * @param roll The rotation around the z-axis, in radians.
	 */
	//void RotateXYZ(const T yaw, const T pitch, const T roll);

	/**
	 * Multiply a vector with the 3x3 rotation part of this matrix.
	 * @param v The vector to transform.
	 * @result The transformed (rotated) vector.
	 */
	inline TVector3<T> Mul3x3(const TVector3<T> &v);

	/**
	 * Returns the inversed version of this matrix.
	 * @result The inversed version of this matrix.
	 */
	inline TMatrix<T> Inversed() const { TMatrix<T> m(*this); m.Inverse(); return m; }

	/**
	 * Returns the transposed version of this matrix.
	 * @result The transposed version of this matrix.
	 */
	inline TMatrix<T> Transposed() const { TMatrix<T> m(*this); m.Transpose(); return m; }

	/**
	 * Returns the adjointed version of this matrix.
	 * @result The adjointed version of this matrix.
	 */
	inline TMatrix<T> Adjointed() const { TMatrix<T> m(*this); m.Adjoint(); return m; }

	/**
	 * Translate the matrix.
	 * @param x The number of units to add to the current translation along the x-axis.
	 * @param y The number of units to add to the current translation along the y-axis.
	 * @param z The number of units to add to the current translation along the z-axis.
	 */
	inline void	Translate(const T x, const T y, const T z) { m44[3][0] += x;	m44[3][1] += y; m44[3][2] += z; }

	/**
	 * Translate the matrix.
	 * @param t The vector containing the translation to add to the current translation of the matrix.
	 */
	inline void	Translate(const TVector3<T> &t) { m44[3][0] += t.x; m44[3][1] += t.y; m44[3][2] += t.z; }

	/**
	 * Set the right vector (must be normalized).
	 * @param xx The x component of the right vector.
	 * @param xy The y component of the right vector.
	 * @param xz The z component of the right vector.
	 */
	inline void SetRight(const T xx, const T xy, const T xz);

	/**
	 * Set the right vector.
	 * @param x The right vector, must be normalized.
	 */
	inline void	SetRight(const TVector3<T>& x);

	/**
	 * Set the up vector (must be normalized).
	 * @param yx The x component of the up vector.
	 * @param yy The y component of the up vector.
	 * @param yz The z component of the up vector.
	 */
	inline void SetUp(const T yx, const T yy, const T yz);

	/**
	 * Set the up vector (must be normalized).
	 * @param y The up vector.
	 */
	inline void SetUp(const TVector3<T>& y);

	/**
	 * Set the forward vector (must be normalized).
	 * @param zx The x component of the forward vector.
	 * @param zy The y component of the forward vector.
	 * @param zz The z component of the forward vector.
	 */
	inline void SetForward(const T zx, const T zy, const T zz);

	/**
	 * Set the forward vector (must be normalized).
	 * @param z The forward vector.
	 */
	inline void SetForward(const TVector3<T>& z);

	/**
	 * Set the translation part of the matrix.
	 * @param tx The translation along the x-axis.
	 * @param ty The translation along the y-axis.
	 * @param tz The translation along the z-axis.
	 */
	inline void	SetTranslation(const T tx, const T ty, const T tz);

	/**
	 * Set the translation part of the matrix.
	 * @param t The translation vector.
	 */
	inline void	SetTranslation(const TVector3<T>& t);

	/**
	 * Get the right vector (writable).
	 * @result The right vector (x-axis).
	 */
	inline TVector3<T>&	GetRight();

	/**
	 * Get the right vector (only readable).
	 * @result The right vector (x-axis).
	 */
	inline const TVector3<T>& GetRight() const;

	/**
	 * Get the up vector (only readable).
	 * @result The up vector (y-axis).
	 */
	inline const TVector3<T>& GetUp() const;

	/**
	 * Get the up vector (writable).
	 * @result The up vector (y-axis).
	 */
	inline TVector3<T>& GetUp();

	/**
	 * Get the forward vector (writable).
	 * @result The forward vector (z-axis).
	 */
	inline TVector3<T>& GetForward();

	/**
	 * Get the forward vector (only readable).
	 * @result The forward vector (z-axis).
	 */
	inline const TVector3<T>& GetForward() const;

	/**
	 * Get the translation part of the matrix (only readable).
	 * @result The vector containing the translation.
	 */
	inline const TVector3<T>& GetTranslation() const;

	/**
	 * Get the translation part of the matrix (writable).
	 * @result The vector containing the translation.
	 */
	inline TVector3<T>& GetTranslation();

	/**
	 * Calculates the determinant of the matrix.
	 * @result The determinant.
	 */
	T CalcDeterminant() const;

	/**
	 * Calculates the euler angles.
	 * @result The euler angles, describing the rotation along each axis, in radians.
	 */
	TVector3<T>	CalcEulerAngles() const;

	/**
	 * Makes this matrix a mirrored version of a specified matrix.
	 * After executing this operation this matrix is the mirrored version of the specified matrix.
	 * @param transform The transformation matrix to mirror (so the original matrix).
	 * @param plane The plane to use as mirror.
	 */
	void Mirror(const TMatrix<T>& transform, const TPlaneEq<T>& plane);

	/**
	 * Makes this matrix a lookat matrix (also known as camera or view matrix).
	 * @param view The view position, so the position of the camera.
	 * @param target The target position, so where the camera is looking at.
	 * @param up The up vector, describing the roll of the camera, where (0,1,0) would mean the camera is straight up and has no roll and
	 * where (0,-1,0) would mean the camera is upside down, etc.
	 */
	void LookAt(const TVector3<T>& view, const TVector3<T>& target, const TVector3<T>& up);

	/**
	 * Makes this matrix a perspective projection matrix.
	 * @param fov The field of view, in radians.
	 * @param aspect The aspect ratio (think of values like 0.75 or 1.33).
	 * @param zNear The distance to the near plane.
	 * @param zFar The distance to the far plane.
	 */
	void Projection(const T fov, const T aspect, const T zNear, const T zFar);

	/**
	 * Makes this matrix an ortho projection matrix, so without perspective.
	 * @param left The left of the image plane.
	 * @param right The right of the image plane.
	 * @param top The top of the image plane.
	 * @param bottom The bottom of the image plane.
	 * @param znear The distance to the near plane.
	 * @param zfar The distance to the far plane.
	 */
	void Ortho(const T left, const T right, const T top, const T bottom, const T znear, const T zfar);

	/**
	 * Makes this matrix a frustum matrix.
	 * @param left The left of the image plane.
	 * @param right The right of the image plane.
	 * @param top The top of the image plane.
	 * @param bottom The bottom of the image plane.
	 * @param znear The distance to the near plane.
	 * @param zfar The distance to the far plane.
	 */
	void Frustum(const T left, const T right, const T top, const T bottom, const T znear, const T zfar);

	/**
	 * Get the handedness of the matrix, which described if the matrix is left- or right-handed.
	 * The value returned by this method is the dot product between the forward vector and the result of the
	 * cross product between the right and up vector. So: DotProduct( Cross(right, up), forward );
	 * If the value returned by this method is positive we are dealing with a matrix which is in a right-handed
	 * coordinate system, otherwise we are dealing with a left-handed coordinate system.
	 * Performing an odd number of reflections reverses the handedness. An even number of reflections is always
	 * equivalent to a rotation, so any series of reflections can always be regarded as a single rotation followed
	 * by at most one reflection.
	 * If a reflection is present (think of a mirror) the handedness will be reversed. A reflection can be detected
	 * by looking at the determinant of the matrix. If the determinant is negative, then a reflection is present.
	 * @result The handedness of the matrix. If this value is positive we are dealing with a matrix in a right handed
	 *         coordinate system. Otherwise we are dealing with one in a left-handed coordinate system.
	 * @see IsRightHanded()
	 * @see IsLeftHanded()
	 */
	T CalcHandedness() const;

	/**
	 * Check if this matrix is symmetric or not.
	 * A materix is said to be symmetric if and only if M(i, j) = M(j, i).
	 * That is, a matrix whose entries are symmetric about the main diagonal.
	 * @param tolerance The maximum difference tolerance between the M(i, j) and M(j, i) entries.
	 *                   The reason for having this tolerance is of course floating point inaccuracy which might have
	 *                   caused some entries to be a bit different.
	 * @result Returns true when the matrix is symmetric, or false when not.
	 */
	bool IsSymmetric(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Check if this matrix is a diagonal matrix or not.
	 * A matrix is said to be a diagonal matrix when only the entries on the diagonal contain non-zero values.
	 * The tolerance value is needed because of possible floating point inaccuracies.
	 * @param tolerance The maximum difference between 0 and the entry on the diagonal.
	 * @result Returns true when the matrix is a diagonal matrix, otherwise false is returned.
	 */
	bool IsDiagonal(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Check if the matrix is orthogonal or not.
	 * A matrix is orthogonal if the vectors in the matrix form an orthonormal set.
	 * This is when the vectors (right, up and forward) are perpendicular to eachother.
	 * If a matrix is orthogonal, the inverse of the matrix is equal to the transpose of the matrix.
	 * This assumption can be used to optimize specific calculations, since the inverse is slower to calculate than
	 * the transpose of the matrix. Also it can speed up by transforming normals with the matrix.
	 * In that example instead of having to use the inverse transpose matrix, you could just use the transpose of the matrix.
	 * @param tolerance The maximum tolerance in the orthonormal test.
	 * @result Returns true when the matrix is orthogonal, otherwise false is returned.
	 */
	bool IsOrthogonal(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Check if the matrix is an identity matrix or not.
	 * @param tolerance The maximum error value per entry in the matrix.
	 * @result Returns true if this matrix is an identity matrix, otherwise false is returned.
	 */
	bool IsIdentity(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Check if the matrix is left handed or not.
	 * @result Returns true when the matrix is left handed, otherwise false is returned (so then it is right handed).
	 */
	inline bool IsLeftHanded() const;

	/**
	 * Check if the matrix is right handed or not.
	 * @result Returns true when the matrix is right handed, otherwise false is returned (so then it is left handed).
	 */
	inline bool IsRightHanded() const;

	/**
	 * Check if this matrix is a pure rotation matrix or not.
	 * @param tolerance The maximum error in the measurement.
	 * @result Returns true when the matrix represents only a rotation, otherwise false is returned.
	 */
	inline bool IsPureRotationMatrix(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Check if this matrix contains a reflection or not.
	 * @param tolerance The maximum error in the measurement.
	 * @result Returns true when the matrix represents a reflection, otherwise false is returned.
	 */
	inline bool IsReflective(const float tolerance = std::numeric_limits<MReal>::epsilon()) const;

	/**
	 * Prints the matrix into the logfile or debug output, using RCore::LOG().
	 * Please note that the values are printed using floats or doubles. So it is not possible
	 * to use this method for printing matrices of vectors or something other than real numbers.
	 */
	void Print();

	/**
	 * Returns a translation matrix.
	 * @param v The translation of the matrix.
	 * @result The translation matrix having the specified translation.
	 */
	static inline TMatrix<T> TranslationMatrix(const TVector3<T>& v) { TMatrix<T> m; m.SetTranslationMatrix(v.x, v.y, v.z); return m; }

	/**
	 * Returns a rotation matrix along the x-axis.
	 * @param rad The angle of rotation, in radians.
	 * @result A rotation matrix.
	 */
	static inline TMatrix<T> RotationMatrixX(const T rad) { TMatrix<T> m; m.SetRotationMatrixX(rad); return m; }

	/**
	 * Returns a rotation matrix along the y-axis.
	 * @param rad The angle of rotation, in radians.
	 * @result A rotation matrix.
	 */
	static inline TMatrix<T> RotationMatrixY(const T rad) { TMatrix<T> m; m.SetRotationMatrixY(rad); return m; }

	/**
	 * Returns a rotation matrix along the z-axis.
	 * @param rad The angle of rotation, in radians.
	 * @result A rotation matrix.
	 */
	static inline TMatrix<T> RotationMatrixZ(const T rad) { TMatrix<T> m; m.SetRotationMatrixZ(rad); return m; }

	/**
	 * Returns a rotation matrix along the x, y and z-axis.
	 * @param eulerAngles The euler angles in radians.
	 * @result A rotation matrix.
	 */
	static inline TMatrix<T> RotationMatrixEuler(const TVector3<T>& eulerAngles) { TMatrix<T> m; m.SetRotationMatrixEuler(eulerAngles); return m; }

	/**
	 * Returns a rotation matrix from a given axis and angle.
	 * @param axis The axis to rotate around.
	 * @param angle The angle of rotation, in radians.
	 * @result A rotation matrix.
	 */
	static inline TMatrix<T> RotationMatrixAxisAngle(const TVector3<T> &axis, const T angle) { TMatrix<T> m; m.SetRotationMatrixAxisAngle(axis, angle); return m; }

	/**
	 * Returns a scale matrix from a given scaling factor.
	 * @param s The vector containing the scaling factors for each axis.
	 * @result A scaling matrix.
	 */
	static inline TMatrix<T> ScaleMatrix(const T s) { TMatrix<T> m; m.SetScaleMatrix(s, s, s); return m; }

	/**
	 * Returns a scaling matrix.
	 * @param x The scaling factor along the x-axis.
	 * @param y The scaling factor along the y-axis.
	 * @param z The scaling factor along the z-axis.
	 * @result A scaling matrix.
	 */
	static inline TMatrix<T> ScaleMatrix(const T x, const T y, const T z) { TMatrix<T> m; m.SetScaleMatrix(x, y, z); return m; }

	// operators
	TMatrix<T>	operator +  (const TMatrix<T>& right) const;
	TMatrix<T>	operator -  (const TMatrix<T>& right) const;
	TMatrix<T>	operator *  (const TMatrix<T>& right) const;
	TMatrix<T>&	operator += (const TMatrix<T>& right);
	TMatrix<T>&	operator -= (const TMatrix<T>& right);
	TMatrix<T>&	operator *= (const TMatrix<T>& right);
	inline void	operator  = (const TMatrix<T>& right);

	// attributes
	union {
		T	m16[16];
		T	m44[4][4];
	};
};


// default types
typedef TMatrix<MReal>	Matrix;
typedef TMatrix<double>	DMatrix;
typedef TMatrix<float>	FMatrix;


template <class T>
TMatrix<T>::TMatrix(const TMatrix<T>& m) {
	std::memcpy(m16, m.m16, sizeof(TMatrix<T>));
}


template <class T>
TMatrix<T>::TMatrix(const TVector3<T>& position, const TQuaternion<T>& rotation, const TVector3<T>& scale) {
	// convert the quaternion to the matrix
	T x = rotation.x;
	T y = rotation.y;
	T z = rotation.z;
	T w = rotation.w;
	T xx = x * x;
	T xy = x * y, yy = y * y;
	T xz = x * z, yz = y * z, zz = z * z;
	T xw = x * w, yw = y * w, zw = z * w, ww = w * w;
	m44[0][0] = (+xx - yy - zz + ww)*scale.x;	m44[0][1] = (+xy + zw + xy + zw)*scale.y;	m44[0][2] = (+xz - yw + xz - yw)*scale.z;	m44[0][3] = 0;
	m44[1][0] = (+xy - zw + xy - zw)*scale.x;	m44[1][1] = (-xx + yy - zz + ww)*scale.y;	m44[1][2] = (+yz + xw + yz + xw)*scale.z;	m44[1][3] = 0;
	m44[2][0] = (+xz + yw + xz + yw)*scale.x;	m44[2][1] = (+yz - xw + yz - xw)*scale.y;	m44[2][2] = (-xx - yy + zz + ww)*scale.z;	m44[2][3] = 0;
	m44[3][0] = position.x;				m44[3][1] = position.y;				m44[3][2] = position.z;				m44[3][3] = 1;
}


template <class T>
TMatrix<T>::TMatrix(const TVector3<T>& position, const TQuaternion<T>& rotation) {
	// convert the quaternion to the matrix
	T x = rotation.x;
	T y = rotation.y;
	T z = rotation.z;
	T w = rotation.w;
	T xx = x * x;
	T xy = x * y, yy = y * y;
	T xz = x * z, yz = y * z, zz = z * z;
	T xw = x * w, yw = y * w, zw = z * w, ww = w * w;
	m44[0][0] = +xx - yy - zz + ww;	m44[0][1] = +xy + zw + xy + zw;	m44[0][2] = +xz - yw + xz - yw;	m44[0][3] = 0;
	m44[1][0] = +xy - zw + xy - zw;	m44[1][1] = -xx + yy - zz + ww;	m44[1][2] = +yz + xw + yz + xw;	m44[1][3] = 0;
	m44[2][0] = +xz + yw + xz + yw;	m44[2][1] = +yz - xw + yz - xw;	m44[2][2] = -xx - yy + zz + ww;	m44[2][3] = 0;
	m44[3][0] = position.x;		m44[3][1] = position.y;		m44[3][2] = position.z;		m44[3][3] = 1;
}



template <class T>
TMatrix<T> TMatrix<T>::operator + (const TMatrix<T>& right) const {
	TMatrix<T> r;
	//const mat44& m = right.m44;

	for (int i = 0; i < 4; i++) {
		r.m44[i][0] = m44[i][0] + right.m44[i][0];
		r.m44[i][1] = m44[i][1] + right.m44[i][1];
		r.m44[i][2] = m44[i][2] + right.m44[i][2];
		r.m44[i][3] = m44[i][3] + right.m44[i][3];
	}

	return r;
}


template <class T>
TMatrix<T> TMatrix<T>::operator - (const TMatrix<T>& right) const {
	TMatrix<T> r;

	for (int i = 0; i < 4; i++) {
		r.m44[i][0] = m44[i][0] - right.m44[i][0];
		r.m44[i][1] = m44[i][1] - right.m44[i][1];
		r.m44[i][2] = m44[i][2] - right.m44[i][2];
		r.m44[i][3] = m44[i][3] - right.m44[i][3];
	}

	return r;
}



template <class T>
TMatrix<T> TMatrix<T>::operator * (const TMatrix<T>& right) const {
	TMatrix<T> r;
	//const mat44& m = right.m44;

	unsigned int i;
	for (i = 0; i < 4; i++) {
		const T* v = m16 + i * 4;
		r.m44[i][0] = v[0] * right.m44[0][0] + v[1] * right.m44[1][0] + v[2] * right.m44[2][0] + v[3] * right.m44[3][0];
		r.m44[i][1] = v[0] * right.m44[0][1] + v[1] * right.m44[1][1] + v[2] * right.m44[2][1] + v[3] * right.m44[3][1];
		r.m44[i][2] = v[0] * right.m44[0][2] + v[1] * right.m44[1][2] + v[2] * right.m44[2][2] + v[3] * right.m44[3][2];
		r.m44[i][3] = v[0] * right.m44[0][3] + v[1] * right.m44[1][3] + v[2] * right.m44[2][3] + v[3] * right.m44[3][3];
	}

	return r;
}


template <class T>
TMatrix<T>& TMatrix<T>::operator += (const TMatrix<T>& right) {
	for (int i = 0; i < 4; i++) {
		m44[i][0] += right.m44[i][0];
		m44[i][1] += right.m44[i][1];
		m44[i][2] += right.m44[i][2];
		m44[i][3] += right.m44[i][3];
	}
	return *this;
}


template <class T>
TMatrix<T>& TMatrix<T>::operator -= (const TMatrix<T>& right) {
	//const mat44& m = right.m44;
	for (int i = 0; i < 4; i++) {
		m44[i][0] -= right.m44[i][0];
		m44[i][1] -= right.m44[i][1];
		m44[i][2] -= right.m44[i][2];
		m44[i][3] -= right.m44[i][3];
	}
	return *this;
}


template <class T>
TMatrix<T>& TMatrix<T>::operator *= (const TMatrix<T>& right) {
	T v[4];
	for (int i = 0; i < 4; i++) {
		v[0] = m44[i][0];
		v[1] = m44[i][1];
		v[2] = m44[i][2];
		v[3] = m44[i][3];
		m44[i][0] = v[0] * right.m44[0][0] + v[1] * right.m44[1][0] + v[2] * right.m44[2][0] + v[3] * right.m44[3][0];
		m44[i][1] = v[0] * right.m44[0][1] + v[1] * right.m44[1][1] + v[2] * right.m44[2][1] + v[3] * right.m44[3][1];
		m44[i][2] = v[0] * right.m44[0][2] + v[1] * right.m44[1][2] + v[2] * right.m44[2][2] + v[3] * right.m44[3][2];
		m44[i][3] = v[0] * right.m44[0][3] + v[1] * right.m44[1][3] + v[2] * right.m44[2][3] + v[3] * right.m44[3][3];
	}
	return *this;
}



template <class T>
inline void TMatrix<T>::operator = (const TMatrix<T>& right) {
	std::memcpy(m16, right.m16, sizeof(TMatrix<T>));
}

template <class T>
T TMatrix<T>::CalcDeterminant() const {
	return
		m44[0][0] * m44[1][1] * m44[2][2] +
		m44[0][1] * m44[1][2] * m44[2][0] +
		m44[0][2] * m44[1][0] * m44[2][1] -
		m44[0][2] * m44[1][1] * m44[2][0] -
		m44[0][1] * m44[1][0] * m44[2][2] -
		m44[0][0] * m44[1][2] * m44[2][1];
}


template <class T>
TVector3<T> TMatrix<T>::CalcEulerAngles() const {
#define EulSafe	     "\000\001\002\000"
#define EulNext	     "\001\002\000\001"
#define EulGetOrd(ord,i,j,k,n,s,f) {unsigned o=ord;f=o&1;s=o&1; n=o&1;i=EulSafe[o&3];j=EulNext[i+n];k=EulNext[i+1-n];}

#define EulOrd(i,p,r,f)	   (((((((i)<<1)+(p))<<1)+(r))<<1)+(f))

#define EulOrdXYZs    EulOrd(0,0,0,0)
#define EulOrdXYXs    EulOrd(0,0,1,0)
#define EulOrdXZYs    EulOrd(0,1,0,0)
#define EulOrdXZXs    EulOrd(0,1,1,0)
#define EulOrdYZXs    EulOrd(1,0,0,0)
#define EulOrdYZYs    EulOrd(1,0,1,0)
#define EulOrdYXZs    EulOrd(1,1,0,0)
#define EulOrdYXYs    EulOrd(1,1,1,0)
#define EulOrdZXYs    EulOrd(2,0,0,0)
#define EulOrdZXZs    EulOrd(2,0,1,0)
#define EulOrdZYXs    EulOrd(2,1,0,0)
#define EulOrdZYZs    EulOrd(2,1,1,0)

#define EulOrdZYXr    EulOrd(0,0,0,1)
#define EulOrdXYXr    EulOrd(0,0,1,1)
#define EulOrdYZXr    EulOrd(0,1,0,1)
#define EulOrdXZXr    EulOrd(0,1,1,1)
#define EulOrdXZYr    EulOrd(1,0,0,1)
#define EulOrdYZYr    EulOrd(1,0,1,1)
#define EulOrdZXYr    EulOrd(1,1,0,1)
#define EulOrdYXYr    EulOrd(1,1,1,1)
#define EulOrdYXZr    EulOrd(2,0,0,1)
#define EulOrdZXZr    EulOrd(2,0,1,1)
#define EulOrdXYZr    EulOrd(2,1,0,1)
#define EulOrdZYZr    EulOrd(2,1,1,1)

	int order = EulOrdXYZs;
	int i, j, k, n, s, f;

	EulGetOrd(order, i, j, k, n, s, f);
	TVector3<T> v;

	if (s == 1) {
		T sy = std::sqrt(m44[j][i] * m44[j][i] + m44[k][i] * m44[k][i]);
		if (sy > std::numeric_limits<MReal>::epsilon() * 16) {
			v.x = std::atan2(m44[j][i], m44[k][i]);
			v.y = std::atan2(sy, m44[i][i]);
			v.z = std::atan2(m44[i][j], -m44[i][k]);
		} else {
			v.x = std::atan2(-m44[k][j], m44[j][j]);
			v.y = std::atan2(sy, m44[i][i]);
			v.z = 0;
		}
	} else {
		T cy = std::sqrt(m44[i][i] * m44[i][i] + m44[i][j] * m44[i][j]);
		if (cy > std::numeric_limits<MReal>::epsilon() * 16) {
			v.x = std::atan2(m44[j][k], m44[k][k]);
			v.y = std::atan2(-m44[i][k], cy);
			v.z = std::atan2(m44[i][j], m44[i][i]);
		} else {
			v.x = std::atan2(-m44[k][j], m44[j][j]);
			v.y = std::atan2(-m44[i][k], cy);
			v.z = 0;
		}
	}

	if (n == 1) { v = -v; }
	if (f == 1) { T t = v.x; v.x = v.z;	v.z = t; }

	return v;
}


template <class T>
void TMatrix<T>::Identity() {
	m44[0][0] = 1; m44[0][1] = 0; m44[0][2] = 0; m44[0][3] = 0;
	m44[1][0] = 0; m44[1][1] = 1; m44[1][2] = 0; m44[1][3] = 0;
	m44[2][0] = 0; m44[2][1] = 0; m44[2][2] = 1; m44[2][3] = 0;
	m44[3][0] = 0; m44[3][1] = 0; m44[3][2] = 0; m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::SetScaleMatrix(const T x, const T y, const T z) {
	m44[0][0] = x; m44[0][1] = 0; m44[0][2] = 0; m44[0][3] = 0;
	m44[1][0] = 0; m44[1][1] = y; m44[1][2] = 0; m44[1][3] = 0;
	m44[2][0] = 0; m44[2][1] = 0; m44[2][2] = z; m44[2][3] = 0;
	m44[3][0] = 0; m44[3][1] = 0; m44[3][2] = 0; m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::SetTranslationMatrix(const T x, const T y, const T z) {
	m44[0][0] = 1; m44[0][1] = 0; m44[0][2] = 0; m44[0][3] = 0;
	m44[1][0] = 0; m44[1][1] = 1; m44[1][2] = 0; m44[1][3] = 0;
	m44[2][0] = 0; m44[2][1] = 0; m44[2][2] = 1; m44[2][3] = 0;
	m44[3][0] = x; m44[3][1] = y; m44[3][2] = z; m44[3][3] = 1;
}

template <class T>
void TMatrix<T>::SetRotationMatrixX(const T angle) {
	const T s = std::sin(angle);
	const T c = std::cos(angle);

	m44[0][0] = 1; m44[0][1] = 0; m44[0][2] = 0; m44[0][3] = 0;
	m44[1][0] = 0; m44[1][1] = c; m44[1][2] = s; m44[1][3] = 0;
	m44[2][0] = 0; m44[2][1] = -s; m44[2][2] = c; m44[2][3] = 0;
	m44[3][0] = 0; m44[3][1] = 0; m44[3][2] = 0; m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::SetRotationMatrixY(const T angle) {
	const T s = std::sin(angle);
	const T c = std::cos(angle);

	m44[0][0] = c; m44[0][1] = 0; m44[0][2] = -s; m44[0][3] = 0;
	m44[1][0] = 0; m44[1][1] = 1; m44[1][2] = 0; m44[1][3] = 0;
	m44[2][0] = s; m44[2][1] = 0; m44[2][2] = c; m44[2][3] = 0;
	m44[3][0] = 0; m44[3][1] = 0; m44[3][2] = 0; m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::SetRotationMatrixZ(const T angle) {
	const T s = std::sin(angle);
	const T c = std::cos(angle);

	m44[0][0] = c; m44[0][1] = s; m44[0][2] = 0; m44[0][3] = 0;
	m44[1][0] = -s; m44[1][1] = c; m44[1][2] = 0; m44[1][3] = 0;
	m44[2][0] = 0; m44[2][1] = 0; m44[2][2] = 1; m44[2][3] = 0;
	m44[3][0] = 0; m44[3][1] = 0; m44[3][2] = 0; m44[3][3] = 1;
}

/*
template <class T>
void TMatrix<T>::SetRotationMatrixXYZ(const T yaw, const T pitch, const T roll)
{
const T sy = std::sin(yaw);
const T cy = std::cos(yaw);
const T sp = std::sin(pitch);
const T cp = std::cos(pitch);
const T sr = std::sin(roll);
const T cr = std::cos(roll);
const T spsy = sp * sy;
const T spcy = sp * cy;

m44[0][0] = cr * cp;
m44[0][1] = sr * cp;
m44[0][2] = -sp;
m44[0][3] = 0;
m44[1][0] = cr * spsy - sr * cy;
m44[1][1] = sr * spsy + cr * cy;
m44[1][2] = cp * sy;
m44[1][3] = 0;
m44[2][0] = cr * spcy + sr * sy;
m44[2][1] = sr * spcy - cr * sy;
m44[2][2] = cp * cy;
m44[2][3] = 0;
m44[3][0] = 0;
m44[3][1] = 0;
m44[3][2] = 0;
m44[3][3] = 1;
}
*/

template <class T>
void TMatrix<T>::SetRotationMatrixEuler(const TVector3<T>& v) {
	*this = TMatrix<T>::RotationMatrixX(v.x) * TMatrix<T>::RotationMatrixY(v.y) * TMatrix<T>::RotationMatrixZ(v.z);
}


template <class T>
void TMatrix<T>::SetRotationMatrixAxisAngle(const TVector3<T>& axis, const T angle) {
	T length2 = axis.SquareLength();
	if (!length2) {
		Identity();
		return;
	}

	TVector3<T> n = axis / std::sqrt(length2);
	const T s = std::sin(angle);
	const T c = std::cos(angle);
	const T k = 1 - c;
	const T xx = n.x * n.x * k + c;
	const T yy = n.y * n.y * k + c;
	const T zz = n.z * n.z * k + c;
	const T xy = n.x * n.y * k;
	const T yz = n.y * n.z * k;
	const T zx = n.z * n.x * k;
	const T xs = n.x * s;
	const T ys = n.y * s;
	const T zs = n.z * s;

	m44[0][0] = xx;
	m44[0][1] = xy + zs;
	m44[0][2] = zx - ys;
	m44[0][3] = 0;
	m44[1][0] = xy - zs;
	m44[1][1] = yy;
	m44[1][2] = yz + xs;
	m44[1][3] = 0;
	m44[2][0] = zx + ys;
	m44[2][1] = yz - xs;
	m44[2][2] = zz;
	m44[2][3] = 0;
	m44[3][0] = 0;
	m44[3][1] = 0;
	m44[3][2] = 0;
	m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::Scale(const T sx, const T sy, const T sz) {
	for (int i = 0; i < 4; i++) {
		m44[i][0] *= sx;
		m44[i][1] *= sy;
		m44[i][2] *= sz;
	}
}


template <class T>
void TMatrix<T>::RotateX(const T angle) {
	T s = std::sin(angle);
	T c = std::cos(angle);

	for (int i = 0; i < 4; i++) {
		const T y = m44[i][1];
		const T z = m44[i][2];
		m44[i][1] = y * c - z * s;
		m44[i][2] = z * c + y * s;
	}
}


template <class T>
void TMatrix<T>::RotateY(const T angle) {
	T s = std::sin(angle);
	T c = std::cos(angle);

	for (int i = 0; i < 4; i++) {
		const T x = m44[i][0];
		const T z = m44[i][2];
		m44[i][0] = x * c + z * s;
		m44[i][2] = z * c - x * s;
	}
}


template <class T>
void TMatrix<T>::RotateZ(const T angle) {
	T s = std::sin(angle);
	T c = std::cos(angle);

	for (int i = 0; i < 4; i++) {
		const T x = m44[i][0];
		const T y = m44[i][1];
		m44[i][0] = x * c - y * s;
		m44[i][1] = x * s + y * c;
	}
}

/*
template <class T>
void TMatrix<T>::RotateXYZ(const T yaw, const T pitch, const T roll)
{
const T sy = std::sin(yaw);
const T cy = std::cos(yaw);
const T sp = std::sin(pitch);
const T cp = std::cos(pitch);
const T sr = std::sin(roll);
const T cr = std::cos(roll);
const T spsy = sp * sy;
const T spcy = sp * cy;
const T m00 = cr * cp;
const T m01 = sr * cp;
const T m02 = -sp;
const T m10 = cr * spsy - sr * cy;
const T m11 = sr * spsy + cr * cy;
const T m12 = cp * sy;
const T m20 = cr * spcy + sr * sy;
const T m21 = sr * spcy - cr * sy;
const T m22 = cp * cy;

for ( int i=0; i<4; i++ )
{
const T x = m44[i][0];
const T y = m44[i][1];
const T z = m44[i][2];
m44[i][0] = x * m00 + y * m10 + z * m20;
m44[i][1] = x * m01 + y * m11 + z * m21;
m44[i][2] = x * m02 + y * m12 + z * m22;
}
}
*/

template <class T>
void TMatrix<T>::MultMatrix(const TMatrix<T>& right) {
	T v[4];

	for (int i = 0; i < 4; i++) {
		v[0] = m44[i][0];
		v[1] = m44[i][1];
		v[2] = m44[i][2];
		v[3] = m44[i][3];
		m44[i][0] = v[0] * right.m44[0][0] + v[1] * right.m44[1][0] + v[2] * right.m44[2][0] + v[3] * right.m44[3][0];
		m44[i][1] = v[0] * right.m44[0][1] + v[1] * right.m44[1][1] + v[2] * right.m44[2][1] + v[3] * right.m44[3][1];
		m44[i][2] = v[0] * right.m44[0][2] + v[1] * right.m44[1][2] + v[2] * right.m44[2][2] + v[3] * right.m44[3][2];
		m44[i][3] = v[0] * right.m44[0][3] + v[1] * right.m44[1][3] + v[2] * right.m44[2][3] + v[3] * right.m44[3][3];
	}
}


template <class T>
void TMatrix<T>::MultMatrix3x4(const TMatrix<T>& right) {
	//const mat44& m = right.m44;
	T v[3];

	for (int i = 0; i < 4; i++) {
		v[0] = m44[i][0];
		v[1] = m44[i][1];
		v[2] = m44[i][2];
		m44[i][0] = v[0] * right.m44[0][0] + v[1] * right.m44[1][0] + v[2] * right.m44[2][0];
		m44[i][1] = v[0] * right.m44[0][1] + v[1] * right.m44[1][1] + v[2] * right.m44[2][1];
		m44[i][2] = v[0] * right.m44[0][2] + v[1] * right.m44[1][2] + v[2] * right.m44[2][2];
	}

	m44[3][0] += right.m44[3][0];
	m44[3][1] += right.m44[3][1];
	m44[3][2] += right.m44[3][2];
}


template <class T>
void TMatrix<T>::MultMatrix3x3(const TMatrix<T>& right) {
	T v[3];

	for (int i = 0; i < 4; i++) {
		v[0] = m44[i][0];
		v[1] = m44[i][1];
		v[2] = m44[i][2];
		m44[i][0] = v[0] * right.m44[0][0] + v[1] * right.m44[1][0] + v[2] * right.m44[2][0];
		m44[i][1] = v[0] * right.m44[0][1] + v[1] * right.m44[1][1] + v[2] * right.m44[2][1];
		m44[i][2] = v[0] * right.m44[0][2] + v[1] * right.m44[1][2] + v[2] * right.m44[2][2];
	}
}


template <class T>
void TMatrix<T>::MultInverseMatrix3x3(const TMatrix<T>& right) {
	const T m00 = m44[0][0];
	const T m01 = m44[0][1];
	const T m02 = m44[0][2];
	const T m10 = m44[1][0];
	const T m11 = m44[1][1];
	const T m12 = m44[1][2];
	const T m20 = m44[2][0];
	const T m21 = m44[2][1];
	const T m22 = m44[2][2];

	for (int i = 0; i < 4; i++) {
		const T* v = right.m16 + i * 4;
		m44[i][0] = v[0] * m00 + v[1] * m10 + v[2] * m20;
		m44[i][1] = v[0] * m01 + v[1] * m11 + v[2] * m21;
		m44[i][2] = v[0] * m02 + v[1] * m12 + v[2] * m22;
	}
}


template <class T>
void TMatrix<T>::Transpose() {
	TMatrix<T> v;

	v.m44[0][0] = m44[0][0];
	v.m44[0][1] = m44[1][0];
	v.m44[0][2] = m44[2][0];
	v.m44[0][3] = m44[3][0];
	v.m44[1][0] = m44[0][1];
	v.m44[1][1] = m44[1][1];
	v.m44[1][2] = m44[2][1];
	v.m44[1][3] = m44[3][1];
	v.m44[2][0] = m44[0][2];
	v.m44[2][1] = m44[1][2];
	v.m44[2][2] = m44[2][2];
	v.m44[2][3] = m44[3][2];
	v.m44[3][0] = m44[0][3];
	v.m44[3][1] = m44[1][3];
	v.m44[3][2] = m44[2][3];
	v.m44[3][3] = m44[3][3];

	*this = v;
}


template <class T>
void TMatrix<T>::TransposeRot() {
	TMatrix<T> v(*this);

	v.m44[0][0] = m44[0][0];
	v.m44[0][1] = m44[1][0];
	v.m44[0][2] = m44[2][0];
	v.m44[1][0] = m44[0][1];
	v.m44[1][1] = m44[1][1];
	v.m44[1][2] = m44[2][1];
	v.m44[2][0] = m44[0][2];
	v.m44[2][1] = m44[1][2];
	v.m44[2][2] = m44[2][2];

	*this = v;
}


template <class T>
void TMatrix<T>::TransposeTranslation() {
	TVector3<T> temp;

	temp.x = m44[3][0];
	temp.y = m44[3][1];
	temp.z = m44[3][2];

	m44[3][0] = m44[0][3];
	m44[3][1] = m44[1][3];
	m44[3][2] = m44[2][3];

	m44[0][3] = temp.x;
	m44[1][3] = temp.y;
	m44[2][3] = temp.z;
}


template <class T>
void TMatrix<T>::Adjoint() {
	TMatrix<T> v;

	v.m44[0][0] = m44[1][1] * m44[2][2] - m44[1][2] * m44[2][1];
	v.m44[0][1] = m44[2][1] * m44[0][2] - m44[2][2] * m44[0][1];
	v.m44[0][2] = m44[0][1] * m44[1][2] - m44[0][2] * m44[1][1];
	v.m44[0][3] = m44[0][3];
	v.m44[1][0] = m44[1][2] * m44[2][0] - m44[1][0] * m44[2][2];
	v.m44[1][1] = m44[2][2] * m44[0][0] - m44[2][0] * m44[0][2];
	v.m44[1][2] = m44[0][2] * m44[1][0] - m44[0][0] * m44[1][2];
	v.m44[1][3] = m44[1][3];
	v.m44[2][0] = m44[1][0] * m44[2][1] - m44[1][1] * m44[2][0];
	v.m44[2][1] = m44[2][0] * m44[0][1] - m44[2][1] * m44[0][0];
	v.m44[2][2] = m44[0][0] * m44[1][1] - m44[0][1] * m44[1][0];
	v.m44[2][3] = m44[2][3];
	v.m44[3][0] = -(m44[0][0] * m44[3][0] + m44[1][0] * m44[3][1] + m44[2][0] * m44[3][2]);
	v.m44[3][1] = -(m44[0][1] * m44[3][0] + m44[1][1] * m44[3][1] + m44[2][1] * m44[3][2]);
	v.m44[3][2] = -(m44[0][2] * m44[3][0] + m44[1][2] * m44[3][1] + m44[2][2] * m44[3][2]);
	v.m44[3][3] = m44[3][3];

	*this = v;
}


template <class T>
TVector3<T>	TMatrix<T>::InverseRot(const TVector3<T> &v) {
	TMatrix<T> m(*this);
	m.Inverse();
	m.SetTranslation(0, 0, 0);
	return v * m;
}


template <class T>
void TMatrix<T>::Inverse() {
	T s = 1.0 / CalcDeterminant();
	TMatrix<T> v;

	v.m44[0][0] = (m44[1][1] * m44[2][2] - m44[1][2] * m44[2][1]) * s;
	v.m44[0][1] = (m44[2][1] * m44[0][2] - m44[2][2] * m44[0][1]) * s;
	v.m44[0][2] = (m44[0][1] * m44[1][2] - m44[0][2] * m44[1][1]) * s;
	v.m44[0][3] = m44[0][3];
	v.m44[1][0] = (m44[1][2] * m44[2][0] - m44[1][0] * m44[2][2]) * s;
	v.m44[1][1] = (m44[2][2] * m44[0][0] - m44[2][0] * m44[0][2]) * s;
	v.m44[1][2] = (m44[0][2] * m44[1][0] - m44[0][0] * m44[1][2]) * s;
	v.m44[1][3] = m44[1][3];
	v.m44[2][0] = (m44[1][0] * m44[2][1] - m44[1][1] * m44[2][0]) * s;
	v.m44[2][1] = (m44[2][0] * m44[0][1] - m44[2][1] * m44[0][0]) * s;
	v.m44[2][2] = (m44[0][0] * m44[1][1] - m44[0][1] * m44[1][0]) * s;
	v.m44[2][3] = m44[2][3];
	v.m44[3][0] = -(v.m44[0][0] * m44[3][0] + v.m44[1][0] * m44[3][1] + v.m44[2][0] * m44[3][2]);
	v.m44[3][1] = -(v.m44[0][1] * m44[3][0] + v.m44[1][1] * m44[3][1] + v.m44[2][1] * m44[3][2]);
	v.m44[3][2] = -(v.m44[0][2] * m44[3][0] + v.m44[1][2] * m44[3][1] + v.m44[2][2] * m44[3][2]);
	v.m44[3][3] = m44[3][3];

	*this = v;
}


template <class T>
void TMatrix<T>::OrthoNormalize() {
	TVector3<T> x = GetRight();
	TVector3<T> y = GetUp();
	TVector3<T> z = GetForward();

	x.Normalize();
	y -= x * x.Dot(y);
	y.Normalize();
	z = x.Cross(y);

	SetRight(x);
	SetUp(y);
	SetForward(z);
}


template <class T>
void TMatrix<T>::Mirror(const TMatrix<T>& transform, const TPlaneEq<T>& plane) {
	// components
	TVector3<T> x = transform.GetRight();
	TVector3<T> y = transform.GetUp();
	TVector3<T> z = transform.GetForward();
	TVector3<T> t = transform.GetTranslation();
	TVector3<T> n = plane.GetNormal();
	TVector3<T> n2 = n * -2;
	T			d = plane.GetDist();

	// mirror translation
	TVector3<T> mt = t + n2 * (t.Dot(n) - d);

	// mirror x rotation
	x += t;
	x += n2 * (x.Dot(n) - d);
	x -= mt;

	// mirror y rotation
	y += t;
	y += n2 * (y.Dot(n) - d);
	y -= mt;

	// mirror z rotation
	z += t;
	z += n2 * (z.Dot(n) - d);
	z -= mt;

	// write result
	SetRight(x);
	SetUp(y);
	SetForward(z);
	SetTranslation(mt);

	m44[0][3] = 0;
	m44[1][3] = 0;
	m44[2][3] = 0;
	m44[3][3] = 1;
}


template <class T>
void TMatrix<T>::LookAt(const TVector3<T>& view, const TVector3<T>& target, const TVector3<T>& up) {
	TVector3<T> z = (target - view).Normalize();
	TVector3<T> x = (up.Cross(z)).Normalize();
	TVector3<T> y = z.Cross(x);

	m44[0][0] = x.x;
	m44[1][0] = x.y;
	m44[2][0] = x.z;
	m44[3][0] = -x.Dot(view);
	m44[0][1] = y.x;
	m44[1][1] = y.y;
	m44[2][1] = y.z;
	m44[3][1] = -y.Dot(view);
	m44[0][2] = z.x;
	m44[1][2] = z.y;
	m44[2][2] = z.z;
	m44[3][2] = -z.Dot(view);
	m44[0][3] = 0;
	m44[1][3] = 0;
	m44[2][3] = 0;
	m44[3][3] = 1;
}



// ortho projection TMatrix<T>
template <class T>
void TMatrix<T>::Ortho(const T left, const T right, const T top, const T bottom, const T znear, const T zfar) {
	m44[0][0] = 2.0 / (right - left);
	m44[1][0] = 0.0;
	m44[2][0] = 0.0;
	m44[3][0] = (right + left) / (right - left);
	m44[0][1] = 0.0;
	m44[1][1] = 2.0 / (top - bottom);
	m44[2][1] = 0.0;
	m44[3][0] = (top + bottom) / (top - bottom);
	m44[0][2] = 0.0;
	m44[1][2] = 0.0;
	m44[2][2] = 2.0 / (zfar - znear);
	m44[3][2] = (zfar + znear) / (znear - zfar);
	m44[0][3] = 0.0;
	m44[1][3] = 0.0;
	m44[2][3] = 0.0;
	m44[3][3] = 1.0;
}


// frustum TMatrix<T>
template <class T>
void TMatrix<T>::Frustum(const T left, const T right, const T top, const T bottom, const T znear, const T zfar) {
	m44[0][0] = 2.0*znear / (right - left);
	m44[1][0] = 0.0;
	m44[2][0] = (right + left) / (right - left);
	m44[3][0] = 0.0;
	m44[0][1] = 0.0;
	m44[1][1] = 2.0*znear / (top - bottom);
	m44[2][1] = (top + bottom) / (top - bottom);
	m44[3][1] = 0.0;
	m44[0][2] = 0.0;
	m44[1][2] = 0.0;
	m44[2][2] = (zfar + znear) / (zfar - znear);
	m44[3][2] = 2.0*zfar*znear / (zfar - znear);
	m44[0][3] = 0.0;
	m44[1][3] = 0.0;
	m44[2][3] = -1.0;
	m44[3][3] = 0.0;
}


// setup projection TMatrix<T>
template <class T>
void TMatrix<T>::Projection(const T fov, const T aspect, const T zNear, const T zFar) {
	const T f = fov * 0.5;
	const T w = aspect * (std::cos(f) / std::sin(f));
	const T h = std::cos(f) / std::sin(f);
	const T Q = zFar / (zFar - zNear);

	memset(m44, 0, 16 * sizeof(T));
	m44[0][0] = w;
	m44[1][1] = h;
	m44[2][2] = Q;
	m44[2][3] = 1.0;
	m44[3][2] = -Q * zNear;
}


template <class T>
inline void TMatrix<T>::SetRight(const T xx, const T xy, const T xz) {
	m44[0][0] = xx;
	m44[0][1] = xy;
	m44[0][2] = xz;
}


template <class T>
inline void TMatrix<T>::SetUp(const T yx, const T yy, const T yz) {
	m44[1][0] = yx;
	m44[1][1] = yy;
	m44[1][2] = yz;
}


template <class T>
inline void TMatrix<T>::SetForward(const T zx, const T zy, const T zz) {
	m44[2][0] = zx;
	m44[2][1] = zy;
	m44[2][2] = zz;
}


template <class T>
inline void TMatrix<T>::SetTranslation(const T tx, const T ty, const T tz) {
	m44[3][0] = tx;
	m44[3][1] = ty;
	m44[3][2] = tz;
}


template <class T>
inline void TMatrix<T>::SetRight(const TVector3<T>& x) {
	m44[0][0] = x.x;
	m44[0][1] = x.y;
	m44[0][2] = x.z;
}


template <class T>
inline void TMatrix<T>::SetUp(const TVector3<T>& y) {
	m44[1][0] = y.x;
	m44[1][1] = y.y;
	m44[1][2] = y.z;
}


template <class T>
inline void TMatrix<T>::SetForward(const TVector3<T>& z) {
	m44[2][0] = z.x;
	m44[2][1] = z.y;
	m44[2][2] = z.z;
}


template <class T>
inline void TMatrix<T>::SetTranslation(const TVector3<T>& t) {
	m44[3][0] = t.x;
	m44[3][1] = t.y;
	m44[3][2] = t.z;
}


template <class T>
inline TVector3<T>& TMatrix<T>::GetRight() {
	return *reinterpret_cast<TVector3<T>*>(m16 + 0);
}


template <class T>
inline TVector3<T>& TMatrix<T>::GetUp() {
	return *reinterpret_cast<TVector3<T>*>(m16 + 4);
}


template <class T>
inline TVector3<T>& TMatrix<T>::GetForward() {
	return *reinterpret_cast<TVector3<T>*>(m16 + 8);
}


template <class T>
inline TVector3<T>& TMatrix<T>::GetTranslation() {
	return *reinterpret_cast<TVector3<T>*>(m16 + 12);
}


template <class T>
inline const TVector3<T>& TMatrix<T>::GetRight() const {
	return *reinterpret_cast<const TVector3<T>*>(m16 + 0);
}


template <class T>
inline const TVector3<T>& TMatrix<T>::GetUp() const {
	return *reinterpret_cast<const TVector3<T>*>(m16 + 4);
}


template <class T>
inline const TVector3<T>& TMatrix<T>::GetForward() const {
	return *reinterpret_cast<const TVector3<T>*>(m16 + 8);
}


template <class T>
inline const TVector3<T>& TMatrix<T>::GetTranslation() const {
	return *reinterpret_cast<const TVector3<T>*>(m16 + 12);
}


template <class T>
inline	TVector3<T>	TMatrix<T>::Mul3x3(const TVector3<T> &v) {
	return TVector3<T>(
		v.x*m44[0][0] + v.y*m44[1][0] + v.z*m44[2][0],
		v.x*m44[0][1] + v.y*m44[1][1] + v.z*m44[2][1],
		v.x*m44[0][2] + v.y*m44[1][2] + v.z*m44[2][2]);
}


template <class T>
inline void operator *= (TVector3<T>& v, const TMatrix<T>& m) {
	v = TVector3<T>(
		v.x*m.m44[0][0] + v.y*m.m44[1][0] + v.z*m.m44[2][0] + m.m44[3][0],
		v.x*m.m44[0][1] + v.y*m.m44[1][1] + v.z*m.m44[2][1] + m.m44[3][1],
		v.x*m.m44[0][2] + v.y*m.m44[1][2] + v.z*m.m44[2][2] + m.m44[3][2]);
}


template <class T>
inline TVector3<T> operator * (const TVector3<T>& v, const TMatrix<T>& m) {
	return TVector3<T>(
		v.x*m.m44[0][0] + v.y*m.m44[1][0] + v.z*m.m44[2][0] + m.m44[3][0],
		v.x*m.m44[0][1] + v.y*m.m44[1][1] + v.z*m.m44[2][1] + m.m44[3][1],
		v.x*m.m44[0][2] + v.y*m.m44[1][2] + v.z*m.m44[2][2] + m.m44[3][2]);
}


// check if the matrix is symmetric or not
template <class T>
bool TMatrix<T>::IsSymmetric(const float tolerance) const {
	// if no tolerance check is needed
	if (tolerance == 0) {
		if (m44[1][0] != m44[0][1]) return false;
		if (m44[2][0] != m44[0][2]) return false;
		if (m44[2][1] != m44[1][2]) return false;
		if (m44[3][0] != m44[0][3]) return false;
		if (m44[3][1] != m44[1][3]) return false;
		if (m44[3][2] != m44[2][3]) return false;
	} else // tolerance check needed
	{
		if (std::abs(m44[1][0] - m44[0][1]) > tolerance) return false;
		if (std::abs(m44[2][0] - m44[0][2]) > tolerance) return false;
		if (std::abs(m44[2][1] - m44[1][2]) > tolerance) return false;
		if (std::abs(m44[3][0] - m44[0][3]) > tolerance) return false;
		if (std::abs(m44[3][1] - m44[1][3]) > tolerance) return false;
		if (std::abs(m44[3][2] - m44[2][3]) > tolerance) return false;
	}

	// yeah, we have a symmetric matrix here
	return true;
}


// check if this matrix is a diagonal matrix or not.
template <class T>
bool TMatrix<T>::IsDiagonal(const float tolerance) const {
	if (tolerance == 0) {
		// for all entries
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				// if we are on the diagonal
				if (x == y) {
					if (m44[y][x] == 0) return false;	// if this entry on the diagonal is 0, we have no diagonal matrix
				} else // we are not on the diagonal
					if (m44[y][x] != 0) return false;	// if the entry isn't equal to 0, it isn't a diagonal matrix
			}
		}
	} else {
		// for all entries
		for (int y = 0; y < 4; y++) {
			for (int x = 0; x < 4; x++) {
				// if we are on the diagonal
				if (x == y) {
					if (std::abs(m44[y][x]) < tolerance)
						return false;	// if this entry on the diagonal is 0, we have no diagonal matrix
				} else // we are not on the diagonal
				{
					if (std::abs(m44[y][x]) > tolerance)
						return false;	// if the entry isn't equal to 0, it isn't a diagonal matrix
				}
			}
		}
	}

	// yeaaah, we have a diagonal matrix here
	return true;
}


// prints the matrix into the logfile or debug output, using RCore::LOG()
template <class T>
void TMatrix<T>::Print() {
	RCore::LOG("");
	RCore::LOG("[%.8f, %.8f, %.8f, %.8f]", m44[0][0], m44[0][1], m44[0][2], m44[0][3]);
	RCore::LOG("[%.8f, %.8f, %.8f, %.8f]", m44[1][0], m44[1][1], m44[1][2], m44[1][3]);
	RCore::LOG("[%.8f, %.8f, %.8f, %.8f]", m44[2][0], m44[2][1], m44[2][2], m44[2][3]);
	RCore::LOG("[%.8f, %.8f, %.8f, %.8f]", m44[3][0], m44[3][1], m44[3][2], m44[3][3]);
	RCore::LOG("");
}


// check if the matrix is orthogonal or not
template <class T>
bool TMatrix<T>::IsOrthogonal(const float tolerance) const {
	// get the matrix vectors
	TVector3<T> right = GetRight();
	TVector3<T> up = GetUp();
	TVector3<T> forward = GetForward();

	// check if the vectors form an orthonormal set
	if (std::abs(right.Dot(up) > tolerance)) return false;
	if (std::abs(right.Dot(forward) > tolerance)) return false;
	if (std::abs(forward.Dot(up) > tolerance)) return false;

	// the vector set is not orthonormal, so the matrix is not an orthogonal one
	return true;
}


// check if the matrix is an identity matrix or not
template <class T>
bool TMatrix<T>::IsIdentity(const float tolerance) const {
	// for all entries
	for (int y = 0; y < 4; y++) {
		for (int x = 0; x < 4; x++) {
			// if we are on the diagonal
			if (x == y) {
				if (std::abs(1.0 - m44[y][x]) > tolerance)
					return false;	// if this entry on the diagonal not 1, we have no identity matrix
			} else // we are not on the diagonal
			{
				if (std::abs(m44[y][x]) > tolerance)
					return false;	// if the entry isn't equal to 0, it isn't an identity matrix
			}
		}
	}

	// yup, we have an identity matrix here :)
	return true;
}


// calculate the handedness of the matrix
template <class T>
T TMatrix<T>::CalcHandedness() const {
	// get the matrix vectors
	TVector3<T> right = GetRight();
	TVector3<T> up = GetUp();
	TVector3<T> forward = GetForward();

	// calculate the handedness (negative result means left handed, positive means right handed)
	return (right.Cross(up)).Dot(forward);
}


// check if the matrix is right handed or not
template <class T>
inline bool TMatrix<T>::IsRightHanded() const {
	return (CalcHandedness() > 0);
}


// check if the matrix is right handed or not
template <class T>
inline bool TMatrix<T>::IsLeftHanded() const {
	return (CalcHandedness() <= 0);
}


// check if this matrix is a pure rotation matrix or not
template <class T>
inline bool TMatrix<T>::IsPureRotationMatrix(const float tolerance) const {
	return (std::abs(1.0 - CalcDeterminant()) < tolerance);
}


// check if the matrix is reflected (mirrored) or not
template <class T>
inline bool TMatrix<T>::IsReflective(const float tolerance) const {
	T determinant = CalcDeterminant();
	return (determinant < 0);
	//return ((determinant > (-1.0 - tolerance)) && (determinant < (-1.0 + tolerance)));	// if the determinant is near -1, it will reflect
}


// calculate the inverse transpose
template <class T>
inline void TMatrix<T>::InverseTranspose() {
	Inverse();
	Transpose();
}


// return the inverse transposed version of this matrix
template <class T>
inline TMatrix<T> TMatrix<T>::InverseTransposed() const {
	Matrix result(*this);
	result.InverseTranspose();
	return result;
}


// normalize a matrix
template <class T>
void TMatrix<T>::Normalize() {
	// get the current vectors
	TVector3<T> right = GetRight();
	TVector3<T> up = GetUp();
	TVector3<T> forward = GetForward();

	// normalize them
	right.Normalize();
	up.Normalize();
	forward.Normalize();

	// update them again with the normalized versions
	SetRight(right);
	SetUp(up);
	SetForward(forward);
}


// returns a normalized version of this matrix
template <class T>
inline TMatrix<T> TMatrix<T>::Normalized() const {
	Matrix result(*this);
	result.Normalize();
	return result;
}

} // namespace
