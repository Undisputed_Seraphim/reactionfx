#pragma once

#include <type_traits>
#include "Quaternion.h"
#include "Algorithms.h"

namespace RCore {

/**
 * The compressed / packed quaternion class.
 * This represents a unit (normalized) quaternion on a packed way, which is 8 bytes per quaternion instead of 16 bytes
 * when using floating point quaternions. Of course we loose a bit of precision, but it isn't too bad for most things.
 * The class provides methods to convert from and to uncompressed quaternions.
 */
template <typename QuatType, typename StorageType>
class TCompressedQuaternion {
	static_assert(std::is_floating_point<QuatType>::value, "");
	static_assert(std::is_integral<StorageType>::value, "");
public:
	/**
	 * Default constructor.
	 * This sets the quaternion to identity.
	 */
	TCompressedQuaternion();

	/**
	 * Create a compressed quaternion from an uncompressed one.
	 * Please note that the uncompressed quaternion has to be normalized or a unit quaternion!
	 * @param quat The normalized uncompressed quaternion.
	 */
	TCompressedQuaternion(const TQuaternion<QuatType>& quat);

	/**
	 * Update the compressed quaternion from an uncompressed one.
	 * Please note that the uncompressed quaternion has to be normalized or a unit quaternion!
	 * @param quat The normalized uncompressed quaternion.
	 */
	void FromQuaternion(const TQuaternion<QuatType>& quat);

	/**
	 * Uncompress the compressed quaternion into an uncompressed one.
	 * @param output The output uncompressed quaternion to write the result in.
	 */
	void UnCompress(TQuaternion<QuatType>* output) const;

	/**
	 * Convert the compressed quaternion into an uncompressed one.
	 * This method works the same as the UnCompress method, but it returns the result instead of specifying
	 * the output quaternion.
	 * @result The uncompressed version of this compressed quaternion.
	 */
	TQuaternion<QuatType> ToQuaternion() const;

public:
	StorageType mX, mY, mZ, mW;	/*The compressed/packed quaternion components values. */

	// the number of steps within the specified range
	enum { CONVERT_VALUE = ((1 << (sizeof(StorageType) << 3)) >> 1) - 1 };
};

// include the code
#include "CompressedQuaternion.inl"

// declare standard types
typedef TCompressedQuaternion<MReal, signed short>	Compressed16BitQuaternion;
typedef TCompressedQuaternion<MReal, signed char>	Compressed8BitQuaternion;

} // namespace