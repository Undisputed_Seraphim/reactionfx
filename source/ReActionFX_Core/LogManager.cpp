#include <cstdarg>
#include "LogManager.h"
#include "LogFile.h"

namespace RCore {

// constructor
LogFileCallback::LogFileCallback(const std::string & filename) {
	mLog = new LogFile(filename);
}


// destructor
LogFileCallback::~LogFileCallback() {
	if (mLog)
		delete mLog;
}


// log callback function
void LogFileCallback::Log(const std::string & text) {
	mLog->LogMessage(text);
}


// return a pointer to the log file
LogFile* LogFileCallback::GetLogFile() const {
	return mLog;
}




// constructor
LogManager::LogManager() {

}


// destructor
LogManager::~LogManager() {
	// get rid of the callbacks
	ClearLogCallbacks();
}



// creates a log file callback and adds it to the stack
LogFile* LogManager::CreateLogFile(const std::string & filename) {
	// create log file callback instance
	LogFileCallback* callback = new LogFileCallback(filename);

	// add log file callback to the stack
	AddLogCallback(callback);

	// return pointer to the log file of the callback
	return callback->GetLogFile();
}


// add a log file callback instance
void LogManager::AddLogCallback(LogCallback* callback) {
	// add the callback to the stack
	if (callback)
		mLogCallbacks.push_back(callback);
}


// remove a specific log callback from the stack
void LogManager::RemoveLogCallback(const int index) {
	if (mLogCallbacks.size() > index) return;
	// remove the callback from the list
	LogCallback* callback = mLogCallbacks[index];

	// get rid of the callback instance
	if (callback)
		delete callback;

	// remove the callback from the stack
	mLogCallbacks.erase(mLogCallbacks.begin() + index);
}


// remove all log callbacks from the stack
void LogManager::ClearLogCallbacks() {
	// get rid of the callbacks
	for (auto logCallback : mLogCallbacks) {
		if (logCallback)
			delete logCallback;
	}
	mLogCallbacks.clear();
}


// retrieve a pointer to the given log callback
LogCallback* LogManager::GetLogCallback(const int index) {
	return mLogCallbacks[index];
}


// return number of log callbacks in the stack
size_t LogManager::GetNumLogCallbacks() const {
	return mLogCallbacks.size();
}


// the main logging method
void LogManager::LogMessage(const std::string & message, const LogFile::ELogMessageLevel& logMessageLevel, const std::string & source) {
	// iterate through all callbacks
	for (auto logCallback : mLogCallbacks) {
		if (logCallback)
			logCallback->Log(message);
	}
}




// global log function which calls the log manager's log function
void LOG(const std::string & what) {

	LogManager::GetInstance().LogMessage(what, LogFile::NORMAL);
}


// global log function which calls the log manager's log function
void LOG(const char* what, ...) {

	char textBuf[4096];
	va_list args;
	va_start(args, what);

	vsprintf(textBuf, what, args);
	va_end(args);

	LogManager::GetInstance().LogMessage(textBuf, LogFile::NORMAL);
}

} // namespace