#include "AnimImporter.h"
#include "ModelImporter.h"

using namespace ReActionFX;
using namespace RCore;

int main(int argc, char *argv[]) {
	if (argc <= 1) {
		printf("No file found.");
		return 0;
	}

	std::string filename = argv[1];
	printf(filename.c_str());

	if (IMPORTER.IsActor(argv[1])) {
		Actor *actor = IMPORTER.LoadActor(filename, filename);
		ModelImporter mimp(actor);
	} else if (IMPORTER.IsMotion(argv[1])) {
		Motion *motion = IMPORTER.LoadMotion(filename, filename);
		AnimImporter aimp(motion);
	} else {
		printf("No LMA file type detected (neither motion nor actor).");
	}

	return 0;
}