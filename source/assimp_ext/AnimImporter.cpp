#include "AnimImporter.h"

using namespace ReActionFX;
using namespace Assimp;

AnimImporter::AnimImporter(ReActionFX::Motion * motion) : motion(motion) {
	anim.mName = motion->GetName();
	anim.mDuration = motion->GetMaxTime();
	//anim.mTicksPerSecond = 

	parse();
}

void AnimImporter::set_skeleton(ReActionFX::Node * skeleton_root) {}

void AnimImporter::parse() {
	if (motion == nullptr)
		return;

	float motionTime = motion->GetMaxTime();
	float motionType = motion->GetType();

	//Cast
	SkeletalMotion *skeleton = (SkeletalMotion*)motion;

	for (int i = 0; i < skeleton->GetNumParts(); i++) {
		MotionPart* part = skeleton->GetPart(i);

		int numKeys = 0;

		std::vector<aiVectorKey> aiPosKeys;
		auto positionkt = part->GetPosTrack();
		numKeys = positionkt.GetNumKeys();
		for (int j = 0; j < numKeys; j++) {
			auto positionkf = positionkt.GetKey(j);
			float kf_time = positionkf->GetTime();
			auto kf_vector = positionkf->GetValue();
			aiVector3D v(kf_vector.x, kf_vector.y, kf_vector.z);
			aiPosKeys.emplace_back(kf_time, v);
		}

		std::vector<aiQuatKey> aiRotKeys;
		auto rotationkt = part->GetRotTrack();
		numKeys = rotationkt.GetNumKeys();
		for (int j = 0; j < numKeys; j++) {
			auto rotationkf = rotationkt.GetKey(j);
			float kf_time = rotationkf->GetTime();
			auto kf_quat = rotationkf->GetValue();
			aiQuaternion q(kf_quat.w, kf_quat.x, kf_quat.y, kf_quat.z);
			aiRotKeys.emplace_back(kf_time, q);
		}

		std::vector<aiVectorKey> aiScaKeys;
		auto scalingkt = part->GetScaleTrack();
		numKeys = scalingkt.GetNumKeys();
		for (int j = 0; j < numKeys; j++) {
			auto scalekf = scalingkt.GetKey(j);
			float kf_time = scalekf->GetTime();
			auto kf_vector = scalekf->GetValue();
			aiVector3D s(kf_vector.x, kf_vector.y, kf_vector.z);
			aiScaKeys.emplace_back(kf_time, s);
		}

		aiNodeAnim node = aiNodeAnim();
		node.mNodeName = part->GetName();
		node.mNumPositionKeys = positionkt.GetNumKeys();
		node.mNumRotationKeys = rotationkt.GetNumKeys();
		node.mNumScalingKeys = scalingkt.GetNumKeys();
		node.mPositionKeys = aiPosKeys.data();
		node.mRotationKeys = aiRotKeys.data();
		node.mScalingKeys = aiScaKeys.data();
		node.mPreState = aiAnimBehaviour::aiAnimBehaviour_REPEAT;
		node.mPostState = aiAnimBehaviour::aiAnimBehaviour_REPEAT;

		nodes.push_back(node);
	}
}