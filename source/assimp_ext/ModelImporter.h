#pragma once

#include <vector>
#include <string>
#include <assimp/material.h>
#include <assimp/mesh.h>
#include <assimp/texture.h>
#include <assimp/scene.h>
#include "ReActionFX.h"

class ModelImporter {
public:
	ModelImporter(ReActionFX::Actor* actor) : actor(actor) {
		cumulative_mesh_count = 0;
		parse();
	}
	~ModelImporter() { delete actor; }

	const aiScene get_scene() const { return scene; }

private:
	void parse();
	aiNode* do_bones(ReActionFX::Node* node, aiNode* parent = nullptr);
	void do_materials(std::shared_ptr<ReActionFX::Material> mat);
	void do_mesh(std::shared_ptr<ReActionFX::Mesh> mesh);
	void do_submesh(ReActionFX::SubMesh* submesh, unsigned int cumulative_processed_vertices);

	aiScene scene;
	ReActionFX::Actor* actor;
	std::vector<aiMaterial> materials;
	std::vector<aiMesh> meshes;
	std::vector<float> Us, Vs;

	unsigned int cumulative_mesh_count;
};