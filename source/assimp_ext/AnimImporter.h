#pragma once

#include <vector>
#include <string>
#include <assimp/anim.h>
#include <assimp/scene.h>
#include "ReActionFX.h"

class AnimImporter {
public:
	AnimImporter(ReActionFX::Motion* motion);
	~AnimImporter() { delete motion; }

	// Sets the parsed motion data against a skeleton tree.
	void set_skeleton(ReActionFX::Node* skeleton_root);

private:
	void parse();

	aiAnimation anim;
	ReActionFX::Motion* motion;		// Raw motion data from ReActionFX.
	std::vector<aiNodeAnim> nodes;	// The list of animation nodes, without skeletal data.
};