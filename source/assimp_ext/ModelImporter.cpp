#include "ModelImporter.h"

using namespace ReActionFX;
using namespace Assimp;

void ModelImporter::parse() {
	if (actor == nullptr)
		return;

	for (size_t i = 0; i < actor->GetNumMaterials(0); i++) {
		do_materials(actor->GetMaterial(0, i));
	}

	// Skeleton Data
	scene.mRootNode = do_bones(actor);
	scene.mNumMeshes = cumulative_mesh_count;

	cumulative_mesh_count = 0;
}


aiNode* ModelImporter::do_bones(Node* node, aiNode* parent) {
	if (node == nullptr)
		return nullptr;

	do_mesh(node->GetMesh(0));

	aiNode* ain = new aiNode(node->GetName());
	ain->mParent = parent;
	ain->mNumMeshes = node->GetNumMeshes();

	std::vector<unsigned int> mesh_index;
	if (node->HasMesh()) {
		for (int i = 0; i < node->GetNumMeshes(); i++) {
			do_mesh(node->GetMesh(0));
			mesh_index.push_back(cumulative_mesh_count);
			cumulative_mesh_count++;
		}
	}
	ain->mNumMeshes = node->GetNumMeshes();
	ain->mMeshes = mesh_index.data();

	auto pos = node->GetLocalPos();
	auto rot = node->GetLocalRot();
	auto sca = node->GetLocalScale();
	aiVector3D aipos = aiVector3D(pos.x, pos.y, pos.z);
	aiQuaternion airot = aiQuaternion(rot.w, rot.x, rot.y, rot.z);
	aiVector3D aisca = aiVector3D(sca.x, sca.y, sca.z);
	ain->mTransformation = aiMatrix4x4(aisca, airot, aipos);
	ain->mNumChildren = node->GetNumChilds();

	std::vector<aiNode*> children;
	for (size_t i = 0; i < node->GetNumChilds(); i++) {
		children.push_back(do_bones(node->GetChild(i), ain));
	}

	return ain;
}


void ModelImporter::do_materials(std::shared_ptr<ReActionFX::Material> mat) {
	if (mat.get() == nullptr)
		return;

	std::string mat_type = mat->GetTypeString();
	if (mat_type.compare("StandardMaterial") != 0)
		return;

	auto stdmat = std::dynamic_pointer_cast<StandardMaterial>(mat);
	aiMaterial aimat;

	aiString* name = new aiString(stdmat->GetName());
	aimat.AddProperty(name, AI_MATKEY_NAME);

	auto ambient = stdmat->GetAmbient();
	aiColor3D* amb = new aiColor3D(ambient.r, ambient.g, ambient.b);
	aimat.AddProperty(amb, 1U, AI_MATKEY_COLOR_AMBIENT);

	auto diffuse = stdmat->GetDiffuse();
	aiColor3D* dif = new aiColor3D(diffuse.r, diffuse.g, diffuse.b);
	aimat.AddProperty(dif, 1U, AI_MATKEY_COLOR_DIFFUSE);

	auto emissive = stdmat->GetEmissive();
	aiColor3D* emi = new aiColor3D(emissive.r, emissive.g, emissive.b);
	aimat.AddProperty(emi, 1U, AI_MATKEY_COLOR_EMISSIVE);

	auto specular = stdmat->GetSpecular();
	aiColor3D* spe = new aiColor3D(specular.r, specular.b, specular.b);
	aimat.AddProperty(spe, 1U, AI_MATKEY_COLOR_SPECULAR);

	float opq[1] = {stdmat->GetOpacity()};
	aimat.AddProperty(opq, 1U, AI_MATKEY_OPACITY);

	float ref[1] = {stdmat->GetShine()};
	aimat.AddProperty(ref, 1U, AI_MATKEY_REFLECTIVITY);

	for (size_t i = 0; i < stdmat->GetNumLayers(); i++) {
		auto lyr = stdmat->GetLayer(i);
		aiString* texname = new aiString(lyr->GetFilename());
		float amt[1] = {lyr->GetAmount()};

		switch (lyr->GetType()) {
		case StandardMaterialLayer::LAYERTYPE_DIFFUSE:
		{
			aimat.AddProperty(texname, AI_MATKEY_TEXTURE_DIFFUSE(i));
			aimat.AddProperty(amt, 1U, AI_MATKEY_TEXBLEND_DIFFUSE(i));
			break;
		}
		case StandardMaterialLayer::LAYERTYPE_OPACITY:
		{
			aimat.AddProperty(texname, AI_MATKEY_TEXTURE_OPACITY(i));
			aimat.AddProperty(amt, 1U, AI_MATKEY_TEXBLEND_OPACITY(i));
			break;
		}
		case StandardMaterialLayer::LAYERTYPE_REFLECT:
		{
			aimat.AddProperty(texname, AI_MATKEY_TEXTURE_REFLECTION(i));
			aimat.AddProperty(amt, 1U, AI_MATKEY_TEXBLEND_REFLECTION(i));
			break;
		}
		default:
		{
			aimat.AddProperty(texname, AI_MATKEY_TEXTURE_AMBIENT(i));
			aimat.AddProperty(amt, 1U, AI_MATKEY_TEXBLEND_AMBIENT(i));
		}
		}
	}
}


void ModelImporter::do_mesh(std::shared_ptr<ReActionFX::Mesh> mesh) {

	// Fetch non-shared vertex attributes.
	for (int i = 0; i < mesh->GetNumVertexAttributeLayers(); i++) {
		auto* val = mesh->GetVertexAttributeLayer(i);
		std::string valtype = val->GetTypeString();

		if (valtype.compare("UVVertexAttributeLayer") == 0) {
			auto uvlayer = mesh->GetUVLayer();
			for (int j = 0; j < mesh->GetNumVertices(); j++) {
				RCore::Vector2 uv = uvlayer->GetUVs()[j];
				Us.push_back(uv.x);
				Vs.push_back(abs(1.0 - uv.y));
			}
		}
	}

	// Do submesh
	unsigned int cumulative_processed_vertices = 0U;
	for (size_t i = 0; i < mesh->GetNumSubMeshes(); i++) {
		do_submesh(mesh->GetSubMesh(i), cumulative_processed_vertices);
		cumulative_processed_vertices += mesh->GetSubMesh(i)->GetNumVertices();
	}

	// At the end of this mesh, clear UV values.
	Us.clear();
	Vs.clear();
}


void ModelImporter::do_submesh(ReActionFX::SubMesh * submesh, unsigned int cumulative_processed_vertices) {
	if (submesh == nullptr)
		return;

	std::vector<aiVector3D> smVertexArray;
	for (size_t i = 0; i < submesh->GetNumVertices(); i++) {
		auto smv = submesh->GetPositions()[i];
		smVertexArray.emplace_back(smv.x, smv.y, -smv.z);
	}

	std::vector<aiFace> smFaces;
	for (int i = 0; i < submesh->GetNumIndices(); i += 3) {
		unsigned int indices[3] = {
			(submesh->GetIndices()[i + 2] - cumulative_processed_vertices),
			(submesh->GetIndices()[i + 1] - cumulative_processed_vertices),
			(submesh->GetIndices()[i + 0] - cumulative_processed_vertices)
		};

		aiFace face = aiFace();
		face.mNumIndices = 3U;
		face.mIndices = indices;
		smFaces.push_back(face);
	}

	std::vector<aiVector3D> smNormals;
	for (int i = 0; i < submesh->GetNumVertices(); i += 3) {
		auto n1 = submesh->GetNormals()[i + 2],
			n2 = submesh->GetNormals()[i + 1],
			n3 = submesh->GetNormals()[i + 0];
		smNormals.emplace_back(n1.x, n1.y, -n1.z);
		smNormals.emplace_back(n2.x, n2.y, -n2.z);
		smNormals.emplace_back(n3.x, n3.y, -n3.z);
	}

	std::vector<aiVector3D> smUs, smVs;
	for (int i = 0; i < submesh->GetNumVertices(); i++) {

	}

	aiMesh sm;
	sm.mNumFaces = smFaces.size();
	sm.mFaces = smFaces.data();
	sm.mNumVertices = submesh->GetNumVertices();
	sm.mVertices = smVertexArray.data();
	sm.mNormals = smNormals.data();
	sm.mNumUVComponents[0] = submesh->GetNumVertices();
	sm.mNumUVComponents[1] = submesh->GetNumVertices();

	meshes.push_back(sm);
}
