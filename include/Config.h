#pragma once

/// Compiler types.
#define COMPILER_MSVC			0x00
#define COMPILER_GNUC			0x03


/// Finds the compiler type and version.
#if defined _MSC_VER
	#define COMPILER COMPILER_MSVC
#elif defined __GNUC__
	#define COMPILER COMPILER_GNUC
#endif


/// Disable conversion compile warning
#if COMPILER == COMPILER_MSVC
	#pragma warning (disable : 4244)	// Conversion from 'double' to 'float', possible loss of data
	#pragma warning (disable : 4800)	// 'int' : forcing value to bool 'true' or 'false' (performance warning)
	#pragma warning (disable : 4245)	// Conversion from 'const int' to 'unsigned long', signed/unsigned mismatch
	#pragma warning (disable : 4267)	// Conversion from 'size_t' to 'int', possible loss of data
//	#pragma warning (disable : 4018)	// Signed/unsigned mismatch
#endif


// Use doubles everywhere instead of floats
//#define HIGH_PRECISION

#ifdef HIGH_PRECISION
	typedef double MReal;
#else
	typedef float MReal;
#endif


#if COMPILER == COMPILER_MSVC
	/// Import function from DLL.
	#define DLL_IMPORT	__declspec(dllimport)
	/// Export function to DLL.
	#define DLL_EXPORT  __declspec(dllexport)
	/// Functions with variable arguments.
	#define VARARGS __cdecl
	/// Standard C function.
	#undef CDECL
	#define CDECL __cdecl
	/// Standard calling convention.
	#define STDCALL __stdcall
	/// Force code to be inline.
	//#define FORCEINLINE __forceinline
	//#define f_inline __forceinline
#elif COMPILER == COMPILER_GNUC
	/// Force code to be inline.
	//#define f_inline inline
#endif


// if C++ exception handling is disabled, force guarding to be off.
#if COMPILER == COMPILER_MSVC
	#ifndef _CPPUNWIND
		#error "Bad VCC option: C++ exception handling must be enabled"
	#endif
#endif //WIN32