#pragma once

// Nodes
#include "Actor.h"
#include "Node.h"
#include "NodeTransform.h"
#include "NodeIDGenerator.h"
#include "NodeAttribute.h"
#include "NodeLimitAttribute.h"
#include "NodeDOF.h"
#include "NodePhysicsAttribute.h"

// Geometry
#include "SimpleMesh.h"
#include "Mesh.h"
#include "SubMesh.h"
#include "HwShaderBuffer.h"
#include "Material.h"
#include "StandardMaterial.h"
#include "FXMaterial.h"

#include "VertexAttributeLayer.h"
#include "UVVertexAttributeLayer.h"
#include "SkinningInfoVertexAttributeLayer.h"

// Mesh deformers
#include "MeshDeformer.h"
#include "MeshDeformerStack.h"
#include "SoftSkinDeformer.h"
#include "SoftSkinManager.h"
#include "SmartMeshMorphDeformer.h"

// Keyframing
#include "KeyFrame.h"
#include "KeyTrack.h"
#include "KeyFrameFinder.h"

// Keyframe generators
#include "KeyFrameGenerator.h"
#include "EyeBlinkKeyFrameGenerator.h"

// Interpolators
#include "LinearInterpolators.h"
#include "BezierInterpolators.h"
#include "HermiteInterpolators.h"
#include "TCBSplineInterpolators.h"

// Motion system
#include "Motion.h"
#include "MotionSystem.h"
#include "MotionLayerSystem.h"
#include "MotionInstance.h"
#include "MotionLayer.h"
#include "MotionLink.h"
#include "MotionQueue.h"
#include "MotionPart.h"
#include "PlayBackInfo.h"
#include "LayerPass.h"
#include "FacialLayerPass.h"
#include "RepositioningLayerPass.h"

// Play modes
#include "PlayMode.h"
#include "PlayModeForward.h"
#include "PlayModeBackward.h"
#include "PlayModePingPong.h"

// Skeletal motion
#include "SkeletalMotion.h"

// Facial motion
#include "FacialMotion.h"
#include "FacialMotionPart.h"
#include "ExpressionPartController.h"
#include "ExpressionPart.h"
#include "MeshExpressionPart.h"
#include "FacialSetup.h"

// Collision detection systems
#include "NodeCollisionSystem.h"
#include "ActorCollisionSystem.h"
#include "SimpleNodeCollisionSystem.h"
#include "SimpleActorCollisionSystem.h"

// Controllers
#include "Controller.h"
#include "PhysicsController.h"

// Constraints
#include "Constraint.h"
#include "LookAtConstraint.h"
#include "PositionConstraint.h"
#include "RotationConstraint.h"
#include "ScaleConstraint.h"
#include "TransformConstraint.h"

// Normal map support
#include "NormalMap.h"
#include "ImageResChecker.h"
#include "NormalMapGenerator.h"
#include "BasicNormalMapGenerator.h"

// Importer
#include "Importer.h"
#include "ChunkProcessors.h"
#include "FileFormatStructs.inl"

// Inverse kinematics
#include "TwoLinkIKSolver.h"
#include "CCDIKSolver.h"

// Jacobian pseudo inverse kinematics
#include "JacobianIKConstraint.h"
#include "JacobianIKActorDOF.h"
#include "JacobianIKData.h"
#include "JacobianIKSolver.h"
#include "JacobianIKController.h"
#include "JacobianIKFixedPoint.h"
#include "JacobianIKRelativePoint.h"

// Character factory
#include "CharacterFactory.h"