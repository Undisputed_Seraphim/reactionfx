#pragma once

// Configuration
#include "Config.h"

// Template patterns
// TODO: Deprecate all template patterns in favor of STL.
#include "Singleton.h"

// Math
#include "Color.h"
#include "Frustum.h"
#include "Matrix4.h"
#include "NMatrix.h"
#include "PlaneEq.h"
#include "Quaternion.h"
#include "Ray.h"
#include "Vector.h"
#include "OBB.h"
#include "AABB.h"
#include "BoundingSphere.h"
#include "Ray.h"
#include "CompressedFloat.h"
#include "CompressedVector.h"
#include "CompressedQuaternion.h"
#include "RootSolver.h"
#include "SwingAndTwist.h"

// Base
// TODO: Deprecate all classes in favor of STL.
#include "Algorithms.h"

// Logging system.
// TODO: Consider moving to glog?
#include "LogFile.h"
#include "LogManager.h"